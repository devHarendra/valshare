//
//  VSCustomAddReportView.h
//  ValShare
//
//  Created by Vivek Kumar on 5/6/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "ImgFullScrUIview.h"

@protocol TicketUpdateDelegate <NSObject>

-(void)ticketUpdateOnserverWithTitle:(NSString *)title withDescription:(NSString *)des infoModel:(NSMutableDictionary*)infoModel;
@end

@interface VSCustomAddReportView : UIViewController < UIPickerViewDelegate, UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    NSMutableDictionary *TKTdetails;
    ImgFullScrUIview *imgFullScr;

}
@property (nonatomic, strong) NSMutableArray *reportTypeArray;
@property (nonatomic, strong) UIPickerView *reportTypePicker;
@property (nonatomic, strong) UIView *customView;
@property (nonatomic, strong) NSString *reportTypeId;
@property (nonatomic, strong) NSString *reportTypeIdForwarded;

@property (nonatomic, strong) VSDirectionModal *DirModel;

@property (nonatomic, strong) NSString *latitudeForAddReport;
@property (nonatomic, strong) NSString *longitudeForAddReport;
@property (nonatomic,assign) id<TicketUpdateDelegate> delegate;


@end
