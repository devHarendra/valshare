//
//  VSCustomAddTicketView.m
//  ValShare
//
//  Created by Vivek Kumar on 5/4/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCustomAddTicketView.h"
#import "VSReportVideo_ImageList.h"

@interface VSCustomAddTicketView () {
    
    int pickerRemove;
    UIView *viewPicker;
    NSInteger  currentbuttonTag;;
}

@end

@implementation VSCustomAddTicketView
@synthesize customView;
@synthesize reportTypeArray = _reportTypeArray;
@synthesize labelTextArray = _labelTextArray;
@synthesize latitude;
@synthesize longitude;
@synthesize reportTypeId;
@synthesize reportTypePicker;
@synthesize image_videoDict = _image_videoDict;
@synthesize localImage_VideoDict = _localImage_VideoDict;
@synthesize image_videoArray = _image_videoArray;
@synthesize report = _report;
@synthesize imageName;

-(NSMutableArray *)reportTypeArray {
    
    if (!_reportTypeArray) {
        _reportTypeArray = [[NSMutableArray alloc] init];
    }
    return _reportTypeArray;
}

-(NSMutableArray *)labelTextArray {
    
    if (!_labelTextArray) {
        _labelTextArray = [[NSMutableArray alloc] init];
    }
    return _labelTextArray;
}
-(NSMutableArray *)image_videoArray {
    
    if (!_image_videoArray) {
        _image_videoArray = [[NSMutableArray alloc] init];
    }
    return _image_videoArray;
}
-(NSMutableDictionary *)image_videoDict {
    
    if (!_image_videoDict) {
        _image_videoDict = [[NSMutableDictionary alloc] init];
    }
    return _image_videoDict;
}

-(NSMutableDictionary *)localImage_VideoDict {
    
    if (!_localImage_VideoDict) {
        _localImage_VideoDict = [[NSMutableDictionary alloc] init];
    }
    return _localImage_VideoDict;
}
-(void)dealloc {
    
    self.customView = nil;
    
}
-(void)setDelegate:(id<TicketAddDelegate>)delegate{
    
    _delegate = delegate;
}
-(void)postAddTicketDetailOnServer:(NSString *)description ReportId:(NSString *)reportId Price:(NSString *)price Latitud:(NSString *)ticketlatitude Longitude:(NSString *)ticketlongitude ReportStatusId:(NSString *)reportstatusId ReportTypeId:(NSString *)reporttypeId RadiusOfImpact:(NSString *)rediusofimpact IsHost:(NSString *)ishost HostId:(NSString *)hostId ThumbsDown:(NSString *)thumbsdown ThumbsUp:(NSString *)thumbsup UserId:(NSString *)userid AddedBy:(NSString *)addedby ReportVideoModel:(NSMutableArray*) reportvideomodel {
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc]init];
    
    [dictData setValue:description forKey:@"description"];
    [dictData setValue:[NSString stringWithFormat:@"%f",[price floatValue]] forKey:@"price"];
    [dictData setValue:[NSString stringWithFormat:@"%f",[ticketlatitude floatValue]]  forKey:@"latitude"];
    [dictData setValue:[NSString stringWithFormat:@"%f",[ticketlongitude floatValue]]  forKey:@"longitude"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[reportstatusId intValue]]  forKey:@"reportStatusId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[reporttypeId intValue]]  forKey:@"reportTypeId"];
    [dictData setValue:[NSString stringWithFormat:@"%f",[rediusofimpact floatValue]]  forKey:@"radiusofImpact"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[thumbsdown intValue]] forKey:@"thumbsDown"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[thumbsup intValue]] forKey:@"thumbsUp"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[hostId intValue]] forKey:@"hostId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[userid intValue]] forKey:@"userId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[ishost intValue]] forKey:@"isHost"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[addedby intValue]] forKey:@"addedBy"];
    [dictData setValue:reportvideomodel forKey:@"reportVideoModelList"];
    
    [[VSParserClass sharedParser]VSAddTicket:dictData WithCompletionBlock:^(BOOL result, id resultObject,NSError *error) {
        NSLog(@"resultObjectresultObject :%@",resultObject);
        
        if (result) {
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                
               
                if (_delegate && [_delegate respondsToSelector:@selector(ticketOnserverWithTitle:withDescription:infoModel:)]) {
                    if (self.report.length == 0) {
                        
                        VSGetAddTicketListModel *listModel = [self.reportTypeArray objectAtIndex:0];
                        self.report = listModel.report;
                    }
                    
                    [dictData setValue:[resultObject objectForKey:@"Data"] forKey:@"reportId"];// for call back function ,here "Data" is report id of tkt.

                    [_delegate ticketOnserverWithTitle:self.report withDescription:description infoModel:dictData];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
            }else {
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            }
            [SVProgressHUD dismiss];
            
        }else {
            
            [SVProgressHUD dismiss];
            
        }
        
        UITextField *price = (UITextField *)[self.view viewWithTag:5556];
        UITextField *radiesOfImpact = (UITextField *)[self.view viewWithTag:5555];
        UITextField *description = (UITextField *)[self.view viewWithTag:6555];
        
        price.text = nil;
        radiesOfImpact.text = nil;
        description.text = @"Enter Description";
    }];
}

-(void)getAddTicketList:(NSString *)userId {
    
    [[VSParserClass sharedParser] VSGetAddTicketList:userId WithCompletionBlock:^(BOOL result, id resultObject,NSError *error) {
        
        if (result) {
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                
                if ([[dictdata valueForKey:@"reportTypeModelList"]isKindOfClass:[NSArray class]]) {
                    
                    NSLog(@"NSArrayNSArray:-%@",[dictdata valueForKey:@"reportTypeModelList"]);
                    NSMutableArray *localArray = (NSMutableArray *)[dictdata valueForKey:@"reportTypeModelList"];
                    
                    [localArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *localDict = (NSMutableDictionary *)[localArray objectAtIndex:idx];
                        VSGetAddTicketListModel *listModel = [[VSGetAddTicketListModel alloc]init];
                        [listModel setIsActive:[localDict valueForKey:@"isActive"]];
                        [listModel setLastModifiedTime:[localDict valueForKey:@"lastModifiedTime"]];
                        [listModel setReport:[localDict valueForKey:@"report"]];
                        [listModel setReportStatusTypeId:[localDict valueForKey:@"reportStatusTypeId"]];
                        [listModel setReportTypeId:[localDict valueForKey:@"reportTypeId"]];
                        [listModel setReportStatusType:[localDict valueForKey:@"reportStatusType"]];
                        [self.reportTypeArray addObject:listModel];
                    }];
                    
                    
                }else if ([[dictdata valueForKey:@"reportTypeModelList"]isKindOfClass:[NSDictionary class]]) {
                    
                    NSLog(@"NSDictionaryNSDictionary:-%@",[dictdata valueForKey:@"reportTypeModelList"]);
                }
            }else {
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            }
            
            [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];
            
        }
        
        
        if (self.reportTypeArray.count >0)
        {
            [self addcustomViewOnView];
        }
        else
        {
              [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Network Error"delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        }
        [viewPicker setHidden:YES];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor lightTextColor]];
    pickerRemove = 0;
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getAddTicketList:[VSSingletonClass sharedMySingleton].loginUserId];
        
    }else {
        
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertview show];
    }
    UITapGestureRecognizer *removeOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Dismisskeys)];
    removeOverlay.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:removeOverlay];
    
}
-(void)removeOverlayFromView {
    
    [self dismissViewControllerAnimated:YES completion:nil];
   // [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftRight];
}

-(void)Dismisskeys

{
    [self.view endEditing:YES];
}


-(void)addcustomViewOnView {
    
    self.customView = [[UIView alloc] init];
    self.customView.frame = (IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 45, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height - 110):(IS_IPHONE_4S) ?CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 45, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height - 110):CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 80, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height - 190);
    self.customView.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0];
    self.customView.userInteractionEnabled = YES;
    [self.view addSubview:self.customView];
    
    
    [self CloseViewController:CGRectMake(customView.frame.size.width-15, customView.frame.origin.y-10, 40, 40)];
    
    NSArray *labelArray = @[@"Report Type",@"Impact Zone",@"Price",@"Description"];
    NSArray *textArray = @[@"Value In meter",@"Price"];
    NSArray *buttonText = @[@"Add Image",@"Add Video"];
    VSGetAddTicketListModel *listModel = [self.reportTypeArray objectAtIndex:0];
    
    [self addHeadingLabelOnCustomView:(IS_IPHONE_5) ? CGRectMake(75 , 5, 250.0f, 35):CGRectMake(85 , 5, 250.0f, 35)  Title:@"Enter Report Detail"];
    
    for (int i = 0; i <4; i ++) {
        [self addLabelOnCustomView:CGRectMake(10, 45.0 + (45 * i) , 100.0f, 25) Title:labelArray[i]];
    }
    
    [self addReportTypeButtonOnView:(IS_IPHONE_5) ? CGRectMake(110, 40.0,self.customView.frame.size.width/2 + 25, 35):(IS_IPHONE_4S) ? CGRectMake(110, 40.0,self.customView.frame.size.width/2 + 25, 35):CGRectMake(110, 40.0,self.customView.frame.size.width/2 + 45, 35) Title:listModel.report];
    
    for (int i = 0; i <2; i ++) {
        
        [self addTextFieldOnOnCustomview:(IS_IPHONE_5) ? CGRectMake(110, 85.0 + (45 * i),self.customView.frame.size.width/2 + 25, 35):(IS_IPHONE_4S) ? CGRectMake(110, 85.0 + (45 * i),self.customView.frame.size.width/2 + 25, 35): CGRectMake(110, 85.0 + (45 * i),self.customView.frame.size.width/2 + 45, 35) Title:textArray[i] Tag:5555 + i];
        
        [self addSubmitButtonOnView:(IS_IPHONE_5) ? CGRectMake(10 + (140 * i), 320, 130, 45):(IS_IPHONE_4S) ? CGRectMake(10 + (140 * i), 320, 130, 45):CGRectMake(10 + (165 * i), 320, 155, 45)   Title:buttonText[i] Tag:8899 + i BGImage:nil];
    }
    
    
    [self addTextFieldDescriptionOnCustomview:(IS_IPHONE_5) ? CGRectMake(110, 178.0 , self.customView.frame.size.width/2 + 25 , 80.0f):(IS_IPHONE_4S) ? CGRectMake(110, 178.0 , self.customView.frame.size.width/2 + 25 , 80.0f):CGRectMake(110, 178.0 , self.customView.frame.size.width/2 + 45 , 80.0f) Title:@"Enter Description" Tag:6555];
    
    [self addCheckBoxButtonOnView:CGRectMake(24 , [UIScreen mainScreen].bounds.size.height*0.74 , kCheckBlackBG1.size.width, kCheckBlackBG1.size.height) BGImage:kCheckBlackBG1 BGSelected:kCheckedBlackBG1 Tag:45654 ];
    
    [self addImgVideoTitleLbl:CGRectMake(10, 300, self.customView.frame.size.width-20, 18) Tag:55454];
    
    [self addLabelForCheckboxOnView:CGRectMake(17 + kCheckBlackBG1.size.width , 374 , 60, 25) Title:@"Is Host" Tag:4425 Font:[UIFont systemFontOfSize:14.0]];
    
    [self addReportAddButtonOnCustom:CGRectMake(25.0, [UIScreen mainScreen].bounds.size.height*0.80 , self.customView.frame.size.width - 20 , 38.0f) Title:@"Add" Tag:655543];
    
    [self addView:(IS_IPHONE6) ? CGRectMake(0,286,self.customView.frame.size.width, 44) : (IS_IPHONE_5) ? CGRectMake(self.customView.frame.origin.x, 418 ,self.customView.frame.size.width, 44) :  CGRectMake(0, 242 ,self.customView.frame.size.width, 44)];
}

-(void)addHeadingLabelOnCustomView:(CGRect)frame Title:(NSString *)text {
    
    UILabel *label = [[UILabel alloc]init];
    [label setText:text];
    [label setFrame:frame];
    [label setFont:[UIFont systemFontOfSize:20.0]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor colorWithRed:139.0f/255.0f green:13.0f/255.0f blue:13.0f/255.0f alpha:1.0]];
    [self.customView addSubview:label];
}

-(void)addLabelOnCustomView:(CGRect)frame Title:(NSString *)text {
    UILabel *label = [[UILabel alloc]init];
    [label setText:text];
    [label setFrame:frame];
    [label setFont:[UIFont systemFontOfSize:14.0]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor colorWithRed:139.0f/255.0f green:13.0f/255.0f blue:13.0f/255.0f alpha:1.0]];
    [self.customView addSubview:label];
}

-(void)addImgVideoTitleLbl:(CGRect)frame Tag:(int)tag
{
    UILabel *label = [[UILabel alloc]init];
    [label setTag:tag];
    [label setFrame:frame];
    [label setHidden:YES];
    [label setFont:[UIFont systemFontOfSize:12.0]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor colorWithRed:139.0f/255.0f green:13.0f/255.0f blue:13.0f/255.0f alpha:1.0]];
    [self.customView addSubview:label];
}

-(void)addReportTypeButtonOnView:(CGRect)frame Title:(NSString *)text {
    
    UIButton *button = [[UIButton alloc]init];
    [button setTag:65554];
    [button setFrame:frame];
    [button.layer setBorderWidth:0.5];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitle:text forState:UIControlStateNormal];
    [button.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(ReportTypeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.customView addSubview:button];
}

-(void)addTextFieldOnOnCustomview:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setTag:tag];
    [textField setFrame:frame];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:title];
    [textField.layer setBorderWidth:1.0];
    [textField setUserInteractionEnabled:YES];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setFont:[UIFont systemFontOfSize:15.0f]];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setKeyboardType:UIKeyboardTypeNumberPad];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.customView addSubview:textField];
}

-(void)addTextFieldDescriptionOnCustomview:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setTag:tag];
    [textField setFrame:frame];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:title];
    [textField.layer setBorderWidth:1.0];
    [textField setUserInteractionEnabled:YES];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setFont:[UIFont systemFontOfSize:15.0f]];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
    [self.customView addSubview:textField];
}


-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.layer setCornerRadius:5.0];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:39.0f/255.0f blue:29.0f/255.0f alpha:1.0]];
    [button addTarget:self action:@selector(uploadImage_videoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.customView addSubview:button];
}

-(void)addCheckBoxButtonOnView:(CGRect)frame BGImage:(UIImage *)bgImage BGSelected:(UIImage *)s_bgImage Tag:(int)tag {
    
    VSButton *button = [VSButton buttonWithType:UIButtonTypeCustom];
    [button setTag:tag];
    [button setFrame:frame];
    [button setBackgroundColor:[UIColor clearColor]];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button setBackgroundImage:s_bgImage forState:UIControlStateSelected];
    [button addTarget:self action:@selector(selectCheckBoxButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}
-(void)addReportAddButtonOnCustom:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.layer setCornerRadius:5.0];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:39.0f/255.0f blue:29.0f/255.0f alpha:1.0]];
    [button addTarget:self action:@selector(addButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(void)CloseViewController:(CGRect)frame
{
    VSButton *button = [[VSButton alloc] init];
    [button setFrame:frame];
    [button setBackgroundColor:[UIColor clearColor]];
    [button setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(removeOverlayFromView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}



-(void)addLabelForCheckboxOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag Font:(UIFont *)font {
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:frame];
    titleLabel.tag = tag;
    titleLabel.font = font;
    titleLabel.text = title;
    [titleLabel setTextColor:[UIColor blackColor]];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.customView addSubview:titleLabel];
}


-(void)addView:(CGRect)frame {
    
    viewPicker = [[UIView alloc]init];
    [viewPicker setFrame:frame];
    [viewPicker setTag:9999];
    [viewPicker setBackgroundColor:[UIColor colorWithRed:183.0f/255.0f green:40.0f/255.0f blue:35.0f/255.0f alpha:1.0f]];
//    [self.view addSubview:viewPicker]; // harry , added after uipicker on self.view
    
    UIButton *donebutton = [[UIButton alloc]init];
    [donebutton setTitle:@"Done" forState:UIControlStateNormal];
    [donebutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [donebutton setFrame:CGRectMake(viewPicker.frame.origin.x, 0.0, 80, 44)];
    [donebutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [donebutton addTarget:self action:@selector(reportDoneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [viewPicker addSubview:donebutton];
    
    UIButton *cancelbutton = [[UIButton alloc]init];
    [cancelbutton setFrame:CGRectMake(viewPicker.frame.size.width - 80, 0.0, 80, 44)];
    [cancelbutton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelbutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [cancelbutton addTarget:self action:@selector(reportCancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [viewPicker addSubview:cancelbutton];
}


//***************Use of textfield delegate*************************//

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

//***************Use of textView delegate*************************//

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@"Enter Description"]) {
        textView.text = @"";
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Enter Description";
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(IBAction)ReportTypeButtonAction:(id)sender {
    
    [viewPicker setHidden:NO];
    [self Dismisskeys];
    if (pickerRemove > 0) {
        [self.reportTypePicker removeFromSuperview];
    }
    self.reportTypePicker = [[UIPickerView alloc]init];
    [self.reportTypePicker setFrame:CGRectMake(self.customView.frame.origin.x, [UIScreen mainScreen].bounds.size.height-150 , self.customView.frame.size.width , 150)];
    [self.reportTypePicker setBackgroundColor:[UIColor grayColor]];
    [self.reportTypePicker setUserInteractionEnabled:YES];
    [self.reportTypePicker setDataSource:(id)self];
    [self.reportTypePicker setDelegate:(id)self];
    [self.view addSubview:self.reportTypePicker];
    [self.view addSubview:viewPicker];
    pickerRemove++;
    
}

/*
 *******************************************************************
 *  UIPickerView delegate and data source methods..
 *******************************************************************
 */

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.reportTypeArray.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    VSGetAddTicketListModel *listModel;
    
    listModel = [self.reportTypeArray objectAtIndex:row];
    return listModel.report;
    
}

-(IBAction)reportDoneButtonClicked:(id)sender {
    
    UIButton *reportTypeButton = (UIButton *)[self.view viewWithTag:65554];
    NSInteger row;
    VSGetAddTicketListModel *listModel;
    
    row = [self.reportTypePicker selectedRowInComponent:0];
    listModel = [self.reportTypeArray objectAtIndex:row];
    [reportTypeButton setTitle:listModel.report forState:UIControlStateNormal];
    self.reportTypeId = [NSString stringWithFormat:@"%@",listModel.reportTypeId];
    self.report = listModel.report;
    [viewPicker setHidden:YES];
    [self.reportTypePicker setHidden:YES];
}

-(IBAction)reportCancelButtonClick:(id)sender {
    [self.reportTypePicker setHidden:YES];
    [viewPicker setHidden:YES];
}
-(IBAction)uploadImage_videoAction:(UIButton *)sender {
    
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc]init];
    imgPicker.delegate=(id)self;
    imgPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    imgPicker.allowsEditing = NO;
    imgPicker.videoQuality = UIImagePickerControllerQualityTypeLow;
    imgPicker.extendedLayoutIncludesOpaqueBars = YES;
    
    if (sender.tag == 8899) {
        currentbuttonTag = sender.tag;
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imgPicker animated:YES completion:NULL];
        
        
        
    }else if (sender.tag == 8900) {
        currentbuttonTag = sender.tag;
        imgPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie,nil];
        
        [self presentViewController:imgPicker animated:YES completion:NULL];
        
    }
}

/*
 *************************************************************
*  Image comprassion --- Harry
 *************************************************************

 */
- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
     UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
      return newImage;
}

-(NSMutableString*)ConvertIntoBytes:(NSData*)ImgData
{
    NSUInteger len = ImgData.length;
    uint8_t *bytes = (uint8_t *)[ImgData bytes];
    NSMutableString *result = [NSMutableString stringWithCapacity:len * 3];
    [result appendString:@"["];
    for (NSUInteger i = 0; i < len; i++) {
        if (i) {
            [result appendString:@","];
        }
        [result appendFormat:@"%d", bytes[i]];
    }
    [result appendString:@"]"];
    
    NSLog(@"SIZE OF  IMAGE: %.2f Mb", (float)ImgData.length/1024);

    return result;
}

-(NSMutableArray*)ConvertIntoArrBytes:(NSData*)ImgData
{
    NSUInteger len = ImgData.length;
    uint8_t *bytes = (uint8_t *)[ImgData bytes];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSLog(@"SIZE OF  IMAGE: %.2f Mb", (float)ImgData.length/1024);

 
    for (NSUInteger i = 0; i < len; i++)
    {
        [arr addObject:[NSNumber numberWithUnsignedChar:bytes[i]]];
    }
    return arr;
}

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                     handler:(void (^)(AVAssetExportSession*))handler
{
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetLowQuality];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         handler(exportSession);
     }];
}




/*
 *******************************************************************
 *  Get image title and video url from liberary..
 *******************************************************************
 */


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
     UILabel *label = (UILabel *)[self.view viewWithTag:55454];
    [label setHidden:false];

       if (currentbuttonTag == 8899) {
           [label setText:@"Please wait, image is loading..."];
           dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
               [self generateRandomString:2];
               NSLog(@"%@",[self generateRandomString:2]);
               UIImage *imageorig = [info valueForKey:UIImagePickerControllerOriginalImage];
               NSData *tempData = UIImageJPEGRepresentation([self imageWithImage:imageorig scaledToSize:CGSizeMake(320, 640)], 0.1);
               
               NSMutableArray *imgBytes = [[NSMutableArray alloc]init];
               imgBytes = [self ConvertIntoArrBytes:tempData];
               
               [self.localImage_VideoDict setValue:[NSString stringWithFormat:@"Val_%@.jpg",[self generateRandomString:2]] forKey:@"fileName"];
               [self.localImage_VideoDict setValue:@"image" forKey:@"documentType"];
               [self.localImage_VideoDict setValue:imgBytes forKey:@"fileDataByteArray"];
//               [self.labelTextArray addObject:[NSString stringWithFormat:@"Val_%@%@",[self generateRandomString:2],@".png"]];
               NSString * result = [self.labelTextArray componentsJoinedByString:@","];
               [label setText:[NSString stringWithFormat:@" %@",result]];
               [self.image_videoArray addObject:self.localImage_VideoDict];
               
               NSLog(@"imageDataimageData:-%@",imgBytes);
           });
           
           
           [picker dismissViewControllerAnimated:YES completion:NULL];

      


//        ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
//        {
//            ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
//            NSLog(@"[imageRep filename] : %@--- %@---", [imageRep filename],imageRep.url);
//            [self.labelTextArray addObject:[imageRep filename]];
//            
//            NSString * result = [self.labelTextArray componentsJoinedByString:@","];
//            NSArray *listaArray = [[imageRep filename] componentsSeparatedByString:@"."];
//            
//            //NSData *data = [NSData dataWithContentsOfURL:imageRep.url];
//             //UIImage *img = [[UIImage alloc] initWithData:data ];
//            UIImage *img = [UIImage imageNamed:@"checkbox"];
//             NSData *imageData = UIImagePNGRepresentation(img);
//            [self.localImage_VideoDict setValue:[listaArray firstObject] forKey:@"fileName"];
//            [self.localImage_VideoDict setValue:@".png" forKey:@"documentType"];
//            [self.localImage_VideoDict setValue:[NSString stringWithFormat:@"%@",imageData] forKey:@"fileDataByteArray"];
//            
////           NSString *myString = [imageRep.url absoluteString];
////            CGImageRef imageRef = [[[UIImage alloc]initWithContentsOfFile:myString]CGImage];
////            UIImage* uiImage = [[UIImage alloc] initWithCGImage:imageRef];
////        NSData *imageData1 = UIImagePNGRepresentation(uiImage);
//            [label setText:[NSString stringWithFormat:@" %@",result]];
//            NSLog(@"==============%@---%@",result,imageData);
//            
//        };
//      
//        ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
//        //[assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
//        [picker dismissViewControllerAnimated:YES completion:NULL];
//        //NSLog(@"==============%@",refURL);

        
    }
       else
       {
           
            NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            NSString *moviePath = [videoUrl path];
            
            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
                NSArray *listItems = [moviePath componentsSeparatedByString:@"/"];
//                NSString *newString = [listItems lastObject];
                
                NSData *data = [NSData dataWithContentsOfFile:moviePath];
                NSMutableArray *imgBytes = [[NSMutableArray alloc]init];
                imgBytes = [self ConvertIntoArrBytes:data];

                
                
//                NSString * a = [newString stringByReplacingOccurrencesOfString:@"trim." withString:@""];
//                NSArray *listArray = [a componentsSeparatedByString:@"."];
                
                [self.localImage_VideoDict setValue:[NSString stringWithFormat:@"Val_%@.MP4",[self generateRandomString:2]] forKey:@"fileName"];
//                [self.localImage_VideoDict setValue:[listArray firstObject] forKey:@"fileName"];
                [self.localImage_VideoDict setValue:@"video" forKey:@"documentType"];
                [self.localImage_VideoDict setValue:imgBytes forKey:@"fileDataByteArray"];                
                [label setText:[NSString stringWithFormat:@" %@",[self.localImage_VideoDict objectForKey:@"fileName"]]];
            }
         
        
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    [self.localImage_VideoDict setValue:[NSString stringWithFormat:@"%d",0] forKey:@"reportVideoImageId"];
//    [self.localImage_VideoDict setValue:@"" forKey:@"oldFileName"]; // by harry
//    [self.localImage_VideoDict setValue:@"" forKey:@"fileURL"]; // by harry
    [self.image_videoArray addObject:self.localImage_VideoDict];
    
    
}

-(NSString*)generateRandomString:(int)num {
    NSMutableString* string = [NSMutableString stringWithCapacity:num];
    for (int i = 0; i < num; i++) {
        [string appendFormat:@"%d", (unichar)('a' + arc4random_uniform(25))];
    }
    NSLog(@"============:-Valshare_%@",string);
    return string;
}

-(IBAction)selectCheckBoxButtonAction:(UIButton *)sender {
    
    currentbuttonTag = sender.tag;
    sender.selected = !sender.selected;
    NSLog(@"self.image_videoDictself.image_videoDict:-%@",self.image_videoDict);
}

-(IBAction)addButtonAction:(id)sender {
    NSLog(@"==================%@",self.image_videoArray);
    UITextField *price = (UITextField *)[self.view viewWithTag:5556];
    UITextField *radiesOfImpact = (UITextField *)[self.view viewWithTag:5555];
    UITextField *description = (UITextField *)[self.view viewWithTag:6555];
    VSGetAddTicketListModel *listModel = [self.reportTypeArray objectAtIndex:0];
    
    if (price.text.length == 0) {
        
        [[[UIAlertView alloc]initWithTitle:nil message:@"Please fill the price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        
    }else if (radiesOfImpact.text.length == 0) {
        
        [[[UIAlertView alloc]initWithTitle:nil message:@"Please fill impact zone" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
    }
    else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            
            [self postAddTicketDetailOnServer:description.text ReportId:@"0" Price:price.text Latitud:self.latitude Longitude:self.longitude ReportStatusId:[NSString stringWithFormat:@"%@",listModel.reportStatusTypeId] ReportTypeId:(self.reportTypeId.length == 0) ? listModel.reportTypeId :self.reportTypeId RadiusOfImpact:[NSString stringWithFormat:@"%@",radiesOfImpact.text] IsHost:(currentbuttonTag == 45654) ? @"1":@"0" HostId:[VSSingletonClass sharedMySingleton].loginUserId ThumbsDown:@"0" ThumbsUp:@"0" UserId:[VSSingletonClass sharedMySingleton].loginUserId AddedBy:[VSSingletonClass sharedMySingleton].loginUserId ReportVideoModel:self.image_videoArray];
        }else {
            
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alertview show];
        }
    }
    
}
@end
