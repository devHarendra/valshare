//
//  VSCustomAddReportView.m
//  ValShare
//
//  Created by Vivek Kumar on 5/6/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCustomAddReportView.h"

@interface VSCustomAddReportView (){
    
    int pickerRemove;
    UIView *viewPicker;
    NSInteger  currentbuttonTag;
}

@end

@implementation VSCustomAddReportView
@synthesize reportTypeArray = _reportTypeArray;
@synthesize reportTypePicker;
@synthesize customView;
@synthesize reportTypeId;
@synthesize latitudeForAddReport;
@synthesize longitudeForAddReport;

-(NSMutableArray *)reportTypeArray {
    
    if (!_reportTypeArray) {
        _reportTypeArray = [[NSMutableArray alloc] init];
    }
    return _reportTypeArray;
}

-(void)setDelegate:(id<TicketUpdateDelegate>)delegate{
    
    _delegate = delegate;
}


-(void)GetTicketDetails:(NSString *)userId
{
    
    [[VSParserClass sharedParser] GetTicketReport:userId WithCompletionBlock:^(BOOL result, id resultObject, NSError *error)
    {
                                                                               
        if (result) {
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"])
            {
                NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                
                NSLog(@"Ticket Data - %@",dictdata);
                TKTdetails = [[NSMutableDictionary alloc]init];
                [TKTdetails setObject:[dictdata objectForKey:@"description"] forKey:@"description"];
                [TKTdetails setObject:[dictdata objectForKey:@"addedBy"] forKey:@"addedBy"];
                [TKTdetails setObject:[dictdata objectForKey:@"radiusofImpact"] forKey:@"radiusofImpact"];
                [TKTdetails setObject:[dictdata objectForKey:@"price"] forKey:@"price"];
                [TKTdetails setObject:[dictdata objectForKey:@"latitude"] forKey:@"latitude"];
                [TKTdetails setObject:[dictdata objectForKey:@"longitude"] forKey:@"longitude"];
                [TKTdetails setObject:[dictdata objectForKey:@"isHost"] forKey:@"isHost"];
                [TKTdetails setObject:[dictdata objectForKey:@"hostId"] forKey:@"hostId"];
                [TKTdetails setObject:[dictdata objectForKey:@"reportId"] forKey:@"reportId"];
                [TKTdetails setObject:[dictdata objectForKey:@"reportStatusId"] forKey:@"reportStatusId"];
                [TKTdetails setObject:[dictdata objectForKey:@"reportTypeId"] forKey:@"reportTypeId"];
                [TKTdetails setObject:[dictdata objectForKey:@"thumbsDown"] forKey:@"thumbsDown"];
                [TKTdetails setObject:[dictdata objectForKey:@"thumbsUp"] forKey:@"thumbsUp"];
                [TKTdetails setObject:[dictdata objectForKey:@"userId"] forKey:@"userId"];
                
                NSMutableArray *imgVidArr = [dictdata objectForKey:@"reportVideoModelList"];
                [self AddScrollViewImgVid:CGRectMake(10, 258, self.customView.frame.size.width-20, 80) Tag:112211 List:imgVidArr];
                
                UIButton *reportTypeButton = (UIButton *)[self.view viewWithTag:65554];
                int num = [[dictdata objectForKey:@"reportTypeId"]integerValue];
                if (num ==1)
                {
                    [reportTypeButton setTitle:@"Parking" forState:UIControlStateNormal];
                    self.reportTypeId = @"1";
                }
                else if (num ==2)
                {
                    [reportTypeButton setTitle:@"Accdent" forState:UIControlStateNormal];
                    self.reportTypeId = @"2";
                }
                
                else if (num ==3)
                {
                    [reportTypeButton setTitle:@"Under Contruction" forState:UIControlStateNormal];
                    self.reportTypeId = @"3";
                }
                
                
                
                UITextField * txt;
                txt = (UITextField*)[self.view viewWithTag:55551];
                [txt setText:[NSString stringWithFormat:@"%@",[dictdata objectForKey:@"radiusofImpact"]]];
                
                txt = (UITextField*)[self.view viewWithTag:5555];
                [txt setText:[NSString stringWithFormat:@"%@",[dictdata objectForKey:@"price"]]];
                
                txt = (UITextField*)[self.view viewWithTag:6555];
                [txt setText:[NSString stringWithFormat:@"%@",[dictdata objectForKey:@"description"]]];
                
                NSLog(@"Ticket Data dictionary - %@",TKTdetails);
                
            }
            else
            {
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            }
            
            [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];
            
        }
}];
}



-(void)getAddTicketList:(NSString *)userId {
    
    [[VSParserClass sharedParser] VSGetAddTicketList:userId WithCompletionBlock:^(BOOL result, id resultObject,NSError *error) {
        
        if (result) {
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                
                if ([[dictdata valueForKey:@"reportTypeModelList"]isKindOfClass:[NSArray class]]) {
                    
                    NSLog(@"NSArrayNSArray:-%@",[dictdata valueForKey:@"reportTypeModelList"]);
                    NSMutableArray *localArray = (NSMutableArray *)[dictdata valueForKey:@"reportTypeModelList"];
                    
                    [localArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *localDict = (NSMutableDictionary *)[localArray objectAtIndex:idx];
                        VSGetAddTicketListModel *listModel = [[VSGetAddTicketListModel alloc]init];
                        [listModel setIsActive:[localDict valueForKey:@"isActive"]];
                        [listModel setLastModifiedTime:[localDict valueForKey:@"lastModifiedTime"]];
                        [listModel setReport:[localDict valueForKey:@"report"]];
                        [listModel setReportStatusTypeId:[localDict valueForKey:@"reportStatusTypeId"]];
                        [listModel setReportTypeId:[localDict valueForKey:@"reportTypeId"]];
                        [listModel setReportStatusType:[localDict valueForKey:@"reportStatusType"]];
                        [self.reportTypeArray addObject:listModel];
                    }];
                    
                    
                }else if ([[dictdata valueForKey:@"reportTypeModelList"]isKindOfClass:[NSDictionary class]]) {
                    
                    NSLog(@"NSDictionaryNSDictionary:-%@",[dictdata valueForKey:@"reportTypeModelList"]);
                }
            }else {
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            }
            
            [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];
            
        }
        
        [self addcustomViewOnView];
        [viewPicker setHidden:YES];
    }];
}

-(void)updateReport:(NSString *)description AddedBy:(NSString *)addedby RadiusOfimpact:(NSString *)radiusofimpact Latitude:(NSString *)latitude Longitude:(NSString *)longitude IsHost:(NSString *)ishost HostId:(NSString *)hostid ReportId:(NSString *)reportid ReportStatusId:(NSString *)reportstatusid ReportTypeId:(NSString *)reporttypeid ThumbsDown:(NSString *)thumbsdown ThumbsUp:(NSString *)thumbsup UserId:(NSString *)userid Price:(NSString *)price{
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc]init];
    
    dictData = TKTdetails;
    
    
    
    
    [dictData setValue:reportTypeId forKey:@"reportTypeId"];
    [dictData setValue:description forKey:@"description"];

    [dictData setValue:[NSString stringWithFormat:@"%d",[radiusofimpact intValue]] forKey:@"radiusofImpact"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[price intValue]] forKey:@"price"];
    
    [[VSParserClass sharedParser]VSUpadetReport:dictData WithCompletionBlock:^(BOOL result, id resultObject,NSError *error) {
        
        NSLog(@"resultObjectresultObject:-%@",resultObject);
        
        if (result) {
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                
                UIButton *reportTypeButton = (UIButton *)[self.view viewWithTag:65554];
                [_delegate ticketUpdateOnserverWithTitle:reportTypeButton.titleLabel.text withDescription:description infoModel:dictData];
                [self dismissViewControllerAnimated:NO completion:nil];

            }else {
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            }
            [SVProgressHUD dismiss];
            
        }else {
            
            [SVProgressHUD dismiss];
            
        }
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.reportTypeId = @"1";
    UIButton *reportTypeButton = (UIButton *)[self.view viewWithTag:65554];
    [reportTypeButton setTitle:@"" forState:UIControlStateNormal];



    [self.view setBackgroundColor:[UIColor lightTextColor]];
    
    pickerRemove = 0;
  
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getAddTicketList:[VSSingletonClass sharedMySingleton].loginUserId];
        
    }else {
        
      [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
   
    
    
    UITapGestureRecognizer *removeOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(HandleTap:)];
    removeOverlay.numberOfTapsRequired = 1;
    [removeOverlay setDelegate:self];
    [self.view addGestureRecognizer:removeOverlay];
    

    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self GetTicketDetails:_reportTypeIdForwarded];
        
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }

    
    
    

}
-(void)removeOverlayFromView:(UITapGestureRecognizer *)tap {
    
    
//    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftRight];
}
-(void)addcustomViewOnView {
    
    self.customView = [[UIView alloc] init];
    self.customView.frame = (IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 80, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height/2 + 130):CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 150, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height /2 - 20);
    self.customView.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0];
    self.customView.userInteractionEnabled = YES;
    [self.view addSubview:self.customView];
    
    NSArray *labelArray = @[@"Report Type",@"Impact Zone", @"Price",@"Description"];
    
    [self addHeadingLabelOnCustomView:(IS_IPHONE_5) ? CGRectMake(75 , 5, 250.0f, 35):CGRectMake(85 ,5, 250.0f, 35)  Title:@"Update Report Detail"];
    
    for (int i = 0; i <4; i ++) {
        [self addLabelOnCustomView:CGRectMake(10, 55.0 + (45 * i) , 100.0f, 25) Title:labelArray[i]];
    }
    [self addReportTypeButtonOnView:(IS_IPHONE_5) ? CGRectMake(110, 50.0,self.customView.frame.size.width/2 + 25, 35):CGRectMake(110, 50.0,self.customView.frame.size.width/2 + 45, 35) Title:[NSString stringWithFormat:@"%@",_DirModel.reportModalReportType]];
    
    
    [self addTextFieldOnOnCustomview:(IS_IPHONE_5) ? CGRectMake(110, 95.0 ,self.customView.frame.size.width/2 + 25, 35): CGRectMake(110, 95.0,self.customView.frame.size.width/2 + 45, 35) Title:@"Enter Impact Zone" Tag:55551]; // Added by harry
    
    
    [self addTextFieldOnOnCustomview:(IS_IPHONE_5) ? CGRectMake(110, 140.0 ,self.customView.frame.size.width/2 + 25, 35): CGRectMake(110, 140.0,self.customView.frame.size.width/2 + 45, 35) Title:@"Price" Tag:5555];
    
    [self addTextViewOnOnCustomview:(IS_IPHONE_5) ? CGRectMake(110, 193.0 , self.customView.frame.size.width/2 + 25 , 60.0f):CGRectMake(110, 231.0 , self.customView.frame.size.width/2 + 45 , 60.0f) Title:@"Enter Description" Tag:6555];
   
    
    [self addReportAddButtonOnCustom:CGRectMake(10.0, 350.0f , self.customView.frame.size.width - 20 , 45.0f) Title:@"Update" Tag:655543];
    
    [self addView:(IS_IPHONE6) ? CGRectMake(0 ,187 ,self.customView.frame.size.width, 44) : (IS_IPHONE_5) ? CGRectMake(0, 187 ,self.customView.frame.size.width, 44) : CGRectMake(0, 200 ,self.customView.frame.size.width, 44)];
    
    
    }

-(void)addHeadingLabelOnCustomView:(CGRect)frame Title:(NSString *)text {
    
    UILabel *label = [[UILabel alloc]init];
    [label setText:text];
    [label setFrame:frame];
    [label setFont:[UIFont systemFontOfSize:20.0]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor colorWithRed:139.0f/255.0f green:13.0f/255.0f blue:13.0f/255.0f alpha:1.0]];
    [self.customView addSubview:label];
}

-(void)addLabelOnCustomView:(CGRect)frame Title:(NSString *)text {
    UILabel *label = [[UILabel alloc]init];
    [label setText:text];
    [label setFrame:frame];
    [label setFont:[UIFont systemFontOfSize:14.0]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor colorWithRed:139.0f/255.0f green:13.0f/255.0f blue:13.0f/255.0f alpha:1.0]];
    [self.customView addSubview:label];
}

-(void)addReportTypeButtonOnView:(CGRect)frame Title:(NSString *)text {
    
    UIButton *button = [[UIButton alloc]init];
    [button setTag:65554];
    [button setFrame:frame];
    [button.layer setBorderWidth:0.5];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitle:text forState:UIControlStateNormal];
    [button.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(forReportTypeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.customView addSubview:button];
}

-(void)addTextFieldOnOnCustomview:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setTag:tag];
    [textField setFrame:frame];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:title];
    [textField.layer setBorderWidth:1.0];
    [textField setUserInteractionEnabled:YES];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setFont:[UIFont systemFontOfSize:15.0f]];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setKeyboardType:UIKeyboardTypeNumberPad];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.customView addSubview:textField];
}

-(void)addTextViewOnOnCustomview:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextView *textview = [[UITextView alloc]init];
    [textview setTag:tag];
    [textview setText:title];
    [textview setFrame:frame];
    [textview setDelegate:(id)self];
    [textview.layer setBorderWidth:1.0];
    [textview setUserInteractionEnabled:YES];
    [textview setTextColor:[UIColor blackColor]];
    [textview setTextAlignment:NSTextAlignmentLeft];
    [textview setFont:[UIFont systemFontOfSize:15.0f]];
    [textview setBackgroundColor:[UIColor whiteColor]];
    [textview.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [textview setTextContainerInset:UIEdgeInsetsMake(0.0, 0.0f, 5.0, 5.0)];
    [self.customView addSubview:textview];
}


-(void)AddScrollViewImgVid:(CGRect)frame Tag:(int)tag List:(NSMutableArray*)listArr
{
    UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:frame];
    [scroll setContentSize:CGSizeMake(80*listArr.count+(listArr.count*2),frame.size.height)];
    [scroll setBackgroundColor:[UIColor clearColor]];
    [scroll setShowsHorizontalScrollIndicator:YES];
    [scroll setShowsVerticalScrollIndicator:NO];
    [self.customView addSubview:scroll];
    

    for (int tag = 0; tag<listArr.count; tag++)
    {
        CGRect aRect = CGRectMake(80*tag, 0, 80, 80);
        UIImageView *img = [[UIImageView alloc]initWithFrame:aRect];
        [img setUserInteractionEnabled:YES];
        [img setTag:1990+tag];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(HandleTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [tapGestureRecognizer setDelegate:self];
        [img addGestureRecognizer:tapGestureRecognizer];

        
        if (tag==0)
        {}
        else
        {
            [img setFrame:CGRectMake(img.frame.origin.x+2, 0, img.frame.size.width, img.frame.size.height)];
        }
        
        NSMutableDictionary *dicA = [[NSMutableDictionary alloc]init];
        dicA = [listArr objectAtIndex:tag];
        [scroll addSubview:img];
        [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dicA objectForKey:@"fileURL"]]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            img.image = [UIImage imageWithData:data];
        }];
    }
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
//    if (touch.view.tag >=1990 )
//    {
//        NSLog(@"loglogloglogloglog -- %ld",(long)touch.view.tag);
//        [self ImageTapped:(UIImageView*)touch.view];
//        return NO;
//
//         }
//    else if (touch.view == self.view || touch.view == self.customView ||[touch.view isKindOfClass:[UIScrollView class]])
//    {
//        NSLog(@"touchtouchtouchtouchtouch -- %ld",(long)touch.view.tag);
//        
//        [self dismissViewControllerAnimated:NO completion:nil];
//        return NO;
//    }
//    return YES;
//   }


-(void)ImageTapped:(UIImageView*)imgView
{
    NSLog(@"%@", NSStringFromSelector(_cmd));
    imgFullScr = [[[NSBundle mainBundle] loadNibNamed:@"ivUIXib" owner:self options:nil]lastObject];
    imgFullScr.imgForZoom = imgView.image;
    [self.view addSubview:imgFullScr];

}

-(IBAction)HandleTap:(UIGestureRecognizer*)Gesture

{
    if ([Gesture view].tag >=1990 )
    {
        NSLog(@"loglogloglogloglog -- %ld",(long)[Gesture view].tag);
        [self ImageTapped:(UIImageView*)[Gesture view]];
        
    }
    else if ([Gesture view] == self.view || [Gesture view] == self.customView ||[[Gesture view] isKindOfClass:[UIScrollView class]])
    {
        NSLog(@"touchtouchtouchtouchtouch -- %ld",(long)[Gesture view].tag);
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}






-(void)addReportAddButtonOnCustom:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.layer setCornerRadius:5.0];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:39.0f/255.0f blue:29.0f/255.0f alpha:1.0]];
    [button addTarget:self action:@selector(updateButtonForReportAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.customView addSubview:button];
}

-(void)addView:(CGRect)frame {
    
    viewPicker = [[UIView alloc]init];
    [viewPicker setFrame:frame];
    [viewPicker setTag:9999];
    [viewPicker setBackgroundColor:[UIColor colorWithRed:183.0f/255.0f green:40.0f/255.0f blue:35.0f/255.0f alpha:1.0f]];
    [self.customView addSubview:viewPicker];
    
    UIButton *donebutton = [[UIButton alloc]init];
    [donebutton setTitle:@"Done" forState:UIControlStateNormal];
    [donebutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [donebutton setFrame:CGRectMake(viewPicker.frame.origin.x, 0.0, 80, 44)];
    [donebutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [donebutton addTarget:self action:@selector(forReportDoneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [viewPicker addSubview:donebutton];
    
    UIButton *cancelbutton = [[UIButton alloc]init];
    [cancelbutton setFrame:CGRectMake(viewPicker.frame.size.width - 80, 0.0, 80, 44)];
    [cancelbutton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelbutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [cancelbutton addTarget:self action:@selector(forReportCancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [viewPicker addSubview:cancelbutton];
}

//***************Use of textfield delegate*************************//

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

//***************Use of textView delegate*************************//

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@"Enter Description"]) {
        textView.text = @"";
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Enter Description";
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


-(IBAction)forReportTypeButtonAction:(id)sender {
    
    [self.view endEditing:YES];
    [viewPicker setHidden:NO];
    if (pickerRemove > 0) {
        [self.reportTypePicker removeFromSuperview];
    }
    self.reportTypePicker = [[UIPickerView alloc]init];
    [self.reportTypePicker setFrame:CGRectMake(0, 230 , self.customView.frame.size.width , 190.0)];
    [self.reportTypePicker setBackgroundColor:[UIColor grayColor]];
    [self.reportTypePicker setUserInteractionEnabled:YES];
    [self.reportTypePicker setDataSource:(id)self];
    [self.reportTypePicker setDelegate:(id)self];
    [self.customView addSubview:self.reportTypePicker];
    pickerRemove++;
}

/*
 *******************************************************************
 *  UIPickerView delegate and data source methods..
 *******************************************************************
 */

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.reportTypeArray.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    VSGetAddTicketListModel *listModel;
    
    listModel = [self.reportTypeArray objectAtIndex:row];
    return listModel.report;
    
}

-(IBAction)forReportDoneButtonClicked:(id)sender {
    
    UIButton *reportTypeButton = (UIButton *)[self.view viewWithTag:65554];
    NSInteger row;
    VSGetAddTicketListModel *listModel;
    
    row = [self.reportTypePicker selectedRowInComponent:0];
    listModel = [self.reportTypeArray objectAtIndex:row];
    [reportTypeButton setTitle:listModel.report forState:UIControlStateNormal];
    self.reportTypeId = [NSString stringWithFormat:@"%@",listModel.reportTypeId];
    
    [viewPicker setHidden:YES];
    [self.reportTypePicker setHidden:YES];
}
-(IBAction)forReportCancelButtonClick:(id)sender {
    
    [viewPicker setHidden:YES];
    [self.reportTypePicker setHidden:YES];
}

-(IBAction)updateButtonForReportAction:(id)sender {
    
    UITextView *description = (UITextView *)[self.view viewWithTag:6555];
    UITextField *price = (UITextField *)[self.view viewWithTag:5555];
    UITextField *RImact = (UITextField *)[self.view viewWithTag:55551];

    VSGetAddTicketListModel *listModel = [self.reportTypeArray objectAtIndex:0];
    if (price.text.length == 0) {
        
        [[[UIAlertView alloc]initWithTitle:nil message:@"Please fill the price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        
    }else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            
        [self updateReport:description.text AddedBy:@"" RadiusOfimpact:[RImact text] Latitude:self.latitudeForAddReport Longitude:self.longitudeForAddReport IsHost:@"0" HostId:[VSSingletonClass sharedMySingleton].loginUserId ReportId:_reportTypeIdForwarded ReportStatusId:listModel.reportStatusTypeId ReportTypeId:self.reportTypeId ThumbsDown:@"0" ThumbsUp:@"0" UserId:[VSSingletonClass sharedMySingleton].loginUserId Price:price.text];
            
        }else {
             [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
}

@end
