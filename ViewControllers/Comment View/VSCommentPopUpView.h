//
//  VSCommentPopUpView.h
//  ValShare
//
//  Created by Sharda Prasad on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "VSReportVote.h"
//#import "UIViewController+MJPopupViewController.h"
#import "VSDirectionModal.h"




@interface VSCommentPopUpView : UIViewController

@property (nonatomic, strong) UIView *commentView;
@property (nonatomic, strong) UIView *reportShareView;

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *reportArray;
@property (nonatomic, strong) NSMutableArray *getCommentArray;
@property (nonatomic, strong) NSString *reportID;
@property (nonatomic, strong) VSDirectionModal *model;

@property (nonatomic, strong) NSString *annonationTitle;
@property (nonatomic, strong) NSString *annonationSubTitle;


@end
