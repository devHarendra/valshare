//
//  VSCustomAddTicketView.h
//  ValShare
//
//  Created by Vivek Kumar on 5/4/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "UIViewController+MJPopupViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>


@protocol TicketAddDelegate <NSObject>

-(void)ticketOnserverWithTitle:(NSString *)title withDescription:(NSString *)des infoModel:(NSMutableDictionary*)infoModel;

@end
@interface VSCustomAddTicketView : UIViewController < UIPickerViewDelegate, UITextFieldDelegate> 

@property (nonatomic, strong) UIView *customView;
@property (nonatomic, strong) NSMutableArray *reportTypeArray;
@property (nonatomic, strong) UIPickerView *reportTypePicker;
@property (nonatomic, strong) NSMutableArray *labelTextArray;
@property (nonatomic, strong) NSMutableDictionary *image_videoDict;
@property (nonatomic, strong) NSMutableDictionary *localImage_VideoDict;
@property (nonatomic, strong) NSMutableArray *image_videoArray;

@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *report;
@property (nonatomic, strong) NSString *reportTypeId;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, assign) id<TicketAddDelegate>delegate;
@end
