//
//  VSCommentPopUpView.m
//  ValShare
//
//  Created by Sharda Prasad on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCommentPopUpView.h"

@interface VSCommentPopUpView () {
    
    UITableView *commentTable;
    NSString *thumbsUp;
    NSString *thumbsDown;
    NSString *totalCount;
    UITextField *reportTextField;
    VSButton *submitButton;
    VSButton *cancelButton;
}

@end

@implementation VSCommentPopUpView
@synthesize dataArray = _dataArray;
@synthesize reportArray = _reportArray;
@synthesize commentView = _commentView;
@synthesize reportShareView = _reportShareView;
@synthesize getCommentArray = _getCommentArray;
@synthesize reportID;
@synthesize model;
@synthesize annonationSubTitle;
@synthesize annonationTitle;





-(NSMutableArray *)getCommentArray {
    
    if (!_getCommentArray) {
        _getCommentArray = [[NSMutableArray alloc] init];
    }
    return _getCommentArray;
}


-(NSMutableArray *)reportArray {
    
    if (!_reportArray) {
        _reportArray = [[NSMutableArray alloc] init];
    }
    return _reportArray;
}

-(NSMutableArray *)dataArray {
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

-(UIView *)commentView {
    
    if (!_commentView) {
        _commentView = [[UIView alloc] init];
        _commentView.frame = (IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 80, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height - 100):CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 80, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height - 230);
        _commentView.backgroundColor = [UIColor colorWithRed:229.0f/255.0f green:91.0f/255.0f blue:91.0f/290.0f alpha:1.0f];
        _commentView.userInteractionEnabled = YES;
        [self.view addSubview:_commentView];
    }
    return _commentView;
}

-(UIView *)reportShareView {
    
    if (!_reportShareView) {
        _reportShareView = [[UIView alloc] init];
        _reportShareView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.origin.y + 80, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height - 230);
        _reportShareView.backgroundColor = [UIColor clearColor];
        _reportShareView.hidden = YES;
        
        UILabel *label = [[UILabel alloc] init];
        [label setNumberOfLines:2];
        [label setTextColor:[UIColor whiteColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setLineBreakMode:NSLineBreakByWordWrapping];
        [label setFont:[UIFont italicSystemFontOfSize:20.0f]];
        [label setText:@"Share report to user mail address."];
        [label setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width, _reportShareView.frame.size.height - (kCommentBG.size.height * 8.0), kCommentBG.size.width, 60)];
        [_reportShareView addSubview:label];
        
        reportTextField = [[UITextField alloc] init];
        [reportTextField setTag:99897];
        [reportTextField setDelegate:(id)self];
        [reportTextField setPlaceholder:@"Email"];
        reportTextField.font = [UIFont italicSystemFontOfSize:14.0f];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 15, 15)];
        [reportTextField setLeftViewMode:UITextFieldViewModeAlways];
        [reportTextField setLeftView:leftView];
        reportTextField.keyboardType = UIKeyboardTypeEmailAddress;
        [reportTextField setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width, _reportShareView.frame.size.height - ((kCommentBG.size.height * 5.2) + 17), kCommentBG.size.width, kCommentBG.size.height)];
        [reportTextField setBackgroundColor:[UIColor whiteColor]];
        [_reportShareView addSubview:reportTextField];
        
        submitButton = [VSButton buttonWithType:UIButtonTypeCustom];
        submitButton.tag = 12320;
        submitButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width, _reportShareView.frame.size.height -  ((kCommentBG.size.height * 4.0) + 10), kCommentBG.size.width, kCommentBG.size.height);
        [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
        [submitButton setBackgroundImage:kCommentBG forState:UIControlStateNormal];
        [submitButton addTarget:self action:@selector(reportShareViewButton11:) forControlEvents:UIControlEventTouchUpInside];
        [_reportShareView addSubview:submitButton];
        
        cancelButton = [VSButton buttonWithType:UIButtonTypeCustom];
        cancelButton.tag = 12321;
        cancelButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width, _reportShareView.frame.size.height - (kCommentBG.size.height * 3), kCommentBG.size.width, kCommentBG.size.height);
        [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [cancelButton setBackgroundImage:kCommentBG forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(reportShareViewButton11:) forControlEvents:UIControlEventTouchUpInside];
        [_reportShareView addSubview:cancelButton];
        
        [UIView animateWithDuration:1.0 animations:^{
            
            _reportShareView.frame = CGRectMake([UIScreen mainScreen].bounds.origin.x + 10, [UIScreen mainScreen].bounds.origin.y + 80, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height - 230);
            _reportShareView.backgroundColor = [UIColor colorWithRed:229.0f/255.0f green:91.0f/255.0f blue:91.0f/290.0f alpha:1.0f];
            
            label.frame = CGRectMake(_reportShareView.bounds.origin.x + 15, _reportShareView.frame.size.height - (kCommentBG.size.height * 8.0), kCommentBG.size.width, 60);
            reportTextField.frame = CGRectMake(_reportShareView.bounds.origin.x + 5, _reportShareView.frame.size.height - ((kCommentBG.size.height * 5.2) + 17), kCommentBG.size.width, kCommentBG.size.height);
            submitButton.frame = CGRectMake(_reportShareView.bounds.origin.x + 5, _reportShareView.frame.size.height - ((kCommentBG.size.height * 4.0) + 10), kCommentBG.size.width, kCommentBG.size.height);
            cancelButton.frame = CGRectMake(_reportShareView.bounds.origin.x + 5, _reportShareView.frame.size.height - (kCommentBG.size.height * 3), kCommentBG.size.width, kCommentBG.size.height);
        }];
        _reportShareView.userInteractionEnabled = YES;
        [self.view addSubview:_reportShareView];
    }
    return _reportShareView;
}

-(IBAction)reportShareViewButton11:(VSButton *)sender {
    
    if (sender.tag == 12320) {
        
        if (![self validateEmail:reportTextField.text]) {
            
            [[[UIAlertView alloc] initWithTitle:nil message:@"Please enter valid email." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        } else {
            if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];

                [self shareDataWithUserInEmail:[VSSingletonClass sharedMySingleton].loginUserId ReportId:self.reportID Email:reportTextField.text];
            }else {
                
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alertview show];
            }
        }
    }else
    {
        [self hideReportView];
    }
    [reportTextField setText:@""];
    //[_reportShareView setHidden:YES];
    //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftRight];
}


-(BOOL)validateEmail:(NSString*)email {
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if (![emailTest evaluateWithObject:email]) {
        return NO;
    }
    return YES;
}

-(void)shareDataWithUserInEmail:(NSString *)userId ReportId:(NSString *)reportId Email:(NSString *)email {
    
    [[VSParserClass sharedParser] shareReportInGmail:reportId UserID:userId Email:email WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        if (result) {
            
            NSLog(@"reportTextField.text :- %@", resultObject);
            
            NSMutableDictionary *loginDict = (NSMutableDictionary *)resultObject;
            if ([[loginDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                [self hideReportView];

                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Report pin sent in user mail inbox." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else {
                [[[UIAlertView alloc] initWithTitle:nil message:@"Server error." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            NSLog(@"Error --------: %@", resultObject);
        }
    }];
}

-(void)dealloc {
    
    self.commentView = nil;
    self.reportShareView = nil;
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [self.reportShareView removeFromSuperview];
}

-(void)thumbsUPService:(NSString *)myReportID UserName:(NSString *)userId{
    
    [[VSParserClass sharedParser] thumbsUpService:myReportID UserID:userId WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            NSMutableDictionary *dataUp = (NSMutableDictionary *)resultObject;
            if ([[dataUp valueForKey:@"status"] isEqualToString:@"Ok"]) {
            } else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[dataUp valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            
           [SVProgressHUD dismiss];
        }
        else {
            [SVProgressHUD dismiss];

        }
    }];
}


-(void)thumbsDownService:(NSString *)myReportID UserName:(NSString *)userName {
    
    [[VSParserClass sharedParser] thumbsDownService:myReportID UserID:userName WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSMutableDictionary *dataDown = (NSMutableDictionary *)resultObject;
            
            if ([[dataDown valueForKey:@"status"] isEqualToString:@"Ok"]) {
                UILabel *like = (UILabel *)[self.view viewWithTag:4434];
                UILabel *disLike = (UILabel *)[self.view viewWithTag:4435];

                NSString *likeTotal = [NSString stringWithFormat:@"%@", @"0"];
                NSString *disLikeTotal = [NSString stringWithFormat:@"%@", @"0"];
                
                [like setText:likeTotal];
                [disLike setText:disLikeTotal];
            } else {
                [[[UIAlertView alloc] initWithTitle:nil message:[dataDown valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];

        }
    }];
}


-(void)reportVote:(NSString *)marketID UserID:(NSString *)userId {
    
    [[VSParserClass sharedParser] reportVoteService:marketID UserID:userId WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                
                NSMutableDictionary *dataDict = (NSMutableDictionary *)[[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                VSReportVote *reportModal = [[VSReportVote alloc] init];
                [reportModal setReportId:[dataDict valueForKey:@"reportId"]];
                [reportModal setReportVote:[dataDict valueForKey:@"reportVote"]];
                [reportModal setThumbsDown:[dataDict valueForKey:@"thumbsDown"]];
                [reportModal setThumbsUp:[dataDict valueForKey:@"thumbsUp"]];
                [self.reportArray addObject:reportModal];
            }
            if (![resultObject valueForKey:@"message"] ) {
                
            
            UILabel *like = (UILabel *)[self.view viewWithTag:4434];
            UILabel *disLike = (UILabel *)[self.view viewWithTag:4435];
            
            
            
            VSReportVote *reportModal = [self.reportArray objectAtIndex:0];

            NSString *likeTotal = [NSString stringWithFormat:@"%@", (reportModal.thumbsUp) ? reportModal.thumbsUp : @"0"];
            NSString *disLikeTotal = [NSString stringWithFormat:@"%@", (reportModal.thumbsDown) ? reportModal.thumbsDown : @"0"];
            [like setText:likeTotal];
            [disLike setText:disLikeTotal];
            }
            
         [SVProgressHUD dismiss];
        }
        
        else {
            [SVProgressHUD dismiss];

            NSLog(@"thumbsDownServicethumbsDownService ::- %@", resultObject);
        }
    }];
}


-(void)postComment:(NSString *)comment UserId:(NSString *)userId ReportId:(NSString *)reportId {
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setObject:comment forKey:@"comment"];
    [dataDict setObject:userId forKey:@"userId"];
    [dataDict setObject:reportId forKey:@"reportId"];

    [[VSParserClass sharedParser] reportCommentManager:dataDict WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSMutableDictionary *dataDict = (NSMutableDictionary *)resultObject;
            if ([dataDict isKindOfClass:[NSDictionary class]]) {
                if ([[dataDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                    [self getComment:self.reportID UserID:[VSSingletonClass sharedMySingleton].loginUserId];
                } else {
                    [[[UIAlertView alloc] initWithTitle:nil message:[dataDict valueForKey:@"status"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
            }else {
                NSLog(@"else: :- %@", dataDict);
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            NSLog(@"else: :- %@", error);
        }
    }];
}

-(void)getComment:(NSString *)reportId UserID:(NSString *)usertID {
    
    [[VSParserClass sharedParser] getComment:reportId UserID:usertID WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        if (result) {
            
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Failure"]) {
                
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }
            
            else{
                
                NSMutableDictionary *dataDict = (NSMutableDictionary *)[[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
            
            NSLog(@"1111111dataDictdataDict :- %@", dataDict);

            [self.getCommentArray removeAllObjects];
                
            if ([[dataDict valueForKey:@"reportCommentModelList"] isKindOfClass:[NSArray class]]) {
                NSMutableArray *lDataArray = (NSMutableArray *)[dataDict valueForKey:@"reportCommentModelList"];
                [lDataArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSMutableDictionary *localDict = (NSMutableDictionary *)[lDataArray objectAtIndex:idx];
                    VSCommentModal *getCommentModal = [[VSCommentModal alloc] init];
                    [getCommentModal setComment:[localDict valueForKey:@"comment"]];
                    [getCommentModal setLastModifiedTime:[localDict valueForKey:@"lastModifiedTime"]];
                    [getCommentModal setReportCommentId:[localDict valueForKey:@"reportCommentId"]];
                    [getCommentModal setReportCommentStatusType:[localDict valueForKey:@"reportCommentStatusType"]];
                    [getCommentModal setReportId:[localDict valueForKey:@"reportId"]];
                    [getCommentModal setReportType:[localDict valueForKey:@"reportType"]];
                    [getCommentModal setThumbsDown:[localDict valueForKey:@"thumbsDown"]];
                    [getCommentModal setThumbsUp:[localDict valueForKey:@"thumbsUp"]];
                    [getCommentModal setUserId:[localDict valueForKey:@"userId"]];
                    [getCommentModal setUserName:[localDict valueForKey:@"userName"]];
                    [getCommentModal setYourChoice:[localDict valueForKey:@"yourChoice"]];
                    [self.getCommentArray addObject:getCommentModal];
                }];
            
            }else {

                NSLog(@"else: :- %@", dataDict);
            }
            }
            [SVProgressHUD dismiss];

        } else {
            [SVProgressHUD dismiss];

            NSLog(@"elseelseelse: :- %@", resultObject);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *reversed = [[self.getCommentArray reverseObjectEnumerator] allObjects];
            [self.getCommentArray removeAllObjects];
            
            self.getCommentArray = (NSMutableArray *)reversed;
            
            [commentTable reloadData];
        });
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        NSLog(@"self.reportID:-%@UserID:-%@",self.reportID,[VSSingletonClass sharedMySingleton].loginUserId);
            [self getComment:self.reportID UserID:[VSSingletonClass sharedMySingleton].loginUserId];
    }else {
        
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertview show];
    }
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self reportVote:self.reportID UserID:[VSSingletonClass sharedMySingleton].loginUserId];
    }else {
        
        [[[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    
    [self.view setBackgroundColor:[UIColor lightTextColor]];
    [self addComponentOnView];
    
    NSLog(@"model.reportModalDescription:%@",model.reportModalDescription);
    
    UITapGestureRecognizer *removeOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeOverlayFromView:)];
    removeOverlay.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:removeOverlay];
}

-(void)removeOverlayFromView:(UITapGestureRecognizer *)tap
{
//  [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftRight];
//    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];

   }

-(void)addComponentOnView
{
    NSArray *likeArray = @[kLikeBG,kUnLikeBG];
    NSArray *likeHighLight = @[kLikeHighLightBG,kUnLikeHighLightBG];

    NSString *likeTotal = [NSString stringWithFormat:@"%@", @"0"];
    NSString *disLikeTotal = [NSString stringWithFormat:@"%@", @"0"];

    [self addLabelOnView:CGRectMake(self.commentView.bounds.origin.x + (kLikeBG.size.width + 23), self.commentView.bounds.origin.y + 13, 95, 25) Tag:4434 Text:likeTotal];
    [self addLabelOnView:CGRectMake(self.commentView.bounds.origin.x + (kLikeBG.size.width + 45) + ((kLikeBG.size.width * 4) - 5), self.commentView.bounds.origin.y + 10, 105, 25) Tag:4435 Text:disLikeTotal];
    
    [self addButtonOnView:CGRectMake(self.commentView.bounds.size.width - (kGmailShareBG.size.width + 4), self.commentView.bounds.origin.y + 9, kGmailShareBG.size.width - 11, kGmailShareBG.size.height - 11) BGImage:kGmailShareBG Tag:44343 Title:nil];
    
    for (int i = 0; i< 2; i++) {
        
        [self addLikeButtonOnView:CGRectMake(self.commentView.bounds.origin.x + 15 + ((kLikeBG.size.width * 4.5) * i), (i == 1) ? self.commentView.bounds.origin.y + 14 : self.commentView.bounds.origin.y + 10, kLikeBG.size.width, kLikeBG.size.height) BGImage:likeArray[i] SBGImage:likeHighLight[i] Tag:99098 + i];
    }
    
    [self addTableViewOnView:(IS_IPHONE_5) ? CGRectMake(self.commentView.bounds.size.width/2 - kCommentBG.size.width/2, self.commentView.bounds.origin.y + 160, kCommentBG.size.width, self.commentView.bounds.size.height/2 - 70):(IS_IPHONE_4S) ?CGRectMake(self.commentView.bounds.size.width/2 - kCommentBG.size.width/2, self.commentView.bounds.origin.y + 160, kCommentBG.size.width, self.commentView.bounds.size.height/2 - 50):CGRectMake(self.commentView.bounds.size.width/2 - kCommentBG.size.width/2, self.commentView.bounds.origin.y + 165, kCommentBG.size.width, self.commentView.bounds.size.height/2 - 70)];
    
    [self addTextViewOnView:CGRectMake(self.commentView.bounds.size.width/2 - kCommentBG.size.width/2, self.commentView.bounds.size.height - 115, kCommentBG.size.width, 60)];
    [self addButtonOnView:CGRectMake(self.commentView.bounds.size.width/2 - kCommentBG.size.width/2, self.commentView.bounds.size.height - 45, kCommentBG.size.width, kCommentBG.size.height) BGImage:kCommentBG Tag:44344 Title:@"Comment"];
    
    [self addViewForReport:CGRectMake(self.commentView.bounds.size.width/2 - kCommentBG.size.width/2, self.commentView.bounds.origin.y + 45, kCommentBG.size.width, 110)];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addLabelOnView:(CGRect)frame Tag:(int)tag Text:(NSString *)text {
    
    UILabel *label = [[UILabel alloc] init];
    [label setTag:tag];
    [label setText:text];
    [label setFrame:frame];
    [label.layer setShadowOpacity:2.0f];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label.layer setShadowOffset:CGSizeMake(0, -1)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:17.0f]];
    [label.layer setShadowColor:[UIColor grayColor].CGColor];
    [self.commentView addSubview:label];
}

-(void)addTableViewOnView:(CGRect)frame {
    
    commentTable = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    [commentTable setBackgroundColor:[UIColor whiteColor]];
    [commentTable setTag:20100];
    [commentTable setDelegate:(id)self];
    [commentTable setDataSource:(id)self];
    [commentTable.layer setCornerRadius:2.0f];
    [commentTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [self.commentView addSubview:commentTable];
}
-(void)addViewForReport:(CGRect)frame {
    
    NSArray *textArray = @[[NSString stringWithFormat:@"Added By :%@",model.reportModalAddedByName],[NSString stringWithFormat:@"Report Type :%@",self.annonationTitle],[NSString stringWithFormat:@"Description :%@",self.annonationSubTitle],];
    UIView *view = [[UIView alloc]init];
    [view setFrame:frame];
    [view setBackgroundColor:[UIColor whiteColor]];
    [self.commentView addSubview:view];
    
    for (int i = 0; i < 3; i++) {
        
        UILabel *label = [[UILabel alloc]init];
        [label setFrame:CGRectMake(5,5 + (30 * i), view.frame.size.width - 20 , 30)];
        [label setText:textArray[i]];
        [label setTextColor:[UIColor blackColor]];
        [label setFont:[UIFont boldSystemFontOfSize:15.0]];
        [label setBackgroundColor:[UIColor clearColor]];
        [view addSubview:label];
    }
}
-(void)addTextViewOnView:(CGRect)frame {
    
    UITextView *textView =[[UITextView alloc]init];
    [textView setTag:76760];
    [textView setFrame:frame];
    [textView setDelegate:(id)self];
    [textView.layer setBorderWidth:1.0f];
    [textView.layer setCornerRadius:2.0f];
    [textView.layer setShadowOpacity:1.0f];
    [textView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [textView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [textView setBackgroundColor:[UIColor whiteColor]];
    [self.commentView addSubview:textView];
}


-(void)addLikeButtonOnView:(CGRect)frame BGImage:(UIImage *)image SBGImage:(UIImage *)sImage Tag:(int)tag {
    
    UIButton *likeButton = [[UIButton alloc] init];
    [likeButton setTag:tag];
    [likeButton setFrame:frame];
    [likeButton setBackgroundImage:image forState:UIControlStateNormal];
    [likeButton setBackgroundImage:sImage forState:UIControlStateSelected];
    [likeButton addTarget:self action:@selector(likeUnLikeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.commentView addSubview:likeButton];
}


-(void)addButtonOnView:(CGRect)frame BGImage:(UIImage *)image Tag:(int)tag Title:(NSString *)title {
    
    UIButton *myButton = [[UIButton alloc] init];
    [myButton setTag:tag];
    [myButton setFrame:frame];
    [myButton setTitle:title forState:UIControlStateNormal];
    [myButton setBackgroundImage:image forState:UIControlStateNormal];
    [myButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
    [myButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [myButton addTarget:self action:@selector(commentButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.commentView addSubview:myButton];
}

-(IBAction)likeUnLikeButtonAction:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.tag == 99098) {
        
    }else {
        
    }
    if (sender.selected) {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self thumbsUPService:self.reportID UserName:[VSSingletonClass sharedMySingleton].loginUserId];

        }else {
            
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alertview show];
        }
        
        
    } else {
        
         if ([[Reachability reachabilityForInternetConnection] isReachable]) {
         [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self thumbsDownService:self.reportID UserName:[VSSingletonClass sharedMySingleton].loginUserId];
             
         }else {
             
             UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [alertview show];
             

         }
    }
}


-(void)textViewDidBeginEditing:(UITextView *)textField {
    
    [self animateTextFieldAndView:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textField {
    
    [self animateTextFieldAndView:NO];
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}


- (void)animateTextFieldAndView:(BOOL) up {
    
    const int movementDistance = 80;
    const float movementDuration = 0.3f;
    
    int movement = (up ? - movementDistance : movementDistance);
    
    [UIView beginAnimations:@"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration:movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(IBAction)commentButtonAction:(UIButton *)Button {
    
    UITextView *textView = (UITextView *)[self.view viewWithTag:76760];
    
    switch (Button.tag) {
        case 44343:
            [self shareReportWithGmailView];
            break;
        case 44344:
            [self.reportShareView setHidden:YES];
            if ([textView.text length] == 0) {
                [[[UIAlertView alloc] initWithTitle:nil message:@"Please enter text." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else {
                if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                    [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
                    
                    [self postComment:textView.text UserId:[VSSingletonClass sharedMySingleton].loginUserId ReportId:self.reportID];
                    [textView setText:@""];
                    
                }else {
                    
                    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [alertview show];
                }
         
            }
            break;
            
        default:
            break;
    }
}


-(void)shareReportWithGmailView {
    
    NSLog(@"shareReportWithGmai-----------lView :-");
    [self.reportShareView setHidden:NO];
    [self showReportView];
}


-(void)showReportView {
    
    [UIView animateWithDuration:1.0 animations:^{
        
         _reportShareView.frame = CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 60, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height - 230);
    }];
}

-(void)hideReportView {
    
    [UIView animateWithDuration:1.0 animations:^{
        
        _reportShareView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.origin.y + 60, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.height - 230);
    }];
}

/*
 ********************************************************************************
 * Table View Data Source and delegate methods.
 ********************************************************************************
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 35;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [self.getCommentArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    VSPinPupUpCell *cell = (VSPinPupUpCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[VSPinPupUpCell alloc] initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    VSCommentModal *getCommentModal = (VSCommentModal *)[self.getCommentArray objectAtIndex:indexPath.row];
    NSString *userName = [NSString stringWithFormat:@"%@", (getCommentModal.userName) ? getCommentModal.userName : @"Guest"];
    
    [cell.myLabel setText:[NSString stringWithFormat:@"%@ : %@", userName, getCommentModal.comment]];
    return cell;
}

@end
