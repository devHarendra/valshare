//
//  VSWeatherTable.h
//  ValShare
//
//  Created by Sharda Prasad on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "VSWeatherTableCell.h"

@protocol weatherTableDelegate <NSObject>

@optional
-(void)getTextFromTableViewCell:(NSString *)text NSIndexPath:(NSInteger)index TableTag:(UITableView *)tableView;
@end

@interface VSWeatherTable : UITableView

-(void)initializeTableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property(nonatomic, assign) id <weatherTableDelegate> weatherDelegate;
@end
