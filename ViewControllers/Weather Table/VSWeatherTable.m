//
//  VSWeatherTable.m
//  ValShare
//
//  Created by Sharda Prasad on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSWeatherTable.h"

@implementation VSWeatherTable
@synthesize dataArray = _dataArray;


//-(NSMutableArray *)dataArray {
//    
//    if (!_dataArray) {
//        _dataArray = [[NSMutableArray alloc] init];
//    }
//    return _dataArray;
//}

-(void)initializeTableView{
    
    if (self) {
        self.dataSource = (id)self;
        self.delegate = (id)self;
        self.backgroundView = nil;
        self.scrollEnabled = YES;
    }
    //[self alphabeticalOrderBySellerName:self.shopArray];
}

-(void)alphabeticalOrderBySellerName:(NSMutableArray *)unsortedArray {
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryName" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [unsortedArray sortedArrayUsingDescriptors:sortDescriptors];
    
    self.dataArray = (NSMutableArray *)sortedArray;
}

#pragma mark tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return ([self.dataArray count] > 0) ? [self.dataArray count] : 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 240;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static  NSString *editableCellIdentifier = @"itemEditCell";
    VSWeatherTableCell * cell = [tableView dequeueReusableCellWithIdentifier:editableCellIdentifier];
    
    if (cell == nil) {
        
        cell = [[VSWeatherTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:editableCellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if ([self.dataArray count] > 0) {
        
        VSWeatherModal *weatherModal = [self.dataArray objectAtIndex:indexPath.row];
        NSLog(@"weatherModal.minimumTemprature%@",weatherModal.minimumTemprature);
        
        [cell.countryLBL setText:[NSString stringWithFormat:@": %@", weatherModal.country]];
        [cell.cityLBL setText:[NSString stringWithFormat:@": %@", weatherModal.city]];
        
        [cell.humadityLBL setText:[NSString stringWithFormat:@": %@", weatherModal.humadity]];
        [cell.pressureLBL setText:[NSString stringWithFormat:@": %@", weatherModal.pressure]];
        
        [cell.minimumTempLBL setText:[NSString stringWithFormat:@": %@", weatherModal.minimumTemprature]];
        [cell.maximumTempLBL setText:[NSString stringWithFormat:@": %@", weatherModal.maximumTemprature]];
        
        [cell.averageTempLBL setText:[NSString stringWithFormat:@": %@", weatherModal.averageTemprature]];
        [cell.shortDescriptionLBL setText:[NSString stringWithFormat:@": %@", weatherModal.shortDescription]];
        
        [cell.dateTime setText:[NSString stringWithFormat:@": %@", weatherModal.dateTime]];
        [cell.windSpeedLBL setText:[NSString stringWithFormat:@": %@", weatherModal.windSpeed]];
        
        [cell.descriptionLBL setText:[NSString stringWithFormat:@": %@", weatherModal.description]];
        
        [cell.cityLBL setHidden:NO];
        [cell.dateTime  setHidden:NO];
        [cell.countryLBL setHidden:NO];
        [cell.humadityLBL setHidden:NO];
        [cell.pressureLBL setHidden:NO];
        [cell.windSpeedLBL setHidden:NO];
        [cell.minimumTempLBL setHidden:NO];
        [cell.maximumTempLBL setHidden:NO];
        [cell.averageTempLBL setHidden:NO];
        [cell.descriptionLBL setHidden:NO];
        [cell.shortDescriptionLBL  setHidden:NO];
    }else {
        
        [cell.cityLBL setHidden:YES];
        [cell.dateTime  setHidden:YES];
        [cell.countryLBL setHidden:NO];
        [cell.humadityLBL setHidden:YES];
        [cell.pressureLBL setHidden:YES];
        [cell.windSpeedLBL setHidden:YES];
        [cell.minimumTempLBL setHidden:YES];
        [cell.maximumTempLBL setHidden:YES];
        [cell.averageTempLBL setHidden:YES];
        [cell.descriptionLBL setHidden:YES];
        [cell.shortDescriptionLBL  setHidden:YES];
        [cell.countryLBL setText:@"Weather data is not found."];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSelector:@selector(deselect) withObject:nil afterDelay:.3];
}

- (void) deselect {
    
    [self deselectRowAtIndexPath:[self indexPathForSelectedRow] animated:YES];
}

@end
