//
//  VSWeatherTableCell.m
//  ValShare
//
//  Created by Sharda Prasad on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSWeatherTableCell.h"

@implementation VSWeatherTableCell

@synthesize averageTempLBL;
@synthesize cityLBL;
@synthesize countryLBL;
@synthesize descriptionLBL;
@synthesize humadityLBL;
@synthesize maximumTempLBL;
@synthesize minimumTempLBL;
@synthesize pressureLBL;
@synthesize shortDescriptionLBL;
@synthesize dateTime;
@synthesize windSpeedLBL;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initialization code
        UILabel *label = [[UILabel alloc] init];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setFont:[UIFont systemFontOfSize:12]];
        label.lineBreakMode = NSLineBreakByTruncatingTail;
        [label setTextColor:[UIColor whiteColor]];
        [self addSubview:label];
        averageTempLBL = label;
        
        UILabel *dataLBL = [[UILabel alloc] init];
        [dataLBL setBackgroundColor:[UIColor clearColor]];
        [dataLBL setFont:[UIFont systemFontOfSize:12]];
        dataLBL.lineBreakMode = NSLineBreakByTruncatingTail;
        [dataLBL setTextColor:[UIColor whiteColor]];
        [self addSubview:dataLBL];
        cityLBL = dataLBL;
        
        UILabel *timeLBL = [[UILabel alloc] init];
        [timeLBL setBackgroundColor:[UIColor clearColor]];
        [timeLBL setFont:[UIFont systemFontOfSize:12]];
        timeLBL.lineBreakMode = NSLineBreakByTruncatingTail;
        [timeLBL setTextColor:[UIColor whiteColor]];
        [self addSubview:timeLBL];
        countryLBL = timeLBL;

        UILabel *tempratureLBL = [[UILabel alloc] init];
        [tempratureLBL setBackgroundColor:[UIColor clearColor]];
        [tempratureLBL setFont:[UIFont systemFontOfSize:12]];
        tempratureLBL.lineBreakMode = NSLineBreakByTruncatingTail;
        [tempratureLBL setTextColor:[UIColor whiteColor]];
        [self addSubview:tempratureLBL];
        descriptionLBL = tempratureLBL;
        
        
        UILabel *humadity = [[UILabel alloc] init];
        [humadity setBackgroundColor:[UIColor clearColor]];
        [humadity setFont:[UIFont systemFontOfSize:12]];
        humadity.lineBreakMode = NSLineBreakByTruncatingTail;
        [humadity setTextColor:[UIColor whiteColor]];
        [self addSubview:humadity];
        humadityLBL = humadity;
        
        UILabel *maximumTemp = [[UILabel alloc] init];
        [maximumTemp setBackgroundColor:[UIColor clearColor]];
        [maximumTemp setFont:[UIFont systemFontOfSize:12]];
        maximumTemp.lineBreakMode = NSLineBreakByTruncatingTail;

        [maximumTemp setTextColor:[UIColor whiteColor]];
        [self addSubview:maximumTemp];
        maximumTempLBL = maximumTemp;
        
        UILabel *minimumTemp = [[UILabel alloc] init];
        [minimumTemp setBackgroundColor:[UIColor clearColor]];
        [minimumTemp setFont:[UIFont systemFontOfSize:12]];
        minimumTemp.lineBreakMode = NSLineBreakByTruncatingTail;
        
        [minimumTemp setTextColor:[UIColor whiteColor]];
        [self addSubview:minimumTemp];
        minimumTempLBL = minimumTemp;
        
        UILabel *pressure = [[UILabel alloc] init];
        [pressure setBackgroundColor:[UIColor clearColor]];
        [pressure setFont:[UIFont systemFontOfSize:12]];
        pressure.lineBreakMode = NSLineBreakByTruncatingTail;
        [pressure setTextColor:[UIColor whiteColor]];
        [self addSubview:pressure];
        pressureLBL = pressure;
        
        UILabel *shortDescription = [[UILabel alloc] init];
        [shortDescription setBackgroundColor:[UIColor clearColor]];
        [shortDescription setFont:[UIFont systemFontOfSize:12]];
        shortDescription.lineBreakMode = NSLineBreakByTruncatingTail;
        [shortDescription setTextColor:[UIColor whiteColor]];
        [self addSubview:shortDescription];
        shortDescriptionLBL = shortDescription;
        
        UILabel *weatherData = [[UILabel alloc] init];
        [weatherData setBackgroundColor:[UIColor clearColor]];
        [weatherData setFont:[UIFont systemFontOfSize:12]];
        [weatherData setTextColor:[UIColor whiteColor]];
        weatherData.lineBreakMode = NSLineBreakByTruncatingTail;

        [self addSubview:weatherData];
        dateTime = weatherData;
        
        UILabel *windSpeed = [[UILabel alloc] init];
        [windSpeed setBackgroundColor:[UIColor clearColor]];
        [windSpeed setFont:[UIFont systemFontOfSize:14]];
        windSpeed.lineBreakMode = NSLineBreakByTruncatingTail;

        [windSpeed setTextColor:[UIColor whiteColor]];
        [self addSubview:windSpeed];
        windSpeedLBL = windSpeed;
        NSArray *dataArray = @[@"Date",@"City",@"Country",@"Humadity",@"Pressure", @"Min. Temp.",@"Max. Temp.",@"Avg. Temp.",@"Wind Speed",@"Short Desc.", @"Description"];
        for (int i = 0; i<= 10; i++) {
            [self addStaticLabel:CGRectMake(5, 5 + (20 * i), 90, 20) Tag:6756 + i Text:dataArray[i]];
            
            UIView *line = [[UIView alloc]  init];
            [line setBackgroundColor:[UIColor darkGrayColor]];
            [line setFrame:CGRectMake(0.0, 235, self.frame.size.width, 1)];
            [self addSubview:line];
        }
    }
    return self;
}

-(void)addStaticLabel:(CGRect)frame Tag:(int)tag Text:(NSString *)text {
    
    UILabel *weatherData = [[UILabel alloc] init];
    [weatherData setTag:tag];
    [weatherData setText:text];
    [weatherData setFrame:frame];
    [weatherData setBackgroundColor:[UIColor clearColor]];
    [weatherData setFont:[UIFont boldSystemFontOfSize:12]];
    [weatherData setTextColor:[UIColor whiteColor]];
    [self addSubview:weatherData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)layoutSubviews {

    [self.dateTime setFrame:CGRectMake(100.0, 5.0, 200.0, 20)];
    [self.cityLBL setFrame:CGRectMake(100.0, 25.0, 200.0, 20)];
    [self.countryLBL setFrame:CGRectMake(100.0, 45.0, 200.0, 20)];
    [self.humadityLBL setFrame:CGRectMake(100.0, 65.0, 200.0, 20)];
    [self.pressureLBL setFrame:CGRectMake(100.0, 85.0, 200.0, 20)];
    [self.minimumTempLBL setFrame:CGRectMake(100.0, 105.0, 200.0, 20)];
    [self.maximumTempLBL setFrame:CGRectMake(100.0, 125.0, 200.0, 20)];
    [self.averageTempLBL setFrame:CGRectMake(100.0, 145.0, 200.0, 20)];
    [self.windSpeedLBL setFrame:CGRectMake(100.0, 165.0, 200.0, 20)];
    [self.shortDescriptionLBL setFrame:CGRectMake(100.0, 185.0, 200.0, 20)];
    [self.descriptionLBL setFrame:CGRectMake(100.0, 205.0, 200.0, 20)];
}

-(void)setNeedsLayout {

}

-(void)setNeedsDisplay {
    
}

@end
