//
//  VSWeatherTableCell.h
//  ValShare
//
//  Created by Sharda Prasad on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VSWeatherTableCell : UITableViewCell

@property (strong,nonatomic) UILabel *averageTempLBL;
@property (strong,nonatomic) UILabel *cityLBL;
@property (strong,nonatomic) UILabel *countryLBL;
@property (strong,nonatomic) UILabel *descriptionLBL;
@property (strong,nonatomic) UILabel *humadityLBL;
@property (strong,nonatomic) UILabel *maximumTempLBL;
@property (strong,nonatomic) UILabel *minimumTempLBL;
@property (strong,nonatomic) UILabel *pressureLBL;
@property (strong,nonatomic) UILabel *shortDescriptionLBL;
@property (strong,nonatomic) UILabel *dateTime;
@property (strong,nonatomic) UILabel *windSpeedLBL;
@end
