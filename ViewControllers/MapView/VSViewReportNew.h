//
//  VSViewReportNew.h
//  ValShare
//
//  Created by iMac Apple on 17/06/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "VSDirectionModal.h"
#import "VSButton.h"

@interface VSViewReportNew : UIView<UIGestureRecognizerDelegate>
{
    BOOL isPopUp;
    UITextField *reportTextField;
    VSButton *submitButton;
    VSButton *cancelButton;
    IBOutlet UIScrollView *scroll;

}

@property (nonatomic, strong) UIView *commentView;
@property (nonatomic, strong) UIView *reportShareView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *reportArray;
@property (nonatomic, strong) NSMutableArray *getCommentArray;
@property (nonatomic, strong) NSString *reportID;
@property (nonatomic, strong) VSDirectionModal *model;
@property (nonatomic, strong) NSString *annonationTitle;
@property (nonatomic, strong) NSString *annonationSubTitle;


@end
