//
//  VSMapController.h
//  ValShare
//
//  Created by Sharda Prasad on 3/31/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//
#import "VSHeader.h"
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MLKMenuPopover.h"
#import "VSUserReportsView.h"
#import "VSCollectionViewTab.h"
#import "VSCompareRouteController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "VSCommentPopUpView.h"
#import <AVFoundation/AVFoundation.h>
#import "LocationManager.h"
#import "MyRouteViewController.h"


@interface VSMapController : UIViewController<MLKMenuPopoverDelegate, GMSMapViewDelegate,UIGestureRecognizerDelegate,LocationDelegate,AVSpeechSynthesizerDelegate>
{
    NSMutableArray *localRoots;
    NSMutableArray *ToSpeechArr;
    CLLocation *locCordinates;
    LocationManager * manager;
    GMSMarker* freelyMarker;
    NSTimer *timer;
    int Remtime;
    BOOL isInitialized,isSearched;

    NSMutableArray *RoutePolylines;

}
//@property (nonatomic, strong)  MKMapView *mapView;
@property (nonatomic, strong)  GMSMapView *mapView;
@property (nonatomic, strong)  NSString *userID;

@property (nonatomic, strong)  NSMutableArray *weatherData;


//@property (nonatomic, strong)  MKPolylineRenderer *routeLineFirst;
//@property (nonatomic, strong)  MKPolylineRenderer *routeLineSecond;
//@property (nonatomic, strong)  MKPolylineRenderer *routeLineThird;

@property (nonatomic, strong)  GMSPolyline *routeLine;
@property (nonatomic, strong)  NSMutableArray *locationarray;
@property (nonatomic, strong)  NSMutableArray *IgnouredTkts;


@property(nonatomic,strong) MLKMenuPopover *menuPopover;

@property(nonatomic,strong) NSArray *menuItems;

@property (nonatomic, strong)  NSString *addTicketLatitude;
@property (nonatomic, strong)  NSString *addTicketLongitude;

@property (nonatomic, strong)  NSString *addReportDetailLatitude;
@property (nonatomic, strong)  NSString *addReportDetailLongitude;
@property (nonatomic, strong)  NSString *head_Title;

@property (nonatomic, strong)  NSString *titleOfannonation;
@property (nonatomic, strong)  NSString *subtitleOfannonation;
@property(strong, nonatomic) GMSPolyline *polyline;
@property(strong, nonatomic) GMSPath *polyLinePath;
@property (nonatomic, strong)  NSString *encodedP;
@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer;

-(void)drawRoute:(NSMutableArray *)userRLArray;

@end
