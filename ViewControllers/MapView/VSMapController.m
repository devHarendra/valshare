//
//  VSMapController.m
//  ValShare
//
//  Created by Sharda Prasad on 3/31/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSMapController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIViewController+MJPopupViewController.h"
#import "AlertClass.h"
#import "AppDelegate.h"
#import "WeatherNewUI.h"

#define MINIMUM_VISIBLE_LATITUDE 0.01
#define MAP_PADDING 1.1
#define locM [LocationManager sharedInstance]


@interface VSMapController ()<TicketAddDelegate,TicketUpdateDelegate,MySelectedRouteDelegate,VSUserReportsViewControllerDelegate,RealTimeNotiDelegate>{
    
    CLLocation *mycurrentLocation;
    CLLocationCoordinate2D longPressedCoord;
    
    MKMapRect lastGoodMapRect;
    NSString *currentLocationString;
    BOOL isFetchFirstTime;
    BOOL manuallyChangingMapRect;
    BOOL firstLocationUpdate_;
    VSSearchView *searchRoute;
    TWSReleaseNotesView *releaseNotesView;
    int firstRouteClick;
    int secondRouteClick;
    int thirdRouteClick;
    int countPin;
    NSMutableArray *latarray;
    NSMutableArray *longarray;
    
    VSCustomAddTicketView *customAddTiket;
    VSCommentPopUpView *commentPopVC;
    BOOL isCustomAddTicketHidden;
    AlertClass *alert ;
    VSViewReportNew *ViewReportTkt ;


}

@property (nonatomic, strong) CLLocationManager *manager;
@end

@implementation VSMapController
@synthesize mapView;
@synthesize userID;
@synthesize weatherData = _weatherData;


//@synthesize routeLineFirst;
//@synthesize routeLineSecond;
//@synthesize routeLineThird;
//
//@synthesize routeLine;
@synthesize locationarray = _locationarray;

@synthesize addTicketLatitude;
@synthesize addTicketLongitude;
@synthesize addReportDetailLatitude;
@synthesize addReportDetailLongitude;
@synthesize head_Title;
@synthesize titleOfannonation;
@synthesize subtitleOfannonation;
@synthesize polyline;
@synthesize encodedP;

-(NSMutableArray *)weatherData {
    
    if (!_weatherData) {
        _weatherData = [[NSMutableArray alloc] init];
    }
    return _weatherData;
}

-(VSViewReportNew*)ViewReportTkt
{
    
    
    return ViewReportTkt;
}

-(NSMutableArray *)latarray {
    
    if (!_weatherData) {
        _weatherData = [[NSMutableArray alloc] init];
    }
    return _weatherData;
}

-(NSMutableArray *)locationarray {
    
    if (!_locationarray) {
        _locationarray = [[NSMutableArray alloc] init];
    }
    return _locationarray;
}

-(void)GetticketsArrounds:(NSString *)u_id Latitute:(NSString* )lat Longnitute:(NSString* )lon {
    
    NSDictionary *dictData = [[NSMutableDictionary alloc] init];
    [dictData setValue:lat forKey:@"latitude"];
    [dictData setValue:lon forKey:@"longitude"];
    [dictData setValue:u_id forKey:@"userId"];
       
    [[VSParserClass sharedParser] valShareGetticketsAround:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSLog(@"CommercialCommercial :- %@", resultObject);
            
            if ([resultObject isKindOfClass:[NSDictionary class]]) {
                
                [locM.modelArray removeAllObjects];
                [locM.routeListArray removeAllObjects];
                [locM.userReportListArray removeAllObjects];
                [_IgnouredTkts removeAllObjects];
                isSearched = false;
                if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                    
                    if ([resultObject valueForKey:@"Data"]) {
                        
                        NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                        VSDirectionModal *model = [[VSDirectionModal alloc]init];
                        [model setRouteId:[dictdata valueForKey:@"routeId"]];
                        [model setRouteIndexId:[dictdata valueForKey:@"routeIndexId"]];
                        [model setSource:[dictdata valueForKey:@"source"]];
                        
                        [model setDestination:[dictdata valueForKey:@"destination"]];
                        [model setRouteEncodedPath:[dictdata valueForKey:@"routeEncodedPath"]];
                        [model setDrivingMode:[dictdata valueForKey:@"drivingMode"]];
                        [model setLastModifiedTime:[dictdata valueForKey:@"lastModifiedTime"]];
                        
                        NSMutableArray *localRouteArray = [dictdata valueForKey:@"routeModelList"];
                        
                        [localRouteArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                            
                            NSMutableDictionary *localRouteDict = (NSMutableDictionary *)[localRouteArray objectAtIndex:idx];
                            
                            VSDirectionModal *model11 = [[VSDirectionModal alloc]init];
                            [model11 setLocalRouteDestination:[localRouteDict valueForKey:@"destination"]];
                            [model11 setLocalRouteDistance:[localRouteDict valueForKey:@"distance"]];
                            [model11 setLocalRouteDrivingMode:[localRouteDict valueForKey:@"drivingMode"]];
                            [model11 setLocalRouteDuration:[localRouteDict valueForKey:@"duration"]];
                            [model11 setLocalRouteRouteEncodedPath:[localRouteDict valueForKey:@"routeEncodedPath"]];
                            [model11 setLocalRouteRouteId:[localRouteDict valueForKey:@"routeId"]];
                            [model11 setLocalRouteSource:[localRouteDict valueForKey:@"source"]];
                            [model11 setLocalRouteSummary:[localRouteDict valueForKey:@"summary"]];
                            [model11 setLocalRouteUserId:[localRouteDict valueForKey:@"userId"]];
                            [model11 setLocalRouteSource:[localRouteDict valueForKey:@"source"]];
                            [model11 setLocalRouteRouteIndexId:[localRouteDict valueForKey:@"routeIndexId"]];
                            [locM.routeListArray addObject:model11];
                        }];
                        
                        if ([[dictdata valueForKey:@"userReportModelList"] isKindOfClass:[NSArray class]]) {
                            
                            NSMutableArray *reportModelArray = [dictdata valueForKey:@"userReportModelList"];
                            
                            
                            
                            NSLog(@"reportModelArrayreportModelArray:-%@",reportModelArray);
                            [reportModelArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                                
                                NSMutableDictionary *localRouteDict = (NSMutableDictionary *)[reportModelArray objectAtIndex:idx];
                                
                                NSMutableArray *ImgVideoArr = [localRouteDict valueForKey:@"reportVideoModelList"];
                                
                                VSDirectionModal *model22 = [[VSDirectionModal alloc]init];
                                [model22 setReportVideoModelList:ImgVideoArr];
                                
                                NSLog(@"ImgVideoArr ImgVideoArr:-%@",ImgVideoArr);
                                NSLog(@"reportVideoModelList reportVideoModelList:-%@",model22.reportVideoModelList);
                                
                                [model22 setReportModalAddedBy:[localRouteDict valueForKey:@"addedBy"]];
                                [model22 setReportModalAddedBy:[localRouteDict valueForKey:@"addedBy"]];
                                
                                [model22 setReportModalDescription:[localRouteDict valueForKey:@"description"]];
                                [model22 setReportModalHostId:[localRouteDict valueForKey:@"hostId"]];
                                [model22 setReportModalIsHost:[localRouteDict valueForKey:@"isHost"]];
                                [model22 setReportModalAddedByName:[localRouteDict valueForKey:@"addedByName"]];
                                [model22 setReportModalLastModifiedTime:[localRouteDict valueForKey:@"lastModifiedTime"]];
                                [model22 setReportModalLatitude:[localRouteDict valueForKey:@"latitude"]];
                                [model22 setReportModalLocation:[localRouteDict valueForKey:@"location"]];
                                [model22 setReportModalLongitude:[localRouteDict valueForKey:@"longitude"]];
                                [model22 setReportModalPrice:[localRouteDict valueForKey:@"price"]];
                                [model22 setReportModalRadiusofImpact:[localRouteDict valueForKey:@"radiusofImpact"]];
                                [model22 setReportModalReportId:[localRouteDict valueForKey:@"reportId"]];
                                [model22 setReportModalReportStatus:[localRouteDict valueForKey:@"reportStatus"]];
                                [model22 setReportModalReportStatusId:[localRouteDict valueForKey:@"reportStatusId"]];
                                [model22 setReportModalReportType:[localRouteDict valueForKey:@"reportType"]];
                                [model22 setReportModalReportTypeId:[localRouteDict valueForKey:@"reportTypeId"]];
                                [model22 setReportModalStatusType:[localRouteDict valueForKey:@"statusType"]];
                                [model22 setReportModalThumbsDown:[localRouteDict valueForKey:@"thumbsDown"]];
                                [model22 setReportModalThumbsUp:[localRouteDict valueForKey:@"thumbsUp"]];
                                [model22 setReportModalUserId:[localRouteDict valueForKey:@"userId"]];
                                [model22 setReportModalUserId:[localRouteDict valueForKey:@"userId"]];
                                //calculating distance from current location for soring tickets
                                CLLocation* LocB = [[CLLocation alloc] initWithLatitude:model22.reportModalLatitude.doubleValue longitude:model22.reportModalLongitude.doubleValue];
                                CLLocationDistance distance = [locCordinates distanceFromLocation:LocB];
                                [model22 setReportModelDistance:distance];
                                [locM.userReportListArray addObject:model22];
                            }];
                            
                            [locM.modelArray addObject:model];
                        }
                    }
                } else {
                    [[[UIAlertView alloc] initWithTitle:nil message:@"Please contact server admin." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
            }else {
                
                NSLog(@"---------else----------::: %@", resultObject);
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            NSLog(@"-------------------::: %@", error);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            /*
             ****************************************************************************************
             * Multiple anotation in map view.
             *****************************************************************************************
             */
            
            [self SortTicketsByDistance];
            for (VSDirectionModal *model22 in locM.userReportListArray)
            {
                float finalLat= model22.reportModalLatitude.doubleValue;
                float finalLon=model22.reportModalLongitude.doubleValue;
                NSLog(@"lat %f  long %f", finalLat,finalLon );
                
                CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
                
                GMSMarker *london = [GMSMarker markerWithPosition:coord];
                
                london.title =model22.reportModalReportType;
                london.snippet = model22.reportModalDescription;
                if ([model22.reportModalReportType isEqualToString:@"Accident"]) {
                    london.icon = [UIImage imageNamed:@"third"];
                }
                else if ([model22.reportModalReportType isEqualToString:@"Under Construction"])
                {
                    london.icon = [UIImage imageNamed:@"second.png"];
                }
                else
                {
                    london.icon = [UIImage imageNamed:@"fourth.png"];
                }
                london.map = self.mapView;
                self.view = london.map;
            }
            
        });
    }];
}




-(void)getDataFromServer:(NSString *)source Destination:(NSString *)destination {
    
    NSLog(@"self.userIDself.userIDself.userID:-%@",self.userID);
    NSMutableDictionary *soapDict = [[NSMutableDictionary alloc] init];
    [soapDict setValue:source forKey:@"source"];
    [soapDict setValue:destination forKey:@"destination"];
    [soapDict setValue:@"DRIVING" forKey:@"drivingMode"];
    [soapDict setValue:@"0" forKey:@"routeIndexId"];
    [soapDict setValue:@"false" forKey:@"postData"];
    [soapDict setValue:@"0" forKey:@"routeId"];
    [soapDict setValue:self.userID forKey:@"userId"];
    
    [[VSParserClass sharedParser] valShareRouteservice:soapDict WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSLog(@"CommercialCommercial :- %@", resultObject);
            
            if ([resultObject isKindOfClass:[NSDictionary class]]) {
                
                [locM.modelArray removeAllObjects];
                [locM.routeListArray removeAllObjects];
                [locM.userReportListArray removeAllObjects];
                [_IgnouredTkts removeAllObjects];
                if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                    
                    if ([resultObject valueForKey:@"Data"]) {
                        
                        NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                        VSDirectionModal *model = [[VSDirectionModal alloc]init];
                        [model setRouteId:[dictdata valueForKey:@"routeId"]];
                        [model setRouteIndexId:[dictdata valueForKey:@"routeIndexId"]];
                        [model setSource:[dictdata valueForKey:@"source"]];
                     
                        [model setDestination:[dictdata valueForKey:@"destination"]];
                        [model setRouteEncodedPath:[dictdata valueForKey:@"routeEncodedPath"]];
                        [model setDrivingMode:[dictdata valueForKey:@"drivingMode"]];
                        [model setLastModifiedTime:[dictdata valueForKey:@"lastModifiedTime"]];
                        
                        NSMutableArray *localRouteArray = [dictdata valueForKey:@"routeModelList"];
                        
                        [localRouteArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                            
                            NSMutableDictionary *localRouteDict = (NSMutableDictionary *)[localRouteArray objectAtIndex:idx];
                            
                            VSDirectionModal *model11 = [[VSDirectionModal alloc]init];
                            [model11 setLocalRouteDestination:[localRouteDict valueForKey:@"destination"]];
                            [model11 setLocalRouteDistance:[localRouteDict valueForKey:@"distance"]];
                            [model11 setLocalRouteDrivingMode:[localRouteDict valueForKey:@"drivingMode"]];
                            [model11 setLocalRouteDuration:[localRouteDict valueForKey:@"duration"]];
                            [model11 setLocalRouteRouteEncodedPath:[localRouteDict valueForKey:@"routeEncodedPath"]];
                            [model11 setLocalRouteRouteId:[localRouteDict valueForKey:@"routeId"]];
                            [model11 setLocalRouteSource:[localRouteDict valueForKey:@"source"]];
                            [model11 setLocalRouteSummary:[localRouteDict valueForKey:@"summary"]];
                            [model11 setLocalRouteUserId:[localRouteDict valueForKey:@"userId"]];
                            [model11 setLocalRouteSource:[localRouteDict valueForKey:@"source"]];
                            [model11 setLocalRouteRouteIndexId:[localRouteDict valueForKey:@"routeIndexId"]];
                            [locM.routeListArray addObject:model11];
                        }];
                        
                        if ([[dictdata valueForKey:@"userReportModelList"] isKindOfClass:[NSArray class]]) {
                            
                            NSMutableArray *reportModelArray = [dictdata valueForKey:@"userReportModelList"];
                            

                            
                            NSLog(@"reportModelArrayreportModelArray:-%@",reportModelArray);
                            [reportModelArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                                
                                NSMutableDictionary *localRouteDict = (NSMutableDictionary *)[reportModelArray objectAtIndex:idx];
                                
                          NSMutableArray *ImgVideoArr = [localRouteDict valueForKey:@"reportVideoModelList"];
                                
                                VSDirectionModal *model22 = [[VSDirectionModal alloc]init];
                                [model22 setReportVideoModelList:ImgVideoArr];
                                
                                NSLog(@"ImgVideoArr ImgVideoArr:-%@",ImgVideoArr);
                                NSLog(@"reportVideoModelList reportVideoModelList:-%@",model22.reportVideoModelList);
                                
                                [model22 setReportModalAddedBy:[localRouteDict valueForKey:@"addedBy"]];
                                [model22 setReportModalAddedBy:[localRouteDict valueForKey:@"addedBy"]];

                                [model22 setReportModalDescription:[localRouteDict valueForKey:@"description"]];
                                [model22 setReportModalHostId:[localRouteDict valueForKey:@"hostId"]];
                                [model22 setReportModalIsHost:[localRouteDict valueForKey:@"isHost"]];
                                [model22 setReportModalAddedByName:[localRouteDict valueForKey:@"addedByName"]];
                                [model22 setReportModalLastModifiedTime:[localRouteDict valueForKey:@"lastModifiedTime"]];
                                [model22 setReportModalLatitude:[localRouteDict valueForKey:@"latitude"]];
                                [model22 setReportModalLocation:[localRouteDict valueForKey:@"location"]];
                                [model22 setReportModalLongitude:[localRouteDict valueForKey:@"longitude"]];
                                [model22 setReportModalPrice:[localRouteDict valueForKey:@"price"]];
                                [model22 setReportModalRadiusofImpact:[localRouteDict valueForKey:@"radiusofImpact"]];
                                [model22 setReportModalReportId:[localRouteDict valueForKey:@"reportId"]];
                                [model22 setReportModalReportStatus:[localRouteDict valueForKey:@"reportStatus"]];
                                [model22 setReportModalReportStatusId:[localRouteDict valueForKey:@"reportStatusId"]];
                                [model22 setReportModalReportType:[localRouteDict valueForKey:@"reportType"]];
                                [model22 setReportModalReportTypeId:[localRouteDict valueForKey:@"reportTypeId"]];
                                [model22 setReportModalStatusType:[localRouteDict valueForKey:@"statusType"]];
                                [model22 setReportModalThumbsDown:[localRouteDict valueForKey:@"thumbsDown"]];
                                [model22 setReportModalThumbsUp:[localRouteDict valueForKey:@"thumbsUp"]];
                                [model22 setReportModalUserId:[localRouteDict valueForKey:@"userId"]];
                                [model22 setReportModalUserId:[localRouteDict valueForKey:@"userId"]];
                      //calculating distance from current location for soring tickets
                                 CLLocation* LocB = [[CLLocation alloc] initWithLatitude:model22.reportModalLatitude.doubleValue longitude:model22.reportModalLongitude.doubleValue];
                                CLLocationDistance distance = [locCordinates distanceFromLocation:LocB];
                                [model22 setReportModelDistance:distance];
                                [locM.userReportListArray addObject:model22];
                            }];
      
                            [locM.modelArray addObject:model];
                        }
                    }
                } else {
                    [[[UIAlertView alloc] initWithTitle:nil message:@"Please contact server admin." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
            }else {
                
                NSLog(@"---------else----------::: %@", resultObject);
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            NSLog(@"-------------------::: %@", error);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (locM.routeListArray) {
                [self.mapView reloadInputViews];
                
                for (VSDirectionModal *path in locM.routeListArray) {
                    NSLog(@"pathpathpath :- %@", path.localRouteRouteId);
                    [self polylineWithEncodedString:path.localRouteRouteEncodedPath];
                }
            } else {
                [[[UIAlertView alloc] initWithTitle:nil message:@"No data found" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
            }
            
            /*
             ****************************************************************************************
             * Multiple anotation in map view.
             *****************************************************************************************
             */

                [self SortTicketsByDistance];
                [self drawRoute:locM.userReportListArray];
                 isSearched = true;

        });
    }];
}

-(void)SortTicketsByDistance
{
    NSArray *someArray = [[NSArray alloc]init];
    someArray = [locM.userReportListArray sortedArrayUsingComparator:^(VSDirectionModal *obj1, VSDirectionModal *obj2)
    {
        NSNumber *rank1  = [NSNumber numberWithFloat: obj1.ReportModelDistance];
        NSNumber *rank2 = [NSNumber numberWithFloat: obj2.ReportModelDistance];
        return (NSComparisonResult)[rank1 compare:rank2];
    }];
    
    locM.userReportListArray = [[NSMutableArray alloc]initWithArray:someArray];
    someArray = nil;
    
}
    

-(void)drawRoute:(NSMutableArray *)userRLArray{
    //[self.mapView clearsContextBeforeDrawing];
    [self.mapView clear];
    
    RoutePolylines = [[NSMutableArray alloc]init];
    
    for (VSDirectionModal *model22 in locM.routeListArray) {
        _polyLinePath = [GMSPath pathFromEncodedPath:model22.localRouteRouteEncodedPath];
        
        self.polyline = [GMSPolyline polylineWithPath:_polyLinePath];
        GMSStrokeStyle *greenToRed = [GMSStrokeStyle gradientFromColor:[UIColor blueColor] toColor:[UIColor blueColor]];
        polyline.spans = @[[GMSStyleSpan spanWithStyle:greenToRed]];
        self.polyline.strokeWidth = 5.f;
        self.polyline.tappable = true;
        
        self.polyline.map = mapView;
            self.view = mapView;
        [RoutePolylines addObject:self.polyline];
        [self.mapView animateToZoom:10.0];
    }

    for (VSDirectionModal *model22 in locM.userReportListArray)
    {
        float finalLat= model22.reportModalLatitude.doubleValue;
        float finalLon=model22.reportModalLongitude.doubleValue;
        NSLog(@"lat %f  long %f", finalLat,finalLon );
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);

        GMSMarker *london = [GMSMarker markerWithPosition:coord];
        
        london.title =model22.reportModalReportType;
        london.snippet = model22.reportModalDescription;
        if ([model22.reportModalReportType isEqualToString:@"Accident"]) {
            london.icon = [UIImage imageNamed:@"third"];
        }
        else if ([model22.reportModalReportType isEqualToString:@"Under Construction"])
        {
            london.icon = [UIImage imageNamed:@"second.png"];
        }
        else
        {
            london.icon = [UIImage imageNamed:@"fourth.png"];
        }
        london.map = self.mapView;
        self.view = london.map;
     }
    
#pragma Display Source and destination marker - 
    [self createSourceDestinationMarkers];

}

-(void)createSourceDestinationMarkers
{
    NSLog(@"local array count - %d",[localRoots count]);
    
    LatAndLongModelClass *latandlongmodel = [[LatAndLongModelClass alloc]init];
    latandlongmodel = [localRoots lastObject];
    
    float finalLat1=latandlongmodel.mylatitude;
    float finalLon1=latandlongmodel.mylongitude;
    CLLocationCoordinate2D coord1 = CLLocationCoordinate2DMake(finalLat1, finalLon1);
    
    GMSMarker* london = [GMSMarker markerWithPosition:coord1];
    
    london.title =@"Destination";
    london.icon = [GMSMarker markerImageWithColor:[UIColor redColor]];
    
    
    london.map = self.mapView;
    self.view = london.map;
    
    latandlongmodel = [[LatAndLongModelClass alloc]init];
    latandlongmodel = [localRoots firstObject];
    
    float finalLat2 = latandlongmodel.mylatitude;
    float finalLon2 = latandlongmodel.mylongitude;
    CLLocationCoordinate2D coord2 = CLLocationCoordinate2DMake(finalLat2, finalLon2);
    GMSMarker* london2 = [GMSMarker markerWithPosition:coord2];
    london2.title =@"Source";
    london2.icon =  [GMSMarker markerImageWithColor:[UIColor greenColor]];
    london2.map = self.mapView;
    self.view = london2.map;
    
    NSMutableArray *markerArr = [[NSMutableArray alloc]initWithObjects:london,london2, nil];
    [self focusMapToShowAllMarkers:markerArr zoom:30.0f]; // to set destination and current location markers on visible bounds....


}




- (void)focusMapToShowAllMarkers:(NSMutableArray*)markers zoom:(float)ZoomLabel
{
    CLLocationCoordinate2D myLocation = ((GMSMarker *)markers.firstObject).position;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
    
    for (GMSMarker *marker in markers)
    {
        bounds = [bounds includingCoordinate:marker.position];

    }
    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:ZoomLabel]];
}




-(void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay
{
//    [self.mapView clear]; // clear map befor drawing path again,  avoiding duplicasy. Harry
    for (GMSPolyline * line in RoutePolylines)
    {
        line.map = nil;
    }
    
    RoutePolylines = [[NSMutableArray alloc]init];
    for (VSDirectionModal *model22 in locM.routeListArray) // for all route blue, harry
    {
        _polyLinePath = [GMSPath pathFromEncodedPath:model22.localRouteRouteEncodedPath];
        
        self.polyline = [GMSPolyline polylineWithPath:_polyLinePath];
        GMSStrokeStyle *greenToRed = [GMSStrokeStyle gradientFromColor:[UIColor blueColor] toColor:[UIColor blueColor]];
        polyline.spans = @[[GMSStyleSpan spanWithStyle:greenToRed]];
        self.polyline.strokeWidth = 5.f;
        self.polyline.tappable = true;
        
        self.polyline.map =  self.mapView;
        self.view = self.mapView;
        [RoutePolylines addObject:self.polyline];
    }
    
    self.polyline =(GMSPolyline*) overlay; // than after loop rout which is selected to be red, harry
    GMSStrokeStyle *red = [GMSStrokeStyle gradientFromColor:[UIColor redColor] toColor:[UIColor redColor]];
    polyline.spans = @[[GMSStyleSpan spanWithStyle:red]];
    self.polyline.strokeWidth = 5.f;
    self.polyline.map =  self.mapView;
    self.view = self.mapView;
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Route selection" message:@"Do you want to remove other routes" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert1.tag = 1201;
    [alert1 show];
    
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    NSLog(@"Happy click");
    CLLocationCoordinate2D markerCord = CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude);
    freelyMarker = marker;
    
    VSDirectionModal *model22;
    for (model22 in locM.userReportListArray)
    {
        float finalLat= model22.reportModalLatitude.doubleValue;
        float finalLon=model22.reportModalLongitude.doubleValue;
        
        int uID = model22.reportModalUserId.intValue;
        int loggedInID = [VSSingletonClass sharedMySingleton].loginUserId.intValue;
        
        NSLog(@"lat %f  long %f", finalLat,finalLon );
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
        if (markerCord.latitude== coord.latitude && markerCord.longitude== coord.longitude )
        {
            if (uID == loggedInID)
            {
                VSCustomAddReportView * addReport = [[VSCustomAddReportView alloc]init]; //for update ticket , harry
                [addReport setDelegate:self];
                [addReport setLatitudeForAddReport:[NSString stringWithFormat:@"%f",markerCord.latitude]];
                [addReport setLongitudeForAddReport:[NSString stringWithFormat:@"%f",markerCord.longitude]];
                [addReport setReportTypeIdForwarded:model22.reportModalReportId];
                [addReport setDirModel:model22];
//                [self presentPopupViewController:addReport animationType:MJPopupViewAnimationSlideLeftLeft];
                [self presentViewController:addReport animated:NO completion:nil];
            }
            else
            {
                [self showPinPupUp:[NSString stringWithFormat:@"%@",model22.reportModalReportId] subtitle:[NSString stringWithFormat:@"%@",model22.reportModalDescription] title:[NSString stringWithFormat:@"%@",model22.reportModalReportType]]; // old screen code
            }
 
       }
    }
  }

#pragma mark - MapKit

- (MKMapRect)makeMapRectWithAnnotations:(NSArray *)annotations {
    
    MKMapRect flyTo = MKMapRectNull;
    for (id <MKAnnotation> annotation in annotations) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        } else {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
    return flyTo;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.mapView reloadInputViews];
    
    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    [app setDelegate:self];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];

}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    isFetchFirstTime = YES;
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController setNavigationBarHidden:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //    [self addLocationManager];
   
    [self addComponentOnView];
    
    NSLog(@"self.userIDself.userIDself.userID :- %@", self.userID);
    [[VSSingletonClass sharedMySingleton] setLoginUserId:self.userID];
   
    countPin = 0;
    latarray = [[NSMutableArray alloc] init];
    longarray = [[NSMutableArray alloc] init];

    [[VSSingletonClass sharedMySingleton] setCustomDelegate:(id)self];
    if (!_IgnouredTkts) _IgnouredTkts = [[NSMutableArray alloc]init];
  [self setupGestureRecognizer];
   
    [self GetLocationUpdates];// for immidiate updates for search field
   
}

-(void)GetLocationUpdates
{
    if (!manager)
    {
        manager = [LocationManager sharedInstance];
        [manager setDelegate:self];
    }
    [manager startUpdatingLocation];
}

-(void)addComponentOnView
{
    [self addMapOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x , [UIScreen mainScreen].bounds.origin.y + 90, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    NSMutableArray *titles = [NSMutableArray arrayWithObjects:[@"Search" uppercaseString], [@"Profile" uppercaseString], [@"Weather" uppercaseString],[@"Compare" uppercaseString],[@"Compare" uppercaseString],[@"Compare" uppercaseString], nil];
    
    [self addCollectionView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x, 18.0, [UIScreen mainScreen].bounds.size.width, 85.0):CGRectMake([UIScreen mainScreen].bounds.origin.x, 15.0, [UIScreen mainScreen].bounds.size.width, 85.0) DataText:titles];
    
    
}


-(void)addCollectionView:(CGRect)frame DataText:(NSMutableArray *)array {
    
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    VSCollectionViewTab *collectionView=[[VSCollectionViewTab alloc] initWithFrame:frame collectionViewLayout:flowLayout];
    [collectionView initilizeGridView];
    [collectionView setTag:7865768];
    [collectionView setDataText:array];
    collectionView.allowsSelection = YES;
    [collectionView setScrollEnabled:NO];
    [collectionView setPagingEnabled:YES];
    [collectionView setAutoresizesSubviews:YES];
    [collectionView setUserInteractionEnabled:YES];
    [collectionView setTabCollectionDelegate:(id)self];
    [collectionView setShowsHorizontalScrollIndicator:YES];
    [collectionView setBackgroundColor:[UIColor clearColor]];
    [collectionView setContentOffset:CGPointZero animated:YES];
    [self.view addSubview:collectionView];
}


-(void)showCatListToShowDropDown:(NSIndexPath *)index Text:(NSString *)text {
    
    [searchRoute removeFromSuperview];
    [self.menuPopover dismissMenuPopover];
    CGRect frame;
    NSArray *menuArray;
    if (index.row == 0)
    {
        [self showSearchRoute]; // added by harry

        /* //  harry not needed  this functionality
        frame = (IS_IPHONE_5) ? CGRectMake(3, 90.2, 120, 88) : CGRectMake(3, 90.3, 120, 90);
        menuArray = [[NSArray alloc] initWithObjects:@"Search", @"Save Route", nil];
        self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:frame menuItems:menuArray];
        [self.menuPopover setBackgroundColor:[UIColor grayColor]];
        self.menuPopover.tag = 99556;
        self.menuPopover.menuPopoverDelegate = self;
        [self.menuPopover showInView:self.view];
      */
    } else if (index.row == 1) {
        
        frame = (IS_IPHONE_5) ? CGRectMake(83, 90.2, 190, 395) : CGRectMake(96, 90.4, 190, 400);
        menuArray = [[NSArray alloc] initWithObjects:@"My Profile", @"My Routes",@"My Vehicle",@"My Reports",@"Report Display Setting",@"CC Details",@"Purchase Details",@"Reset Password",@"Feedback",@"Notification Setting",@"Sign Out", nil];
        self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:frame menuItems:menuArray];
        [self.menuPopover setBackgroundColor:[UIColor grayColor]];
        self.menuPopover.tag = 99557;
        self.menuPopover.menuPopoverDelegate = self;
        [self.menuPopover showInView:self.view];
        
    }
    else if (index.row == 2 )
    {
        /*
        releaseNotesView = [TWSReleaseNotesView viewWithReleaseNotesTitle:[NSString stringWithFormat:@"Today's Weather Information"] text:@"• Create custom stations of your favorite podcasts that update automatically with new episodes\n• Choose whether your stations begin playing with the newest or oldest unplayed episode\n• Your stations are stored in iCloud and kept up-to-date on all of your devices\n• Create an On-The-Go playlist with your own list of episodes\n• Playlists synced from iTunes now appear in the Podcasts app\n• The Now Playing view has been redesigned with easier to use playback controls\n• Addressed an issue with resuming playback when returning to the app\n• Additional performance and stability improvements" closeButtonTitle:@"Close" UserID:@"11"];
        
        // Show the release notes view
        [releaseNotesView showInView:self.view];
         
         */
        
        
        WeatherNewUI *Weather =   [[[NSBundle mainBundle] loadNibNamed:@"WeatherXib" owner:self options:nil]lastObject];
        [Weather drawRect:self.view.frame];
        [self.view addSubview:Weather];
        
    } else {
        
        if (locM.routeListArray.count > 0) {
            [self showSaveRoute]; // added by harry
            /*   // by  harry this is not required
            VSCompareRouteController *compareRoute = [[VSCompareRouteController alloc] init];
            [compareRoute setDateArray:self.routeListArray];
            [self.navigationController pushViewController:compareRoute animated:YES];
             */
        }else {
            [self showSaveRoute]; // added by harry
        }
    }
}


#pragma mark -
#pragma mark MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex {
    
    [self.menuPopover dismissMenuPopover];
    
    if (menuPopover.tag == 99556) {
        
        switch (selectedIndex) {
            case 0:
                [self showSearchRoute];
                break;
            case 1:
                [self showSaveRoute];
                break;
            default:
                break;
        }
    } else {
        
        switch (selectedIndex) {
            case 0:
                [self showMyProfile];
                break;
            case 1:
                [self showMyRoutes];
                break;
            case 2:
                [self showMyVehicle];
                break;
            case 3:
                [self showMyReports];
                break;
            case 4:
                [self showMyReportsDisplaySettings];
                break;
            case 5:
                [self showCCDetails];
                break;
            case 6:
                [self showPurchaseDetails];
                break;
            case 7:
                [self showResetPassword];
                break;
            case 8:
                [self showFeedBack];
                break;
            case 9:
                [self NotificationSetting];
                break;
            case 10:
                [self showSignOut];
                break;

            default:
                break;
        }
    }
}

-(void)showSearchRoute {
    
    searchRoute = [[VSSearchView alloc] init];
    [searchRoute setTag:8878];
    [searchRoute setSearchViewDelegate:(id)self];
    [searchRoute setCurrentLocation:currentLocationString];
    [self.view addSubview:searchRoute];
    [searchRoute setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    [searchRoute setFrame:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y + 90, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height):CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    searchRoute = nil;
}

-(void)searchFieldText:(NSString *)sourceText Destination:(NSString *)destination {
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getDataFromServer:(sourceText) ? sourceText : @"" Destination:(destination) ? destination : @""];
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

-(void)showSaveRoute {
    
    /* // Removed by harry 
    if (self.routeListArray.count > 0) {
        
        VSSaveRouteViewController *saveRoute = [[VSSaveRouteViewController alloc] init];
        [saveRoute setDateArray:self.routeListArray];
        [self.navigationController pushViewController:saveRoute animated:YES];
    } else {
        
        //[[[UIAlertView alloc] initWithTitle:nil message:@"Please check your search route." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        searchRoute = [[VSSearchView alloc] init];
        [searchRoute setTag:8878];
        [searchRoute setSearchViewDelegate:(id)self];
        [self.view addSubview:searchRoute];
        [searchRoute setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
        [searchRoute setFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        searchRoute = nil;
    }
     */
    VSSaveRouteViewController *saveRoute = [[VSSaveRouteViewController alloc] init]; // added by harry
    [saveRoute setDateArray:locM.routeListArray];
    [self.navigationController pushViewController:saveRoute animated:YES];
   }

-(void)showMyProfile {
    
    VSUpdateProfileViewController *updateProfile = [[VSUpdateProfileViewController alloc]init];
    [updateProfile setUserID:self.userID];
    [self.navigationController pushViewController:updateProfile animated:YES];
}

-(void)showMyRoutes {
    
    MyRouteViewController *routeVC = [[MyRouteViewController alloc] init];
    [routeVC setDelegate:self];
    [self.navigationController pushViewController:routeVC animated:YES];
}

-(void)showMyVehicle {
    
    VehicleViewController *vehicleList = [[VehicleViewController alloc] init];
    [vehicleList setUserId:self.userID];
    [self.navigationController pushViewController:vehicleList animated:YES];
}

-(void)showMyReports {
    
    VSUserReportsView *myReports = [[VSUserReportsView alloc] init];
    [myReports setDelegate:self];
    [self.navigationController pushViewController:myReports animated:YES];
    
}

-(void)showMyReportsDisplaySettings {
    
    VSReportDisplayView *reportDisplay = [[VSReportDisplayView alloc]init];
    [reportDisplay setUserId:self.userID];
    [self.navigationController pushViewController:reportDisplay animated:YES];
}


-(void)showCCDetails {
    
    VSCCDetailViewController *cardDetail =[[VSCCDetailViewController alloc]init];
    [cardDetail setUserID:self.userID];
    [self.navigationController pushViewController:cardDetail animated:YES];
}

-(void)showPurchaseDetails {
    
    VSPurchesDetailView *detailView = [[VSPurchesDetailView alloc]init];
    [detailView setUserId:self.userID];
    [self.navigationController pushViewController:detailView animated:YES];
}

-(void)showResetPassword {
    
    VSResetPasswordView *resetpassword = [[VSResetPasswordView alloc]init];
    [self.navigationController pushViewController:resetpassword animated:YES];
}

-(void)showFeedBack {
    
    VSFeedBackViewController *feedBack = [[VSFeedBackViewController alloc]init];
    [self.navigationController pushViewController:feedBack animated:YES];
}

-(void)NotificationSetting
{
    NotiSettingsVC *NVC = [[NotiSettingsVC alloc]init];
    [self.navigationController pushViewController:NVC animated:YES];
}


-(void)showSignOut {
    
   UIAlertView *alert2=  [[UIAlertView alloc] initWithTitle:nil message:@"Do You Really Want To Log Out?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    alert2.tag = 1200;
    [alert2 show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag ==1200)// logout
    {
        if (buttonIndex == 0) {
            
            [[VSSingletonClass sharedMySingleton] setLoginUserId:nil];
            [self.navigationController setNavigationBarHidden:YES];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            
        }
    }
    
    else if (alertView.tag ==1201) //route selection
    {
        if (buttonIndex ==1) {
            for (GMSPolyline * line in RoutePolylines)
            {
                line.map = nil;
            }
           
        } else {
            
        }

    
    }
    
}




/*
 ***********************************************************************************************
 * Implementd MKMap view methods and delegates
 ***********************************************************************************************
 */

- (void)polylineWithEncodedString:(NSString *)encodedString {
    
    const char *bytes = [encodedString UTF8String];
    NSUInteger length = [encodedString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    NSUInteger idx = 0;
    
    NSUInteger count = length / 4;
    CLLocationCoordinate2D *coords = calloc(count, sizeof(CLLocationCoordinate2D));
    NSUInteger coordIdx = 0;
    
    float latitude = 0;
    float longitude = 0;
    
    localRoots = [[NSMutableArray alloc] init];
    [longarray removeAllObjects];
    [latarray removeAllObjects];

    
    
    while (idx < length)
    {
        char byte = 0;
        int res = 0;
        char shift = 0;
        
        do {
            byte = bytes[idx++] - 63;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
        latitude += deltaLat;
        
        shift = 0;
        res = 0;
        
        do {
            
            byte = bytes[idx++] - 0x3F;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
        longitude += deltaLon;
        
        float finalLat = latitude * 1E-5;
        float finalLon = longitude * 1E-5;
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
        NSString *lat = [NSString stringWithFormat:@"%f", finalLat];
        NSString *lon = [NSString stringWithFormat:@"%f", finalLon];

        if(longarray ==nil || [longarray count]<=0)
        {

            [longarray insertObject:lon atIndex:0];
            [latarray insertObject:lat atIndex:0];

        }
        else
        {
            [latarray insertObject:lat atIndex:1];
            [longarray insertObject:lon atIndex:1];

        }

        
      
        coords[coordIdx++] = coord;
        
        LatAndLongModelClass *latandlongmodel = [[LatAndLongModelClass alloc]init];
        [latandlongmodel setMylatitude:finalLat];
        [latandlongmodel setMylongitude:finalLon];
        [localRoots addObject:latandlongmodel];
        
        if (coordIdx == count) {
            NSUInteger newCount = count + 10;
            coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
            count = newCount;
        }
    }
}


-(void)addMapOnView:(CGRect)frame {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:19.017615
                                                            longitude:72.856164
                                                                 zoom:10];
    self.mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.myLocationButton = YES;
    self.view = self.mapView;//dheeraj
    self.mapView.delegate=self;
    for (id gestureRecognizer in self.mapView.gestureRecognizers)
    {
        NSLog(@"mapview recognizer %@",gestureRecognizer);
        if (![gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
        {
            [self.mapView removeGestureRecognizer:gestureRecognizer];
        }
    }
    
    // Listen to the myLocation property of GMSMapView.
    [self.mapView addObserver:self
                   forKeyPath:@"myLocation"
                      options:NSKeyValueObservingOptionNew
                      context:NULL];
    
    self.view = self.mapView;
    
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mapView.myLocationEnabled = YES;
    });
    firstLocationUpdate_ = NO;
    
}

- (void)dealloc {
    [self.mapView removeObserver:self
                      forKeyPath:@"myLocation"
                         context:NULL];
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!firstLocationUpdate_) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        firstLocationUpdate_ = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        self.mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                             zoom:12];
        
    }
}
-(void)DidUpdateToLocation :(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                            longitude:newLocation.coordinate.longitude
                                                                 zoom:15.0];
    [self.mapView animateToCameraPosition:camera];
    
    if (!isInitialized)
    {
        isInitialized = true;
        
        [self GetticketsArrounds:self.userID Latitute:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude] Longnitute:[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude]];
     
    }
    //...
}

-(void)DidUpdateLocations:(NSArray *)locations;
{
//    [self.manager stopUpdatingLocation]; //    harry
//    self.manager = nil;
    CLLocation * currentLocLocal = [locations lastObject];
    
    locCordinates = [[CLLocation alloc] initWithLatitude:currentLocLocal.coordinate.latitude longitude:currentLocLocal.coordinate.longitude];
    
    MKCoordinateRegion mapRegion;
    mapRegion.center = currentLocLocal.coordinate;
    mapRegion.span = MKCoordinateSpanMake(0.3, 0.3);
    //[self.mapView setShowsUserLocation:YES];
  
    
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(currentLocLocal.coordinate.latitude, currentLocLocal.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
        NSLog(@"reverse geocoding results:");
      
        NSMutableString * result = [[NSMutableString alloc] init];
            GMSAddress* addressObj = [[response results] firstObject];
            for (NSObject * obj in addressObj.lines)
            {
                [result appendString:[obj description]];
            }
        currentLocationString = result;
        NSLog(@"The concatenated string is %@", currentLocationString);
    }];
    

// isInitialized is used for detrmined user location is determined first time for drwaing tickets on map.
    if (!isInitialized)
    {
        isInitialized = true;
        
        [self GetticketsArrounds:self.userID Latitute:[NSString stringWithFormat:@"%f",currentLocLocal.coordinate.latitude] Longnitute:[NSString stringWithFormat:@"%f",currentLocLocal.coordinate.longitude]];
    }
    
    
#pragma determing nearest ticket - - -
// isSearched is used for prevent alerting nearest tickets befor user searched for path.
    if (!isSearched) return;
    VSDirectionModal * model22= [self closestLocationToLocation:[locations lastObject]];
    if (model22 == nil || model22 == (id)[NSNull null])return;
    NSLog(@"closest_reportModalDescription_is - %@",model22.reportModalDescription);

    BOOL contain = [_IgnouredTkts containsObject:model22.reportModalDescription]; // To check if this location is already notified.
    if (contain)return;
                
                [_IgnouredTkts addObject:model22.reportModalDescription];
                NSLog(@"Contain %hhd",contain);
                NSLog(@"_IgnouredTkts %@",_IgnouredTkts);
    
                if (!alert)
                {
                    [self CreateAlertView:model22];
                }
                else
                {
                 //   [self CloseAlertview:self]; // Remove existing
                    [self CreateAlertView:model22]; // then create new
                }
             }

-(void)CreateAlertView :(VSDirectionModal*)model
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * alertStr = [NSString stringWithFormat:@"%@ Ahead",model.reportModalReportType];
    NSString *placeStr = [NSString stringWithFormat:@"%@",model.reportModalDescription];
    
    NSLog(@"noti settings ForPopUp %@ -- ",[defaults objectForKey:@"ForPopUp"]);
    NSLog(@"noti settings ForVoice %@ -- ",[defaults objectForKey:@"ForVoice"]);
    
    if ([[defaults objectForKey:@"ForPopUp"]boolValue]&& isSearched) // pop up disabled/enable from notification settings
    {
        AlertClass *iClass =[[[NSBundle mainBundle] loadNibNamed:@"AlertXib" owner:self options:nil]lastObject];
    
    iClass.Alertlbl.text = alertStr;
    iClass.placeLbl.text = placeStr;
    iClass.priceLbl.text = [NSString stringWithFormat:@"Price - %.2f",model.reportModalPrice.floatValue];
    iClass.radiusLbl.text = [NSString stringWithFormat:@"Radius of Impact - %.2f",model.reportModalRadiusofImpact.floatValue];
//    [alert.OkBtn addTarget:self action:@selector(CloseAlertview:) forControlEvents:UIControlEventTouchUpInside];
    [iClass drawRect:self.view.frame];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate window] addSubview:iClass];
}
    
    
    if ([[defaults objectForKey:@"ForVoice"]boolValue] && isSearched) // voice voice disabled/enable from notification settings
    {
        if (!ToSpeechArr) ToSpeechArr = [[NSMutableArray alloc]init];
        NSString *speech = [NSString stringWithFormat:@"%@ %@",alertStr,placeStr];

        [ToSpeechArr addObject:speech];
        
        if (ToSpeechArr.count>0)[self SpechNow];
    }
}

-(IBAction)pinButtonPressedForAddTicket:(UIButton *)sender
{
    [self showCustomViewForTicket];
}

-(void)showPinPupUp:(NSString *)reportId subtitle:(NSString*)sub title:(NSString*)atitle
{
    VSDirectionModal *model = [locM.userReportListArray objectAtIndex:0];
    
    [ViewReportTkt removeFromSuperview];
    ViewReportTkt = nil;
    
    ViewReportTkt =   [[[NSBundle mainBundle] loadNibNamed:@"VSViewReportXib" owner:self options:nil]lastObject];
    
        [ViewReportTkt setReportID:reportId];
        [ViewReportTkt setAnnonationTitle:atitle];
        [ViewReportTkt setAnnonationSubTitle:sub];
        [ViewReportTkt setModel:model];
    [ViewReportTkt drawRect:self.view.frame];

  
    [self.view addSubview:ViewReportTkt];
}

//HostId:[VSSingletonClass sharedMySingleton].loginUserId
-(void)showCustomViewForTicket {
   /*
    if ([[VSSingletonClass sharedMySingleton].loginUserId isEqualToString:@""]) {
        VSCustomAddReportView * addReport = [[VSCustomAddReportView alloc]init]; //for update ticket , harry
        [addReport setLatitudeForAddReport:self.addReportDetailLatitude];
        [addReport setLongitudeForAddReport:self.addReportDetailLongitude];
        [self presentPopupViewController:addReport animationType:MJPopupViewAnimationSlideLeftLeft];
    }
    else
    {
    */
        customAddTiket = [[VSCustomAddTicketView alloc]init];
        [customAddTiket setLatitude:self.addTicketLatitude];
        [customAddTiket setLongitude:self.addTicketLongitude];
        [customAddTiket setDelegate:self];
        isCustomAddTicketHidden= TRUE;
        [self presentViewController:customAddTiket animated:YES completion:nil];
 
        
//    }
}
/*
 ***********************************************************************************************
 * Route taped methots
 ***********************************************************************************************
 */

- (void)setupGestureRecognizer {
    NSLog(@"%s",__PRETTY_FUNCTION__);
    UILongPressGestureRecognizer *tap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedMap1:)];
    tap.delegate =self;
    tap.minimumPressDuration = 0.5;
    [self.mapView addGestureRecognizer:tap];
}

//- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
//    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
//}

 - (void)userTappedMap1:(UIGestureRecognizer *)recognizer {
 NSLog(@"%s",__PRETTY_FUNCTION__);
 if (recognizer.state == UIGestureRecognizerStateEnded) {
 self.head_Title = nil;
 CGPoint tappedPoint = [recognizer locationInView:self.mapView];
// CLLocationCoordinate2D coord= [self.mapView convertPoint:tappedPoint toCoordinateFromView:self.mapView]; /
   CLLocationCoordinate2D coord=  [self.mapView.projection coordinateForPoint: tappedPoint];
     
 longPressedCoord = coord;
 //  MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
 // annotationPoint.coordinate = coord;
 // annotationPoint.title = @"Add Ticket";
 //annotationPoint.subtitle = @"Hello";
 //[self.mapView addAnnotation:annotationPoint];
 
 NSLog(@"=================%f:-%f",coord.latitude,coord.longitude);
 self.addTicketLatitude = [NSString stringWithFormat:@"%f",coord.latitude];
 self.addTicketLongitude = [NSString stringWithFormat:@"%f",coord.longitude];
 [self showCustomViewForTicket];
 
 }
 }



-(void)setUpGestureRecognizerForLine {
    NSLog(@"%s",__PRETTY_FUNCTION__);
    // NSLog(@"=========:-%@",self.routeLine.title);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapedForLine:)];
    [tap setNumberOfTouchesRequired:1];
    [tap setDelegate:(id)self];
    [self.mapView addGestureRecognizer:tap];
    
}


-(void)tapedForLine:(UIGestureRecognizer *)recognizer {
    NSLog(@"%s",__PRETTY_FUNCTION__);
    //[self addZoomPolyLine:recognizer];
}


#pragma mark - DrawRouteBySelectingRouteFromMyRoutes Delegate

-(void)DrawRouteBySelectingRouteFromMyRoutes:(NSString*)Encodedpath
{
    NSLog(@"DrawRouteBySelectingRouteFromMyRoutes delegate callback");
    [self.mapView clear];
    
    [self polylineWithEncodedString:Encodedpath];
    _polyLinePath = [GMSPath pathFromEncodedPath:Encodedpath];
    
    self.polyline = [GMSPolyline polylineWithPath:_polyLinePath];
    GMSStrokeStyle *greenToRed = [GMSStrokeStyle gradientFromColor:[UIColor blueColor] toColor:[UIColor blueColor]];
    polyline.spans = @[[GMSStyleSpan spanWithStyle:greenToRed]];
    self.polyline.strokeWidth = 5.f;
    self.polyline.tappable = true;
    self.polyline.map = mapView;
    self.view = mapView;
    [self.mapView animateToZoom:10.0];
    [self createSourceDestinationMarkers];
    
}


#pragma mark - TicketAddSuccessfully Delegate from appdelegate runtime

-(void)AddMarkerRuntimeWithTitle:(NSString *)title withDescription:(NSString *)des infoModel:(NSMutableDictionary*)infoModel
{
    [self AddMarkerRuntime:title withDescription:des infoModel:infoModel];
    
    UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:nil message:@"New Report added." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alerView show];
}


#pragma mark - TicketAddSuccessfully Delegate

-(void)ticketOnserverWithTitle:(NSString *)title withDescription:(NSString *)des infoModel:(NSMutableDictionary*)infoModel
{
    [self AddmarkerOnMap:title withDescription:des infoModel:infoModel];
    
    UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:nil message:@"Report added successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alerView show];
}

#pragma mark - TicketUpdateSuccessfully Delegate

-(void)ticketUpdateOnserverWithTitle:(NSString *)title withDescription:(NSString *)des infoModel:(NSMutableDictionary*)infoModel
{
    freelyMarker.map = nil;
    [self AddmarkerOnMap:title withDescription:des infoModel:infoModel];
    
    UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:nil message:@"Report updated successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alerView show];
}

#pragma mark - Show report details from selecting my profile Delegate

-(void)TicketDetailsFromMyReport:(NSString *)title withDescription:(NSString *)des infoModel:(VSReportModelClass*)infoModel
{
    [self.mapView clear];
    
    VSDirectionModal *model22 = [[VSDirectionModal alloc]init]; // adding Ticket details after call back for display without server side fetching.
    [model22 setReportModalAddedBy:infoModel.addedBy];
    [model22 setReportModalDescription:infoModel.description];
    [model22 setReportModalHostId:infoModel.hostId];
    [model22 setReportModalIsHost:infoModel.isHost];
    [model22 setReportModalAddedByName:infoModel.addedBy];
    [model22 setReportModalLastModifiedTime:infoModel.lastModifiedTime];
    [model22 setReportModalLatitude:infoModel.latitude];
    [model22 setReportModalLocation:infoModel.location];
    [model22 setReportModalLongitude:infoModel.longitude];
    [model22 setReportModalPrice:infoModel.price];
    [model22 setReportModalRadiusofImpact:infoModel.radiusofImpact];
    [model22 setReportModalReportId:infoModel.reportId];
    [model22 setReportModalReportStatus:infoModel.reportStatus];
    [model22 setReportModalReportStatusId:infoModel.reportStatusId];
    [model22 setReportModalReportType:infoModel.reportType];
    [model22 setReportModalReportTypeId:infoModel.reportTypeId];
    [model22 setReportModalStatusType:infoModel.statusType];
    [model22 setReportModalThumbsDown:infoModel.thumbsDown];
    [model22 setReportModalThumbsUp:infoModel.thumbsUp];
    [model22 setReportModalUserId:infoModel.userId];
    [locM.userReportListArray addObject:model22];
    
    float finalLat= model22.reportModalLatitude.doubleValue;
    float finalLon=model22.reportModalLongitude.doubleValue;
    NSLog(@"lat %f  long %f", finalLat,finalLon );
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
    
    
    GMSMarker *london = [GMSMarker markerWithPosition:coord];
    
    london.title =title;
    london.snippet = des;
    if ([title isEqualToString:@"Accident"]) {
        london.icon = [UIImage imageNamed:@"third"];
    }
    else if ([title isEqualToString:@"Under Construction"])
    {
        london.icon = [UIImage imageNamed:@"second.png"];
    }
    else
    {
        london.icon = [UIImage imageNamed:@"fourth.png"];
    }
    london.map = self.mapView;
    self.view = london.map;
    self.mapView.camera = [GMSCameraPosition cameraWithTarget:london.position
                                                         zoom:12];
    
  
}

#pragma mark - TicketAdd/update delegate helper method

-(void)AddmarkerOnMap:(NSString *)title withDescription:(NSString *)des infoModel:(NSMutableDictionary*)infoModel
{
    VSDirectionModal *model22 = [[VSDirectionModal alloc]init]; // adding Ticket details after call back for display without server side fetching.
    [model22 setReportModalAddedBy:[infoModel valueForKey:@"addedBy"]];
    [model22 setReportModalDescription:[infoModel valueForKey:@"description"]];
    [model22 setReportModalHostId:[infoModel valueForKey:@"hostId"]];
    [model22 setReportModalIsHost:[infoModel valueForKey:@"isHost"]];
    [model22 setReportModalAddedByName:[infoModel valueForKey:@"addedByName"]];
    [model22 setReportModalLastModifiedTime:[infoModel valueForKey:@"lastModifiedTime"]];
    [model22 setReportModalLatitude:[infoModel valueForKey:@"latitude"]];
    [model22 setReportModalLocation:[infoModel valueForKey:@"location"]];
    [model22 setReportModalLongitude:[infoModel valueForKey:@"longitude"]];
    [model22 setReportModalPrice:[infoModel valueForKey:@"price"]];
    [model22 setReportModalRadiusofImpact:[infoModel valueForKey:@"radiusofImpact"]];
    [model22 setReportModalReportId:[infoModel valueForKey:@"reportId"]];
    [model22 setReportModalReportStatus:[infoModel valueForKey:@"reportStatus"]];
    [model22 setReportModalReportStatusId:[infoModel valueForKey:@"reportStatusId"]];
    [model22 setReportModalReportType:title];
    [model22 setReportModalReportTypeId:[infoModel valueForKey:@"reportTypeId"]];
    [model22 setReportModalStatusType:[infoModel valueForKey:@"statusType"]];
    [model22 setReportModalThumbsDown:[infoModel valueForKey:@"thumbsDown"]];
    [model22 setReportModalThumbsUp:[infoModel valueForKey:@"thumbsUp"]];
    [model22 setReportModalUserId:[infoModel valueForKey:@"userId"]];
    [locM.userReportListArray addObject:model22];
    
    float finalLat= model22.reportModalLatitude.doubleValue;
    float finalLon=model22.reportModalLongitude.doubleValue;
    NSLog(@"AddmarkerOnMap lat %f  long %f", finalLat,finalLon );
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
    
    GMSMarker *london = [GMSMarker markerWithPosition:coord];
    
    london.title =title;
    london.snippet = des;
    if ([title isEqualToString:@"Accident"]) {
        london.icon = [UIImage imageNamed:@"third"];
    }
    else if ([title isEqualToString:@"Under Construction"])
    {
        london.icon = [UIImage imageNamed:@"second.png"];
    }
    else
    {
        london.icon = [UIImage imageNamed:@"fourth.png"];
    }
    london.map = self.mapView;
    self.view = london.map;
  }


-(void)AddMarkerRuntime:(NSString *)title withDescription:(NSString *)des infoModel:(NSMutableDictionary*)infoModel
{
    VSDirectionModal *model22 = [[VSDirectionModal alloc]init]; // adding Ticket details after call back for display without server side fetching.
    [model22 setReportModalAddedBy:[infoModel valueForKey:@"addedBy"]];
    [model22 setReportModalDescription:[infoModel valueForKey:@"description"]];
    [model22 setReportModalHostId:[infoModel valueForKey:@"hostId"]];
    [model22 setReportModalIsHost:[infoModel valueForKey:@"isHost"]];
    [model22 setReportModalAddedByName:[infoModel valueForKey:@"addedByName"]];
    [model22 setReportModalLastModifiedTime:[infoModel valueForKey:@"lastModifiedTime"]];
    [model22 setReportModalLatitude:[infoModel valueForKey:@"latitude"]];
    [model22 setReportModalLocation:[infoModel valueForKey:@"location"]];
    [model22 setReportModalLongitude:[infoModel valueForKey:@"longitude"]];
    [model22 setReportModalPrice:[infoModel valueForKey:@"price"]];
    [model22 setReportModalRadiusofImpact:[infoModel valueForKey:@"radiusofImpact"]];
    [model22 setReportModalReportId:[infoModel valueForKey:@"reportId"]];
    [model22 setReportModalReportStatus:[infoModel valueForKey:@"reportStatus"]];
    [model22 setReportModalReportStatusId:[infoModel valueForKey:@"reportStatusId"]];
    [model22 setReportModalReportType:title];
    [model22 setReportModalReportTypeId:[infoModel valueForKey:@"reportTypeId"]];
    [model22 setReportModalStatusType:[infoModel valueForKey:@"statusType"]];
    [model22 setReportModalThumbsDown:[infoModel valueForKey:@"thumbsDown"]];
    [model22 setReportModalThumbsUp:[infoModel valueForKey:@"thumbsUp"]];
    [model22 setReportModalUserId:[infoModel valueForKey:@"userId"]];
    
    //calculating distance from current location for soring tickets
    CLLocation* LocB = [[CLLocation alloc] initWithLatitude:model22.reportModalLatitude.doubleValue longitude:model22.reportModalLongitude.doubleValue];
    CLLocationDistance distance = [locCordinates distanceFromLocation:LocB];
    [model22 setReportModelDistance:distance];
    [locM.userReportListArray addObject:model22];
    [self SortTicketsByDistance];
    
    float finalLat= model22.reportModalLatitude.doubleValue;
    float finalLon=model22.reportModalLongitude.doubleValue;
    NSLog(@"AddmarkerOnMap lat %f  long %f", finalLat,finalLon );
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
    
    GMSMarker *london = [GMSMarker markerWithPosition:coord];
    
    london.title =title;
    london.snippet = des;
    if ([title isEqualToString:@"Accident"]) {
        london.icon = [UIImage imageNamed:@"third"];
    }
    else if ([title isEqualToString:@"Under Construction"])
    {
        london.icon = [UIImage imageNamed:@"second.png"];
    }
    else
    {
        london.icon = [UIImage imageNamed:@"fourth.png"];
    }
    london.map = self.mapView;
    self.view = london.map;
}



#pragma Nearest ticket finding/triggering/Notify ------

- (VSDirectionModal*)closestLocationToLocation:(CLLocation*)currLocation
{
    VSDirectionModal *model;
    for (VSDirectionModal *model22 in locM.userReportListArray)
    {
        float finalLat= model22.reportModalLatitude.doubleValue;
        float finalLon=model22.reportModalLongitude.doubleValue;
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:currLocation.coordinate.latitude longitude:currLocation.coordinate.longitude];
        
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:finalLat longitude:finalLon];
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        NSLog(@"lat_closestLocationToLocation %f  long_closestLocationToLocation %f closesDistance %f", finalLat,finalLon, distance);

        BOOL contain = [_IgnouredTkts containsObject:model22.reportModalDescription];
        NSLog(@"Iscontain ? -- %hhd & Distance - %f",contain,distance);
        
        
        
        if (distance <=400 && !contain)
        {
            model = model22;
            return model;

        }
    }
      //closestLocation is now the location from your array which is closest to the current location or nil if there are no locations in your array.
              return 0;
}

-(IBAction)Closebytimer:(id)sender
{
    Remtime++;
    if (Remtime!=10) return;
    [self CloseAlertview:self];
}

-(IBAction)CloseAlertview:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    for (UIView *view in [[appDelegate window] subviews] )
    {
        if ([view isKindOfClass:[AlertClass class]])
            [view removeFromSuperview];
    }
    [alert removeFromSuperview];
    alert = nil;
    
    
    
}

-(void)SpechNow
{
    self.synthesizer = [[AVSpeechSynthesizer alloc] init];
    self.synthesizer.delegate =self;
    NSString *speech = [NSString stringWithFormat:@"%@",[ToSpeechArr firstObject]];
    AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:speech];
    utterance.rate = AVSpeechUtteranceMinimumSpeechRate;
    [self.synthesizer speakUtterance:utterance];
}

-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    NSLog(@"Playback finished-- %@",utterance.speechString);
    if ([ToSpeechArr containsObject:utterance.speechString]&&[ToSpeechArr count]>0)
    {
        [ToSpeechArr removeObject:utterance.speechString];
    }
    
    if (ToSpeechArr.count>0) [self SpechNow];
    
    if (ToSpeechArr.count<=0)
    {
        Remtime = 0;
        if (!timer)
        {
            timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                     target:self
                                                   selector:@selector(Closebytimer:)
                                                   userInfo:nil
                                                    repeats:YES];
        }
    }
  }






@end
