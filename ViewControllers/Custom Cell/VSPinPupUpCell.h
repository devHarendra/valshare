//
//  VSPinPupUpCellTableViewCell.h
//  ValShare
//
//  Created by Vivek Kumar on 4/4/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VSPinPupUpCell : UITableViewCell
@property (strong,nonatomic) UILabel *myLabel;
@end
