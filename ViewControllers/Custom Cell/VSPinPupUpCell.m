//
//  VSPinPupUpCellTableViewCell.m
//  ValShare
//
//  Created by Vivek Kumar on 4/4/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSPinPupUpCell.h"

@implementation VSPinPupUpCell
@synthesize myLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10.0f, 5.0f, self.frame.size.width - 20, 20.0f)];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        [label setTextColor:[UIColor blackColor]];
        [self addSubview:label];
        myLabel = label;
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setNeedsLayout {
    
}


@end
