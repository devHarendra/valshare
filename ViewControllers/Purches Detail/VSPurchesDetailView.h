//
//  VSPurchesDetailView.h
//  ValShare
//
//  Created by Vivek Kumar on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VSPurchesDetailView : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong , nonatomic) NSMutableArray *dateArray;
@property (nonatomic,strong) NSMutableArray *purchesHistoryArray;

@end
