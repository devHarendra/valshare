//
//  VSPurchesDetailView.m
//  ValShare
//
//  Created by Vivek Kumar on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSPurchesDetailView.h"
#import "VSHeader.h"

@implementation VSPurchesDetailView
@synthesize dateArray = _dateArray;
@synthesize purchesHistoryArray = _purchesHistoryArray;

-(NSMutableArray *)dateArray {
    
    if (!_dateArray) {
        _dateArray = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5", nil];
    }
    return _dateArray;
}
-(NSMutableArray *)purchesHistoryArray {
    
    if (!_purchesHistoryArray) {
        _purchesHistoryArray = [[NSMutableArray alloc] init];
    }
    return _purchesHistoryArray;
}

-(void)getPurchesHistoryListFromSerVer {
    
    [[VSParserClass sharedParser]getPurchesHistoryList:@"3" WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
        
        if ([dictdata isKindOfClass:[NSDictionary class]]) {
            
            VSPurchesHistoryListModel *model = [[VSPurchesHistoryListModel alloc]init];
            [model setLoyaltyPointEquivalent:[dictdata valueForKey:@"loyaltyPointEquivalent"]];
            [model setNextPaymentDate:[dictdata valueForKey:@"nextPaymentDate"]];
            
            [self.dateArray addObject:model];
            
            if ([[dictdata valueForKey:@"purchaseHistoryModelList"] isKindOfClass:[NSArray class]]){
                
                NSMutableArray *localArray = (NSMutableArray *)[dictdata valueForKey:@"purchaseHistoryModelList"];
                
                NSLog(@"NSDictionary%@",localArray);
                
                [localArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    
                    NSMutableDictionary *localHistoryDict = (NSMutableDictionary *)[localArray objectAtIndex:idx];
                    
                    VSPurchesHistoryListModel *model11 = [[VSPurchesHistoryListModel alloc]init];
                    
                    [model11 setCashPrice:[localHistoryDict valueForKey:@"cashPrice"]];
                    [model11 setLoyaltyPoint:[localHistoryDict valueForKey:@"loyaltyPoint"]];
                    [model11 setPaymentPackageId:[localHistoryDict valueForKey:@"paymentPackageId"]];
                    [model11 setPuchaseHistoryId:[localHistoryDict valueForKey:@"puchaseHistoryId"]];
                    [model11 setPurcahseHistoryStatusId:[localHistoryDict valueForKey:@"purcahseHistoryStatusId"]];
                    [model11 setEmail:[localHistoryDict valueForKey:@"email"]];
                    [model11 setCashPrice:[localHistoryDict valueForKey:@"cashPrice"]];
                    [model11 setCreatedTime:[localHistoryDict valueForKey:@"createdTime"]];
                    [model11 setCardNumber:[localHistoryDict valueForKey:@"cardNumber"]];
                    [model11 setPurcahseHistoryStatus:[localHistoryDict valueForKey:@"purcahseHistoryStatus"]];
                    [model11 setPackagePrice:[localHistoryDict valueForKey:@"packagePrice"]];
                    [model11 setPackageName:[localHistoryDict objectForKey:@"packageName"]];
                    [model11 setTransactionId:[localHistoryDict objectForKey:@"transactionId"]];
                    [model11 setUserId:[localHistoryDict objectForKey:@"userId"]];

                    [self.purchesHistoryArray addObject:model11];
                    
                }];
            }else {
                NSLog(@"purcahseHistoryStatusIdelse%@",[dictdata valueForKey:@"purchaseHistoryModelList"]);
            }
        }
        else if ([dictdata isKindOfClass:[NSArray class]]) {
            
            NSLog(@"NSArray%@",dictdata);
        }else {
            
            NSLog(@"else%@",dictdata);
        }
     [self addTableOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height) Tag:2450];
    }];
}

-(void)deletePurchesHistoryFromList:(NSString *)deletePurchesHistoryId {
    
    [[VSParserClass sharedParser] deletePurchesHistory:deletePurchesHistoryId WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSLog(@"deletePurchesHistoryresultObject:-%@",resultObject);
        
        if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Your purches detail is delete successfully " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            
        }else {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Your purches detail is not delete successfully " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"Purches Detail"];
    [self.navigationItem setHidesBackButton:NO];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    [self addComponentOnView];
    [self getPurchesHistoryListFromSerVer];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addComponentOnView {
    
    
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    UITableView *cardTable = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
    [cardTable setTag:tag];
    [cardTable setDelegate:(id)self];
    [cardTable setDataSource:(id)self];
    [cardTable setBackgroundColor:[UIColor clearColor]];
    [cardTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [self.view addSubview:cardTable];
}

-(void)addButtonOnView:(CGRect)frame title:(NSString *)text Tag:(int)tag {
    
    UIButton *saveButton = [[UIButton alloc]init];
    [saveButton setTag:tag];
    [saveButton setFrame:frame];
    [saveButton setTitle:text forState:UIControlStateNormal];
    [saveButton.titleLabel setFont:(IS_IPHONE_5 || IS_IPHONE_4S) ? [UIFont boldSystemFontOfSize:12] : [UIFont boldSystemFontOfSize:14]];
    [saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [saveButton setBackgroundImage:kRegisterButtonBG forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(addCardAndPaymentAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
}

-(IBAction)addCardAndPaymentAction:(UIButton *)sender {
    
    if (sender.tag == 55645) {
        
        NSLog(@"addCardAndPaymentAction");
    } else {
        
        NSLog(@"addCardAnd--PaymentActionadd--CardAndPaymentAction");
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 115;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.purchesHistoryArray count];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
      VSPurchesHistoryListModel *listModel = [self.purchesHistoryArray objectAtIndex:indexPath.row];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self deletePurchesHistoryFromList:listModel.puchaseHistoryId];
        
        [self.purchesHistoryArray removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    MyRouteTableCell *cell = (MyRouteTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[MyRouteTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setBackgroundColor:[UIColor clearColor]];
    }
        VSPurchesHistoryListModel *listModel = [self.purchesHistoryArray objectAtIndex:indexPath.row];
    
    [cell.myId setText:@"Id"];
   
    [cell.source setText:@"TranscationId"];
    [cell.destination setText:@"Price"];
    [cell.date setText:@"Status"];
    [cell.effectiveTo setText:@"Date"];
    
    [cell.myFinalId setText:[NSString stringWithFormat:@": %@",listModel.puchaseHistoryId]];
     [cell.myFinalId setTag:indexPath.row];
    [cell.finalSource setText:[NSString stringWithFormat:@": %@",listModel.transactionId]];
    [cell.finalDestination setText:[NSString stringWithFormat:@": %@",listModel.cashPrice]];
    [cell.finalDate setText:[NSString stringWithFormat:@": %@",listModel.purcahseHistoryStatus]];
    [cell.finalEffectiveTo setText:[NSString stringWithFormat:@": %@",listModel.createdTime]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VSPurchesHistoryListModel *listModel = [self.purchesHistoryArray objectAtIndex:indexPath.row];
    [self navigateToDetailscreen:listModel];
}

-(void)navigateToDetailscreen:(VSPurchesHistoryListModel *)listModel {
    
    VSPurchesHistoryView *historyView = [[VSPurchesHistoryView alloc]init];
    [historyView setListModel:listModel];
    [self.navigationController pushViewController:historyView animated:YES];
}


@end
