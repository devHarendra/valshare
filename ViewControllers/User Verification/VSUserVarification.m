//
//  VSUserVarification.m
//  ValShare
//
//  Created by Sharda Prasad on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSUserVarification.h"
#import "VSforgotVC.h"

@interface VSUserVarification ()

@end

@implementation VSUserVarification
@synthesize userID;
@synthesize loginStatus;

-(void)checkVarificationTokenFrom:(NSString *)userID1 Token:(NSString *)token {
    
    [[VSParserClass sharedParser] checkVerifyToken:userID1 Token:token WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSLog(@"checkVerifyTokencheckVerifyToken :- %@", resultObject);
        
        if (result) {
            
            NSMutableDictionary *resultData = (NSMutableDictionary *)resultObject;
            if ([[resultData valueForKey:@"status"] isEqualToString:@"Ok"]) {
                
                NSLog(@"------00000 : %@", [VSSingletonClass sharedMySingleton].loginUserId);
                [[VSSingletonClass sharedMySingleton] setLoginUserId:userID1];
                [self navigateToAddReset:userID1 Token:token];
            }else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[resultData valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}


-(void)navigateToAddReset:(NSString *)userID1 Token:(NSString*)token {
    
    VSforgotVC *reset = [[VSforgotVC alloc] init];
    [reset setUserID:userID1];
    [reset setToken:token];
    [self.navigationController pushViewController:reset animated:YES];
}

-(void)navigateToAddVehicle:(NSString *)userID1 {
    
    VSVehicleInfoViewController *addVehicleVC = [[VSVehicleInfoViewController alloc] init];
    [addVehicleVC setUserID:userID1];
    [addVehicleVC setVCTitle:@"add"];
    [addVehicleVC setLoginStatus:@"varification"];
    [self.navigationController pushViewController:addVehicleVC animated:YES];
}

-(void)resendVarificationCodeService:(NSString *)userID1 {
    
    [[VSParserClass sharedParser] resendToken:userID1 WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            NSMutableDictionary *resendDict = (NSMutableDictionary *)resultObject;
            if ([[resendDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                [[[UIAlertView alloc] initWithTitle:nil message:@"Please check your email account for token." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@",[resendDict valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            NSLog(@"stringstringstringstringstringstring :- %@ ", resultObject);
        }
    }];
}


-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO];
    self.title = [NSString stringWithFormat:@"varification token send in your mail"];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self addComponentOnView];
}


#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addComponentOnView {
    
    NSString *placeHolder = [NSString stringWithFormat:@"%@", @"Type varification token here..."];
    NSArray *buttonTitle = @[@"Verify",@"Resend varification token"];
    
    [self addTextFieldOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kEmail_BG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 40 + (kEmail_BG.size.height + 10), kEmail_BG.size.width, kEmail_BG.size.height) Title:placeHolder Tag:45340 BGImage:nil SecureText:NO KeyBoardType:UIKeyboardTypeEmailAddress];
    
    for (int i = 0; i<2; i++) {
        
        [self addButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kRegisterButtonBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 125 + ((kRegisterButtonBG.size.height + 10) * i), kRegisterButtonBG.size.width, kRegisterButtonBG.size.height) BGImage:kRegisterButtonBG Tag:88789 + i Title:buttonTitle[i]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTextFieldOnView:(CGRect)frame Title:(NSString *)placeHolder Tag:(int)tag BGImage:(UIImage *)bgImage SecureText:(Boolean)secureText KeyBoardType:(UIKeyboardType)kType{
    
    VSTextField *textField = [[VSTextField alloc] init];
    [textField setTag:tag];
    [textField setFrame:frame];
    [textField setDelegate:(id)self];
    [textField setSecureTextEntry:secureText];
    [textField setBackground:bgImage];
    [textField setKeyboardType:kType];
    [textField.layer setBorderWidth:1.0f];
    [textField setPlaceholder:placeHolder];
    [textField setSecureTextEntry:secureText];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentCenter];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setFont:[UIFont italicSystemFontOfSize:14.0f]];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 5, 5)];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    [textField setRightView:rightView];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 5, 5)];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    [textField setLeftView:leftView];
    
    [self.view addSubview:textField];
}

-(void)addButtonOnView:(CGRect)frame BGImage:(UIImage *)bgImage Tag:(int)tag Title:(NSString *)title {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button setEnabled:YES];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont italicSystemFontOfSize:15.0f]];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(varificationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}


-(IBAction)varificationButtonAction:(VSButton *)button {
    
    VSTextField *textField = (VSTextField *)[self.view viewWithTag:45340];
    
    switch (button.tag) {
        case 88789:
            if ([textField.text length] == 0) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Please enter varification token." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else {
                
                NSLog(@"textFieldtextFieldtextField :- %@", textField.text);
                
                if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                    
                    [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
                    [self checkVarificationTokenFrom:self.userID Token:textField.text];
                }else {
                    
                    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertview show];
                }
            }
            
            break;
        case 88790:
            [self resendVarificationCode];
            break;
        default:
            break;
    }
}

-(void)resendVarificationCode {
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self resendVarificationCodeService:[VSSingletonClass sharedMySingleton].loginUserId];
    }else {
        
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertview show];
        
        
    }
}

@end
