//
//  VSUserVarification.h
//  ValShare
//
//  Created by Sharda Prasad on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@interface VSUserVarification : UIViewController

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *loginStatus;

@end
