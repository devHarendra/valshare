//
//  VSSaveRouteViewController.m
//  ValShare
//
//  Created by Sharda Prasad on 4/3/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSSaveRouteViewController.h"


@interface VSSaveRouteViewController () {
    UITableView *routeTable;
}

@end

@implementation VSSaveRouteViewController
@synthesize dateArray = _dateArray;

-(NSMutableArray *)dateArray {
    
    if (!_dateArray) {
        _dateArray = [[NSMutableArray alloc] init];
    }
    return _dateArray;
}

-(void)getSaveRouteService:(NSString *)source Destination:(NSString *)destination Driving:(NSString *)driving RouteIndexId:(NSString *)routeIndexid UserID:(NSString *)userId RouteID:(NSString *)routeid distance:(NSString *)distance Duration:(NSString *)duration RouteEncodedPath:(NSString *)routeEncodedPath Summary:(NSString *)summary {
    int uid = [userId intValue];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM d, yyyy hh:mm:ss a"];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    
    NSMutableDictionary *saveDict = [[NSMutableDictionary alloc] init];
    [saveDict setValue:dateString forKey:@"createdTime"];
    [saveDict setValue:[NSString stringWithFormat:@"%d",[routeid intValue]] forKey:@"routeId"];
    [saveDict setValue:distance forKey:@"distance"];
    [saveDict setValue:destination forKey:@"destination"];
    [saveDict setValue:[NSNumber numberWithInt:uid] forKey:@"userId"];
    [saveDict setValue:@"false" forKey:@"postData"];
    [saveDict setValue:summary forKey:@"summary"];
    [saveDict setValue:routeEncodedPath forKey:@"routeEncodedPath"];
    [saveDict setValue:source forKey:@"source"];
    [saveDict setValue:driving forKey:@"drivingMode"];
    [saveDict setValue:duration forKey:@"duration"];
    [saveDict setValue:[NSString stringWithFormat:@"%d",[routeIndexid intValue]] forKey:@"routeIndexId"];
    
    NSLog(@"saveDictsaveDictsaveDictsaveDict :- %@", saveDict);
    
    [[VSParserClass sharedParser] saveRouteService:saveDict WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSLog(@"resultObjectresultObjectresultObject:-%@",resultObject);
        if (result) {
            
            NSMutableDictionary *resultData = (NSMutableDictionary *)resultObject;
            [[[UIAlertView alloc] initWithTitle:nil message:[resultData valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.title = [NSString stringWithFormat:@"%@", @"Compare Route"]; // removed by harry
    self.title = [NSString stringWithFormat:@"%@", @"Save Route"]; // Added by harry

    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    [self addComponentOnView];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    [self addTableOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y + 10, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 5) Tag:2450];
    
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    routeTable = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    [routeTable setTag:tag];
    [routeTable setDelegate:(id)self];
    [routeTable setDataSource:(id)self];
    [routeTable setBackgroundColor:[UIColor clearColor]];
    [routeTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [routeTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:routeTable];
}

/*
 *******************************************************************************************
 * TableView DataSource and Delegate methods.
 *******************************************************************************************
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 120;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return (![self.dateArray count] == 0) ? [self.dateArray count] :1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    
    return (![self.dateArray count] == 0) ? YES : NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    VSSaveRouteCell *cell = (VSSaveRouteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[VSSaveRouteCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    if (![self.dateArray count] == 0) {
        
        [cell.routeText setHidden:NO];
        [cell.timeText setHidden:NO];
        [cell.distanceText setHidden:NO];
        [cell.viaRouteText setHidden:NO];
        [cell.route setHidden:NO];
        [cell.time setHidden:NO];
        [cell.distance setHidden:NO];
        [cell.viaRoute setHidden:NO];
        [cell.saveButton setHidden:NO];
        
        
        VSDirectionModal *routeModal = [self.dateArray objectAtIndex:indexPath.row];
        
        [cell.routeText setText:@"Route : "];
        [cell.timeText setText:@"Time : "];
        [cell.distanceText setText:@"Distance : "];
        [cell.viaRouteText setText:@"Via : "];
        
        [cell.route setText:[NSString stringWithFormat:@"%@", routeModal.localRouteRouteIndexId]];
        [cell.time setText:[NSString stringWithFormat:@"%@", routeModal.localRouteDuration]];
        [cell.distance setText:[NSString stringWithFormat:@"%@",routeModal.localRouteDistance]];
        [cell.viaRoute setText:[NSString stringWithFormat:@"%@",routeModal.localRouteSource]];
        
        
        [cell.saveButton setTag:67676 + indexPath.row];
        [cell.saveButton addTarget:self action:@selector(saveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.saveButton setTitle:@"Save" forState:UIControlStateNormal];
        
    }else {
        [cell.routeText setText:@"No Data Found"];
        [cell.timeText setHidden:YES];
        [cell.distanceText setHidden:YES];
        [cell.viaRouteText setHidden:YES];
        [cell.route setHidden:YES];
        [cell.time setHidden:YES];
        [cell.distance setHidden:YES];
        [cell.viaRoute setHidden:YES];
        [cell.saveButton setHidden:YES];
        
    }
    return cell;
}

-(IBAction)saveButtonAction:(UIButton *)button {
    
    //VSSaveRouteCell *cell = (VSSaveRouteCell *)[routeTable cellForRowAtIndexPath:[NSIndexPath indexPathForItem:button.tag - 67676 inSection:0]];
    
    VSDirectionModal *routeModal = [self.dateArray objectAtIndex:button.tag - 67676];
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getSaveRouteService:routeModal.localRouteSource Destination:routeModal.localRouteDestination Driving:routeModal.localRouteDrivingMode RouteIndexId:routeModal.routeIndexId UserID:[VSSingletonClass sharedMySingleton].loginUserId RouteID:routeModal.routeId distance:routeModal.localRouteDistance Duration:routeModal.localRouteDuration RouteEncodedPath:routeModal.localRouteRouteEncodedPath Summary:routeModal.localRouteSummary];
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    
}

@end
