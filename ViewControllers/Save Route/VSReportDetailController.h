//
//  VSReportDetailController.h
//  ValShare
//
//  Created by Vivek Kumar on 4/20/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@interface VSReportDetailController : UIViewController

@property (strong , nonatomic) VSReportModelClass *modelClass;

@end
