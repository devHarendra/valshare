//
//  VSReportDisplayView.h
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "NIDropDown.h"
#import "VSReportDisplaySetting.h"

@interface VSReportDisplayView : UIViewController<NIDropDownDelegate> {
    
    NIDropDown *dropDown;
}
@property (nonatomic, strong) NSMutableArray *reportTypeArray;
@property (nonatomic, strong) NSMutableArray *dayArray;
@property (nonatomic, strong) NSMutableArray *meterArray;
@property (nonatomic, strong) NSMutableArray *dayTextArray;
@property (nonatomic, strong) NSMutableArray *dataModalArray;
@property (nonatomic, strong) NSMutableArray *checkBoxSelectedArray;
@property (nonatomic,strong)  NSString       *userId;


@end
