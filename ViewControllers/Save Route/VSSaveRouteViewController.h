//
//  VSSaveRouteViewController.h
//  ValShare
//
//  Created by Sharda Prasad on 4/3/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSHeader.h"
#import <UIKit/UIKit.h>


@interface VSSaveRouteViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSMutableArray *dateArray;

@end
