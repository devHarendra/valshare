//
//  VSUserReportsView.m
//  ValShare
//
//  Created by Sharda Prasad on 4/20/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSUserReportsView.h"
#import "VSSingletonClass.h"

@interface VSUserReportsView () {
    
    UITableView *userReportTable;
}

@end

@implementation VSUserReportsView
@synthesize dataArray = _dataArray;
@synthesize cancelButton;
@synthesize isFiltered;
@synthesize filteredTableData = _filteredTableData;
@synthesize delegate = _delegate;

-(NSMutableArray *)dataArray {
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

-(NSMutableArray *)filteredTableData {
    
    if (!_filteredTableData) {
        _filteredTableData = [[NSMutableArray alloc] init];
    }
    return _filteredTableData;
}

-(void)getReportViewFromServer {
    
    [[VSParserClass sharedParser] VSGetReportDetail:[VSSingletonClass sharedMySingleton].loginUserId WithCompletionBlock:^(BOOL result ,id resultObject, NSError *error) {
        
        if (result) {
            NSLog(@"dictdatadictdatadictdata:-%@",resultObject);
            
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Failure"]) {
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                [SVProgressHUD dismiss];
            } else {
                
                NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                
                
                if ([[dictdata valueForKey:@"reportModelList"]isKindOfClass:[NSArray class]]) {
                    
                    NSLog(@"reportModelList==NSArray:-%@",[dictdata valueForKey:@"reportModelList"]);
                    
                    NSMutableArray *localArray = (NSMutableArray *)[dictdata valueForKey:@"reportModelList"];
                    
                    NSLog(@"NSDictionary%@",localArray);
                    
                    [localArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        
                        NSMutableDictionary *localReportDict = (NSMutableDictionary *)[localArray objectAtIndex:idx];
                        
                        VSReportModelClass *modelClass = [[VSReportModelClass alloc]init];
                        [modelClass setAddedBy:[localReportDict valueForKey:@"addedBy"]];
                        [modelClass setCreatedTime:[localReportDict valueForKey:@"createdTime"]];
                        [modelClass setMyDescription:[localReportDict valueForKey:@"description"]];
                        [modelClass setHostId:[localReportDict valueForKey:@"hostId"]];
                        [modelClass setLastModifiedTime:[localReportDict valueForKey:@"lastModifiedTime"]];
                        [modelClass setLatitude:[localReportDict valueForKey:@"latitude"]];
                        [modelClass setLongitude:[localReportDict valueForKey:@"longitude"]];
                        [modelClass setPrice:[localReportDict valueForKey:@"price"]];
                        [modelClass setRadiusofImpact:[localReportDict valueForKey:@"radiusofImpact"]];
                        [modelClass setReportId:[localReportDict valueForKey:@"reportId"]];
                        [modelClass setReportStatus:[localReportDict valueForKey:@"reportStatus"]];
                        [modelClass setReportStatusId:[localReportDict valueForKey:@"reportStatusId"]];
                        [modelClass setReportType:[localReportDict valueForKey:@"reportType"]];
                        [modelClass setReportTypeId:[localReportDict valueForKey:@"reportTypeId"]];
                        [modelClass setReporterEmail:[localReportDict valueForKey:@"reporterEmail"]];
                        [modelClass setStatusType:[localReportDict valueForKey:@"statusType"]];
                        [modelClass setThumbsDown:[localReportDict valueForKey:@"thumbsDown"]];
                        [modelClass setThumbsUp:[localReportDict valueForKey:@"thumbsUp"]];
                        [modelClass setUserId:[localReportDict valueForKey:@"userId"]];
                        [modelClass setLocation:[localReportDict valueForKey:@"location"]];
                        
                        [self.dataArray addObject:modelClass];
                    }];
                }else {
                    NSLog(@"reportModelList==else:-%@",[dictdata valueForKey:@"reportModelList"]);
                }
            }
            NSMutableArray *temp = [[NSMutableArray alloc]init];  // reversing array for letest reports first.
            temp = [[self.dataArray reverseObjectEnumerator] allObjects].mutableCopy;
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:temp];
            [SVProgressHUD dismiss];

        } else {
            [SVProgressHUD dismiss];
            [VSEnumClass showAlertWithTitle:nil msg:@""];
        }
        
        [self addTableOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y + 45, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 125) Tag:2450];
    }];
   
}

-(void)deleteMyReports:(NSString *)reportId Index:(NSIndexPath *)indexPath {
    
    [[VSParserClass sharedParser] getMyReportDelete:reportId WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        if (result) {
            
            NSLog(@"dfkjhgdfkghlkdfglkfdgiufdgiufd :- %@", resultObject);
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                [self.dataArray removeObjectAtIndex:indexPath.row];
                [userReportTable reloadData];
            }else {
                
            }
        } else {
            NSLog(@"dflkj;ldksjfl;dksjflkjd : - %@", error);
        }
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.view  setUserInteractionEnabled:YES];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setTitle:@"My Reports"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    [self addComponenetOnView];
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getReportViewFromServer];
        
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

-(void)setDelegate:(id<VSUserReportsViewControllerDelegate>)delegate
{
    _delegate = delegate;
}



#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addComponenetOnView {
    
    [self addSearchBarOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x , [UIScreen mainScreen].bounds.origin.y + 10, [UIScreen mainScreen].bounds.size.width, 30)];
}

-(void)addSearchBarOnView:(CGRect)frame {
    
    UISearchBar * searchBar = [[UISearchBar alloc]init];
    [searchBar setTag:2000];
    [searchBar setFrame:frame];
    [searchBar setDelegate:(id)self];
    [searchBar setPlaceholder:@"Search"];
    [searchBar.layer setBorderWidth:1.0f];
    [searchBar setReturnKeyType:UIReturnKeySearch];
    [searchBar setBackgroundColor:[UIColor whiteColor]];
    [searchBar.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [searchBar setBackgroundImage:[UIImage imageNamed:@"searchBox"]];
    [self.view addSubview:searchBar];
    
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    userReportTable = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
    [userReportTable setTag:tag];
    [userReportTable setDelegate:(id)self];
    [userReportTable setDataSource:(id)self];
    [userReportTable setBackgroundColor:[UIColor clearColor]];
    [userReportTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [userReportTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [self.view addSubview:userReportTable];
}


#pragma mark - filter table

- (void)filterContentForSearchText:(NSString*)searchText {
    
    NSLog(@"==============%@",searchText);
    /*
     Update the filtered array based on the search text and scope.
     */
    [self.filteredTableData removeAllObjects]; // First clear the filtered array.
    /*
     
     Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
     */
    if([searchText length] > 0) {
        
#define kOption (NSCaseInsensitiveSearch|NSRegularExpressionSearch)
        
        for (VSReportModelClass *modelClass in self.dataArray)
        {
            NSComparisonResult result=[modelClass.myDescription compare:searchText options:kOption range:[modelClass.myDescription rangeOfString:searchText options:kOption]];
            if (result==NSOrderedSame) {
                [self.filteredTableData addObject:modelClass];
            }else {}
        }
    }else {
        isFiltered = NO;
    }
    UITableView *tableView = (UITableView *)[self.view viewWithTag:2450];
    [tableView reloadData];
}

//************* Use of UISearchBar delegate  **************//

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    isFiltered = YES;
    [self filterContentForSearchText:searchBar.text];
    UITableView *tableView = (UITableView *)[self.view viewWithTag:2450];
    [tableView reloadData];
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    isFiltered = NO;
    UITableView *tableView = (UITableView *)[self.view viewWithTag:2450];
    [tableView reloadData];
    [searchBar setShowsCancelButton:NO animated:YES];
    
}

//************* Use of UITableView delegate and dataSourrce **************//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 180;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return (isFiltered) ? self.filteredTableData.count : (![self.dataArray count] == 0) ? self.dataArray.count:1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    
    return (![self.dataArray count] == 0) ? YES : NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        VSReportModelClass *modelClass = (isFiltered) ? [self.filteredTableData objectAtIndex:indexPath.row]:[self.dataArray objectAtIndex:indexPath.row];
        [self deleteMyReports:modelClass.reportId Index:indexPath];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    MyRouteTableCell *cell = (MyRouteTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[MyRouteTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setBackgroundColor:[UIColor clearColor]];
        
    }
    
    if (![self.dataArray count] == 0) {
        
        [cell.myId setHidden:NO];
        [cell.source setHidden:NO];
        [cell.destination setHidden:NO];
        [cell.date setHidden:NO];
        [cell.effectiveTo setHidden:NO];
        [cell.purchesStatus setHidden:NO];
        [cell.finalDestination setHidden:NO];
        [cell.finalSource setHidden:NO];
        [cell.finalEffectiveTo setHidden:NO];
        [cell.finalPurchesStatus setHidden:NO];
        [cell.finalDate setHidden:NO];
        
        VSReportModelClass *modelClass = (isFiltered) ? [self.filteredTableData objectAtIndex:indexPath.row]:[self.dataArray objectAtIndex:indexPath.row];
        
        [cell.myId setText:@"Id"];
        [cell.source setText:@"Price"];
        [cell.destination setText:@"Time"];
        [cell.date setText:@"Type"];
        [cell.effectiveTo setText:@"Description"];
        [cell.purchesStatus setText:@"Location"];
        
        [cell.purchesStatus setFrame:CGRectMake(15.0f, 121.0f, 100.0f, 40.0f)];
        [cell.finalEffectiveTo setFrame:CGRectMake(125.0f, 81.0f,  [UIScreen mainScreen].bounds.size.width/2 + 40 , 40.0f)];
        [cell.finalPurchesStatus setFrame:CGRectMake(125.0f, 121.0f, [UIScreen mainScreen].bounds.size.width/2 + 40, 40.0f)];
        
        [cell.myFinalId setText:[NSString stringWithFormat:@": %@",modelClass.reportId]];
        [cell.finalSource setText:[NSString stringWithFormat:@": %@",modelClass.price]];
        [cell.finalDestination setText:[NSString stringWithFormat:@": %@",modelClass.createdTime]];
        [cell.finalDate setText:[NSString stringWithFormat:@": %@",modelClass.reportType]];
        [cell.finalEffectiveTo setText:[NSString stringWithFormat:@": %@",modelClass.myDescription]];
        [cell.finalEffectiveTo setNumberOfLines:2];
        [cell.finalPurchesStatus setText:[NSString stringWithFormat:@": %@",modelClass.location]];
        [cell.finalPurchesStatus setNumberOfLines:2];
        
    }else {
        
        [cell.myId setHidden:YES];
        [cell.source setHidden:YES];
        [cell.destination setHidden:YES];
        [cell.date setHidden:YES];
        [cell.effectiveTo setHidden:YES];
        [cell.purchesStatus setHidden:YES];
        [cell.finalDestination setHidden:YES];
        [cell.finalDate setHidden:YES];
        [cell.finalEffectiveTo setHidden:YES];
        [cell.finalPurchesStatus setHidden:YES];
        [cell.finalSource setText:@"No Data Found"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VSReportModelClass *modelClass = (isFiltered) ? [self.filteredTableData objectAtIndex:indexPath.row]:[self.dataArray objectAtIndex:indexPath.row];
    
    [self navigateToDetailScreen:modelClass];
}

-(void)navigateToDetailScreen:(VSReportModelClass *)modelClass
{
    if (_delegate && [_delegate respondsToSelector:@selector(TicketDetailsFromMyReport:withDescription:infoModel:)])
    {
        [_delegate TicketDetailsFromMyReport:modelClass.reportType withDescription:modelClass.myDescription infoModel:modelClass];
        
    }

    
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
