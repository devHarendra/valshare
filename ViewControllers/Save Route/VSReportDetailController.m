//
//  VSReportDetailController.m
//  ValShare
//
//  Created by Vivek Kumar on 4/20/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSReportDetailController.h"

@interface VSReportDetailController ()

@end

@implementation VSReportDetailController
@synthesize modelClass;

-(void)postReportDetailOnServer:(NSString *)userId HostID:(NSString *)hostid Price:(NSString *)price Description:(NSString *)description ReportId:(NSString*)reportid ReportType:(NSString*)reporttype {
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc]init];
    [dictData setValue:userId forKey:@"userId"];
    [dictData setValue:hostid forKey:@"hostId"];
    [dictData setValue:[NSString stringWithFormat:@"%f",[price floatValue]] forKey:@"price"];
    [dictData setValue:description forKey:@"description"];
    [dictData setValue:reportid forKey:@"reportId"];
    [dictData setValue:reporttype forKey:@"reportType"];
    
    [[VSParserClass sharedParser] VSSaveReportDetail:dictData WithCompletionBlock:^(BOOL result,id resultObject, NSError *error){
        
        NSLog(@"resultObjectresultObjectresultObject:%@",resultObject);
        if (result) {
            [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            [SVProgressHUD dismiss];
            
        }else {
            [SVProgressHUD dismiss];
            NSLog(@"resultObjectresultObjectresultObject:%@",resultObject);
            
        }
    }];
    
    
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"Report Details"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    
    [self addComponentOnView];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addComponentOnView {

    NSArray *textArray = @[(modelClass.reportType) ? modelClass.reportType : @"",(modelClass.price) ? modelClass.price : @"",(modelClass.createdTime) ? modelClass.createdTime : @"",(modelClass.location) ? modelClass.location : @"",(modelClass.myDescription) ? modelClass.myDescription : @"",@""];
    
    NSArray *placeHolder = @[@"Report Type",@"Price",@"Created Time",@"Location",@"Report Description",@"Report Comment"];
    NSLog(@"1-:%@2-:%@3:-%@4:-%@5:-%@",modelClass.reportType,modelClass.price,modelClass.createdTime,modelClass.location,modelClass.myDescription);
    
    for (int i = 0; i < 6; i++) {
        
        [self addTextFieldOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 10 + (i * 45), kLoginBG.size.width - 3, 35) Title:textArray[i] PlaceHolder:placeHolder[i] Tag:50101 + i];
    }
    
    [self addSubmitButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 287, kLoginBG.size.width - 3, kLoginBG.size.height)  Title:@"Save" Tag:8899 BGImage:kLoginBG];
}

-(void)addTextFieldOnView:(CGRect)frame Title:(NSString *)titleText PlaceHolder:(NSString *)pTitle Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setFrame:frame];
    [textField setTag:tag];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:pTitle];
    [textField.layer setBorderWidth:1.0f];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    [textField setText:[NSString stringWithFormat:@"%@", titleText]];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:textField];
}

-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(saveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(IBAction)saveButtonAction:(id)sender {
    
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        
        UITextField *reportTypeText = (UITextField *)[self.view viewWithTag:50101];
        UITextField *priceText = (UITextField *)[self.view viewWithTag:50102];
        UITextField *createdTimeText = (UITextField *)[self.view viewWithTag:50103];
        UITextField *locationText = (UITextField *)[self.view viewWithTag:50104];
        UITextField *myDescriptionText = (UITextField *)[self.view viewWithTag:50105];
        UITextField *reportCommentText = (UITextField *)[self.view viewWithTag:50106];
        
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self postReportDetailOnServer:modelClass.userId HostID:modelClass.hostId Price:modelClass.price  Description:modelClass.myDescription ReportId:modelClass.reportId ReportType:modelClass.reportType];
    }else {
        
        [[[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
 }


@end
