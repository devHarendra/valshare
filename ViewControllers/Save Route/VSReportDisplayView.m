//
//  VSReportDisplayView.m
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSReportDisplayView.h"
#import "VSCommanReportModal.h"

@interface VSReportDisplayView (){
    
    int  currentbuttonTag;
    UIView *firstView;
    UIView *secondView;
    NSString *reportRange;
    NSString *reportTypecal;
    NSString *reportTypeRangeNumber;
    
}

@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation VSReportDisplayView
@synthesize dayArray = _dayArray;
@synthesize meterArray = _meterArray;
@synthesize scrollView = _scrollView;
@synthesize dayTextArray = _dayTextArray;
@synthesize dataModalArray = _dataModalArray;
@synthesize reportTypeArray = _reportTypeArray;
@synthesize checkBoxSelectedArray = _checkBoxSelectedArray;
@synthesize userId;

-(UIScrollView *)scrollView {
    
    if (!_scrollView) {
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, firstView.bounds.origin.y + 35, firstView.frame.size.width, (kCheckboxBG.size.height) * 5.5)];
        [_scrollView setTag:78560];
        [_scrollView setScrollEnabled:YES];
        [_scrollView setUserInteractionEnabled:YES];
        [_scrollView setBackgroundColor:[UIColor clearColor]];
        [firstView addSubview:_scrollView];
    }
    return _scrollView;
}

-(NSMutableArray *)checkBoxSelectedArray {
    
    if(!_checkBoxSelectedArray) {
        _checkBoxSelectedArray = [[NSMutableArray alloc] init];
    }
    return _checkBoxSelectedArray;
}

-(NSMutableArray *)reportTypeArray {
    
    if(!_reportTypeArray) {
        _reportTypeArray = [[NSMutableArray alloc] init];
    }
    return _reportTypeArray;
}

-(NSMutableArray *)dataModalArray {
    
    if(!_dataModalArray) {
        _dataModalArray = [[NSMutableArray alloc] init];
    }
    return _dataModalArray;
}


-(NSMutableArray *)dayArray {
    
    if(!_dayArray) {
        _dayArray = [[NSMutableArray alloc] init];
    }
    return _dayArray;
}

-(NSMutableArray *)dayTextArray {
    
    if(!_dayTextArray) {
        _dayTextArray = [[NSMutableArray alloc] init];
    }
    return _dayTextArray;
}

-(NSMutableArray *)meterArray {
    
    if(!_meterArray) {
        _meterArray = [[NSMutableArray alloc] init];
    }
    return _meterArray;
}

-(void)dealloc {
    
    self.checkBoxSelectedArray = nil;
    self.reportTypeArray = nil;
    self.dataModalArray = nil;
    self.dayTextArray = nil;
    self.meterArray = nil;
    self.dayArray = nil;
}

-(void)getReportDisplaySetting:(NSString *)userID {
    
    [[VSParserClass sharedParser] getReportDisplaySetting:userID WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        NSLog(@"resultObjectresultObjectresultObject:-%@",resultObject);
        if (result) {
            
            NSMutableDictionary *localSettingsDict = (NSMutableDictionary *)[[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
            
            NSLog(@"localSettingsDict :- %@", localSettingsDict);
            
            if ([[resultObject objectForKey:@"status"] isEqualToString:@"Ok"]) {
                
                VSReportDisplaySetting *settingModal = [[VSReportDisplaySetting alloc] init];
                [settingModal setUserId:[localSettingsDict valueForKey:@"userId"]];
                [settingModal setDistanceUnitDictionary:[localSettingsDict valueForKey:@"distanceUnitDictionary"]];
                [settingModal setDistanceUnitId:[localSettingsDict valueForKey:@"distanceUnitId"]];
                [settingModal setReportDisplaySettingId:[localSettingsDict valueForKey:@"reportDisplaySettingId"]];
                [settingModal setReportRange:[localSettingsDict valueForKey:@"reportRange"]];
                [settingModal setReportTime:[localSettingsDict valueForKey:@"reportTime"]];
                [settingModal setReportTypeDictionary:[localSettingsDict valueForKey:@"reportTypeDictionary"]];
                [settingModal setReportTypeId:[localSettingsDict valueForKey:@"reportTypeId"]];
                [settingModal setReportTypeIdList:[localSettingsDict valueForKey:@"reportTypeIdList"]];
                [settingModal setTimeUnitDictionary:[localSettingsDict valueForKey:@"timeUnitDictionary"]];
                [settingModal setTimeUnitId:[localSettingsDict valueForKey:@"timeUnitId"]];
                [self.dataModalArray addObject:settingModal];
                [SVProgressHUD dismiss];
            } else {
                
                [SVProgressHUD dismiss];
                return;
                
            }
            
        } else {
            [SVProgressHUD dismiss];
        }
        VSReportDisplaySetting *settingModal = [self.dataModalArray objectAtIndex:0];
        NSMutableDictionary *timeData = (NSMutableDictionary *)settingModal.timeUnitDictionary;
        NSMutableDictionary *reportTypeData = (NSMutableDictionary *)settingModal.reportTypeDictionary;
        NSMutableDictionary *distanceUnitData = (NSMutableDictionary *)settingModal.distanceUnitDictionary;
        
        [reportTypeData enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            VSCommanReportModal *commanModal = [[VSCommanReportModal alloc] init];
            [commanModal setReportTypeKey:key];
            [commanModal setReportTypeValue:obj];
            [self.reportTypeArray addObject:commanModal];
        }];
        
        [distanceUnitData enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            VSCommanReportModal *commanModal = [[VSCommanReportModal alloc] init];
            [commanModal setDistanceUnitKey:key];
            [commanModal setDistanceUnitValue:obj];
            [self.meterArray addObject:commanModal];
        }];
        
        [timeData enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            VSCommanReportModal *commanModal = [[VSCommanReportModal alloc] init];
            [commanModal setTimeUnitKey:key];
            [commanModal setTimeUnitValue:obj];
            [self.dayTextArray addObject:commanModal];
        }];
        
        [self adddComponentOnView];
    }];
}

/**
 *  add Report Display Setting Methods
 *  @param userId               Int
 *  @param distanceUnitId       Int
 *  @param timeUnitId           Int
 *  @param reportTime           Double
 *  @param reportRange          Float
 *  @param reportTypeId         Int
 *  @param reportTypeIdString   String
 *  @param block Block Returning Success OR Failure
 */

-(void)saveReportDisplaySetting:(NSString *)userId1 distanceUnitId:(NSString *)distanceUnitId TimeUnitId:(NSString *)timeUnitId ReportTime:(NSString *)reportTime ReportRange:(NSString *)reportRange1 ReportTypeId:(NSString *)reportTypeId reportTypeIdString:(NSString *)reportTypeIdString {
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
    [dictData setObject:[NSString stringWithFormat:@"%d",[userId1 intValue]] forKey:@"userId"];
    [dictData setObject:[NSString stringWithFormat:@"%d",[distanceUnitId intValue]] forKey:@"distanceUnitId"];
    [dictData setObject:[NSString stringWithFormat:@"%d",[timeUnitId intValue]] forKey:@"timeUnitId"];
    [dictData setObject:[NSString stringWithFormat:@"%d",[reportTime intValue]] forKey:@"reportTime"];
    [dictData setObject:[NSString stringWithFormat:@"%d",[reportRange1 intValue]] forKey:@"reportRange"];
    [dictData setObject:[NSString stringWithFormat:@"%d",[reportTypeId intValue]] forKey:@"reportTypeId"];
    [dictData setObject:reportTypeIdString forKey:@"reportTypeIdString"];
    
    [[VSParserClass sharedParser] saveReportDisplaySetting:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                [VSEnumClass showAlertWithTitle:nil msg:@"Your Reports update successfully."];
            } else {
                [VSEnumClass showAlertWithTitle:nil msg:[resultObject valueForKey:@"message"]];
            }
            [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];
            [VSEnumClass showAlertWithTitle:nil msg:[resultObject valueForKey:@"error"]];
        }
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.view  setUserInteractionEnabled:YES];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setTitle:@"Report Display Setting"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getReportDisplaySetting:self.userId];
    }else {
        
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertview show];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeKeyBoard:)];
    [tap setNumberOfTapsRequired:1];
    [secondView addGestureRecognizer:tap];
}


-(IBAction)removeKeyBoard:(id)sender {
    
    UITextField *textField = (UITextField *)[self.view viewWithTag:2225];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector

- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)adddComponentOnView {
    
    [self addFirstViewOnFrame:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 10.0, [UIScreen mainScreen].bounds.origin.y + 30.0, [UIScreen mainScreen].bounds.size.width - 20.0f, [UIScreen mainScreen].bounds.size.height/2 - 160):CGRectMake([UIScreen mainScreen].bounds.origin.x + 10.0, [UIScreen mainScreen].bounds.origin.y + 30.0, [UIScreen mainScreen].bounds.size.width - 20.0f, [UIScreen mainScreen].bounds.size.height/2 - 110)];
    
    [self addSecondViewOnFrame: (IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 10.0, [UIScreen mainScreen].bounds.origin.y + 30.0f + firstView.frame.size.height + 15, [UIScreen mainScreen].bounds.size.width - 20.0, firstView.frame.size.height + 75):CGRectMake([UIScreen mainScreen].bounds.origin.x + 10.0, [UIScreen mainScreen].bounds.origin.y + 30.0f + firstView.frame.size.height + 15, [UIScreen mainScreen].bounds.size.width - 20.0, firstView.frame.size.height + 45)];
}

-(void)addFirstViewOnFrame:(CGRect)frame {
    
    firstView = [[UIView alloc] init];
    [firstView setFrame:frame];
    [firstView.layer setBorderWidth:1.5f];
    [firstView setBackgroundColor:[UIColor clearColor]];
    [firstView setUserInteractionEnabled:YES];
    [firstView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self.view addSubview:firstView];
    
    UILabel *reportLabel = [[UILabel alloc]init];
    [reportLabel setFrame:CGRectMake(firstView.bounds.origin.x + 10, firstView.bounds.origin.y+10, firstView.bounds.size.width/3, 25)];
    reportLabel.tag = 4000;
    reportLabel.text = @"Report Type";
    [reportLabel setTextColor:[UIColor blackColor]];
    reportLabel.backgroundColor = [UIColor clearColor];
    [reportLabel setTextAlignment:NSTextAlignmentLeft];
    reportLabel.font = [UIFont boldSystemFontOfSize:15.0];
    [firstView addSubview:reportLabel];
    
    CGFloat scrollViewScrollHeight = 0.0f;
    
    VSReportDisplaySetting *settingModal = [self.dataModalArray objectAtIndex:0];
    NSMutableArray *checkedArray = (NSMutableArray *)settingModal.reportTypeIdList;
    
    Boolean checkedValue = NO;

   
    for (int i = 0; i < self.reportTypeArray.count; i++) {
        
        VSCommanReportModal *commanModal = [self.reportTypeArray objectAtIndex:i];
        

        CGRect frame = (IS_IPHONE_5) ? CGRectMake(firstView.bounds.origin.x + 20, self.scrollView.bounds.origin.y + 5 + (i * (kCheckBlackBG1.size.height + 8)) , 250, kCheckBlackBG1.size.height) : (IS_IPHONE_4S) ? CGRectMake(firstView.bounds.origin.x + 20, self.scrollView.bounds.origin.y + (i * (kCheckBlackBG1.size.height - 1)) , 250, kCheckBlackBG1.size.height) : CGRectMake(firstView.bounds.origin.x + 20, firstView.bounds.origin.y + 5 + (i * (kCheckBlackBG1.size.height + 8)), 250, kCheckBlackBG1.size.height);
        
       NSLog(@"1212121222222 - %@", commanModal.reportTypeKey);
        int searchkey = [checkedArray containsObject:commanModal.reportTypeKey];

        NSLog(@"checkedArraycheckedArraycheckedArraycheckedArraycommanModal - %@ searchkey - %d", commanModal.reportTypeKey, searchkey);

        for (NSString * checked in checkedArray) {
            checkedValue = ([checked intValue] == [commanModal.reportTypeKey intValue]) ? YES : NO;
            //NSLog(@"checkedArray--checkedValue %d", checkedValue);
        }

        NSLog(@"checkedValue %d", checkedValue);
        
        SSCheckBoxView *checkBox;
        checkBox = [[SSCheckBoxView alloc] initWithFrame:frame style:kSSCheckBoxViewStyleMono checked:checkedValue];
        [checkBox setTag:i];
        [checkBox setText:commanModal.reportTypeValue];
        [checkBox setStateChangedBlock:^(SSCheckBoxView *check) {
            
            VSCommanReportModal *commanModal11 = [self.reportTypeArray objectAtIndex:check.tag];
            (check.checked) ? [self.checkBoxSelectedArray addObject:commanModal11.reportTypeKey] : [self.checkBoxSelectedArray removeObject:commanModal11.reportTypeKey];
        }];
        
        [self.scrollView addSubview:checkBox];
        
        scrollViewScrollHeight = scrollViewScrollHeight + kCheckBlackBG1.size.height;
    }
    [self.scrollView setContentSize:CGSizeMake(0.0, 35 + scrollViewScrollHeight)];
}

-(void)addSecondViewOnFrame:(CGRect)frame {
    
    NSArray *textArray = @[@"Report Range",@"Report Time Range"];
    NSMutableArray *textArray1 = [NSMutableArray arrayWithArray:@[@"1",@"Day"]];

    VSReportDisplaySetting *modalClass;
    if (self.dataModalArray.count>0) {
        
        modalClass = [self.dataModalArray firstObject];
        [textArray1 removeAllObjects];
        [textArray1 addObject:(modalClass.reportTime)?[NSString stringWithFormat:@"%@",modalClass.reportTime]:@"1"];
        [textArray1 addObject:(modalClass.timeUnitDictionary)?[modalClass.timeUnitDictionary objectForKey:[NSString stringWithFormat:@"%@",modalClass.timeUnitId]]:@"1"];
    }
    
    secondView = [[UIView alloc]init];
    [secondView setFrame:frame];
    [secondView.layer setBorderWidth:1.0f];
    [secondView setBackgroundColor:[UIColor clearColor]];
    [secondView setUserInteractionEnabled:YES];
    [secondView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self.view addSubview:secondView];
    
    for (int i = 0; i < 2; i++) {
        
        UILabel *reportLabel = [[UILabel alloc]init];
        [reportLabel setFrame:(IS_IPHONE_5) ? CGRectMake(secondView.bounds.origin.x+10, secondView.bounds.origin.y + 10 +(i *70), secondView.bounds.size.width/3+40, 25):(IS_IPHONE_4S) ? CGRectMake(secondView.bounds.origin.x+10, secondView.bounds.origin.y + 10 +(i *70), secondView.bounds.size.width/3+40, 25):CGRectMake(secondView.bounds.origin.x+10, secondView.bounds.origin.y + 10 +(i *70), secondView.bounds.size.width/3+20, 25)];
        reportLabel.text = textArray[i];
        reportLabel.tag = 4000 + i;
        [reportLabel setTextColor:[UIColor blackColor]];
        reportLabel.backgroundColor = [UIColor clearColor];
        [reportLabel setTextAlignment:NSTextAlignmentLeft];
        reportLabel.font = [UIFont boldSystemFontOfSize:15.0];
        [secondView addSubview:reportLabel];
    }
    
    UITextField *textField = [[UITextField alloc] init];
    [textField setFrame:CGRectMake(secondView.bounds.origin.x + 30, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3 + 10, 35)];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:@"0.0"];
    [textField setTag:2225];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentCenter];
    [textField setFont:[UIFont systemFontOfSize:15.0f]];
    [textField setKeyboardType:UIKeyboardTypeDecimalPad];
    [textField setBackgroundColor:[UIColor lightGrayColor]];
    [textField setText:[NSString stringWithFormat:@"%@",modalClass.reportRange]];
    [secondView addSubview:textField];
    
    UIButton *btnMeterSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnMeterSelect.frame = (IS_IPHONE_5) ? CGRectMake(secondView.bounds.origin.x +secondView.bounds.size.width/3+70, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3 + 10, 35):(IS_IPHONE_4S) ? CGRectMake(secondView.bounds.origin.x + secondView.bounds.size.width/3+70, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3 + 10, 35) : CGRectMake(secondView.bounds.origin.x+secondView.bounds.size.width/3 + 80, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3 + 10, 35);
    
    btnMeterSelect.tag = 72000;
    [btnMeterSelect setBackgroundColor:[UIColor lightGrayColor]];
    btnMeterSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
    [btnMeterSelect setTitle:(modalClass)?[modalClass.distanceUnitDictionary objectForKey:[NSString stringWithFormat:@"%@",modalClass.distanceUnitId]]:@"Meter"  forState:UIControlStateNormal];
    [btnMeterSelect.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [btnMeterSelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnMeterSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnMeterSelect addTarget:self action:@selector(selectMeterClicked:) forControlEvents:UIControlEventTouchUpInside];
    [secondView addSubview:btnMeterSelect];
    
    for (int i = 0; i < 2; i++) {
        
        UIButton *btnDaySelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnDaySelect.frame = (IS_IPHONE_5) ? CGRectMake(secondView.bounds.origin.x + 25 + (i * 150), secondView.bounds.origin.y + 115, secondView.bounds.size.width/3 + 10, 35): (IS_IPHONE_4S) ? CGRectMake(secondView.bounds.origin.x + 25 + (i * 150), secondView.bounds.origin.y + 110, secondView.bounds.size.width/3 + 10, 35) : CGRectMake(secondView.bounds.origin.x + 35 + (i * 163), secondView.bounds.origin.y + 110, secondView.bounds.size.width/3 + 10, 35);
        btnDaySelect.tag = 9000 + i;
        [btnDaySelect setBackgroundColor:[UIColor lightGrayColor]];
        btnMeterSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
        [btnDaySelect setTitle:textArray1[i] forState:UIControlStateNormal];
        [btnDaySelect.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
        [btnDaySelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btnDaySelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [btnDaySelect addTarget:self action:@selector(selectSDayClicked:) forControlEvents:UIControlEventTouchUpInside];
        [secondView addSubview:btnDaySelect];
    }
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:4445];
    
    [button setFrame:(IS_IPHONE6PLUS) ? CGRectMake(secondView.bounds.origin.x + 35, secondView.bounds.origin.y + 160, kLoginBG.size.width+15, kLoginBG.size.height) : (IS_IPHONE_4S) ?  CGRectMake(secondView.bounds.origin.x + 25, secondView.bounds.origin.y + 155, kLoginBG.size.width-35, kLoginBG.size.height) : (IS_IPHONE_5) ? CGRectMake(secondView.bounds.origin.x + 10, secondView.frame.size.height - (kLoginBG.size.height + 8), secondView.frame.size.width - 20, kLoginBG.size.height) : CGRectMake(secondView.bounds.origin.x + 35, secondView.bounds.origin.y + 160, secondView.frame.size.width - 65, kLoginBG.size.height)];
    
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [button setTitle:@"Save" forState:UIControlStateNormal];
    [button setBackgroundImage:kLoginBG forState:UIControlStateNormal];
    [button addTarget:self action:@selector(saveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [secondView addSubview:button];
}

/*
 ************************************************************************************
 * UITextField delegate methods...
 ************************************************************************************
 */

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(range.length + range.location > textField.text.length) { return NO; }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 10;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self animateTextFieldAndView:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self animateTextFieldAndView:textField up:NO];
}

- (void)animateTextFieldAndView: (UITextField*) textField up:(BOOL) up {
    
    const int movementDistance = 150;
    const float movementDuration = 0.3f;
    
    int movement = (up ? - movementDistance : movementDistance);
    
    [UIView beginAnimations:@"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration:movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)selectMeterClicked:(UIButton *)sender {
    
    currentbuttonTag=sender.tag;
    [dropDown removeFromSuperview];
    
    NSMutableArray *distanceUnitArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i< self.meterArray.count; i++) {
        
        VSCommanReportModal *commanModal = [self.meterArray objectAtIndex:i];
        [distanceUnitArray addObject:commanModal.distanceUnitValue];
    }
    if(dropDown == nil) {
        
        CGFloat f = 100;
        dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:distanceUnitArray ImgArr:nil Direction:@"down"];
        dropDown.delegate = self;
    } else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

-(IBAction)selectSDayClicked:(UIButton *)sender {
    
    if (sender.tag == 9000) {
        
        currentbuttonTag = sender.tag;
        [dropDown removeFromSuperview];
        
        self.dayArray = [NSMutableArray arrayWithObjects:@"1", @"2", @"3",@"4", @"5", @"6",@"7", @"8", @"9",@"10", @"11", @"12",@"13", @"14", @"15",@"16", @"17", @"18",@"19", @"20", @"21",@"22", @"23", @"24",@"25", @"26", @"27",@"28", @"29", @"30",@"31", nil];
        
        if(dropDown == nil) {
            
            CGFloat f = 110;
            dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:self.dayArray ImgArr:nil Direction:@"up"];
            dropDown.delegate = self;
        } else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    } else {
        
        currentbuttonTag = sender.tag;
        [dropDown removeFromSuperview];
        
        NSMutableArray *dayUnitArray = [[NSMutableArray alloc] init];
        
        for (int i = 0; i< self.dayTextArray.count; i++) {
            
            VSCommanReportModal *commanModal = [self.dayTextArray objectAtIndex:i];
            [dayUnitArray addObject:commanModal.timeUnitValue];
        }
        
        if(dropDown == nil) {
            
            CGFloat f = 110;
            dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:dayUnitArray ImgArr:nil Direction:@"up"];
            dropDown.delegate = self;
        }else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [self rel];
}

-(void)rel {
    
    dropDown = nil;
}

-(void)getDataFromDropDownCell:(NSIndexPath *)index {
    
    VSCommanReportModal *commanModal;
    switch (currentbuttonTag) {
            
        case 72000:
            
            commanModal = [self.meterArray objectAtIndex:index.row];
            reportRange = [NSString stringWithFormat:@"%@", (self.meterArray) ? commanModal.distanceUnitKey : @"0"];
            break;
        case 9000:
            
            reportTypeRangeNumber = [NSString stringWithFormat:@"%@", (self.dayArray) ? [self.dayArray objectAtIndex:index.row] : @"0"];
            break;
        case 9001:
            
            commanModal = [self.dayTextArray objectAtIndex:index.row];
            reportTypecal = [NSString stringWithFormat:@"%@", (self.dayTextArray) ? commanModal.timeUnitKey : @"0"];
            break;
        default:
            break;
    }
}

-(IBAction)saveButtonAction:(UIButton *)sender {
    
    UITextField *textField = (UITextField *)[self.view viewWithTag:2225];
    NSString *joinedComponents = [self.checkBoxSelectedArray componentsJoinedByString:@","];
    
    //{"reportTypeIdString":"3,2,","reportRange":0.0,"reportTime":0.0,"reportTypeId":0,"distanceUnitId":1,"timeUnitId":3,"userId":3}

    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self saveReportDisplaySetting:[VSSingletonClass sharedMySingleton].loginUserId distanceUnitId:reportRange TimeUnitId:reportTypecal ReportTime:reportTypeRangeNumber ReportRange:(textField.text.length>0) ? textField.text : @"0.0" ReportTypeId:@"0" reportTypeIdString:joinedComponents];
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    [textField resignFirstResponder];
}

@end
