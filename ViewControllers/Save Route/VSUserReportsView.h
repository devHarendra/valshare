//
//  VSUserReportsView.h
//  ValShare
//
//  Created by Sharda Prasad on 4/20/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSHeader.h"
#import <UIKit/UIKit.h>
#import "VSReportModelClass.h"
@protocol VSUserReportsViewControllerDelegate <NSObject>

-(void)addCustomPinForReport:(NSString *)latitude Longitude:(NSString *)longitude HeadTitle:(NSString *)headtitle;
-(void)TicketDetailsFromMyReport:(NSString *)title withDescription:(NSString *)des infoModel:(VSReportModelClass*)infoModel;

@end
@interface VSUserReportsView : UIViewController <UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate>

@property (strong ,nonatomic) NSMutableArray *dataArray;
@property (strong ,nonatomic) UIButton *cancelButton;
@property (nonatomic, weak) id <VSUserReportsViewControllerDelegate>delegate;

@property (nonatomic, assign) bool isFiltered;
@property (strong, nonatomic) NSMutableArray* filteredTableData;

@end
