//
//  VSCompareRouteController.h
//  ValShare
//
//  Created by Sharda Prasad on 4/9/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "VSDirectionModal.h"

@interface VSCompareRouteController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSMutableArray *dateArray;

@end
