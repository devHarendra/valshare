//
//  VSCompareRouteController.m
//  ValShare
//
//  Created by Sharda Prasad on 4/9/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCompareRouteController.h"

@interface VSCompareRouteController (){
    UITableView *routeTable;
}

@end

@implementation VSCompareRouteController
@synthesize dateArray = _dateArray;

-(NSMutableArray *)dateArray {
    
    if (!_dateArray) {
        _dateArray = [[NSMutableArray alloc] init];
    }
    return _dateArray;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.title = [NSString stringWithFormat:@"%@", @"Compare Route"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];

    [self.view setBackgroundColor:[UIColor whiteColor]];
    // Do any additional setup after loading the view.
    
    [self addComponentOnView];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    [self addTableOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y + 10, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 5) Tag:2450];
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    routeTable = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    [routeTable setTag:tag];
    [routeTable setDelegate:(id)self];
    [routeTable setDataSource:(id)self];
    [routeTable setBackgroundColor:[UIColor clearColor]];
    [routeTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [routeTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:routeTable];
}

/*
 *******************************************************************************************
 * TableView DataSource and Delegate methods.
 *******************************************************************************************
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 120;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [self.dateArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    VSSaveRouteCell *cell = (VSSaveRouteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[VSSaveRouteCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setBackgroundColor:[UIColor clearColor]];
    }

    VSDirectionModal *routeModal = [self.dateArray objectAtIndex:indexPath.row];
    [cell.saveButton setHidden:YES];

    [cell.routeText setText:@"Route : "];
    [cell.timeText setText:@"Time : "];
    [cell.distanceText setText:@"Distance : "];
    [cell.viaRouteText setText:@"Via : "];
    
    if (cell.saveButton.hidden) {
        [cell.distance setFrame:CGRectMake(85.0f, 30.0f, 250, 40.0f)];
    }
    [cell.route setText:[NSString stringWithFormat:@"%@", routeModal.localRouteRouteIndexId]];
    [cell.time setText:[NSString stringWithFormat:@"%@", routeModal.localRouteDuration]];
    [cell.distance setText:[NSString stringWithFormat:@"%@",routeModal.localRouteDistance]];
    [cell.viaRoute setText:[NSString stringWithFormat:@"%@",routeModal.localRouteSource]];
    
    
    return cell;
}

@end
