//
//  VSFeedBackViewController.m
//  ValShare
//
//  Created by Vivek Kumar on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSFeedBackViewController.h"

@interface VSFeedBackViewController ()

@end
@implementation VSFeedBackViewController

-(void)postFeedbackOnServer:(NSString *)feedback RadioButtonID:(NSString *)radiobuttonid UserId:(NSString *)userid {
    
    NSDictionary *dictData = [[NSMutableDictionary alloc] init];
    [dictData setValue:feedback forKey:@"message"];
    [dictData setValue:[NSString stringWithFormat:@"%@",radiobuttonid] forKey:@"feedbackTypeId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[userid intValue]] forKey:@"userId"];
    
    [[VSParserClass sharedParser]valShareFeedback:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSMutableDictionary *loginDict = (NSMutableDictionary *)resultObject;
            
            if ([[loginDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                switch ([radiobuttonid integerValue]) {
                    case 1:
                        [[[UIAlertView alloc] initWithTitle:nil message:@"Your bug report has been send succesfully." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                        break;
                    case 2:
                        [[[UIAlertView alloc] initWithTitle:nil message:@"Your Feedback has been send succesfully." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                        break;
                    case 3:
                        [[[UIAlertView alloc] initWithTitle:nil message:@"Your contact request send successfully.\n We will respond you shortly." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                        break;
                        
                    default:
                        break;
                }
                
            }else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
            
        }else {
            [SVProgressHUD dismiss];
            
            [[[UIAlertView alloc] initWithTitle:nil message:[resultObject valueForKey:@"message"]delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];

        }
        
    }];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationItem setTitle:@"Feedback"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    [self addComponentOnView];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    NSArray *textArray = @[@"Bug Report",@"Feedback",@"Contact"];
    
    for (int i =0; i < 3; i++) {
        
        [self addRadioButtonOnview:CGRectMake([UIScreen mainScreen].bounds.size.width/2+6 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y+57+(i * 30), kRadioButtonBG.size.width + 100, kRadioButtonBG.size.height) Tag:2000+i Title:textArray[i]];
    }
    [self addTextViewOnFrame:(IS_IPHONE_5) ?CGRectMake([UIScreen mainScreen].bounds.size.width/2+6 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y+145, [UIScreen mainScreen].bounds.size.width/2+123, 80):CGRectMake([UIScreen mainScreen].bounds.size.width/2+6 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y+145, [UIScreen mainScreen].bounds.size.width/2+100, 80)];
    
    [self addButtonOnView:(IS_IPHONE6PLUS) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2+6 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y+240, kLoginBG.size.width+20, kLoginBG.size.height):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2+6 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y+240, kLoginBG.size.width-28, kLoginBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2+6 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y+240, kLoginBG.size.width-5, kLoginBG.size.height)  Title:@"Submit" Tag:96756 BGImage:kLoginBG];
    
    QRadioButton *radioButton = (QRadioButton *)[self.view viewWithTag:2000];
    [radioButton setChecked:YES];
    
}

-(void)addRadioButtonOnview:(CGRect)frame Tag:(int)tag Title:(NSString *)text {
    
    QRadioButton *rUserName = [[QRadioButton alloc] initWithDelegate:self groupId:@"groupId1"];
    [rUserName setFrame:frame];
    [rUserName setTag:tag];
    [rUserName setTitle:text forState:UIControlStateNormal];
    [rUserName setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rUserName.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [self.view addSubview:rUserName];
}

- (void)addTextViewOnFrame:(CGRect)frame {
    
    UITextView *textView = [[UITextView alloc]init];
    [textView setFrame:frame];
    [textView setDelegate:(id)self];
    [textView setText:@"Feedback"];
    [textView setTag:2010];
    [textView.layer setBorderWidth:0.5];
    [textView setTextColor:[UIColor blackColor]];
    [textView setBackgroundColor:[UIColor whiteColor]];
    [textView setFont:[UIFont systemFontOfSize:15.0f]];
    [textView setBackgroundColor:[UIColor whiteColor]];
    [textView setTextAlignment:NSTextAlignmentLeft];
    [textView setContentInset:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
    [self.view addSubview:textView];
}


-(void)addButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(submitButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ((textView.text =  @"Feedback")) {
        textView.text = @"";
    }
    
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Feedback";
    }
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


-(IBAction)submitButtonAction:(id)sender {
    
    UITextView *feedback = (UITextView *)[self.view viewWithTag:2010];
    QRadioButton *radiobutton1 = (QRadioButton *)[self.view viewWithTag:2000];
    QRadioButton *radiobutton2 = (QRadioButton *)[self.view viewWithTag:2001];
    QRadioButton *radiobutton3 = (QRadioButton *)[self.view viewWithTag:2002];
    
    NSLog(@"radiobutton1:%@",radiobutton1.titleLabel.text);
    
    if ([feedback.text isEqualToString:@"Feedback"]) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please fill feedback" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            
            [self postFeedbackOnServer:feedback.text RadioButtonID:(radiobutton2.checked == 1)? @"2":(radiobutton3.checked == 1)? @"3":@"1" UserId:[VSSingletonClass sharedMySingleton].loginUserId];
        }
        
        else {  [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        
        UITextView *textView = (UITextView*)[self.view viewWithTag:2010];
        [textView setText:@"Feedback"];
        [textView resignFirstResponder];
    }
}
@end
