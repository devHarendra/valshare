//
//  VSPurchesHistoryView.m
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSPurchesHistoryView.h"

@interface VSPurchesHistoryView () {
    VSHistoryModelClass *modelClass;
}

@end

@implementation VSPurchesHistoryView
@synthesize listModel = _listModel;
@synthesize dateArray = _dateArray;
@synthesize purchesHistoryArray = _purchesHistoryArray;

-(NSMutableArray *)dateArray {
    
    if (!_dateArray) {
        _dateArray = [[NSMutableArray alloc] init];
    }
    return _dateArray;
}
-(NSMutableArray *)purchesHistoryArray {
    
    if (!_purchesHistoryArray) {
        _purchesHistoryArray = [[NSMutableArray alloc] init];
    }
    return _purchesHistoryArray;
}

-(void)getPurchesHistoryListFromServer:(NSString *)purchaseId {
    
    [[VSParserClass sharedParser]getPurchesHistory:purchaseId WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSLog(@"resultObjectresultObjectresultObjectresultObject:-%@",resultObject);
        NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
        
        if ([dictdata isKindOfClass:[NSDictionary class]]) {
            
            NSLog(@"NSDictionaryNSDictionaryNSDictionary%@",dictdata);
            modelClass = [[VSHistoryModelClass alloc]init];
            [modelClass setCardNumber:([dictdata valueForKey:@"cardNumber"]) ? [dictdata valueForKey:@"cardNumber"] : @""];
            [modelClass setCashPrice:([dictdata valueForKey:@"cashPrice"]) ? [dictdata valueForKey:@"cashPrice"] : @""];
            [modelClass setCity:([dictdata valueForKey:@"city"]) ? [dictdata valueForKey:@"city"] : @""];
            [modelClass setCountry:([dictdata valueForKey:@"country"]) ? [dictdata valueForKey:@"country"] : @""];
            [modelClass setCreatedTime:([dictdata valueForKey:@"createdTime"]) ? [dictdata valueForKey:@"createdTime"] : @""];
            [modelClass setEffectiveFrom:([dictdata valueForKey:@"effectiveFrom"]) ? [dictdata valueForKey:@"effectiveFrom"] : @""];
            [modelClass setEffectiveTo:([dictdata valueForKey:@"effectiveTo"]) ? [dictdata valueForKey:@"effectiveTo"] : @""];
            [modelClass setLastModifiedTime:([dictdata valueForKey:@"lastModifiedTime"]) ? [dictdata valueForKey:@"lastModifiedTime"] : @""];
            [modelClass setLoyaltyPoint:([dictdata valueForKey:@"loyaltyPoint"]) ? [dictdata valueForKey:@"loyaltyPoint"] : @""];
            [modelClass setLoyaltyPointEquivalent:([dictdata valueForKey:@"loyaltyPointEquivalent"]) ? [dictdata valueForKey:@"loyaltyPointEquivalent"] : @""];
            [modelClass setPackageName:([dictdata valueForKey:@"packageName"]) ? [dictdata valueForKey:@"packageName"] : @""];
            [modelClass setPackagePrice:([dictdata valueForKey:@"packagePrice"]) ? [dictdata valueForKey:@"packagePrice"] : @""];
            [modelClass setPaymentPackageId:([dictdata valueForKey:@"paymentPackageId"]) ? [dictdata valueForKey:@"paymentPackageId"]: @""];
            [modelClass setPuchaseHistoryId:([dictdata valueForKey:@"puchaseHistoryId"]) ? [dictdata valueForKey:@"puchaseHistoryId"] : @""];
            [modelClass setPurcahseHistoryStatus:([dictdata valueForKey:@"purcahseHistoryStatus"]) ? [dictdata valueForKey:@"purcahseHistoryStatus"] : @""];
            [modelClass setPurcahseHistoryStatusId:([dictdata valueForKey:@"purcahseHistoryStatusId"]) ? [dictdata valueForKey:@"purcahseHistoryStatusId"] : @""];
            [modelClass setStreetAddress:([dictdata valueForKey:@"streetAddress"]) ? [dictdata valueForKey:@"streetAddress"] : @""];
            [modelClass setTransactionId:([dictdata valueForKey:@"transactionId"]) ? [dictdata valueForKey:@"transactionId"] : @""];
            [modelClass setUserId:([dictdata valueForKey:@"userId"]) ? [dictdata valueForKey:@"userId"] : @""];
            [modelClass setZipCode:([dictdata valueForKey:@"zipCode"]) ? [dictdata valueForKey:@"zipCode"] :@""];

            [self.dateArray addObject:modelClass];
            [SVProgressHUD dismiss];
        }else if ([dictdata isKindOfClass:[NSArray class]]) {
            [SVProgressHUD dismiss];
            NSLog(@"NSArray%@",dictdata);
        }else {
            [SVProgressHUD dismiss];
            NSLog(@"else%@",dictdata);
        }
        
        NSArray *detailsArray = @[[NSString stringWithFormat:@": %@",modelClass.transactionId],
                                  [NSString stringWithFormat:@": %@",modelClass.packageName],
                                  [NSString stringWithFormat:@": $ %@",modelClass.packagePrice],
                                  [NSString stringWithFormat:@": $ %@",modelClass.cashPrice],
                                  [NSString stringWithFormat:@": %@",modelClass.cardNumber],
                                  [NSString stringWithFormat:@": %@",modelClass.createdTime],
                                  [NSString stringWithFormat:@": %@",modelClass.purcahseHistoryStatus],[NSString stringWithFormat:@": %@",modelClass.loyaltyPoint],
                                  [NSString stringWithFormat:@": $ %@ (per loyalty point)",modelClass.loyaltyPointEquivalent]];
        
        NSArray *address = @[[NSString stringWithFormat:@": %@",modelClass.streetAddress],
                             [NSString stringWithFormat:@": %@",modelClass.city],
                             [NSString stringWithFormat:@": %@",modelClass.country],
                             [NSString stringWithFormat:@": %@",modelClass.zipCode]];
        
        [self addDataComponentOnView:detailsArray Address:address];
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setTitle:@"Purchase History Detail"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getPurchesHistoryListFromServer:self.listModel.puchaseHistoryId];
    }else {
        [[[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    [self adddComponentOnView];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adddComponentOnView {
    
    
    NSArray *textArray = @[@"Purchase Detail",@"Billing address"];
    NSArray *localArray = @[@"Transaction ID",@"Package Name",@"Package Price",@"Cash Price",@"Card Number",@"Purchase Date",@"Status",@"Loyalty point",@"Loyalty Equivalent"];
    NSArray *localAddress = @[@"Street Address",@"City",@"Country",@"Zip Code"];

    for (int i = 0; i<2; i++) {
        
        [self addLocalTextLabel:CGRectMake([UIScreen mainScreen].bounds.origin.x + 10, [UIScreen mainScreen].bounds.origin.y + 10 + (([UIScreen mainScreen].bounds.size.height/2 - 25) * i), 120, 20) Tag:5435 Text:textArray[i]];
    }
    
    for (int i = 0; i< 9; i++) {
        [self addLocalTextLabel:CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.origin.y + 38 + (25 * i), 125, 20) Tag:45634 + i  Text:localArray[i]];
    }
    
    for (int i = 0; i< 4; i++) {
        [self addLocalTextLabel:CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.size.height/2 + 15 + (25 * i), 110, 20) Tag:45634 + i  Text:localAddress[i]];
    }
    
    [self addSubmitButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.size.height - 120, kLoginBG.size.width, kLoginBG.size.height)  Title:@"Download" Tag:88909 BGImage:kLoginBG];
}

-(void)addDataComponentOnView:(NSArray *)detailsArray Address:(NSArray *)address {
    
    for (int i = 0; i< 9; i++) {
        
        [self addDataTextLabel:CGRectMake([UIScreen mainScreen].bounds.origin.x + 150, [UIScreen mainScreen].bounds.origin.y + 38  + (25 * i), 160, 20) Tag:45614 + i   Text:detailsArray[i]];
    }
    for (int i = 0; i< 4; i++) {
        
        [self addDataTextLabel:CGRectMake([UIScreen mainScreen].bounds.origin.x + 150, [UIScreen mainScreen].bounds.size.height/2 + 15 + (25 * i), 160, 20) Tag:45614 + i   Text:address[i]];
    }
}

-(void)addLocalTextLabel:(CGRect)frame Tag:(int)tag Text:(NSString *)text {
    
    UILabel *label = [[UILabel alloc] init];
    [label setText:text];
    [label setFrame:frame];
    [label setTextColor:[UIColor blackColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:(tag == 5435) ? [UIFont boldSystemFontOfSize:14.0f] : [UIFont systemFontOfSize:13.0f]];
    [self.view addSubview:label];
}

-(void)addDataTextLabel:(CGRect)frame Tag:(int)tag Text:(NSString *)text {
    
    UILabel *label = [[UILabel alloc] init];
    [label setText:text];
    [label setFrame:frame];
    [label setTextColor:[UIColor blackColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont systemFontOfSize:12.0f]];
    [self.view addSubview:label];
}

-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(downloadButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(IBAction)downloadButtonAction:(UIButton *)sender {
 
    [sender setHidden:YES];
    [self saveScreenshotToPhotosAlbum:self.view];
    [sender setHidden:NO];
    [VSEnumClass showAlertWithTitle:nil msg:@"Your purchase history download in photo library."];
}

- (void)saveScreenshotToPhotosAlbum:(UIView *)view
{
    UIImageWriteToSavedPhotosAlbum([self captureView:self.view], nil, nil, nil);
}

- (UIImage*)captureView:(UIView *)view
{
    CGRect rect = [[UIScreen mainScreen] bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end
