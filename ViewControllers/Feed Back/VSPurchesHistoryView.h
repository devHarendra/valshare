//
//  VSPurchesHistoryView.h
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "VSPurchesHistoryListModel.h"


@interface VSPurchesHistoryView : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) NSMutableArray *dateArray;
@property (nonatomic,strong) NSMutableArray *purchesHistoryArray;
@property (nonatomic,strong) VSPurchesHistoryListModel *listModel;

@end
