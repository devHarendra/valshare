//
//  VSReportDisplayView.m
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSReportDisplayView.h"

@interface VSReportDisplayView (){
    
    int  currentbuttonTag;
    
}

@end

@implementation VSReportDisplayView
@synthesize dayArray = _dayArray;
@synthesize dayTextArray = _dayTextArray;
@synthesize meterArray = _meterArray;

-(NSMutableArray *)dayArray {
    
    if(!_dayArray) {
        _dayArray = [[NSMutableArray alloc] init];
    }
    return _dayArray;
}

-(NSMutableArray *)dayTextArray {
    
    if(!_dayTextArray) {
        _dayTextArray = [[NSMutableArray alloc] init];
    }
    return _dayTextArray;
}

-(NSMutableArray *)meterArray {
    
    if(!_meterArray) {
        _meterArray = [[NSMutableArray alloc] init];
    }
    return _meterArray;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationItem setTitle:@"Report Display Setting"];
    [self adddComponentOnView];
}

-(void)adddComponentOnView {
    
    if (IS_IPHONE_4S) {
        
        [self addFirstViewOnFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x + 10.0, [UIScreen mainScreen].bounds.origin.y + 30.0, [UIScreen mainScreen].bounds.size.width-17.0, [UIScreen mainScreen].bounds.size.height/2-100)];
        
        [self addSecondViewOnFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x + 10.0, [UIScreen mainScreen].bounds.origin.y + 70.0+[UIScreen mainScreen].bounds.size.height/2-130, [UIScreen mainScreen].bounds.size.width-17.0, [UIScreen mainScreen].bounds.size.height/2-37)];
        
        
    }else {
        
        [self addFirstViewOnFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x + 10.0, [UIScreen mainScreen].bounds.origin.y + 30.0, [UIScreen mainScreen].bounds.size.width-17.0, [UIScreen mainScreen].bounds.size.height/2-150)];
        
        [self addSecondViewOnFrame:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 10.0, [UIScreen mainScreen].bounds.origin.y + 60.0+[UIScreen mainScreen].bounds.size.height/2-170, [UIScreen mainScreen].bounds.size.width-17.0, [UIScreen mainScreen].bounds.size.height/2-80):CGRectMake([UIScreen mainScreen].bounds.origin.x + 10.0, [UIScreen mainScreen].bounds.origin.y + 70.0+[UIScreen mainScreen].bounds.size.height/2-180, [UIScreen mainScreen].bounds.size.width-17.0, [UIScreen mainScreen].bounds.size.height/2-125)];
        
    }
}

-(void)addFirstViewOnFrame:(CGRect)frame {
    
    NSArray *textArray = @[@"Under Construction",@"Accident",@"Parking",@"New Report Type"];
    
    UIView *firstView = [[UIView alloc]init];
    [firstView setFrame:frame];
    [firstView setBackgroundColor:[UIColor clearColor]];
    [firstView.layer setBorderWidth:1.5f];
    [firstView.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [self.view addSubview:firstView];
    
    UILabel *reportLabel = [[UILabel alloc]init];
    [reportLabel setFrame:CGRectMake(firstView.bounds.origin.x+10, firstView.bounds.origin.y+10, firstView.bounds.size.width/3, 25)];
    reportLabel.text = @"Report Type";
    reportLabel.tag = 4000;
    reportLabel.font = [UIFont systemFontOfSize:15.0];
    [reportLabel setTextColor:[UIColor whiteColor]];
    reportLabel.backgroundColor = [UIColor clearColor];
    [reportLabel setTextAlignment:NSTextAlignmentLeft];
    [firstView addSubview:reportLabel];
    
    for (int i = 0; i < 4; i++) {
        
        VSButton *button = [VSButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:(IS_IPHONE_5) ? CGRectMake(firstView.bounds.origin.x + 20, firstView.bounds.origin.y + 45 +(i *22) , kCheckboxBG.size.width, 17):(IS_IPHONE_4S) ?CGRectMake(firstView.bounds.origin.x + 20, firstView.bounds.origin.y + 45 +(i *22) , kCheckboxBG.size.width, 17):CGRectMake(firstView.bounds.origin.x + 20, firstView.bounds.origin.y + 45 +(i *35) , kCheckboxBG.size.width, 20)];
        [button setTag:2000+i];
        [button setBackgroundImage:kCheckboxBG forState:UIControlStateNormal];
        [button setBackgroundImage:kCheckedBG forState:UIControlStateSelected];
        [button addTarget:self action:@selector(selectCheckBoxButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [firstView addSubview:button];
    }
    
    for (int i = 0; i < 4 ; i++) {
        
        UILabel *textLabel = [[UILabel alloc]init];
        [textLabel setFrame:(IS_IPHONE_5) ? CGRectMake(firstView.bounds.origin.x+50, firstView.bounds.origin.y+38+(i *23), firstView.bounds.size.width/3+50, 25):(IS_IPHONE_4S) ? CGRectMake(firstView.bounds.origin.x+50, firstView.bounds.origin.y+38+(i *23), firstView.bounds.size.width/3+50, 25):CGRectMake(firstView.bounds.origin.x+50, firstView.bounds.origin.y+42+(i *35), firstView.bounds.size.width/3+50, 25)];
        textLabel.text = textArray[i];
        textLabel.tag = 1000+i;
        textLabel.font = (IS_IPHONE_5) ? [UIFont boldSystemFontOfSize:16.0]:(IS_IPHONE_4S) ? [UIFont boldSystemFontOfSize:16.0]:[UIFont boldSystemFontOfSize:18.0];
        [textLabel setTextColor:[UIColor whiteColor]];
        textLabel.backgroundColor = [UIColor clearColor];
        [textLabel setTextAlignment:NSTextAlignmentLeft];
        [firstView addSubview:textLabel];
    }
    
}

-(void)addSecondViewOnFrame:(CGRect)frame {
    
    NSArray *textArray = @[@"Report Range",@"Report Type Range"];
    NSArray *textArray1 = @[@"1",@"Day"];
    
    UIView *secondView = [[UIView alloc]init];
    [secondView setFrame:frame];
    [secondView setBackgroundColor:[UIColor clearColor]];
    [secondView.layer setBorderWidth:1.0f];
    [secondView.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [self.view addSubview:secondView];
    
    for (int i = 0; i < 2; i++) {
        
        UILabel *reportLabel = [[UILabel alloc]init];
        [reportLabel setFrame:(IS_IPHONE_5) ? CGRectMake(secondView.bounds.origin.x+10, secondView.bounds.origin.y + 10 +(i *70), secondView.bounds.size.width/3+40, 25):(IS_IPHONE_4S) ? CGRectMake(secondView.bounds.origin.x+10, secondView.bounds.origin.y + 10 +(i *70), secondView.bounds.size.width/3+40, 25):CGRectMake(secondView.bounds.origin.x+10, secondView.bounds.origin.y + 10 +(i *70), secondView.bounds.size.width/3+20, 25)];
        reportLabel.text = textArray[i];
        reportLabel.tag = 4000 + i;
        reportLabel.font = [UIFont systemFontOfSize:15.0];
        [reportLabel setTextColor:[UIColor whiteColor]];
        reportLabel.backgroundColor = [UIColor clearColor];
        [reportLabel setTextAlignment:NSTextAlignmentLeft];
        [secondView addSubview:reportLabel];
    }
    
    UITextView *textView = [[UITextView alloc]init];
    [textView setFrame:(IS_IPHONE_5) ? CGRectMake(secondView.bounds.origin.x+25, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3+10, 35):(IS_IPHONE_4S) ? CGRectMake(secondView.bounds.origin.x+25, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3+10, 35):CGRectMake(secondView.bounds.origin.x+35, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3+10, 35)];
    [textView setDelegate:(id)self];
    [textView setText:@"0.0"];
    [textView setTag:2225];
    [textView setTextColor:[UIColor blackColor]];
    [textView setBackgroundColor:[UIColor whiteColor]];
    [textView setFont:[UIFont systemFontOfSize:15.0f]];
    [textView setBackgroundColor:[UIColor whiteColor]];
    [textView setTextAlignment:NSTextAlignmentLeft];
    [textView setContentInset:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
    [secondView addSubview:textView];
    
    UIButton *btnMeterSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnMeterSelect.frame = (IS_IPHONE_5) ? CGRectMake(secondView.bounds.origin.x+secondView.bounds.size.width/3+70, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3+10, 35):(IS_IPHONE_4S) ? CGRectMake(secondView.bounds.origin.x+secondView.bounds.size.width/3+70, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3+10, 35):CGRectMake(secondView.bounds.origin.x+secondView.bounds.size.width/3+80, secondView.bounds.origin.y + 40, secondView.bounds.size.width/3+10, 35);
    btnMeterSelect.tag = 7000;
    [btnMeterSelect setBackgroundColor:[UIColor whiteColor]];
    btnMeterSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
    [btnMeterSelect setTitle:@"Meter" forState:UIControlStateNormal];
    [btnMeterSelect.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [btnMeterSelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnMeterSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnMeterSelect addTarget:self action:@selector(selectMeterClicked:) forControlEvents:UIControlEventTouchUpInside];
    [secondView addSubview:btnMeterSelect];
    
    for (int i = 0; i < 2; i++) {
        
        UIButton *btnDaySelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnDaySelect.frame = (IS_IPHONE_5) ? CGRectMake(secondView.bounds.origin.x + 25 + (i*148), secondView.bounds.origin.y + 110, secondView.bounds.size.width/3+10, 35): (IS_IPHONE_4S) ? CGRectMake(secondView.bounds.origin.x + 25 + (i*148), secondView.bounds.origin.y + 110, secondView.bounds.size.width/3+10, 35):CGRectMake(secondView.bounds.origin.x + 35 + (i*165), secondView.bounds.origin.y + 110, secondView.bounds.size.width/3+10, 35);
        btnDaySelect.tag = 9000+i;
        [btnDaySelect setBackgroundColor:[UIColor whiteColor]];
        btnMeterSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
        [btnDaySelect setTitle:textArray1[i] forState:UIControlStateNormal];
        [btnDaySelect.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
        [btnDaySelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btnDaySelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [btnDaySelect addTarget:self action:@selector(selectSDayClicked:) forControlEvents:UIControlEventTouchUpInside];
        [secondView addSubview:btnDaySelect];
        
    }
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:4445];
    [button setFrame:(IS_IPHONE6PLUS) ? CGRectMake(secondView.bounds.origin.x + 35, secondView.bounds.origin.y + 160, kLoginBG.size.width+15, kLoginBG.size.height):(IS_IPHONE_4S) ?  CGRectMake(secondView.bounds.origin.x + 25, secondView.bounds.origin.y + 155, kLoginBG.size.width-35, kLoginBG.size.height):(IS_IPHONE_5) ? CGRectMake(secondView.bounds.origin.x + 25, secondView.bounds.origin.y + 155, kLoginBG.size.width-35, kLoginBG.size.height):CGRectMake(secondView.bounds.origin.x + 35, secondView.bounds.origin.y + 160, kLoginBG.size.width+5, kLoginBG.size.height)];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [button setTitle:@"Save" forState:UIControlStateNormal];
    [button setBackgroundImage:kLoginBG forState:UIControlStateNormal];
    [button addTarget:self action:@selector(saveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [secondView addSubview:button];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ((textView.text =  @"0.0")) {
        textView.text = @"";
    }
    
    textView.textColor = [UIColor blackColor];
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"0.0";
    }
    
    textView.textColor = [UIColor lightGrayColor];
    [textView resignFirstResponder];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}



-(IBAction)selectCheckBoxButtonAction:(UIButton *)sender {
    
    if (sender.tag == 2000 ) {
        
        sender.selected = !sender.selected;
        
    } else if (sender.tag == 2001 ) {
        
        sender.selected = !sender.selected;
        
    } else if (sender.tag == 2002 ) {
        
        sender.selected = !sender.selected;
        
    }else {
        
        sender.selected = !sender.selected;
    }
    
}

-(IBAction)selectMeterClicked:(UIButton *)sender {
    
    currentbuttonTag=sender.tag;
    [dropDown removeFromSuperview];
    
    self.meterArray = [NSMutableArray arrayWithObjects:@"Meter", nil];
    if(dropDown == nil) {
        
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:self.meterArray ImgArr:nil Direction:@"down"];
        dropDown.delegate = self;
    } else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    
}

-(IBAction)selectSDayClicked:(UIButton *)sender {
    
    if (sender.tag == 9000) {
        
        currentbuttonTag=sender.tag;
        [dropDown removeFromSuperview];
        
        self.dayArray = [NSMutableArray arrayWithObjects:@"01", @"02", @"03",@"04", @"05", @"06",@"07", @"08", @"09",@"10", @"11", @"12",@"13", @"14", @"15",@"16", @"17", @"18",@"19", @"20", @"21",@"22", @"23", @"24",@"25", @"26", @"27",@"28", @"29", @"30",@"31", nil];
        if(dropDown == nil) {
            
            CGFloat f = 200;
            dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:self.dayArray ImgArr:nil Direction:@"down"];
            dropDown.delegate = self;
        } else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    } else {
        
        currentbuttonTag=sender.tag;
        [dropDown removeFromSuperview];
        
        self.dayTextArray = [NSMutableArray arrayWithObjects:@"Day", nil];
        if(dropDown == nil) {
            
            CGFloat f = 200;
            dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:self.dayTextArray ImgArr:nil Direction:@"down"];
            dropDown.delegate = self;
        }else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [self rel];
}

-(void)rel {
    dropDown = nil;
}

-(IBAction)saveButtonAction:(UIButton *)sender {
    
}

@end
