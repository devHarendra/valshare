//
//  VSAutoSearchTable.m
//  ValShare
//
//  Created by Sharda Prasad on 4/3/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSAutoSearchTable.h"

@implementation VSAutoSearchTable
@synthesize dataArray = _dataArray;


-(NSMutableArray *)dataArray {
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

-(void)initializeTableView{
    
    if (self) {
        self.dataSource = (id)self;
        self.delegate = (id)self;
        self.backgroundView = nil;
        self.scrollEnabled = YES;
    }
    //[self alphabeticalOrderBySellerName:self.shopArray];
}

-(void)alphabeticalOrderBySellerName:(NSMutableArray *)unsortedArray
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryName" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [unsortedArray sortedArrayUsingDescriptors:sortDescriptors];
    
    self.dataArray = (NSMutableArray *)sortedArray;
}

#pragma mark tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.dataArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 35;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static  NSString *editableCellIdentifier = @"itemEditCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:editableCellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:editableCellIdentifier];
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.textLabel setTextColor:[UIColor blackColor]];
        [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    [cell.textLabel setText:[NSString stringWithFormat:@"%@", [self.dataArray objectAtIndex:indexPath.row]]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.tableDelegate respondsToSelector:@selector(getTextFromTableViewCell:NSIndexPath:TableTag:)]) {
        [self.tableDelegate getTextFromTableViewCell:[self.dataArray objectAtIndex:indexPath.row] NSIndexPath:indexPath.row TableTag:tableView];
    }
    [self performSelector:@selector(deselect) withObject:nil afterDelay:.3];
}

- (void) deselect {
    
    [self deselectRowAtIndexPath:[self indexPathForSelectedRow] animated:YES];
}

@end
