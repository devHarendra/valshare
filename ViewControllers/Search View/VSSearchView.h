//
//  VSSearchView.h
//  ValShare
//
//  Created by Sharda Prasad on 4/2/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "VSAutoSearchTable.h"
#import "SearchLocationModel.h"

@class VSSearchDropDown;
@protocol searchViewDelegate <NSObject>

@optional
-(void)searchFieldText:(NSString *)sourceText Destination:(NSString *)destination;

@end

@interface VSSearchView : UIView <UITextFieldDelegate> {
    
}

@property (nonatomic, strong) UITextField   *sourceField;
@property (nonatomic, strong) UITextField   *destinationField;
@property (nonatomic, strong) VSButton      *searchButton;
@property (nonatomic, strong) VSButton      *cancelButton;
@property (nonatomic, strong) VSSearchDropDown  *searchDropDown;
@property (nonatomic, strong) VSAutoSearchTable *tableView;
@property (nonatomic, strong) NSString *currentLocation;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong)  NSMutableArray *searclLocationArray;
@property (nonatomic, strong) id <searchViewDelegate> searchViewDelegate;
@end
