//
//  VSSearchView.m
//  ValShare
//
//  Created by Sharda Prasad on 4/2/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSSearchView.h"
#import "VSSearchDropDown.h"
#import "VSAutoSearchTable.h"
#import "AppDelegate.h"
@implementation VSSearchView
@synthesize sourceField;
@synthesize destinationField;
@synthesize searchButton;
@synthesize cancelButton;
@synthesize dataArray = _dataArray;
@synthesize searclLocationArray = _searclLocationArray;
@synthesize searchDropDown = _searchDropDown;
@synthesize tableView = _tableView;

-(NSMutableArray *)searclLocationArray {
    
    if (!_searclLocationArray) {
        _searclLocationArray = [[NSMutableArray alloc] init];
    }
    return _searclLocationArray;
}

-(NSMutableArray *)dataArray {
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

-(void)getSearchDataFromGoogle:(NSString *)searchText Tag:(NSInteger)tag {
    
    [[VSParserClass sharedParser] volShareGetLocationBySearch:searchText WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        [self.searclLocationArray removeAllObjects];
        [self.dataArray removeAllObjects];
        
        if ([resultObject isKindOfClass:[NSDictionary class]]) {
            
            NSMutableDictionary *localdict = (NSMutableDictionary *)[resultObject objectForKey:@"predictions"];
            
            if ([localdict isKindOfClass:[NSArray class]]) {
                
                NSMutableArray *bdsjkfds = (NSMutableArray *)localdict;
                
                [bdsjkfds enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    
                    NSMutableDictionary *dictData = (NSMutableDictionary *)[bdsjkfds objectAtIndex:idx];
                    SearchLocationModel *locationModel = [[SearchLocationModel alloc]init];
                    [locationModel setLocation_name:[dictData valueForKey:@"description"]];
                    [self.searclLocationArray addObject:locationModel];
                }];
            }else if ([localdict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"localdictNSDictionary%@",localdict);
            }else {
            }
            
            for (SearchLocationModel *locationModel in self.searclLocationArray) {
                if (![self.dataArray containsObject:locationModel.location_name]) {
                    
                    [self.dataArray addObject:locationModel.location_name];
                }
            }
            
            UIView *localView = (UIView *)[self viewWithTag:777678];
            
            CGRect frame;
            int tagData;
            if (tag == 44543) {
                frame = (IS_IPHONE_5) ? CGRectMake(localView.frame.origin.x + 5, 180, localView.frame.size.width - 10, 170) : CGRectMake(localView.frame.origin.x + 5, 225, localView.frame.size.width - 10, 170);
                tagData = 144543;
            } else {
                
                frame = (IS_IPHONE_5) ? CGRectMake(localView.frame.origin.x + 5, 210, localView.frame.size.width - 10, 170) : CGRectMake(localView.frame.origin.x + 5, 270, localView.frame.size.width - 10, 170);
                tagData = 244543;
            }
            
            [self.tableView setHidden:NO];
            [self.tableView reloadData];
            [self.tableView setFrame:frame];
            [self.tableView setTag:tagData];
            
        }else if ([resultObject isKindOfClass:[NSArray class]]) {
            
            NSLog(@"da-------- :- %@", resultObject);
        }
    }];
}


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setUserInteractionEnabled:YES];
        
        UIView *localView = [[UIView alloc] init];
        [localView setTag:777678];
        [localView setBackgroundColor:[UIColor grayColor]];
        [localView setFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.size.height/2 - 170, [UIScreen mainScreen].bounds.size.width - 30, [UIScreen mainScreen].bounds.size.width/2)];
        [self addSubview:localView];
        
        self.sourceField = [[UITextField alloc] init];
        self.sourceField.tag = 44543;
        [self.sourceField setDelegate:(id)self];
        [self.sourceField setPlaceholder:_currentLocation];
        [self.sourceField setTextAlignment:NSTextAlignmentLeft];
        [self.sourceField setBackgroundColor:[UIColor whiteColor]];
        [self.sourceField setFont:[UIFont systemFontOfSize:14.0f]];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 15, 15)];
        [self.sourceField setLeftViewMode:UITextFieldViewModeAlways];
        [self.sourceField setLeftView:leftView];
        [localView addSubview:self.sourceField];
        
        self.destinationField = [[UITextField alloc] init];
        self.destinationField.tag = 55454;
        [self.destinationField setDelegate:(id)self];
        [self.destinationField setPlaceholder:@"Destination"];
        [self.destinationField setTextAlignment:NSTextAlignmentLeft];
        [self.destinationField setBackgroundColor:[UIColor whiteColor]];
        [self.destinationField setFont:[UIFont systemFontOfSize:14.0f]];
        UIView *leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 15, 15)];
        [self.destinationField setLeftViewMode:UITextFieldViewModeAlways];
        [self.destinationField setLeftView:leftView1];
        [localView addSubview:self.destinationField];
        
        self.searchButton = [VSButton buttonWithType:UIButtonTypeCustom];
        [self.searchButton setTitle:@"Search" forState:UIControlStateNormal];
        [self.searchButton setBackgroundImage:kLoginBG forState:UIControlStateNormal];
        [self.searchButton addTarget:self action:@selector(searchButtonActrion:) forControlEvents:UIControlEventTouchUpInside];
        [localView addSubview:self.searchButton];
    
        
        self.cancelButton = [[VSButton alloc] init];
        [self.cancelButton setFrame:CGRectMake(localView.frame.size.width-15, localView.frame.origin.y-15, 40, 40)];
        [self.cancelButton setBackgroundColor:[UIColor clearColor]];
        [self.cancelButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
        [self.cancelButton addTarget:self action:@selector(cancelButtonActrion:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.cancelButton];

        
        [self.sourceField setFrame:CGRectMake(5.0, 25, localView.frame.size.width - 10, 35)];
        [self.destinationField setFrame:CGRectMake(5.0, 70, localView.frame.size.width - 10, 35)];
        [self.searchButton setFrame:CGRectMake(5.0, 115, localView.frame.size.width - 10, 40)];
//        [button setFrame:CGRectMake(localView.frame.size.width - (kCancelBG.size.width/2 +10), - 25.0, kCancelBG.size.width, kCancelBG.size.height)];
        
        [self addTableViewOnView:frame DataArray:self.dataArray Tag:12345];
     /*
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSearchView:)];
        [tap setNumberOfTapsRequired:1];
        tap.cancelsTouchesInView = YES;
        [localView addGestureRecognizer:tap];
    */
    }
    return self;
}
-(void)setCurrentLocation:(NSString *)currentLocation{
    _currentLocation = currentLocation;
    [self.sourceField setPlaceholder:currentLocation];
}
-(void)removeSearchView:(UITapGestureRecognizer *)tap {
    
    [self removeFromSuperview];
    if (self.tableView) {
        [self.tableView setHidden:YES];
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        
    }
    return self;
}

-(void)setNeedsDisplay {
    
}

-(void)addTableViewOnView:(CGRect)frame DataArray:(NSMutableArray *)data Tag:(int)tag {
    
    self.tableView = [[VSAutoSearchTable alloc] initWithFrame:frame style:UITableViewStylePlain];
    self.tableView.tag = tag;
    self.tableView.hidden = YES;
    self.tableView.dataArray = data;
    self.tableView.scrollEnabled = YES;
    [self.tableView initializeTableView];
    [self.tableView setTableDelegate:(id)self];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    [[UIApplication sharedApplication].keyWindow addSubview:self.tableView];
}

-(void)getTextFromTableViewCell:(NSString *)text NSIndexPath:(NSInteger)index  TableTag:(UITableView *)tableView {
    
    if (tableView.tag == 144543) {
        [self.sourceField setText:text];
    } else {
        [self.destinationField setText:text];
    }
    [self.tableView setHidden:YES];
}

-(IBAction)searchButtonActrion:(id)sender {
    
    if ([self.sourceField.text length] == 0 && !_currentLocation) {
        [VSEnumClass showAlertWithTitle:nil msg:@"Please enter source location"];
    } else if ([self.destinationField.text length] == 0) {
        [VSEnumClass showAlertWithTitle:nil msg:@"Please enter destination location"];
    } else {
        
        if ([self.searchViewDelegate respondsToSelector:@selector(searchFieldText:Destination:)]) {
            if (self.sourceField.text.length>0) {
               [self.searchViewDelegate searchFieldText:self.sourceField.text Destination:self.destinationField.text];
            }else{
            [self.searchViewDelegate searchFieldText:_currentLocation Destination:self.destinationField.text];
            }
        }
        [self removeFromSuperview];
    }
}

-(IBAction)cancelButtonActrion:(id)sender {
    [self removeFromSuperview];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *stringData = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField.tag == 44543) {
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            
            [self getSearchDataFromGoogle:stringData Tag:textField.tag];

        }else {
            
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertview show];
        }
        
    } else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            
            [self getSearchDataFromGoogle:stringData Tag:textField.tag];

        }else {
            
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertview show];
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

@end
