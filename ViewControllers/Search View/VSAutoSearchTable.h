//
//  VSAutoSearchTable.h
//  ValShare
//
//  Created by Sharda Prasad on 4/3/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol autoSearchTableDelegate <NSObject>

@optional
-(void)getTextFromTableViewCell:(NSString *)text NSIndexPath:(NSInteger)index TableTag:(UITableView *)tableView;
@end

@interface VSAutoSearchTable : UITableView

-(void)initializeTableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property(nonatomic, assign) id <autoSearchTableDelegate> tableDelegate;

@end
