//
//  VSUpdateProfileCell.h
//  ValShare
//
//  Created by Vivek Kumar on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol updateProfileCell <NSObject>

@optional
-(void)shouldChangeCharactersInRangeMethodIsCalling:(NSString *)enteredText  textField:(UITextField *)text;
- (BOOL)textFieldShouldReturnKeyboard:(UITextField *)textField;

@end

@interface VSUpdateProfileCell : UITableViewCell <UITextFieldDelegate>

@property (strong,nonatomic) UILabel     *labelName;
@property (strong,nonatomic) UITextField *textFieldText;
@property (strong,nonatomic) UIButton    *dateButton;
@property (strong,nonatomic) UIButton    *monthButton;
@property (strong,nonatomic) UIButton    *yearButton;

@property (nonatomic, retain) id <updateProfileCell> updateProfileCellDelegate;

@end
