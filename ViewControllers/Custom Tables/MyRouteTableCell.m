//
//  MyRouteTableCell.m
//  VolShare
//
//  Created by Vivek Kumar on 4/3/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import "MyRouteTableCell.h"

@implementation MyRouteTableCell
@synthesize myId;
@synthesize source;
@synthesize destination;
@synthesize date;
@synthesize status;
@synthesize myFinalId;
@synthesize finalSource;
@synthesize finalDestination;
@synthesize finalDate;
@synthesize finalStatus;

@synthesize effectiveTo;
@synthesize purchesStatus;
@synthesize packagePrice;
@synthesize loyalityPoint;
@synthesize finalEffectiveTo;
@synthesize finalPurchesStatus;
@synthesize finalPackagePrice;
@synthesize finalLoyalityPoint;
@synthesize line;
@synthesize RouteIdForservice;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UILabel *myRouteId = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 5.0f, 110.0f, 20.0f)];
        [myRouteId setBackgroundColor:[UIColor clearColor]];
        [myRouteId setFont:[UIFont boldSystemFontOfSize:14]];
        [myRouteId setTextColor:[UIColor blackColor]];
        [self addSubview:myRouteId];
        myId = myRouteId;
        
        
        // this is to hold id for sending with service and hidden for users
        UILabel *idHolder = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 5.0f, 110.0f, 20.0f)];
        [idHolder setBackgroundColor:[UIColor clearColor]];
        [idHolder setFont:[UIFont boldSystemFontOfSize:14]];
        [idHolder setTextColor:[UIColor clearColor]];
        [idHolder setHidden:YES];
        [self addSubview:idHolder];
        RouteIdForservice = idHolder;

        
        
        
        UILabel *mySource = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 27.0f,110.0f, 20.0f)];
        [mySource setBackgroundColor:[UIColor clearColor]];
        [mySource setFont:[UIFont boldSystemFontOfSize:14]];
        [mySource setTextColor:[UIColor blackColor]];
        [self addSubview:mySource];
        source = mySource;
        
        UILabel *myDestination = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 49.0f, 110.0f, 20.0f)];
        [myDestination setBackgroundColor:[UIColor clearColor]];
        [myDestination setFont:[UIFont boldSystemFontOfSize:14]];
        [myDestination setTextColor:[UIColor blackColor]];
        [self addSubview:myDestination];
        destination = myDestination;
        
        UILabel *mydate = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 71.0f, 110, 20)];
        [mydate setBackgroundColor:[UIColor clearColor]];
        [mydate setTextColor:[UIColor blackColor]];
        [mydate setFont:[UIFont boldSystemFontOfSize:14]];
        [self addSubview:mydate];
        date = mydate;
        
        
        UILabel *myStatus = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 93.0f, 110, 20)];
        [myStatus setBackgroundColor:[UIColor clearColor]];
        [myStatus setTextColor:[UIColor blackColor]];
        [myStatus setFont:[UIFont boldSystemFontOfSize:14]];
        [self addSubview:myStatus];
        status = myStatus;
        
        UILabel *myFinalRouteId = [[UILabel alloc] initWithFrame:CGRectMake(120.0, 5.0f, self.frame.size.width/2 + 35, 20.0f)];
        [myFinalRouteId setBackgroundColor:[UIColor clearColor]];
        [myFinalRouteId setFont:[UIFont systemFontOfSize:14]];
        [myFinalRouteId setTextColor:[UIColor blackColor]];
        [self addSubview:myFinalRouteId];
        myFinalId = myFinalRouteId;
        
        UILabel *myFinalSource = [[UILabel alloc] initWithFrame:CGRectMake(120.0, 27.0f,self.frame.size.width/2 + 35, 20.0f)];
        [myFinalSource setBackgroundColor:[UIColor clearColor]];
        [myFinalSource setFont:[UIFont systemFontOfSize:14]];
        [myFinalSource setTextColor:[UIColor blackColor]];
        [self addSubview:myFinalSource];
        finalSource = myFinalSource;
        
        UILabel *myFinalDestination = [[UILabel alloc] initWithFrame:CGRectMake(120.0, 49.0f, self.frame.size.width/2 + 35, 20.0f)];
        [myFinalDestination setBackgroundColor:[UIColor clearColor]];
        [myFinalDestination setFont:[UIFont systemFontOfSize:14]];
        [myFinalDestination setTextColor:[UIColor blackColor]];
        [self addSubview:myFinalDestination];
        finalDestination = myFinalDestination;
        
        UILabel *myFinaldate = [[UILabel alloc] initWithFrame:CGRectMake(120.0, 71.0f,self.frame.size.width/2 + 35, 20)];
        [myFinaldate setBackgroundColor:[UIColor clearColor]];
        [myFinaldate setTextColor:[UIColor blackColor]];
        [myFinaldate setFont:[UIFont systemFontOfSize:14]];
        [self addSubview:myFinaldate];
        finalDate = myFinaldate;
        
        UILabel *myFinalStatus = [[UILabel alloc] initWithFrame:CGRectMake(120.0, 93.0f,self.frame.size.width/2 + 35, 20)];
        [myFinalStatus setBackgroundColor:[UIColor clearColor]];
        [myFinalStatus setTextColor:[UIColor blackColor]];
        [myFinalStatus setFont:[UIFont systemFontOfSize:14]];
        [self addSubview:myFinalStatus];
        finalStatus = myFinalStatus;
        
        // New table view cell component.

        
        UILabel *localEffective = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 91.0f, 100.0f, 20.0f)];
         [localEffective setTextColor:[UIColor blackColor]];
        [localEffective setFont:[UIFont boldSystemFontOfSize:14]];
        [self addSubview:localEffective];
        effectiveTo = localEffective;
        
        UILabel *localPurches = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 111.0f,100.0f, 20.0f)];
        [localPurches setBackgroundColor:[UIColor clearColor]];
        [localPurches setFont:[UIFont boldSystemFontOfSize:14]];
        [localPurches setTextColor:[UIColor blackColor]];
        [self addSubview:localPurches];
        purchesStatus = localPurches;
        
        UILabel *localPackagePrice = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 131.0f, 100.0f, 20.0f)];
        [localPackagePrice setBackgroundColor:[UIColor clearColor]];
        [localPackagePrice setFont:[UIFont boldSystemFontOfSize:14]];
        [localPackagePrice setTextColor:[UIColor blackColor]];
        [self addSubview:localPackagePrice];
        packagePrice = localPackagePrice;
        
        UILabel *localLoility = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 151.0f, 100, 20)];
        [localLoility setBackgroundColor:[UIColor clearColor]];
        [localLoility setTextColor:[UIColor blackColor]];
        [localLoility setFont:[UIFont boldSystemFontOfSize:14]];
        [self addSubview:localLoility];
        loyalityPoint = localLoility;
        
        UILabel *localFinalEffective = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 91.0f,  self.frame.size.width/2 + 30, 20.0f)];
        [localFinalEffective setBackgroundColor:[UIColor clearColor]];
        [localFinalEffective setFont:[UIFont systemFontOfSize:14]];
        [localFinalEffective setTextColor:[UIColor blackColor]];
        [self addSubview:localFinalEffective];
        finalEffectiveTo = localFinalEffective;
        
        UILabel *localFinalPurchage = [[UILabel alloc] initWithFrame:CGRectMake(125.0f, 111.0f, self.frame.size.width/2 + 30, 20.0f)];
        [localFinalPurchage setBackgroundColor:[UIColor clearColor]];
        [localFinalPurchage setFont:[UIFont systemFontOfSize:14]];
        [localFinalPurchage setTextColor:[UIColor blackColor]];
        [self addSubview:localFinalPurchage];
        finalPurchesStatus = localFinalPurchage;
        
        UILabel *localFinalPackage = [[UILabel alloc] initWithFrame:CGRectMake(125.0f, 131.0f,  self.frame.size.width/2 + 30, 20.0f)];
        [localFinalPackage setBackgroundColor:[UIColor clearColor]];
        [localFinalPackage setFont:[UIFont systemFontOfSize:14]];
        [localFinalPackage setTextColor:[UIColor blackColor]];
        [self addSubview:localFinalPackage];
        finalPackagePrice = localFinalPackage;
        
        UILabel *localFinalLoyality = [[UILabel alloc] initWithFrame:CGRectMake(125.0, 151.0f,  self.frame.size.width/2 + 30, 20)];
        [localFinalLoyality setBackgroundColor:[UIColor clearColor]];
        [localFinalLoyality setTextColor:[UIColor blackColor]];
        [localFinalLoyality setFont:[UIFont systemFontOfSize:14]];
        [self addSubview:localFinalLoyality];
        finalLoyalityPoint = localFinalLoyality;
        
        UIView *lineView = [[UIView alloc]init];
        [lineView setFrame:CGRectMake(0.0, 178.0f, [UIScreen mainScreen].bounds.size.width, 1.0)];
        [lineView setBackgroundColor:[UIColor lightGrayColor]];
        [self addSubview:lineView];
        line = lineView;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setNeedsLayout {
    
}

@end
