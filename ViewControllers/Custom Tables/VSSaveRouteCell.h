//
//  VSSaveRouteCell.h
//  ValShare
//
//  Created by Vivek Kumar on 4/6/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VSSaveRouteCell : UITableViewCell

@property (strong,nonatomic) UILabel *route;
@property (strong,nonatomic) UILabel *time;
@property (strong,nonatomic) UILabel *distance;
@property (strong,nonatomic) UILabel *viaRoute;

@property (strong,nonatomic) UILabel *routeText;
@property (strong,nonatomic) UILabel *timeText;
@property (strong,nonatomic) UILabel *distanceText;
@property (strong,nonatomic) UILabel *viaRouteText;

@property (strong,nonatomic) UIButton *saveButton;


@end
