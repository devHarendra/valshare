//
//  VehicleViewController.h
//  VolShare
//
//  Created by Vivek Kumar on 4/3/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VehicleTableCell : UITableViewCell

@property (strong,nonatomic) UILabel *type;
@property (strong,nonatomic) UILabel *name;
@property (strong,nonatomic) UILabel *registrationNo;
@property (strong,nonatomic) UILabel *date;

@property (strong,nonatomic) UILabel *finalType;
@property (strong,nonatomic) UILabel *finalName;
@property (strong,nonatomic) UILabel *finalRegistrationNo;
@property (strong,nonatomic) UILabel *finalDate;

@end
