//
//  VehicleViewController.m
//  VolShare
//
//  Created by Vivek Kumar on 4/3/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import "VehicleTableCell.h"

@implementation VehicleTableCell
@synthesize  type;
@synthesize  name;
@synthesize  registrationNo;
@synthesize  date;
@synthesize  finalType;
@synthesize  finalName;
@synthesize  finalRegistrationNo;
@synthesize  finalDate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UILabel *myType = [[UILabel alloc]initWithFrame:CGRectMake(15.0f, 5.0f, 120.0f, 20.0f)];
        [myType setBackgroundColor:[UIColor clearColor]];
        [myType setFont:[UIFont boldSystemFontOfSize:14]];
        [myType setTextColor:[UIColor blackColor]];
        [self addSubview:myType];
        type = myType;
        
        UILabel *myName = [[UILabel alloc]initWithFrame:CGRectMake(15.0f, 30.0f,120.0f, 20.0f)];
        [myName setBackgroundColor:[UIColor clearColor]];
        [myName setFont:[UIFont boldSystemFontOfSize:14]];
        [myName setTextColor:[UIColor blackColor]];
        [self addSubview:myName];
        name = myName;
        
        UILabel *myRegistrationNo = [[UILabel alloc]initWithFrame:CGRectMake(15.0f, 52.0f, 120.0f, 20.0f)];
        [myRegistrationNo setBackgroundColor:[UIColor clearColor]];
        [myRegistrationNo setFont:[UIFont boldSystemFontOfSize:14]];
        [myRegistrationNo setTextColor:[UIColor blackColor]];
        [self addSubview:myRegistrationNo];
        registrationNo = myRegistrationNo;
        
        UILabel *mydate = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 74.0f, 120, 20)];
        [mydate setBackgroundColor:[UIColor clearColor]];
        [mydate setTextColor:[UIColor blackColor]];
        [mydate setFont:[UIFont boldSystemFontOfSize:14]];
        [self addSubview:mydate];
        date = mydate;
        
        UILabel *myFinalType = [[UILabel alloc]initWithFrame:CGRectMake(135.0f, 5.0f, 150.0f, 20.0f)];
        [myFinalType setBackgroundColor:[UIColor clearColor]];
        [myFinalType setFont:[UIFont systemFontOfSize:14]];
        [myFinalType setTextColor:[UIColor blackColor]];
        [self addSubview:myFinalType];
        finalType = myFinalType;
        
        UILabel *myFinalName = [[UILabel alloc]initWithFrame:CGRectMake(135.0f, 30.0f,150.0f, 20.0f)];
        [myFinalName setBackgroundColor:[UIColor clearColor]];
        [myFinalName setFont:[UIFont systemFontOfSize:14]];
        [myFinalName setTextColor:[UIColor blackColor]];
        [self addSubview:myFinalName];
        finalName = myFinalName;
        
        UILabel *myFinalRegistrationNo = [[UILabel alloc]initWithFrame:CGRectMake(135.0f, 52.0f, 150.0f, 20.0f)];
        [myFinalRegistrationNo setBackgroundColor:[UIColor clearColor]];
        [myFinalRegistrationNo setFont:[UIFont systemFontOfSize:14]];
        [myFinalRegistrationNo setTextColor:[UIColor blackColor]];
        [self addSubview:myFinalRegistrationNo];
        finalRegistrationNo = myFinalRegistrationNo;
        
        UILabel *myFinaldate = [[UILabel alloc]initWithFrame:CGRectMake(135.0, 74.0f, [UIScreen mainScreen].bounds.size.width, 20)];
        [myFinaldate setBackgroundColor:[UIColor clearColor]];
        [myFinaldate setTextColor:[UIColor blackColor]];
        [myFinaldate setFont:[UIFont systemFontOfSize:14]];
        [self addSubview:myFinaldate];
        finalDate = myFinaldate;
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setNeedsLayout {
    
}

@end
