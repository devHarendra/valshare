//
//  VSUpdateProfileCell.m
//  ValShare
//
//  Created by Vivek Kumar on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSUpdateProfileCell.h"
#import "VSHeader.h"

@implementation VSUpdateProfileCell

@synthesize labelName;
@synthesize textFieldText;
@synthesize dateButton;
@synthesize monthButton;
@synthesize yearButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self setUserInteractionEnabled:YES];
        
        // Initialization code
        UILabel *localLabel = [[UILabel alloc]initWithFrame:CGRectMake(20.0f, 13.0f, self.frame.size.width - 30, 20.0f)];
        [localLabel setBackgroundColor:[UIColor clearColor]];
        [localLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [localLabel setTextColor:[UIColor blackColor]];
        //[self addSubview:localLabel];
        ///labelName = localLabel;
        
        UITextField *localTextField = [[UITextField alloc] init];
        [localTextField setFrame: CGRectMake(20.0f, 10.0f, self.frame.size.width - 30, 30)];
        [localTextField setDelegate:(id)self];
        [localTextField.layer setBorderWidth:0.5];
        [localTextField setTextColor:[UIColor blackColor]];
        [localTextField setFont:[UIFont systemFontOfSize:14]];
        [localTextField setBackgroundColor:[UIColor clearColor]];
        [localTextField.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
        
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10, 10)];
        [localTextField setLeftViewMode:UITextFieldViewModeAlways];
        [localTextField setLeftView:leftView];
        
        [self addSubview:localTextField];
        self.textFieldText = localTextField;
        
        UIButton *localdDateButton = [[UIButton alloc]init];
        [localdDateButton setFrame:CGRectMake(20.0f, 10.0f,self.frame.size.width - 30, 30.0f)];
        [localdDateButton.layer setBorderWidth:0.4];
        [localdDateButton bringSubviewToFront:self];
        [localdDateButton setBackgroundColor:[UIColor clearColor]];
        [localdDateButton.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [localdDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [localdDateButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [localdDateButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
        [localdDateButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [self addSubview:localdDateButton];
        dateButton = localdDateButton;
        
        
        UIButton *localMonthButton = [[UIButton alloc]init];
        [localMonthButton setFrame:CGRectMake(20.0f, 10.0f, self.frame.size.width - 30, 30.0f)];
        [localMonthButton setBackgroundColor:[UIColor clearColor]];
        [localMonthButton.layer setBorderWidth:0.5];
        [localMonthButton bringSubviewToFront:self];
        [localMonthButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [localMonthButton.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [localMonthButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [localMonthButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
        [localMonthButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [self addSubview:localMonthButton];
        monthButton = localMonthButton;
        
        
        UIButton *localYearButton = [[UIButton alloc]init];
        [localYearButton setFrame:CGRectMake(20, 10.0f, self.frame.size.width - 30, 30.0f)];
        [localYearButton setBackgroundColor:[UIColor clearColor]];
        [localYearButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [localYearButton.layer setBorderWidth:0.5];
        [localYearButton.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [localYearButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [localYearButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [localYearButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
        [self addSubview:localYearButton];
        yearButton = localYearButton;
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.frame.size.height - 2, self.frame.size.width, 1)];
        [lineView setBackgroundColor:[UIColor lightGrayColor]];
       // [self addSubview:lineView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setNeedsLayout {
    
}

@end
