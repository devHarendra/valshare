//
//  VSSaveRouteCell.m
//  ValShare
//
//  Created by Vivek Kumar on 4/6/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSSaveRouteCell.h"
#import "VSHeader.h"

@implementation VSSaveRouteCell

@synthesize  route;
@synthesize  time;
@synthesize  distance;
@synthesize  viaRoute;
@synthesize  saveButton;

@synthesize timeText;
@synthesize routeText;
@synthesize distanceText;
@synthesize viaRouteText;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initialization code
        UILabel *myRoute = [[UILabel alloc] init];
        [myRoute setTextColor:[UIColor blackColor]];
        [myRoute setFont:[UIFont systemFontOfSize:15]];
        [myRoute setBackgroundColor:[UIColor clearColor]];
        [self addSubview:myRoute];
        self.route = myRoute;
        
        UILabel *myTime = [[UILabel alloc] init];
        [myTime setTextColor:[UIColor blackColor]];
        [myTime setFont:[UIFont systemFontOfSize:15]];
        [myTime setBackgroundColor:[UIColor clearColor]];
        [self addSubview:myTime];
        self.time = myTime;
        
        UILabel *myDistance = [[UILabel alloc] init];
        [myDistance setNumberOfLines:2];
        [myDistance setTextColor:[UIColor blackColor]];
        [myDistance setFont:[UIFont systemFontOfSize:15]];
        [myDistance setBackgroundColor:[UIColor clearColor]];
        [myDistance setLineBreakMode:NSLineBreakByWordWrapping];
        [self addSubview:myDistance];
        self.distance = myDistance;
        
        UILabel *myViaRoute = [[UILabel alloc] init];
        [myViaRoute setNumberOfLines:2];
        [myViaRoute setTextColor:[UIColor blackColor]];
        [myViaRoute setBackgroundColor:[UIColor clearColor]];
        [myViaRoute setLineBreakMode:NSLineBreakByWordWrapping];
        [myViaRoute setFont:[UIFont systemFontOfSize:15]];
        [self addSubview:myViaRoute];
        self.viaRoute = myViaRoute;

        
        UILabel *myRoute1 = [[UILabel alloc] init];
        [myRoute1 setTextColor:[UIColor blackColor]];
        [myRoute1 setBackgroundColor:[UIColor clearColor]];
        [myRoute1 setFont:[UIFont boldSystemFontOfSize:15]];
        [self addSubview:myRoute1];
        self.routeText = myRoute1;

        
        UILabel *myTime1 = [[UILabel alloc] init];
        [myTime1 setTextColor:[UIColor blackColor]];
        [myTime1 setBackgroundColor:[UIColor clearColor]];
        [myTime1 setFont:[UIFont boldSystemFontOfSize:15]];
        [self addSubview:myTime1];
        self.timeText = myTime1;
        
        UILabel *myDistance1 = [[UILabel alloc] init];
        [myDistance1 setTextColor:[UIColor blackColor]];
        [myDistance1 setBackgroundColor:[UIColor clearColor]];
        [myDistance1 setFont:[UIFont boldSystemFontOfSize:15]];
        [self addSubview:myDistance1];
        self.distanceText = myDistance1;
        
        
        UILabel *myViaRoute1 = [[UILabel alloc] init];
        [myViaRoute1 setTextColor:[UIColor blackColor]];
        [myViaRoute1 setBackgroundColor:[UIColor clearColor]];
        [myViaRoute1 setFont:[UIFont boldSystemFontOfSize:15]];
        [self addSubview:myViaRoute1];
        self.viaRouteText = myViaRoute1;

        
        UIButton *mySaveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        mySaveButton.clipsToBounds = YES;
        [mySaveButton.layer setCornerRadius:4.0f];
        [mySaveButton setBackgroundColor:[UIColor grayColor]];
        [mySaveButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
        [mySaveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:mySaveButton];
        saveButton = mySaveButton;
        
        UIView *lineView = [[UIView alloc] init];
        [lineView setBackgroundColor:[UIColor grayColor]];
        [lineView setFrame:CGRectMake(0.0, 117.0f, [UIScreen mainScreen].bounds.size.width, 1)];
        [self addSubview:lineView];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)layoutSubviews {
    
    [self.route setFrame:CGRectMake(65.0f, 5.0f, 120.0f, 20.0f)];
    [self.time setFrame:CGRectMake(self.frame.size.width/2 + 80, 5.0f, 100.0f, 20.0f)];
    [self.distance setFrame:CGRectMake(85.0f, 30.0f, 170, 40.0f)];
    [self.viaRoute setFrame:(self.saveButton.hidden) ? CGRectMake(45.0f, 70.0f, 350, 40.0f) : CGRectMake(45.0, 70.0f, 170, 40)];
    
    [self.routeText setFrame:CGRectMake(10.0f, 5.0f, 55.0f, 20.0f)];
    [self.timeText setFrame:CGRectMake(self.frame.size.width/2 + 32, 5.0f, 50.0f, 20.0f)];
    [self.distanceText setFrame:CGRectMake(10.0f, 38.0f, 75, 20.0f)];
    [self.viaRouteText setFrame:CGRectMake(10.0, 78.0f, 40, 20)];

    [self.saveButton setFrame:CGRectMake(self.frame.size.width/2 + 70, 70.0f, 80.0f, 35)];
}

@end
