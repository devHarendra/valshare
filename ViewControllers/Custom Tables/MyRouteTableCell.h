//
//  MyRouteTableCell.h
//  VolShare
//
//  Created by Vivek Kumar on 4/3/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRouteTableCell : UITableViewCell

@property (strong,nonatomic) UILabel *myId;
@property (strong,nonatomic) UILabel *source;
@property (strong,nonatomic) UILabel *destination;
@property (strong,nonatomic) UILabel *date;
@property (strong,nonatomic) UILabel *status;

@property (strong,nonatomic) UILabel *myFinalId;
@property (strong,nonatomic) UILabel *finalSource;
@property (strong,nonatomic) UILabel *finalDestination;
@property (strong,nonatomic) UILabel *finalDate;
@property (strong,nonatomic) UILabel *finalStatus;


@property (strong,nonatomic) UILabel *effectiveTo;
@property (strong,nonatomic) UILabel *purchesStatus;
@property (strong,nonatomic) UILabel *packagePrice;
@property (strong,nonatomic) UILabel *loyalityPoint;
@property (strong,nonatomic) UILabel *RouteIdForservice;


@property (strong,nonatomic) UILabel *finalEffectiveTo;
@property (strong,nonatomic) UILabel *finalPurchesStatus;
@property (strong,nonatomic) UILabel *finalPackagePrice;
@property (strong,nonatomic) UILabel *finalLoyalityPoint;
@property (strong,nonatomic) UIView *line;

@end
