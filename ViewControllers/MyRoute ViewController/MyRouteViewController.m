//
//  MyRouteViewController.m
//  VolShare
//
//  Created by Vivek Kumar on 4/3/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import "MyRouteViewController.h"

@interface MyRouteViewController () {
    UITableView *routeTable;
}

@end

@implementation MyRouteViewController
@synthesize routeListArray = _routeListArray;
@synthesize delegate = _MySelectedRouteDelegate;

#define locM [LocationManager sharedInstance]

-(NSMutableArray *)routeListArray {
    
    if (!_routeListArray) {
        _routeListArray = [[NSMutableArray alloc] init];
    }
    return _routeListArray;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

-(void)setDelegate:(id<MySelectedRouteDelegate>)delegate{
    
    _MySelectedRouteDelegate = delegate;
}

-(void)deleteRouteFromList:(NSString *)routeId Index:(NSIndexPath *)indexPath {
    
    [[VSParserClass sharedParser] VSDeletRoute:routeId WithCompletionBlock:
     ^(BOOL result, id resultObject, NSError *error) {
         
         NSLog(@"resultObjectresultObjectresultObject:-%d -- %@ -- %d",indexPath.row, routeId, [self.routeListArray count]);
         
         if (result) {
             
             if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                 
                 [VSEnumClass showAlertWithTitle:nil msg:@"Your route detail is delete successfully"];
                 self.routeListArray = [[NSMutableArray alloc] initWithArray:self.routeListArray];
                 [self.routeListArray removeObjectAtIndex:indexPath.row];
                 [routeTable reloadData];
             }else {
                 [VSEnumClass showAlertWithTitle:nil msg:@"Your route detail is not delete successfully"];
             }
             [SVProgressHUD dismiss];
         }else {
             
             [[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
         }
     }];
}

-(void)getMyRouteList:(NSString *)userId {
    
    [[VSParserClass sharedParser] getMyRoutesList:userId WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        if (result) {
            
            if ([resultObject isKindOfClass:[NSDictionary class]]) {
                
                [self.routeListArray removeAllObjects];
                
                if ([resultObject valueForKey:@"Data"]) {
                    
                    NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                    NSLog(@"dictdatadictdatadictdatadictdata:-%@",dictdata);
                    NSMutableArray *localRouteArray = [dictdata valueForKey:@"routeModelList"];
                    [localRouteArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        
                        NSMutableDictionary *localRouteDict = (NSMutableDictionary *)[localRouteArray objectAtIndex:idx];
                        VSDirectionModal *model11 = [[VSDirectionModal alloc]init];
                        [model11 setLocalRouteDestination:[localRouteDict valueForKey:@"destination"]];
                        [model11 setLocalRouteDistance:[localRouteDict valueForKey:@"distance"]];
                        [model11 setLocalRouteDrivingMode:[localRouteDict valueForKey:@"drivingMode"]];
                        [model11 setLocalRouteDuration:[localRouteDict valueForKey:@"duration"]];
                        [model11 setLocalRouteRouteEncodedPath:[localRouteDict valueForKey:@"routeEncodedPath"]];
                        [model11 setLocalRouteRouteId:[localRouteDict valueForKey:@"routeId"]];
                        [model11 setLocalRouteSource:[localRouteDict valueForKey:@"source"]];
                        [model11 setLocalRouteSummary:[localRouteDict valueForKey:@"summary"]];
                        [model11 setLocalRouteUserId:[localRouteDict valueForKey:@"userId"]];
                        [model11 setLocalRouteSource:[localRouteDict valueForKey:@"source"]];
                        [model11 setLocalRouteRouteIndexId:[localRouteDict valueForKey:@"routeIndexId"]];
                        [model11 setLastModifiedTime:[localRouteDict valueForKey:@"lastModifiedTime"]];
                        
                        [self.routeListArray addObject:model11];
                    }];
                }
                
                NSArray *sortedArray = [self.routeListArray sortedArrayUsingComparator:^NSComparisonResult(VSDirectionModal *model11, VSDirectionModal *model12){
                    
                    return my_int_compare([model11.localRouteRouteIndexId intValue], [model12.localRouteRouteIndexId intValue]);;
                }];
                
                [self.routeListArray removeAllObjects];
                self.routeListArray = (NSMutableArray *)sortedArray;
                
                if ([self.routeListArray count] == 0) {
                    [[[UIAlertView alloc] initWithTitle:nil message:@"No Route found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                } else {
                    [self addTableOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y + 5.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 55) Tag:2450];
                }
                
            }else {
                
                NSLog(@"---------else----------::: %@", resultObject);
            }
            
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            
        }
    }];
}



-(void)getSelectedRouteDetails:(NSString *)Routeid {
    
    [[VSParserClass sharedParser] getSelectedRouteDetails:Routeid WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSLog(@"CommercialCommercial :- %@", resultObject);
            
            if ([resultObject isKindOfClass:[NSDictionary class]]) {
                
                if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                    
                    if ([resultObject valueForKey:@"Data"]) {
                        
                        NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                        
                        [self.navigationController popViewControllerAnimated:YES];
                        if (_MySelectedRouteDelegate && [_MySelectedRouteDelegate respondsToSelector:@selector(DrawRouteBySelectingRouteFromMyRoutes:)])
                        {
                            [_MySelectedRouteDelegate DrawRouteBySelectingRouteFromMyRoutes:[dictdata valueForKey:@"routeEncodedPath"]];
                        }
                    }
                }
                else {
                    [[[UIAlertView alloc] initWithTitle:nil message:@"Please contact server admin." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
            }else {
                
                NSLog(@"---------else----------::: %@", resultObject);
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            
        }
    }];
}


static inline int my_int_compare(int x, int y) { return (x > y) - (x < y); }

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getMyRouteList:[VSSingletonClass sharedMySingleton].loginUserId];
        
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    self.title = [NSString stringWithFormat:@"%@", @"My Routes"];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    routeTable = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
    [routeTable setBackgroundColor:[UIColor clearColor]];
    [routeTable setTag:tag];
    [routeTable setDelegate:(id)self];
    [routeTable setDataSource:(id)self];
    [routeTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [routeTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:routeTable];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 110;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return (![self.routeListArray count] == 0) ? [self.routeListArray count] : 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    
    return (![self.routeListArray count] == 0) ? YES : NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VSDirectionModal *model22 = [self.routeListArray objectAtIndex:indexPath.row];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self deleteRouteFromList:model22.localRouteRouteId Index:indexPath];
            
        }else {
            
            [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    MyRouteTableCell *cell = (MyRouteTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[MyRouteTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell setBackgroundColor:[UIColor clearColor]];
        UIView *lineView = [[UIView alloc]init];
        [lineView setFrame:CGRectMake(0.0, 110.0, [UIScreen mainScreen].bounds.size.width, 0.5)];
        [lineView setBackgroundColor:[UIColor lightGrayColor]];
        [cell.contentView addSubview:lineView];
        [lineView setHidden:(![self.routeListArray count] == 0) ? NO :YES];
        
    }
    
    if (![self.routeListArray count] == 0) {
        
        [cell.date setHidden:NO];
        [cell.source setHidden:NO];
        [cell.myFinalId setHidden:NO];
        [cell.finalDate setHidden:NO];
        [cell.finalSource setHidden:NO];
        [cell.destination setHidden:NO];
        [cell.finalDestination setHidden:NO];
        [cell.line setHidden:YES];
        
        VSDirectionModal *model22 = [self.routeListArray objectAtIndex:indexPath.row];
        
        [cell.myId setText:@"Id"];
        [cell.source setText:@"Source"];
        [cell.destination setText:@"Destination"];
        [cell.date setText:@"Date"];
        
        [cell.myFinalId setText:[NSString stringWithFormat:@": %d", indexPath.row]];
        [cell.finalSource setText:[NSString stringWithFormat:@": %@", model22.localRouteSource]];
        [cell.finalDestination setText:[NSString stringWithFormat:@": %@", model22.localRouteDestination]];
        [cell.finalDate setText:[NSString stringWithFormat:@": %@", model22.lastModifiedTime]];
        [cell.RouteIdForservice setText:[NSString stringWithFormat:@"%@", model22.localRouteRouteId]];

        
    } else {
        
        [cell.line setHidden:YES];
        [cell.myId setHidden:NO];
        [cell.date setHidden:YES];
        [cell.source setHidden:YES];
        [cell.myFinalId setHidden:YES];
        [cell.finalDate setHidden:YES];
        [cell.destination setHidden:YES];
        [cell.finalSource setHidden:YES];
        [cell.finalDestination setHidden:YES];
        [cell.myId setText:@"No Route Found"];
        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MyRouteTableCell *cell = (MyRouteTableCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getSelectedRouteDetails:cell.RouteIdForservice.text];
        
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}





@end
