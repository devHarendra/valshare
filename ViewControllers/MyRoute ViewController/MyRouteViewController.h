//
//  MyRouteViewController.h
//  VolShare
//
//  Created by Vivek Kumar on 4/3/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@protocol MySelectedRouteDelegate <NSObject>

-(void)DrawRouteBySelectingRouteFromMyRoutes:(NSString*)Encodedpath;

@end



@interface MyRouteViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)  NSMutableArray *routeListArray;
@property (nonatomic, weak) id<MySelectedRouteDelegate>delegate;
@end
