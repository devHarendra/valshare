//
//  VSVehicleViewController.m
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSVehicleInfoViewController.h"

@interface VSVehicleInfoViewController (){
    
    int  currentbuttonTag;
    
}

@end

@implementation VSVehicleInfoViewController
@synthesize vehicleTypeArray = _vehicleTypeArray;

-(NSMutableArray *)vehicleTypeArray {
    
    if(!_vehicleTypeArray) {
        _vehicleTypeArray = [[NSMutableArray alloc] init];
    }
    return _vehicleTypeArray;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationItem setHidesBackButton:NO];
    [self.navigationItem setTitle:@"Vehicle Information"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationItem setHidesBackButton:NO];
    [self.navigationItem setTitle:@"Vehicle Information"];
    [self adddComponentOnView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adddComponentOnView {
    
    NSArray *text = @[@"Vehicle Name",@"Registration Number"];
    
    [self addButtonOnView:(IS_IPHONE6PLUS) ? CGRectMake([UIScreen mainScreen].bounds.origin.x+[UIScreen mainScreen].bounds.size.width/3 - 90, [UIScreen mainScreen].bounds.origin.y + 40, [UIScreen mainScreen].bounds.size.width/2+98, 30):CGRectMake([UIScreen mainScreen].bounds.origin.x+[UIScreen mainScreen].bounds.size.width/3 - 78, [UIScreen mainScreen].bounds.origin.y + 40, [UIScreen mainScreen].bounds.size.width/2+98, 30) Title:@"Vehicle Type" Tag:2000];
    
    for (int i =0; i < 2; i++) {
        
        [self addTextViewOnFrame:(IS_IPHONE_5) ?CGRectMake([UIScreen mainScreen].bounds.origin.x+30, [UIScreen mainScreen].bounds.origin.y+85+(i*90), [UIScreen mainScreen].bounds.size.width/2+100, 70):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x+30, [UIScreen mainScreen].bounds.origin.y+85+(i*90), [UIScreen mainScreen].bounds.size.width/2+100, 70):CGRectMake([UIScreen mainScreen].bounds.origin.x+45, [UIScreen mainScreen].bounds.origin.y+85+(i*90), [UIScreen mainScreen].bounds.size.width/2+100, 70) Title:text[i] Tag:2225 + i];
    }
    
  
    [self addCheckBoxButtonOnView:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2-11), [UIScreen mainScreen].bounds.origin.y + 260, kCheckboxBG.size.width, kCheckboxBG.size.height):(IS_IPHONE6PLUS) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 +20), [UIScreen mainScreen].bounds.origin.y + 260, kCheckboxBG.size.width, kCheckboxBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2-3), [UIScreen mainScreen].bounds.origin.y + 260, kCheckboxBG.size.width, kCheckboxBG.size.height) BGImage:kCheckboxBG BGSelected:kCheckedBG Tag:45654 ];
    
    [self addLabelForCheckboxOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x+55+kCheckboxBG.size.width, [UIScreen mainScreen].bounds.origin.y + 260, [UIScreen mainScreen].bounds.size.width/3+20, 25) Title:@"Mark As Defult" Tag:4425 Font:[UIFont systemFontOfSize:14.0]];
    
    [self addUpdateButtonOnView:(IS_IPHONE6PLUS) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2-18, [UIScreen mainScreen].bounds.origin.y+297, kLoginBG.size.width+20, kLoginBG.size.height):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2+12, [UIScreen mainScreen].bounds.origin.y+297, kLoginBG.size.width-30, kLoginBG.size.height):(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2+20, [UIScreen mainScreen].bounds.origin.y+297, kLoginBG.size.width-30, kLoginBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2+5 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y+297, kLoginBG.size.width-5, kLoginBG.size.height) Title:@"Update Vehicle" Tag:3000 BGImage:kLoginBG];
    
    
}


-(void)addButtonOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UIButton *btnSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnSelect.tag = tag;
    btnSelect.frame = frame;
    [btnSelect setBackgroundColor:[UIColor whiteColor]];
    btnSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
    [btnSelect setTitle:title forState:UIControlStateNormal];
    [btnSelect.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [btnSelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnSelect addTarget:self action:@selector(selectTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSelect];
}

- (void)addTextViewOnFrame:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextView *textView = [[UITextView alloc]init];
    [textView setFrame:frame];
    [textView setDelegate:(id)self];
    [textView setText:title];
    [textView setTag:tag];
    [textView setTextColor:[UIColor blackColor]];
    [textView setBackgroundColor:[UIColor whiteColor]];
    [textView setFont:[UIFont systemFontOfSize:15.0f]];
    [textView setBackgroundColor:[UIColor whiteColor]];
    [textView setTextAlignment:NSTextAlignmentLeft];
    [textView setContentInset:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
    [self.view addSubview:textView];
}

-(void)addCheckBoxButtonOnView:(CGRect)frame BGImage:(UIImage *)bgImage BGSelected:(UIImage *)s_bgImage Tag:(int)tag {
    
    VSButton *button = [VSButton buttonWithType:UIButtonTypeCustom];
    [button setTag:tag];
    [button setFrame:frame];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button setBackgroundImage:s_bgImage forState:UIControlStateSelected];
    [button addTarget:self action:@selector(selectBoxButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(void)addLabelForCheckboxOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag Font:(UIFont *)font {
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:frame];
    titleLabel.text = title;
    titleLabel.tag = tag;
    titleLabel.font = font;
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleLabel];
}

-(void)addUpdateButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(updateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if (textView.tag == 2225) {
        
        if ((textView.text =  @"Vehicle Name")) {
            textView.text = @"";
        }
    }else if (textView.tag == 2226) {
        
        if ((textView.text =  @"Registration Number")) {
            textView.text = @"";
        }
    }
    
    textView.textColor = [UIColor blackColor];
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (textView.tag == 2225) {
        
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Vehicle Name";
        }
    }else if  (textView.tag == 2226) {
        
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Registration Number";
        }
    }
    
    textView.textColor = [UIColor lightGrayColor];
    [textView resignFirstResponder];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


-(IBAction)selectTypeClicked:(UIButton *)sender {
    
    
    [dropDown removeFromSuperview];
    
    currentbuttonTag = sender.tag;
    
    
    self.vehicleTypeArray = [NSMutableArray arrayWithObjects:@"Bus", @"Truck",@"Car", nil];
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:self.vehicleTypeArray ImgArr:nil Direction:@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [self rel];
}

-(void)rel {
    dropDown = nil;
}

-(IBAction)updateButtonAction:(id)sender {
}

-(IBAction)selectBoxButtonAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
}



@end
