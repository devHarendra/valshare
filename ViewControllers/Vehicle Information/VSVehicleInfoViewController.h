//
//  VSVehicleViewController.h
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "NIDropDown.h"

@interface VSVehicleInfoViewController : UIViewController <NIDropDownDelegate> {
    
    NIDropDown *dropDown;
    
}

@property (nonatomic, strong) NSMutableArray *vehicleTypeArray;

@end
