//
//  NotiSettingsVC.m
//  ValShare
//
//  Created by Sagar Singh on 20/06/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "NotiSettingsVC.h"

@interface NotiSettingsVC ()


@end

NSUserDefaults *defaults;


@implementation NotiSettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"%@", @"Notification Setting"]; // Added by harry
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    
    UIView* viewBG = (UIView*)[self.view viewWithTag:20];
    [viewBG.layer setCornerRadius:5.0];
    [viewBG.layer setBorderColor:[UIColor redColor].CGColor];
    [viewBG.layer setBorderWidth:1.5];
    [viewBG setBackgroundColor:[UIColor clearColor]];
    
    UIButton* saveBtn = (UIButton*)[self.view viewWithTag:10];
    [saveBtn.layer setCornerRadius:5.0];
    [saveBtn.layer setBackgroundColor:[UIColor redColor].CGColor];
    [saveBtn addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [(UIButton*)[self.view viewWithTag:25] setSelected:[[defaults objectForKey:@"ForPopUp"]boolValue]];
    [(UIButton*)[self.view viewWithTag:30] setSelected:[[defaults objectForKey:@"ForVoice"]boolValue]];

}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)ForPopUp:(UIButton*)btn
{
   if (!btn.selected) [btn setSelected:YES];
   else [btn setSelected:false];
    
    [defaults setObject:[NSNumber numberWithBool:btn.selected] forKey:@"ForPopUp"];
    
    NSLog(@"noti settings %@ -- ",[defaults objectForKey:@"ForPopUp"]);

    
}

-(IBAction)ForVoice:(UIButton*)btn
{
    if (!btn.selected) [btn setSelected:YES];
    else [btn setSelected:false];
    
    [defaults setObject:[NSNumber numberWithBool:btn.selected] forKey:@"ForVoice"];

    NSLog(@"noti settings %@ -- ",[defaults objectForKey:@"ForVoice"]);
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
