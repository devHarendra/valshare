//
//  VSSignUpView.h
//  ValShare
//
//  Created by Sharda Prasad on 3/30/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "NIDropDown.h"
#import "VSRegistrationType.h"
#import "QuartzCore/QuartzCore.h"
#import "UIViewController+MJPopupViewController.h"

@interface VSSignUpView : UIViewController <NIDropDownDelegate, CLLocationManagerDelegate,UIPickerViewDataSource, UIPickerViewDelegate> {
     NIDropDown *dropDown;
}

@property (nonatomic, retain) NSMutableDictionary *fbDic;

@property (nonatomic, retain) NSString          *dayString;
@property (nonatomic, retain) NSString          *monthsString;
@property (nonatomic, retain) NSString          *yearString;
@property (nonatomic, retain) NSString          *genderTag;
@property (nonatomic, retain) NSString          *userLocation;

@property (nonatomic, strong) NSMutableArray    *dayArray;
@property (nonatomic, strong) NSMutableArray    *monthsArray;
@property (nonatomic, strong) NSMutableArray    *yearArray;

@property (nonatomic, strong) NSMutableArray    *genderTypeArray;
@property (nonatomic, strong) NSMutableArray    *accountTypeArray;
@property (nonatomic, strong) UIPickerView      *genderPicker;
@property (nonatomic, strong) UIPickerView      *accountTypePicker;


@property (nonatomic, strong) UIScrollView      *scrollView;
@property (nonatomic, retain) NSString          *genderKeyString;
@property (nonatomic, retain) NSString          *accountTypeKeyString;



@end
