//
//  VSCommercialUserPop.m
//  ValShare
//
//  Created by Sharda Prasad on 4/28/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCommercialUserPop.h"

@interface VSCommercialUserPop () {
    
    UIView *commercialPop;
}

@end

@implementation VSCommercialUserPop

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addComponentOnView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    commercialPop = [[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.size.height/2 - 110, [UIScreen mainScreen].bounds.size.width - 30, 220)];
    [commercialPop setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(10, 15, commercialPop.frame.size.width - 20, 20)];
    [label setTextColor:[UIColor blackColor]];
    [label setText:@"Please Enter Company ID"];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [commercialPop addSubview:label];
    
    UITextField *textField = [[UITextField alloc] init];
    [textField setTag:234324];
    [textField setFrame:CGRectMake(10, 60, commercialPop.frame.size.width - 20, 35)];
    [textField setDelegate:(id)self];
    [textField setSecureTextEntry:NO];
    [textField setPlaceholder:@"Please enter company id"];
    [textField setSecureTextEntry:NO];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setKeyboardType:UIKeyboardTypeDefault];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    
    [textField.layer setBorderWidth:1.0f];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10, 10)];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    [textField setLeftView:leftView];
    [commercialPop addSubview:textField];
    
    UIButton *button = [[UIButton alloc] init];
    [button setTag:8768];
    [button setFrame:CGRectMake(10, commercialPop.frame.size.height - 90, commercialPop.frame.size.width - 20, 35)];
    [button setTitle:@"Submit" forState:UIControlStateNormal];
    [button setBackgroundImage:kRegisterButtonBG forState:UIControlStateNormal];
    [button addTarget:self action:@selector(submitButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [commercialPop addSubview:button];
    
    button = [[UIButton alloc] init];
    [button setTag:8769];
    [button setFrame:CGRectMake(10, commercialPop.frame.size.height - 50, commercialPop.frame.size.width - 20, 35)];
    [button setTitle:@"Cancel" forState:UIControlStateNormal];
    [button setBackgroundImage:kRegisterButtonBG forState:UIControlStateNormal];
    [button addTarget:self action:@selector(submitButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [commercialPop addSubview:button];
    [self.view addSubview:commercialPop];
}


//************ Use textField Delegate *************

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

-(IBAction)submitButtonAction:(UIButton *)sender {
    
    UITextField *textField = (UITextField *)[self.view viewWithTag:234324];
    NSLog(@"dfljfdil;gjfdoigjiof");
    if (sender.tag == 8768) {
        if ([textField.text length] == 0) {
            [VSEnumClass showAlertWithTitle:nil msg:@"Please enter company id."];
        } else {
            [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self checkCommercialUser:textField.text];
        }
    } else {
        
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftRight];
    }
}


-(void)checkCommercialUser:(NSString *)companyTagID {
    
    [[VSParserClass sharedParser] checkCommercialUser:companyTagID WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSString *string = [[NSString alloc]initWithData:resultObject encoding:NSUTF8StringEncoding];
            if ([string isEqualToString:@"true"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([self.submitPopUpDelegate respondsToSelector:@selector(submitButtonActionWithID:)]) {
                        [self.submitPopUpDelegate submitButtonActionWithID:companyTagID];
                    }
                });
            } else {
                [SVProgressHUD dismiss];
                [[[UIAlertView alloc] initWithTitle:nil message:@"Please check your company ID." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
        } else {
            [SVProgressHUD dismiss];
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}

@end
