//
//  VSCommercialUserPop.h
//  ValShare
//
//  Created by Sharda Prasad on 4/28/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@protocol submitPopUpDelegate <NSObject>

-(void)submitButtonActionWithID:(NSString *)text;

@end

@interface VSCommercialUserPop : UIViewController

@property (nonatomic, retain) id <submitPopUpDelegate> submitPopUpDelegate;
@end
