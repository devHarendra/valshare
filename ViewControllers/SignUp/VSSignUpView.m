//
//  VSSignUpView.m
//  ValShare
//
//  Created by Sharda Prasad on 3/30/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSSignUpView.h"
#import "VSCommercialUserPop.h"

@interface VSSignUpView() {
    
    CLLocationManager   *locationManager;
    CLLocation          *currentLocation;
    CLPlacemark         *placemark;
    
    Boolean             checkUserName;
    NSInteger           currentbuttonTag;
    Boolean             checkEmail;
    Boolean             AccType;
    Boolean             genType;


    Boolean             termsCheck;
    NSUInteger       PressCount;
    
    int genderTag;
    
}

@end

@implementation VSSignUpView
@synthesize scrollView = _scrollView;
@synthesize dayArray = _dayArray;
@synthesize monthsArray = _monthsArray;
@synthesize yearArray = _yearArray;
@synthesize genderTag  = _genderTag;
@synthesize userLocation = _userLocation;
@synthesize genderTypeArray = _genderTypeArray;
@synthesize accountTypeArray = _accountTypeArray;
@synthesize genderPicker;
@synthesize accountTypePicker;
@synthesize genderKeyString;
@synthesize accountTypeKeyString;

-(NSMutableArray *)genderTypeArray {
    if(!_genderTypeArray) {
        _genderTypeArray = [[NSMutableArray alloc] init];
    }
    return _genderTypeArray;
}

-(NSMutableArray *)accountTypeArray {
    
    if(!_accountTypeArray) {
        _accountTypeArray = [[NSMutableArray alloc] init];
    }
    return _accountTypeArray;
}

-(NSMutableArray *)monthsArray {
    if(!_monthsArray) {
        _monthsArray = [[NSMutableArray alloc] init];
    }
    return _monthsArray;
}

-(NSMutableArray *)yearArray {
    if(!_yearArray) {
        _yearArray = [[NSMutableArray alloc] init];
    }
    return _yearArray;
}

-(UIScrollView *)scrollView {
    
    if (!_scrollView) {
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, [UIScreen mainScreen].bounds.origin.y + 5, self.view.frame.size.width, [UIScreen mainScreen].bounds.size.height - self.navigationController.navigationBar.bounds.size.height)];
        [_scrollView setTag:78560];
        [_scrollView setScrollEnabled:YES];
        [_scrollView setUserInteractionEnabled:YES];
        [_scrollView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_scrollView];
    }
    return _scrollView;
}

-(void)dealloc {
    
    placemark = nil;
    self.dayArray = nil;
    self.dayString = nil;
    self.genderTag = nil;
    self.yearArray = nil;
    currentLocation = nil;
    locationManager = nil;
    self.scrollView = nil;
    self.yearString = nil;
    self.monthsArray = nil;
    self.monthsString = nil;
    self.userLocation = nil;
}

-(void)getUserTypeAndGender {
    
    [[VSParserClass sharedParser] getRegistrationTypeWithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        if (result) {
            
            NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
            NSMutableDictionary *genderTypeDic = [dictdata valueForKey:@"genderTypeDictionary"];
            NSMutableDictionary *accountTypeDic = [dictdata valueForKey:@"accountTypeDictionary"];
            
            NSLog(@"getUserTypeAndGender :- %@", dictdata);
            
            [genderTypeDic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                
                VSRegistrationType *registrationType = [[VSRegistrationType alloc] init];
                [registrationType setGenderTypeKey:key];
                [registrationType setGenderTypeObject:obj];
                [self.genderTypeArray addObject:registrationType];
            }];
            
            [accountTypeDic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                
                VSRegistrationType *registrationType = [[VSRegistrationType alloc] init];
                [registrationType setAccountTypeKey:key];
                [registrationType setAccountTypeObject:obj];
                [self.accountTypeArray addObject:registrationType];
            }];
        }else {
            [VSEnumClass showAlertWithTitle:nil msg:[NSString stringWithFormat:@"%@", error]];
        }
        [SVProgressHUD dismiss];
    }];
}

-(void)checkEmail:(NSString *)email UserName:(NSString *)userName MobileNo:(NSString *)mobNumber DOB:(NSString *)dob Password:(NSString *)password Gender:(NSString *)gender Location:(NSString *)location AccountType:(NSString *)accounttype CompanyID:(NSString *)companyID {
    
    [[VSParserClass sharedParser] checkEmail:email WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            NSString *string = [[NSString alloc]initWithData:resultObject encoding:NSUTF8StringEncoding];
            
            if ([string boolValue]) {
                if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                    [self checkUserName:email UserName:userName MobileNo:mobNumber DOB:dob Password:password Gender:gender Location:location AccountType:accounttype CompanyID:companyID];
                }else {
                    
                    [[[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                }
            } else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"This EMail id already registered." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                
                [SVProgressHUD dismiss];
            }
        }else {
        }
    }];
}

-(void)checkUserName:(NSString *)email UserName:(NSString *)userName MobileNo:(NSString *)mobNumber DOB:(NSString *)dob Password:(NSString *)password Gender:(NSString *)gender Location:(NSString *)location AccountType:(NSString *)accounttype CompanyID:(NSString *)companyID {
    
    [[VSParserClass sharedParser] checkUserName:userName WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSString *string = [[NSString alloc]initWithData:resultObject encoding:NSUTF8StringEncoding];
            
            if ([string boolValue]) {
                
                if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                    [self userSignUp:email UserName:userName MobileNo:mobNumber DOB:dob Password:password Gender:gender Location:location AccountType:accounttype CompanyID:companyID];
                }else {
                    
                    [[[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                }
            } else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"This username already registered." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [SVProgressHUD dismiss];
            }
        }else {
        }
    }];
}

-(void)userSignUp:(NSString *)email UserName:(NSString *)userName MobileNo:(NSString *)mobNumber DOB:(NSString *)dob Password:(NSString *)password Gender:(NSString *)gender Location:(NSString *)location AccountType:(NSString *)accounttype CompanyID:(NSString *)companyID
 {
    
     NSString *myString = dob;
     NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
     dateFormatter.dateFormat = @"dd/mm/yyyy";

     NSDate *yourDate = [dateFormatter dateFromString:myString];
     NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
     
     NSTimeInterval seconds = [yourDate timeIntervalSinceReferenceDate];
     double milliseconds = seconds*1000;
     
    NSMutableDictionary *signUPDict;
    if ([companyID isEqualToString:@"00"]) {
        
        signUPDict = [[NSMutableDictionary alloc] init];
        [signUPDict setObject:email forKey:@"email"];
        [signUPDict setObject:userName forKey:@"userName"];
        [signUPDict setObject:mobNumber forKey:@"contactNumber"];
        [signUPDict setObject:[NSNumber numberWithDouble:milliseconds] forKey:@"DOB"];
        [signUPDict setObject:password forKey:@"password"];
        [signUPDict setObject:[NSString stringWithFormat:@"%d", [gender intValue]] forKey:@"genderTypeId"];
        [signUPDict setObject:@"0" forKey:@"RoleTypeId"];
        [signUPDict setObject:[NSString stringWithFormat:@"%d", [accounttype intValue]] forKey:@"accountTypeId"];
        
        if (location.length>0) {
            [signUPDict setObject:location forKey:@"location"];
        }else{
            [signUPDict setObject:@"" forKey:@"location"];
        }
        
    } else {
        
        signUPDict = [[NSMutableDictionary alloc] init];
        [signUPDict setObject:email forKey:@"email"];
        [signUPDict setObject:userName forKey:@"userName"];
        [signUPDict setObject:mobNumber forKey:@"contactNumber"];
        [signUPDict setObject:[NSString stringWithFormat:@"%lld",[dob longLongValue]] forKey:@"DOB"];
        [signUPDict setObject:password forKey:@"password"];
        [signUPDict setObject:[NSString stringWithFormat:@"%d", [gender intValue]] forKey:@"genderTypeId"];
        [signUPDict setObject:@"0" forKey:@"RoleTypeId"];
        [signUPDict setObject:[NSString stringWithFormat:@"%d", [accounttype intValue]] forKey:@"accountTypeId"];
        [signUPDict setObject:companyID forKey:@"companyIdentificationId"];
        
        if (location.length>0) {
            [signUPDict setObject:location forKey:@"location"];
        }else{
            [signUPDict setObject:@"" forKey:@"location"];
        }
    }

    [[VSParserClass sharedParser] valShareUserSignUp:signUPDict WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSMutableDictionary *signUpData = (NSMutableDictionary *)resultObject;
            if ([[signUpData valueForKey:@"status"] isEqualToString:@"Ok"]) {
                
                NSLog(@"signUpDatasignUpDatasignUpData : %@", signUpData);
                if (![companyID isEqualToString:@"00"]) {
                    [[[UIAlertView alloc] initWithTitle:nil message:@"Your details has been registered successfully . We will respond you shortly." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }else {
                    [self  navigationToVarificationScreen:[signUpData valueForKey:@"userId"]];
                }
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            NSLog(@"elseelseelseelseelse :- %@", error);
        }
    }];
}

- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated {
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)navigationToVarificationScreen:(NSString *)userID {
    
    VSTextField *email = (VSTextField *)[self.view viewWithTag:45343];
    VSTextField *userName = (VSTextField *)[self.view viewWithTag:45344];
    VSTextField *mobileNo = (VSTextField *)[self.view viewWithTag:45345];
    VSTextField *password = (VSTextField *)[self.view viewWithTag:453043];
    VSTextField *cPassword = (VSTextField *)[self.view viewWithTag:453044];

    [email setText:@""];
    [userName setText:@""];
    [mobileNo setText:@""];
    [password setText:@""];
    [cPassword setText:@""];
    [self.navigationController setNavigationBarHidden:NO];

    VSUserVarification *userVarification = [[VSUserVarification alloc] init];
    [userVarification setUserID:userID];
    [userVarification setLoginStatus:@"varification"];
    [self.navigationController pushViewController:userVarification animated:YES];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getUserTypeAndGender];
    }else {
        
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertview show];
    }
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    checkEmail = NO;
    termsCheck = NO;
    AccType = NO;
    genType = NO;


    checkUserName = NO;
    genderTag = 2;
    
    self.title = [NSString stringWithFormat:@"%@",@"Register"];
    [self addComponentOnView];
    [self getCurrentLocation];
    
    UIView *view = (UIView *)[self.view viewWithTag:9999];
    [view setHidden:YES];
    PressCount = 0;
    NSLog(@"Facebook dic - %@",_fbDic);
    if (_fbDic) [self FilformFromFb];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getCurrentLocation {
    
    locationManager = [CLLocationManager new];
    [locationManager setDelegate:(id)self];
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBestForNavigation];
    [locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [[[UIAlertView alloc] initWithTitle:nil message:@"Failed to Get Your Location"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    //currentLocation = [locations lastObject];
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
        } else {
            NSLog(@"=== %@", error.debugDescription);
        }
    } ];
}

-(void)addComponentOnView {
    
    NSArray *textFieldBG = @[kEmail_BG,kUserName_BG,kMobileNo_BG];
    NSArray *buttonTitle   = @[@"Gender",@"Account Type"];
    
    NSArray *placeHolderText = @[@"Email",@"Username",@"Mobile no."];
    NSArray *securePHolder   = @[@"Password",@"Confirm password"];
    
    for (int i = 0; i< 2; i++) {
        
        [self addGenderAndAccountTypeButton:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kEmail_BG.size.width/2 + ((kEmail_BG.size.width/2 + 10) * i), [UIScreen mainScreen].bounds.origin.y + 20, kEmail_BG.size.width/2 - 10, kEmail_BG.size.height) : CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kEmail_BG.size.width/2 + ((kEmail_BG.size.width/2 + 5 ) * i), [UIScreen mainScreen].bounds.origin.y + 40, kEmail_BG.size.width/2 -5, kEmail_BG.size.height) Title:buttonTitle[i] Tag:6999 +i ];
    }
    
    for (int j = 0; j<= 2; j++) {
        
        [self addTextFieldOnView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kEmail_BG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 60 + ((kEmail_BG.size.height + 10) * j), kEmail_BG.size.width, kEmail_BG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kEmail_BG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 80 + ((kEmail_BG.size.height + 10) * j), kEmail_BG.size.width, kEmail_BG.size.height) Title:placeHolderText[j] Tag:45343 + j BGImage:textFieldBG[j] SecureText:NO KeyBoardType:(j == 2) ? UIKeyboardTypeNumberPad: UIKeyboardTypeEmailAddress];
    }
    
    for (int j = 0; j< 2; j++) {
        
        [self addTextFieldOnView:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kEmail_BG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 - 87 + ((kEmail_BG.size.height + 10) * j), kEmail_BG.size.width, kEmail_BG.size.height):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kEmail_BG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 - 17 + ((kEmail_BG.size.height + 10) * j), kEmail_BG.size.width, kEmail_BG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kEmail_BG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 - 37 + ((kEmail_BG.size.height + 10) * j), kEmail_BG.size.width, kEmail_BG.size.height) Title:securePHolder[j] Tag:453043 + j BGImage:kPassword_BG SecureText:YES KeyBoardType:UIKeyboardTypeDefault];
    }
    
    /**********************************************************************
     Date Of Birth Drop Down
     **********************************************************************/
    
    UIButton *btnSelectD = [self addDropDownOnTheView:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 1), [UIScreen mainScreen].bounds.size.height/2 - 130, 75, kEmail_BG.size.height):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 1), [UIScreen mainScreen].bounds.size.height/2 - 60, 75, kEmail_BG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 1), [UIScreen mainScreen].bounds.size.height/2 - 80, 75, kEmail_BG.size.height) Tag:4505];
    
    UIButton *btnSelectM = [self addDropDownOnTheViewForMonth:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 82), [UIScreen mainScreen].bounds.size.height/2 - 130, 75, kEmail_BG.size.height):(IS_IPHONE_4S) ?CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 82), [UIScreen mainScreen].bounds.size.height/2 - 60, 75, kEmail_BG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 82), [UIScreen mainScreen].bounds.size.height/2 - 80, 75, kEmail_BG.size.height) Tag:4506];
    
    UIButton *btnSelectY = [self addDropDownOnTheViewForYear:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 + 20, [UIScreen mainScreen].bounds.size.height/2 - 130, 75, kEmail_BG.size.height):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 + 20, [UIScreen mainScreen].bounds.size.height/2 - 60, 75, kEmail_BG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 + 20, [UIScreen mainScreen].bounds.size.height/2 - 80, 75, kEmail_BG.size.height) Tag:4507];
    
    [self addCalendarButtonOnView:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width -(kCalendarBG.size.width + 17), [UIScreen mainScreen].bounds.size.height/2 - 80, kCalendarBG.size.width, kCalendarBG.size.height) : (IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width -(kCalendarBG.size.width + 19), [UIScreen mainScreen].bounds.size.height/2 - 60, kCalendarBG.size.width, kCalendarBG.size.height):(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width -(kCalendarBG.size.width + 45), [UIScreen mainScreen].bounds.size.height/2 - 130, kCalendarBG.size.width, kCalendarBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width -(kCalendarBG.size.width + 45), [UIScreen mainScreen].bounds.size.height/2 - 100, kCalendarBG.size.width, kCalendarBG.size.height)  BGImage:kCalendarBG];
    
    NSArray *termsText = @[@"Remember my location", @"I agree with all terms and conditions"];
    
    for (int l = 0; l<=1; l++) {
        
        [self addCheckBoxButtonOnView:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 1), [UIScreen mainScreen].bounds.size.height/2 - 5  + ((kCheckboxBG.size.height + 10) * l), kCheckboxBG.size.width, kCheckboxBG.size.height):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 1), [UIScreen mainScreen].bounds.size.height/2 + 65 + ((kCheckboxBG.size.height + 10) * l), kCheckboxBG.size.width, kCheckboxBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 1), [UIScreen mainScreen].bounds.size.height/2 + 45 + ((kCheckboxBG.size.height + 10) * l), kCheckboxBG.size.width, kCheckboxBG.size.height) BGImage:kCheckboxBG BGSelected:kCheckedBG Tag:45654 + l];
        
        [self addLabelForTerms:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - (kCheckboxBG.size.width + 10)), [UIScreen mainScreen].bounds.size.height/2 - 5  + ((kCheckboxBG.size.height + 10) * l), [UIScreen mainScreen].bounds.size.width/2 + 100, 20):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - (kCheckboxBG.size.width + 10)), [UIScreen mainScreen].bounds.size.height/2 + 65 + ((kCheckboxBG.size.height + 10) * l), [UIScreen mainScreen].bounds.size.width/2 + 100, 20):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - (kCheckboxBG.size.width + 10)), [UIScreen mainScreen].bounds.size.height/2 + 45 + ((kCheckboxBG.size.height + 10) * l), [UIScreen mainScreen].bounds.size.width/2 + 100, 20)  Title:termsText[l] Tag:56567 + l Font:[UIFont systemFontOfSize:13.0]];
    }
    
    [self addRegisterButtonOnView:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 1), [UIScreen mainScreen].bounds.size.height/2 + 65, kRegisterButtonBG.size.width, kRegisterButtonBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 1), [UIScreen mainScreen].bounds.size.height/2 + 125, kRegisterButtonBG.size.width, kRegisterButtonBG.size.height) BGImage:kRegisterButtonBG];
    
    [self addView:(IS_IPHONE6) ? CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 + 75,  [UIScreen mainScreen].bounds.size.width, 44):(IS_IPHONE_5) ? CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 + 25,  [UIScreen mainScreen].bounds.size.width, 44):CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 - 20,  [UIScreen mainScreen].bounds.size.width, 44)];
    
    [self.scrollView addSubview:btnSelectD];
    [self.scrollView addSubview:btnSelectM];
    [self.scrollView addSubview:btnSelectY];
    [self.scrollView setContentSize:CGSizeMake(0.0, [UIScreen mainScreen].bounds.size.height/2 + 400)];
}


-(void)addLabelForTerms:(CGRect)frame Title:(NSString *)title Tag:(int)tag Font:(UIFont *)font {
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:frame];
    titleLabel.text = title;
    titleLabel.tag = tag;
    titleLabel.font = font;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:titleLabel];
}

-(void)addTextFieldOnView:(CGRect)frame Title:(NSString *)placeHolder Tag:(int)tag BGImage:(UIImage *)bgImage SecureText:(Boolean)secureText KeyBoardType:(UIKeyboardType)kType{
    
    VSTextField *textField = [[VSTextField alloc] init];
    [textField setTag:tag];
    [textField setFrame:frame];
    [textField setDelegate:(id)self];
    [textField setSecureTextEntry:NO];
    [textField setBackground:bgImage];
    [textField setPlaceholder:placeHolder];
    [textField setSecureTextEntry:secureText];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setKeyboardType:kType];
    [textField setFont:[UIFont italicSystemFontOfSize:14.0f]];
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 30, 30)];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    [textField setRightView:rightView];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10, 10)];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    [textField setLeftView:leftView];
    
    [self.scrollView addSubview:textField];
}

-(void)FilformFromFb
{
    UIButton *genderBtn = (UIButton*)[self.scrollView viewWithTag:6999];
    if ([[_fbDic objectForKey:@"gender"] isEqualToString:@"male"])
    {
        self.genderKeyString = @"1";
         [genderBtn setTitle:@"Male" forState:UIControlStateNormal];
    }
    else  if ([[_fbDic objectForKey:@"gender"] isEqualToString:@"female"])
    {
        self.genderKeyString = @"2";
        [genderBtn setTitle:@"Female" forState:UIControlStateNormal];
    }
   
    VSTextField *email = (VSTextField*)[self.scrollView viewWithTag:45343];
    [email setText:[NSString stringWithFormat:@"%@",[_fbDic objectForKey:@"email"]]];
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    
//    if(range.length + range.location > textField.text.length) { return NO; }
//    
//    NSUInteger newLength = [textField.text length] + [string length] - range.length;
//    
//    if (textField.tag == 45343) {
//        
//        if ([textField.text length] == 1) {
//        }
//        return newLength <= 30;
//    } else {
//        return newLength <= 20;
//    }
//    return NO;
//}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self animateTextFieldAndView:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self animateTextFieldAndView:textField up:NO];
}

- (void)animateTextFieldAndView: (UITextField*) textField up:(BOOL) up {
    
    const int movementDistance = 70;
    const float movementDuration = 0.3f;
    
    int movement = (up ? - movementDistance : movementDistance);
    
    [UIView beginAnimations:@"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration:movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}


-(void)addAnimationOnTextField:(float)cSet {
    
    [UIView animateWithDuration:.5
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self.view setFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, cSet)];
                     }
                     completion:nil];
}


-(void)addGenderAndAccountTypeButton:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UIButton *button = [[UIButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button addTarget:self action:@selector(genderAndAccountTypeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:button];
}

-(void)addCalendarButtonOnView:(CGRect)frame BGImage:(UIImage *)bgImage {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:56456];
    [button setFrame:frame];
    [button setEnabled:YES];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(userButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:button];
}


-(void)addRegisterButtonOnView:(CGRect)frame BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:56456];
    [button setFrame:frame];
    [button setTitle:@"Register" forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(registerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:button];
}

-(void)addCheckBoxButtonOnView:(CGRect)frame BGImage:(UIImage *)bgImage BGSelected:(UIImage *)s_bgImage Tag:(int)tag {
    
    VSButton *button = [VSButton buttonWithType:UIButtonTypeCustom];
    [button setTag:tag];
    [button setFrame:frame];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button setBackgroundImage:s_bgImage forState:UIControlStateSelected];
    [button addTarget:self action:@selector(checkBoxButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:button];
}

-(void)addView:(CGRect)frame {
    
    UIView *myview = [[UIView alloc]init];
    [myview setFrame:frame];
    [myview setTag:9999];
    [myview setBackgroundColor:[UIColor colorWithRed:192.0f/255.0f green:63.0f/255.0f blue:53.0f/255.0f alpha:1.0f]];
    [self.view addSubview:myview];
    
    UIButton *donebutton = [[UIButton alloc]init];
    [donebutton setFrame:CGRectMake(myview.frame.origin.x,0.0, 80, 44)];
    [donebutton setTitle:@"Done" forState:UIControlStateNormal];
    [donebutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [donebutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [donebutton addTarget:self action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [myview addSubview:donebutton];
    
    UIButton *cancelbutton = [[UIButton alloc]init];
    [cancelbutton setFrame:CGRectMake(myview.frame.size.width - 80, 0.0, 80, 44)];
    [cancelbutton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelbutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [cancelbutton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [myview addSubview:cancelbutton];
}

-(IBAction)genderAndAccountTypeAction:(UIButton *)sender {
    
    UIView *view = (UIView *)[self.view viewWithTag:9999];
    [view setHidden:NO];
    
    if (sender.tag == 6999) {
        
        currentbuttonTag = sender.tag;
        if (PressCount > 0) {
            [self.genderPicker removeFromSuperview];
        }
        self.genderPicker = [[UIPickerView alloc]init];
        [self.genderPicker setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 220 , [UIScreen mainScreen].bounds.size.width, 220)];
        [self.genderPicker setBackgroundColor:[UIColor whiteColor]];
        [self.genderPicker setUserInteractionEnabled:YES];
        [self.genderPicker setDataSource:(id)self];
        [self.genderPicker setDelegate:(id)self];
        [self.view addSubview:self.genderPicker];
        PressCount++;
        
    }else {
        
        currentbuttonTag = sender.tag;
        if (PressCount > 0) {
            [self.accountTypePicker removeFromSuperview];
        }
        self.accountTypePicker = [[UIPickerView alloc]init];
        [self.accountTypePicker setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 220 , [UIScreen mainScreen].bounds.size.width, 220)];
        [self.accountTypePicker setBackgroundColor:[UIColor whiteColor]];
        [self.accountTypePicker setUserInteractionEnabled:YES];
        [self.accountTypePicker setDataSource:(id)self];
        [self.accountTypePicker setDelegate:(id)self];
        [self.view addSubview:self.accountTypePicker];
        PressCount++;
        
    }
}

//*****************Use of PickerViewDelegate PickerViewDataSource *********************

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (currentbuttonTag == 6999) {return self.genderTypeArray.count;}
    else {return self.accountTypeArray.count;}
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    VSRegistrationType *registrationType;
    if (currentbuttonTag == 6999) {
        
        registrationType = [self.genderTypeArray objectAtIndex:row];
        genType = YES;
        return registrationType.genderTypeObject;

        
    }else {
        registrationType = [self.accountTypeArray objectAtIndex:row];
        AccType = YES;
        return registrationType.accountTypeObject;
    }
}

-(IBAction)doneButtonClicked:(UIButton *)sender {
    
    UIView *view = (UIView *)[self.view viewWithTag:9999];
    UIButton *genderButton = (UIButton *)[self.view viewWithTag:6999];
    UIButton *accountButton = (UIButton *)[self.view viewWithTag:7000];
    NSInteger row;
    VSRegistrationType *registrationType;
    
    if (currentbuttonTag == 6999) {
        
        row = [self.genderPicker selectedRowInComponent:0];
        registrationType = [self.genderTypeArray objectAtIndex:row];
        [genderButton setTitle:registrationType.genderTypeObject forState:UIControlStateNormal];
        self.genderKeyString = [NSString stringWithFormat:@"%@", registrationType.genderTypeKey];
    }else {
        
        row = [self.accountTypePicker selectedRowInComponent:0];
        registrationType = [self.accountTypeArray objectAtIndex:row];
        [accountButton setTitle:registrationType.accountTypeObject forState:UIControlStateNormal];
        self.accountTypeKeyString = [NSString stringWithFormat:@"%@",registrationType.accountTypeKey];
        
    }
    
    [view setHidden:YES];
    [self.genderPicker setHidden:YES];
    [self.accountTypePicker setHidden:YES];
}

-(IBAction)cancel:(id)sender {
    
    UIView *view = (UIView *)[self.view viewWithTag:9999];
    [self.accountTypePicker setHidden:YES];
    [self.genderPicker setHidden:YES];
    [view setHidden:YES];
}

-(IBAction)checkBoxButtonAction:(VSButton *)button {
    button.selected = !button.selected;
    
    switch (button.tag) {
        case 45654:
            self.userLocation = (placemark.locality) ? placemark.locality : @"";
            button.tag = 456504;
            break;
        case 456504:
            self.userLocation = @"";
            button.tag = 45654;
            break;
        case 45655:
            termsCheck = YES;
            button.tag = 55655;
            break;
        case 55655:
            termsCheck = NO;
            button.tag = 45655;
            break;
        default:
            break;
    }
}

#pragma mark - Drop Down Day

-(UIButton *)addDropDownOnTheView:(CGRect)frame Tag:(int)tag {
    
    UIButton *btnSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnSelect.tag = tag;
    btnSelect.frame = frame;
    [btnSelect setBackgroundColor:[UIColor whiteColor]];
    btnSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
    [btnSelect setTitle:@"Day" forState:UIControlStateNormal];
    [btnSelect.titleLabel setFont:[UIFont italicSystemFontOfSize:14.0f]];
    [btnSelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnSelect addTarget:self action:@selector(selectClicked:) forControlEvents:UIControlEventTouchUpInside];
    return btnSelect;
}

- (IBAction)selectClicked:(UIButton*)sender {
    
    currentbuttonTag = sender.tag;
    [dropDown removeFromSuperview];
    
    self.dayArray = [NSMutableArray arrayWithObjects:@"01", @"02", @"03",@"04", @"05", @"06",@"07", @"08", @"09",@"10", @"11", @"12",@"13", @"14", @"15",@"16", @"17", @"18",@"19", @"20", @"21",@"22", @"23", @"24",@"25", @"26", @"27",@"28", @"29", @"30",@"31", nil];
    if(dropDown == nil) {
        
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:self.dayArray ImgArr:nil Direction:@"down"];
        dropDown.delegate = self;
    }else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

#pragma mark - Drop Down for Month

-(UIButton *)addDropDownOnTheViewForMonth:(CGRect)frame Tag:(int)tag {
    
    UIButton *btnSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnSelect.tag = tag;
    btnSelect.frame = frame;
    [btnSelect setBackgroundColor:[UIColor whiteColor]];
    btnSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
    [btnSelect setTitle:@"Month" forState:UIControlStateNormal];
    [btnSelect.titleLabel setFont:[UIFont italicSystemFontOfSize:14.0f]];
    [btnSelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnSelect addTarget:self action:@selector(selectMonth:) forControlEvents:UIControlEventTouchUpInside];
    return btnSelect;
}

- (IBAction)selectMonth:(UIButton*)sender {
    
    currentbuttonTag = sender.tag;
    [dropDown removeFromSuperview];
    VSTextField *textField = (VSTextField *)[self.view viewWithTag:45345];
    [textField resignFirstResponder];
    
    self.monthsArray = [NSMutableArray arrayWithObjects:@"01", @"02", @"03", @"04", @"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:self.monthsArray ImgArr:nil Direction:@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

#pragma mark - Drop Down for Month

-(UIButton *)addDropDownOnTheViewForYear:(CGRect)frame Tag:(int)tag {
    
    UIButton *btnSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnSelect.frame = frame;
    btnSelect.tag = tag;
    [btnSelect setBackgroundColor:[UIColor whiteColor]];
    btnSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
    [btnSelect setTitle:@"Year" forState:UIControlStateNormal];
    [btnSelect.titleLabel setFont:[UIFont italicSystemFontOfSize:14.0f]];
    [btnSelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnSelect addTarget:self action:@selector(selectYear:) forControlEvents:UIControlEventTouchUpInside];
    return btnSelect;
}

- (IBAction)selectYear:(UIButton *)sender {
    
    currentbuttonTag = sender.tag;
    [dropDown removeFromSuperview];
    VSTextField *textField = (VSTextField *)[self.view viewWithTag:45345];
    [textField resignFirstResponder];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int i2  = [[formatter stringFromDate:[NSDate date]] intValue];
    
    for (int i = 1930; i<=i2; i++) {
        
        [self.yearArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    [[self.yearArray reverseObjectEnumerator] allObjects];
    
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:self.yearArray ImgArr:nil Direction:@"down"];
        dropDown.delegate = self;
    } else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

-(void) getDataFromDropDownCell:(NSIndexPath *)index {
    
    switch (currentbuttonTag) {
        case 4505:
            self.dayString = [self.dayArray objectAtIndex:index.row];
            break;
        case 4506:
            NSLog(@"self.monthsArrayself.monthsArray : %@", self.monthsArray);
            
            self.monthsString = [self.monthsArray objectAtIndex:index.row];
            break;
        case 4507:
            self.yearString = [self.yearArray objectAtIndex:index.row];
            break;
        default:
            break;
    }
}


- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [self rel];
}

-(void)rel {
    dropDown = nil;
}

#pragma mark - Helpers

- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId {
    
    NSLog(@"didSelectedRadioButton :-= %@ --- %@", radio.titleLabel.text, groupId);
    genderTag = ([radio.titleLabel.text isEqualToString:@"Male"]) ? 2 : 1;
}

-(IBAction)registerButtonAction:(VSButton *)button {
    
    NSLog(@"registerButtonActionregisterButtonAction ");
    
    VSTextField *email = (VSTextField *)[self.view viewWithTag:45343];
    VSTextField *userName = (VSTextField *)[self.view viewWithTag:45344];
    VSTextField *mobileNo = (VSTextField *)[self.view viewWithTag:45345];
    VSTextField *password = (VSTextField *)[self.view viewWithTag:453043];
    VSTextField *cPassword = (VSTextField *)[self.view viewWithTag:453044];
    
    NSString *dateOfBirth = [NSString stringWithFormat:@"%@/%@/%@",self.dayString,self.monthsString, self.yearString];
    
    NSLog(@"dateOfBirthdateOfBirth :- %@", dateOfBirth);
    
    if (!genType) {
        [VSEnumClass showAlertWithTitle:@"Please select Gender"];
    }

    
    else  if (!AccType) {
        [VSEnumClass showAlertWithTitle:@"Please select account type"];
    }
    
    else if (![self validateEmail:email.text]) {
        [VSEnumClass showAlertWithTitle:@"Please enter valid email address."];
    } else if ([userName.text length] == 0) {
        [VSEnumClass showAlertWithTitle:@"Please enter valid user name."];
    }else if (![self isMobileNumberValid:mobileNo.text]) {
        [VSEnumClass showAlertWithTitle:@"Please enter valid mobile number."];
    }
    
    else if ([self.dayString length] == 0 || [self.yearString length] == 0 || [self.yearString length] == 0) {
        
        [VSEnumClass showAlertWithTitle:@"Please add your DOB."];
    }

    
    else if (password.text.length <8 || password.text.length >16) {
        [VSEnumClass showAlertWithTitle:@"password should be 8-12 characters long"];
    }else if (![password.text isEqualToString:cPassword.text]) {
        [VSEnumClass showAlertWithTitle:@"Your password is not match."];
    }
       else if (termsCheck == NO) {
        [VSEnumClass showAlertWithTitle:@"Please select terms and condition box."];
    }else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            
            if ([self.accountTypeKeyString isEqualToString:@"2"]) {
                
                VSCommercialUserPop *commercialPopUp = [[VSCommercialUserPop alloc] init];
                [commercialPopUp setSubmitPopUpDelegate:(id)self];
                [self presentPopupViewController:commercialPopUp animationType:MJPopupViewAnimationSlideLeftLeft];
            } else {
                
                [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeGradient];
                [self checkEmail:email.text UserName:userName.text MobileNo:mobileNo.text DOB:dateOfBirth Password:password.text Gender:self.genderKeyString Location:self.userLocation AccountType:self.accountTypeKeyString CompanyID:@"00"];
            }
        }else {
            
            [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
}

-(void)submitButtonActionWithID:(NSString *)text {
    
    NSLog(@"gl;jdflgjdfl;kgfdkjgfkdj :- %@", text);
    
    VSTextField *email = (VSTextField *)[self.view viewWithTag:45343];
    VSTextField *userName = (VSTextField *)[self.view viewWithTag:45344];
    VSTextField *mobileNo = (VSTextField *)[self.view viewWithTag:45345];
    VSTextField *password = (VSTextField *)[self.view viewWithTag:453043];
    VSTextField *cPassword = (VSTextField *)[self.view viewWithTag:453044];
    
    NSString *dateOfBirth = [NSString stringWithFormat:@"%@/%@/%@",self.dayString,self.monthsString, self.yearString];
    
    NSLog(@"dateOfBirthdateOfBirth :- %@", dateOfBirth);
    
    if (![self validateEmail:email.text]) {
        [VSEnumClass showAlertWithTitle:@"Please enter valid email address."];
    } else if ([userName.text length] == 0) {
        [VSEnumClass showAlertWithTitle:@"Please enter valid user name."];
    }else if (![self isMobileNumberValid:mobileNo.text]) {
        [VSEnumClass showAlertWithTitle:@"Please enter mobile number."];
    }else if (![password.text isEqualToString:cPassword.text]) {
        [VSEnumClass showAlertWithTitle:@"Your password is not match."];
    }else if ([self.dayString length] == 0 || [self.yearString length] == 0 || [self.yearString length] == 0) {
        
        [VSEnumClass showAlertWithTitle:@"Please add your DOB."];
    }else if (termsCheck == NO) {
        [VSEnumClass showAlertWithTitle:@"Please select terms and condition box."];
    }else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            
            [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self checkEmail:email.text UserName:userName.text MobileNo:mobileNo.text DOB:dateOfBirth Password:password.text Gender:self.genderKeyString Location:self.userLocation AccountType:self.accountTypeKeyString CompanyID:text];
        }else {
            
            [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftRight];
}

-(BOOL)isMobileNumberValid:(NSString *)mNumber {
    
    if ( [mNumber length]>=10 && [mNumber length]<=12){
        
        BOOL valid;
        NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
        NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:mNumber];
        valid = [alphaNums isSupersetOfSet:inStringSet];
        return valid;
    }
    return NO;
   
}

-(BOOL)validateEmail:(NSString*)email {
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if (![emailTest evaluateWithObject:email]) {
        return NO;
    }
    return YES;
}
-(IBAction)userButtonAction:(id)sender {
    
    NSLog(@"userButtonAction");
}

@end
