//
//  VSforgotVC.m
//  ValShare
//
//  Created by Sagar Singh on 18/06/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSforgotVC.h"

@interface VSforgotVC ()

@end

@implementation VSforgotVC

-(void)postResetPassword:(NSString *)newPassword UserID:(NSString *)userid CurrentPassword:(NSString *)currentpassword {
    
    NSDictionary *dictData = [[NSMutableDictionary alloc] init];
    
    [dictData setValue:newPassword forKey:@"newPassword"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[userid intValue]] forKey:@"userId"];
    [dictData setValue:_token forKey:@"token"];

//    [dictData setValue:currentpassword forKey:@"currentPassword"];
    
    [[VSParserClass sharedParser] valShareResetPassword:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSLog(@"valS1111hareResetPasswordvalShareforgot :- %@", resultObject);
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Your password is change successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                
                NSMutableDictionary *resetDict = (NSMutableDictionary *)resultObject;
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [resetDict valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        }else {
            
            [SVProgressHUD dismiss];
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationItem setTitle:@"Reset Password"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    
    [self addComponentOnView];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    NSArray *textArray = @[@"Current Password",@"New Password",@"Conform New Password"];
    for (int i = 0; i  < 3 ; i++) {
        
        [self addTextFieldOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kPasswordBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 25.0f + (i * (kPasswordBG.size.height + 10)), kPasswordBG.size.width, kEmail_BG.size.height) Title:textArray[i] Tag:1100 + i];
    }
    
    [self addSubmitButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 193, kLoginBG.size.width, kEmail_BG.size.height)  Title:@"Submit" Tag:8899 BGImage:kLoginBG];
}


-(void)addTextFieldOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setTag:tag];
    [textField setFrame:frame];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:title];
    [textField setSecureTextEntry:YES];
    [textField.layer setBorderWidth:0.5];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setBackgroundColor:[UIColor whiteColor]];
    
    if (textField.tag == 1100) {
        [textField setHidden:YES];
    }
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    [self.view addSubview:textField];
}


-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(submitButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

//***** Use of UiTextField delegate *****

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    
    if (textField.tag == 1101) {
        pw1 = textField;
 
    }
    else if (textField.tag == 1102)
    {
        pw2 = textField;
    }
    
    return YES;
    
}



-(IBAction)submitButtonAction:(UIButton *)sender
{
       if (pw1.text.length == 0) {
        
        [[[UIAlertView alloc]initWithTitle:nil message:@"Please enter new password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }
    
    else if (pw1.text.length <8 || pw1.text.length >16) {
        [VSEnumClass showAlertWithTitle:@"password should be 8-12 characters long"];
    }
    
    else if (![pw1.text isEqualToString:pw2.text]) {
        
        [[[UIAlertView alloc] initWithTitle:nil message:@"Please conform new password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    } else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self postResetPassword:pw1.text UserID:[VSSingletonClass sharedMySingleton].loginUserId CurrentPassword:nil];
        }else {
            
            [[[UIAlertView alloc] initWithTitle:nil message:@"Please check internet connection.q" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }
}

@end
