//
//  VSCollectionViewCell.h
//  ValShare
//
//  Created by Sharda Prasad on 4/1/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@protocol tabDelegate <NSObject>

@end

@interface VSCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleText;

@property (nonatomic, assign) id <tabDelegate> tabDelegate;
@end
