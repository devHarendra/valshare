//
//  VSCollectionViewTab.m
//  ValShare
//
//  Created by Sharda Prasad on 4/1/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCollectionViewTab.h"

@implementation VSCollectionViewTab
@synthesize dataText = _dataText;
@synthesize imageArray = _imageArray;

-(NSMutableArray *)dataText {
    
    if (!_dataText) {
        _dataText = [[NSMutableArray alloc] init];
    }
    return _dataText;
}

-(NSMutableArray *)imageArray {
    
    if (!_imageArray) {
        _imageArray = [[NSMutableArray alloc] initWithObjects:[UIImage imageNamed:@"Search"],[UIImage imageNamed:@"Profile"],[UIImage imageNamed:@"weather"],[UIImage imageNamed:@"Search"],[UIImage imageNamed:@"Search"], nil];
    }
    return _imageArray;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 1. My Card, request your account, process transaction, searc
 */

-(void)initilizeGridView {
    
    [self setDelegate:self];
    [self setDataSource:self];
    [self setScrollEnabled:YES];
    [self registerClass:[VSCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    colorChange = 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.dataText count];//[self.dataText count]
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    VSCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    [cell setTabDelegate:(id)self];
    
    cell.backgroundColor=[UIColor grayColor];
    
    [cell.imageView setImage:[self.imageArray objectAtIndex:indexPath.row]];
    [cell.titleText setText:[self.dataText objectAtIndex:indexPath.row]];
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (IS_IPHONE_4S) {
        
        return  CGSizeMake(77.3, 70);

    }else {
        return (IS_IPHONE_5) ? CGSizeMake(77.3, 70) : CGSizeMake(91, 70);

    }
}

#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake( -10, 3, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    //VSCollectionViewCell *cell = (VSCollectionViewCell *)[colView cellForItemAtIndexPath:indexPath];
    //cell.backgroundColor = [UIColor redColor];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    //VSCollectionViewCell *cell = (VSCollectionViewCell *)[colView cellForItemAtIndexPath:indexPath];
    //cell.backgroundColor = [UIColor grayColor];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    if ([self.tabCollectionDelegate respondsToSelector:@selector(showCatListToShowDropDown:Text:)]){
        [self.tabCollectionDelegate showCatListToShowDropDown:indexPath Text:@""];
    }
    VSCollectionViewCell *cell = (VSCollectionViewCell *)[collectionView  cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor redColor];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    VSCollectionViewCell *cell = (VSCollectionViewCell *)[collectionView  cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor grayColor];
}

@end
