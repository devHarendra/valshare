//
//  VSCollectionViewCell.m
//  ValShare
//
//  Created by Sharda Prasad on 4/1/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCollectionViewCell.h"

@implementation VSCollectionViewCell
@synthesize imageView;
@synthesize titleText;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.layer.opaque = YES;
        [self.layer setCornerRadius:3];
        self.layer.shouldRasterize = YES;
        [self.layer setMasksToBounds:YES];
        [self.layer setShadowRadius:8.0f];
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        [self.layer setRasterizationScale:[[UIScreen mainScreen] scale]];
        
        UIImageView *gridImageView = [[UIImageView alloc] init];
        [gridImageView.layer setShadowRadius:5.0f];
        gridImageView.layer.shadowColor = [UIColor blackColor].CGColor;

        [self addSubview:gridImageView];
        self.imageView = gridImageView;

        UILabel *label = [[UILabel alloc] init];
        [label setTag:245];
        [label.layer setShadowRadius:5.0f];
        [label setFont:[UIFont systemFontOfSize:13.0f]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        label.layer.shadowColor = [UIColor blackColor].CGColor;

        [self addSubview:label];
        self.titleText = label;
    }
    return self;
}

-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    [self.imageView setFrame:CGRectMake(20.0, 8.0, [UIImage imageNamed:@"Profile"].size.width, [UIImage imageNamed:@"Profile"].size.height)];
    [self.titleText setFrame:(IS_IPHONE_5) ? CGRectMake(0.0f, self.imageView.frame.size.height + 13, 80, 20) : CGRectMake(3.0f, self.imageView.frame.size.height + 13, 84, 20)];

}

@end
