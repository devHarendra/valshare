//
//  VSCollectionViewTab.h
//  ValShare
//
//  Created by Sharda Prasad on 4/1/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSCollectionViewCell.h"
#import "VSHeader.h"

@protocol tabCollectionDelegate <NSObject>

-(void)showCatListToShowDropDown:(NSIndexPath *)index Text:(NSString *)text;

@end

@interface VSCollectionViewTab : UICollectionView <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate> {
    int colorChange;
}

-(void)initilizeGridView;

@property (nonatomic, assign) id <tabCollectionDelegate> tabCollectionDelegate;
@property (nonatomic, strong) NSMutableArray *dataText;
@property (nonatomic, strong) NSMutableArray *imageArray;

@end
