//
//  VehicleViewController.m
//  VolShare
//
//  Created by Vivek Kumar on 4/3/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import "VehicleViewController.h"

@interface VehicleViewController () {
    
    UITableView *vehicleTable;
}

@end

@implementation VehicleViewController
@synthesize dataArray = _dataArray;
@synthesize userId = _userId;

-(NSMutableArray *)dataArray {
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)getVehicleListData:(NSString *)userId {
    
    [[VSParserClass sharedParser] getUserVehicleList:userId WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else {
                
                NSMutableDictionary *localVehicleDict = (NSMutableDictionary *)[[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                
                if ([localVehicleDict isKindOfClass:[NSDictionary class]]) {
                    
                    NSMutableArray *dataArray = (NSMutableArray *)[localVehicleDict valueForKey:@"vehicleModelList"];
                    
                    NSLog(@"dataArraydataArraydataArray :- %@", dataArray);
                    [self.dataArray removeAllObjects];
                    [dataArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *enumerateDict = [dataArray objectAtIndex:idx];
                        VSVehicleListModal *vehicleModal = [[VSVehicleListModal alloc] init];
                        [vehicleModal setIsDefault:[enumerateDict valueForKey:@"isDefault"]];
                        [vehicleModal setLastModifiedTime:[enumerateDict valueForKey:@"lastModifiedTime"]];
                        [vehicleModal setRegistrationNumber:[enumerateDict valueForKey:@"registrationNumber"]];
                        [vehicleModal setUserVehicleInfoId:[enumerateDict valueForKey:@"userVehicleInfoId"]];
                        [vehicleModal setUserVehicleInfoStatus:[enumerateDict valueForKey:@"userVehicleInfoStatus"]];
                        [vehicleModal setUserVehicleInfoStatusId:[enumerateDict valueForKey:@"userVehicleInfoStatusId"]];
                        [vehicleModal setVehicleType:[enumerateDict valueForKey:@"vehicleType"]];
                        [vehicleModal setVehicleTypeId:[enumerateDict valueForKey:@"vehicleTypeId"]];
                        [vehicleModal setVehicleTypeName:[enumerateDict valueForKey:@"vehicleTypeName"]];
                        [vehicleModal setUserId:[enumerateDict valueForKey:@"userId"]];
                        [self.dataArray addObject:vehicleModal];
                    }];
                } else {
                    NSLog(@"elseelseelse:--- %@", localVehicleDict);
                }
            }
            [SVProgressHUD dismiss];
            
        }else {
            [SVProgressHUD dismiss];
            
            NSLog(@"getUserVehicleList:--- %@", resultObject);
        }
        
        [vehicleTable removeFromSuperview];
        [self addTableOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 60) Tag:2450];
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)deleteVehicle:(NSString *)vehicleId IndexPath:(NSIndexPath *)index {
    
    [[VSParserClass sharedParser] getDeleteVehicleList:vehicleId WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        if (result) {
            
            NSLog(@"dkjhbfdkjghfdk :- %@", resultObject);
            
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Vehicle deleted successfully." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [self.dataArray removeObjectAtIndex:index.row];
                [vehicleTable reloadData];
                [SVProgressHUD dismiss];
            } else {
                
                [SVProgressHUD dismiss];
                [[[UIAlertView alloc] initWithTitle:nil message:@"Vehicle not deleted successfully." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
            
        } else {
            
            [SVProgressHUD dismiss];
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}

- (void)addVehicleOnTableAfterAdd {
    
    if ([[Reachability reachabilityForInternetConnection]isReachable]) {
        
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getVehicleListData:[VSSingletonClass sharedMySingleton].loginUserId];
        
    }else {
        
        [[[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if ([[Reachability reachabilityForInternetConnection]isReachable]) {
        
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        
        [self getVehicleListData:[VSSingletonClass sharedMySingleton].loginUserId];
    }else {
        
        [[[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    
    self.title = [NSString stringWithFormat:@"%@", @"My Vehicle"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    self.navigationItem.rightBarButtonItem = [self setRightBarButton];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [[VSSingletonClass sharedMySingleton] setCustomDelegate:(id)self];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setRightBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kAddVehicleBG.size.width - 5, kAddVehicleBG.size.height - 5);
    [btnSettings setImage:kAddVehicleBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnRightBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc] initWithCustomView:btnSettings];
    
    return barBtnItem;
}

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnRightBarButtonAction:(id)sender {
    
    VSVehicleInfoViewController *vehicleInfo = [[VSVehicleInfoViewController alloc]init];
    [vehicleInfo setVCTitle:@"add"];
    [vehicleInfo setUserID:self.userId];
    [self.navigationController pushViewController:vehicleInfo animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    vehicleTable = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    [vehicleTable setTag:tag];
    [vehicleTable setDelegate:(id)self];
    [vehicleTable setDataSource:(id)self];
    [vehicleTable setBackgroundColor:[UIColor clearColor]];
    [vehicleTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [vehicleTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [self.view addSubview:vehicleTable];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return (![self.dataArray count] == 0)?[self.dataArray count]:1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    
    VSVehicleListModal *vehicleModal = [self.dataArray objectAtIndex:indexPath.row];
    
    return (self.dataArray.count == 0) ? NO :([vehicleModal.userVehicleInfoStatus isEqualToString:@"Default"]) ? NO :YES;
   
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VSVehicleListModal *vehicleModal = [self.dataArray objectAtIndex:indexPath.row];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            
            [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self deleteVehicle:vehicleModal.userVehicleInfoId IndexPath:indexPath];
        }else {
            
            [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    VehicleTableCell *cell = (VehicleTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[VehicleTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setBackgroundColor:[UIColor clearColor]];
        
        UIView *lineView = [[UIView alloc]init];
        [lineView setFrame:CGRectMake(0.0, 110.0, [UIScreen mainScreen].bounds.size.width, 0.5)];
        [lineView setBackgroundColor:[UIColor lightGrayColor]];
        [cell.contentView addSubview:lineView];
        [lineView setHidden:(![self.dataArray count] == 0) ? NO :YES];
        
    }
    
    if (![self.dataArray count] == 0) {
        
        [cell.type setHidden:NO];
        [cell.name setHidden:NO];
        [cell.date setHidden:NO];
        [cell.finalDate setHidden:NO];
        [cell.finalName setHidden:NO];
        [cell.finalType setHidden:NO];
        [cell.registrationNo setHidden:NO];
        [cell.finalRegistrationNo setHidden:NO];
        
        VSVehicleListModal *vehicleModal = [self.dataArray objectAtIndex:indexPath.row];
        
        [cell.type setText:@"Type"];
        [cell.name setText:@"Name"];
        [cell.date setText:@"Date"];
        [cell.registrationNo setText:@"Registration No"];
        
        [cell.finalType setText:[NSString stringWithFormat:@": %@", vehicleModal.vehicleType]];
        [cell.finalName setText:[NSString stringWithFormat:@": %@", vehicleModal.vehicleTypeName]];
        [cell.finalRegistrationNo setText:[NSString stringWithFormat:@": %@", vehicleModal.registrationNumber]];
        [cell.finalDate setText:[NSString stringWithFormat:@": %@", vehicleModal.lastModifiedTime]];
        cell.accessoryType = UITableViewCellAccessoryDetailButton;
        
    } else {
        
        [cell.type setHidden:YES];
        [cell.name setHidden:YES];
        [cell.date setHidden:YES];
        [cell.finalDate setHidden:YES];
        [cell.finalType setHidden:YES];
        [cell.registrationNo setHidden:YES];
        [cell.finalRegistrationNo setHidden:YES];
        [cell.finalName setText:@"No data found"];
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (![self.dataArray count] == 0) {
        
        VSVehicleListModal *vehicleModal = [self.dataArray objectAtIndex:indexPath.row];
        [self navigateToUpdateScreen:vehicleModal];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    VSVehicleListModal *vehicleModal = [self.dataArray objectAtIndex:indexPath.row];
    [self navigateToDetailScreen:vehicleModal];
}

-(void)navigateToUpdateScreen:(VSVehicleListModal *)vehicleData {
    
    NSLog(@"navigateToUpdateScreen %@ -- %@ -- %@", vehicleData.userId, vehicleData.userVehicleInfoStatusId, vehicleData.userVehicleInfoId);
    
    VSVehicleInfoViewController *vehicleInfo = [[VSVehicleInfoViewController alloc]init];
    [vehicleInfo setVCTitle:@"update"];
    [vehicleInfo setVehicleModal:vehicleData];
    [vehicleInfo setVehicleInfoStatus:vehicleData.userVehicleInfoStatus];
    [self.navigationController pushViewController:vehicleInfo animated:YES];
}

-(void)navigateToDetailScreen:(VSVehicleListModal *)vehicleData {
    
    VSVehicelDetailView *detailView = [[VSVehicelDetailView alloc]init];
    [detailView setVehicleModal:vehicleData];
    [self.navigationController pushViewController:detailView animated:YES];
}

@end
