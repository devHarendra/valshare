//
//  VSVehicleViewController.m
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSVehicleInfoViewController.h"

@interface VSVehicleInfoViewController () {
    
    int         currentbuttonTag;
    int         vehicleTypeTag;
    NSString    *vehicleTypeNameTag;
    NSString     *markAsDefaultsTag;
}

@end
@implementation VSVehicleInfoViewController
@synthesize vehicleTypeArray = _vehicleTypeArray;
@synthesize vehicleModal = _vehicleModal;
@synthesize VCTitle;
@synthesize loginStatus;
@synthesize userID;

-(NSMutableArray *)vehicleTypeArray {
    
    if(!_vehicleTypeArray) {
        _vehicleTypeArray = [[NSMutableArray alloc] init];
    }
    return _vehicleTypeArray;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)addVehicleInfo:(NSString *)userId registrationNumber:(NSString *)registrationNumber vehicleTypeName:(NSString *)vehicleTypeName vehicleType:(NSString *)vehicleType isDefault:(NSString *)isDefault vehicleTypeId:(NSString *)vehicleTypeId {
    
    NSMutableDictionary *vehicledata = [[NSMutableDictionary alloc] init];
    [vehicledata setObject:registrationNumber forKey:@"registrationNumber"];
    [vehicledata setObject:vehicleTypeName forKey:@"vehicleTypeName"];
    [vehicledata setObject:userId forKey:@"userId"];
    [vehicledata setObject:vehicleType forKey:@"vehicleType"];
    [vehicledata setObject:isDefault forKey:@"isDefault"];// = true or false
    [vehicledata setObject:vehicleTypeId forKey:@"vehicleTypeId"];// = 1 or 2, 3,
    
    NSLog(@"addVehicleInfo:addVehicleInfo: :- %@", vehicledata);
    
    [[VSParserClass sharedParser] getAddVehicle:vehicledata WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSLog(@"getAddVehicle :---------:- %@", resultObject);
            
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([self.loginStatus isEqualToString:@"varification"]) {
                        [self navigateToAddCard:self.loginStatus];
                    }else {
                        [[[UIAlertView alloc] initWithTitle:nil message:@"Vehicle Add Successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                    }
                });
            }else if ([[resultObject valueForKey:@"status"] isEqualToString:@"Exception"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [resultObject valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Problem in adding vehicle, please contact to admin." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            NSLog(@"addVehicleInfo :- %@", resultObject);
        }
    }];
}
-(void)navigateToAddCard:(NSString *)varification {
    
    VSCardDetailViewController *cardDetails = [[VSCardDetailViewController alloc] init];
    [cardDetails setFirstLogin:@"varification"];
    [cardDetails setUserID:self.userID];
    [self.navigationController pushViewController:cardDetails animated:YES];
}

-(void)addUpdateVehicleInfo:(NSString *)userId registrationNumber:(NSString *)registrationNumber vehicleTypeName:(NSString *)vehicleTypeName vehicleType:(NSString *)vehicleType isDefault:(NSString *)isDefault vehicleTypeId:(NSString *)vehicleTypeId userVehicleInfoStatusId:(NSString *)userVehicleInfoStatusId userVehicleInfoId:(NSString *)userVehicleInfoId {
    
    NSMutableDictionary *vehicledata = [[NSMutableDictionary alloc] init];
    [vehicledata setObject:registrationNumber forKey:@"registrationNumber"];
    [vehicledata setObject:vehicleTypeName forKey:@"vehicleTypeName"];
    [vehicledata setObject:userId forKey:@"userId"];
    [vehicledata setObject:vehicleType forKey:@"vehicleType"];
    [vehicledata setObject:isDefault forKey:@"isDefault"];// = true or false
    [vehicledata setObject:vehicleTypeId forKey:@"vehicleTypeId"];// = 1 or 2, 3,
    [vehicledata setObject:userVehicleInfoStatusId forKey:@"userVehicleInfoStatusId"];
    [vehicledata setObject:userVehicleInfoId forKey:@"userVehicleInfoId"];
    
    [[VSParserClass sharedParser] getUpdateVehicleList:vehicledata WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSMutableDictionary *dictData = (NSMutableDictionary *)resultObject;
            if ([[dictData valueForKey:@"status"] isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[dictData valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }else if ([[dictData valueForKey:@"status"] isEqualToString:@"Exception"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[dictData valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Vehicle update successfully." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if ([[VSSingletonClass sharedMySingleton].customDelegate respondsToSelector:@selector(addVehicleOnTableAfterAdd)]) {
        [[VSSingletonClass sharedMySingleton].customDelegate addVehicleOnTableAfterAdd];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    
    markAsDefaultsTag = [NSString stringWithFormat:@"%@", @"false"];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController setNavigationBarHidden:NO];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.title = [NSString stringWithFormat:@"%@", @"Vehicle Information"];
    
    if(![loginStatus isEqualToString:@"varification"]) {
        self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    }
    [self adddComponentOnView];
    
}


#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc] initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adddComponentOnView {
    
    NSArray *updateData;
    if (![VCTitle isEqualToString:@"add"]) {
        updateData = @[self.vehicleModal.vehicleTypeName,self.vehicleModal.registrationNumber];
    }
    NSArray *text = @[@"Vehicle Name",@"Registration Number"];
    
    [self addButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 40, kLoginBG.size.width, 30) Title:(self.vehicleModal.vehicleType) ? self.vehicleModal.vehicleType : @"Vehicle Type" Tag:2000];
    
    for (int i =0; i < 2; i++) {
        
        [self addTextViewOnFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y+85+(i * 90), kLoginBG.size.width, 70) Title:(![VCTitle isEqualToString:@"add"]) ? updateData[i] : text[i] Tag:2225 + i];
    }
    
    if(![loginStatus isEqualToString:@"varification"] && ![_VehicleInfoStatus isEqualToString:@"Default"]) {
        
        [self addCheckBoxButtonOnView:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2-11), [UIScreen mainScreen].bounds.origin.y + 260, kCheckBlackBG1.size.width, kCheckBlackBG1.size.height):(IS_IPHONE6PLUS) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 +15), [UIScreen mainScreen].bounds.origin.y + 260, kCheckBlackBG1.size.width, kCheckBlackBG1.size.height) : CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2+2), [UIScreen mainScreen].bounds.origin.y + 260, kCheckBlackBG1.size.width, kCheckBlackBG1.size.height) BGImage:kCheckBlackBG1 BGSelected:kCheckedBlackBG1 Tag:45654 ];
        
        [self addLabelForCheckboxOnView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 25 + kCheckBlackBG1.size.width, [UIScreen mainScreen].bounds.origin.y + 260, [UIScreen mainScreen].bounds.size.width/3+20, 25):CGRectMake([UIScreen mainScreen].bounds.origin.x + 50 + kCheckBlackBG1.size.width, [UIScreen mainScreen].bounds.origin.y + 260, [UIScreen mainScreen].bounds.size.width/3+20, 25) Title:@"Mark As Defult" Tag:4425 Font:[UIFont systemFontOfSize:14.0]];
    }
  
    
    [self addUpdateButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 297, kLoginBG.size.width, kLoginBG.size.height) Title:([VCTitle isEqualToString:@"add"]) ? @"add Vehicle" : @"Update Vehicle" Tag:([VCTitle isEqualToString:@"add"]) ? 31000 : 32000 BGImage:kLoginBG];
}

-(void)addButtonOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UIButton *btnSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnSelect.tag = tag;
    btnSelect.frame = frame;
    [btnSelect setBackgroundColor:[UIColor lightGrayColor]];
    [btnSelect setTitle:title forState:UIControlStateNormal];
    btnSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
    [btnSelect.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [btnSelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnSelect addTarget:self action:@selector(selectTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSelect];
}

- (void)addTextViewOnFrame:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextView *textView = [[UITextView alloc]init];
    [textView setFrame:frame];
    [textView setDelegate:(id)self];
    [textView setText:title];
    [textView setTag:tag];
    [textView.layer setBorderWidth:0.5];
    [textView setTextColor:([VCTitle isEqualToString:@"add"]) ?  [UIColor lightGrayColor] : [UIColor blackColor]];
    [textView setFont:[UIFont systemFontOfSize:15.0f]];
    [textView setBackgroundColor:[UIColor whiteColor]];
    [textView setTextAlignment:NSTextAlignmentLeft];
    [textView setContentInset:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
    [self.view addSubview:textView];
}


-(void)addCheckBoxButtonOnView:(CGRect)frame BGImage:(UIImage *)bgImage BGSelected:(UIImage *)s_bgImage Tag:(int)tag {
    
    VSButton *button = [VSButton buttonWithType:UIButtonTypeCustom];
    [button setTag:tag];
    [button setFrame:frame];
    [button setSelected:([self.vehicleModal.isDefault boolValue]) ? YES : NO];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button setBackgroundImage:s_bgImage forState:UIControlStateSelected];
    [button addTarget:self action:@selector(selectBoxButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(void)addLabelForCheckboxOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag Font:(UIFont *)font {
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:frame];
    titleLabel.text = title;
    titleLabel.tag = tag;
    titleLabel.font = font;
    [titleLabel setTextColor:[UIColor blackColor]];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleLabel];
}

-(void)addUpdateButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(updateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (BOOL)textFieldShouldReturn:(UITextView *)textView {
    
    [textView resignFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if (textView.tag == 2225) {
        
        if ([textView.text isEqualToString:@"Vehicle Name"]) {
            textView.text = @"";
        }
    }else if (textView.tag == 2226) {
        
        if ([textView.text isEqualToString:@"Registration Number"]) {
            textView.text = @"";
        }
    }
    textView.textColor = [UIColor blackColor];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (textView.tag == 2225) {
        
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Vehicle Name";
        }
    }else if  (textView.tag == 2226) {
        
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Registration Number";
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(IBAction)selectTypeClicked:(UIButton *)sender {
    
    [dropDown removeFromSuperview];
    currentbuttonTag = sender.tag;
    
    self.vehicleTypeArray = [NSMutableArray arrayWithObjects:@"Truck", @"Car",@"Suv", nil];
    if(dropDown == nil) {
        
        CGFloat f = 115;
        dropDown = [[NIDropDown alloc] showDropDown:sender Height:&f Arr:self.vehicleTypeArray ImgArr:nil Direction:@"down"];
        dropDown.delegate = self;
    }else {
        
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

-(void) getDataFromDropDownCell:(NSIndexPath *)index {
    
    vehicleTypeNameTag = [NSString stringWithFormat:@"%@", [self.vehicleTypeArray objectAtIndex:index.row]];
    vehicleTypeTag = index.row + 1;
}

- (void) niDropDownDelegateMethod: (NIDropDown *)sender {
    [self rel];
}

-(void)rel {
    dropDown = nil;
}

-(IBAction)selectBoxButtonAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    switch (sender.tag) {
        case 45654:
            
            markAsDefaultsTag = [NSString stringWithFormat:@"%@", @"true"];
            sender.tag = 450654;
            break;
        case 450654:
            
            markAsDefaultsTag = [NSString stringWithFormat:@"%@", @"false"];
            sender.tag = 45654;
            break;
        default:
            break;
    }
}

-(IBAction)updateButtonAction:(VSButton *)button {
    
    switch (button.tag) {
        case 31000:
            [self addVehicle];
            break;
        case 32000:
            [self updateVehicle];
            break;
        default:
            break;
    }
}


-(void)addVehicle {
    
    UITextView *vehicleTypeName = (UITextView *)[self.view viewWithTag:2225];
    UITextView *registrationNumber = (UITextView *)[self.view viewWithTag:2226];
    UIButton *vehicleTypeButton = (UIButton *)[self.view viewWithTag:2000];
    
    NSLog(@"1:-%d2:-%d",vehicleTypeName.text.length,registrationNumber.text.length);
    if ([vehicleTypeButton.titleLabel.text isEqualToString:@"Vehicle Type"]) {
        
        [[[UIAlertView alloc]initWithTitle:nil message:@"Please select vehicle type" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
        
    }else if ([vehicleTypeName.text isEqualToString:@"Vehicle Name"]){
        
        [[[UIAlertView alloc]initWithTitle:nil message:@"Please fill vehicle name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
        
    }else if ([registrationNumber.text isEqualToString:@"Registration Number"]) {
        
        [[[UIAlertView alloc]initWithTitle:nil message:@"Please fill registration number" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
    }
    else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self addVehicleInfo:self.userID registrationNumber:registrationNumber.text vehicleTypeName:vehicleTypeName.text vehicleType:vehicleTypeNameTag isDefault:@"true" vehicleTypeId:[NSString stringWithFormat:@"%d", vehicleTypeTag]];
        }else {
            
            [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
}

-(void)updateVehicle {
    
    UITextView *vehicleTypeName = (UITextView *)[self.view viewWithTag:2225];
    UITextView *registrationNumber = (UITextView *)[self.view viewWithTag:2226];
    
    NSLog(@"self.vehicleModal.vehicleTypeId:-%@markAsDefaultsTag:-%@",self.vehicleModal.vehicleTypeId,vehicleTypeNameTag);
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self addUpdateVehicleInfo:self.vehicleModal.userId registrationNumber:registrationNumber.text vehicleTypeName:vehicleTypeName.text vehicleType:(vehicleTypeNameTag) ? vehicleTypeNameTag : self.vehicleModal.vehicleTypeName isDefault:[NSString stringWithFormat:@"%@", markAsDefaultsTag] vehicleTypeId:[NSString stringWithFormat:@"%d",(vehicleTypeTag) ? vehicleTypeTag:[self.vehicleModal.vehicleTypeId intValue]] userVehicleInfoStatusId:self.vehicleModal.userVehicleInfoStatusId userVehicleInfoId:self.vehicleModal.userVehicleInfoId];
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
    }
}

@end
