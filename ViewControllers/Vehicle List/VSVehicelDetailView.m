//
//  VSVehicelDetailView.m
//  ValShare
//
//  Created by Vivek Kumar on 5/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSVehicelDetailView.h"


@interface VSVehicelDetailView ()

@end

@implementation VSVehicelDetailView
@synthesize vehicleModal;
@synthesize intractController;

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"Add Vehicel Document"];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    [self addComponentOnView];
}

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc] initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addComponentOnView {
    
    NSArray *textArray = @[[NSString stringWithFormat:@"Vehicle Type:               %@",self.vehicleModal.vehicleType],[NSString stringWithFormat:@"Vehicle Name:              %@",self.vehicleModal.vehicleTypeName],[NSString stringWithFormat:@"Registration Number:  %@",self.vehicleModal.registrationNumber],[NSString stringWithFormat:@"Last Modified Time:     %@",self.vehicleModal.lastModifiedTime]];
    
    for (int i = 0; i < 4; i++) {
        
        [self addLabelOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.origin.y + 10 + (30 * i), [UIScreen mainScreen].bounds.size.width - 20, 30) title:textArray[i] Tag:55556+i];
    }
    
    [self addLabelForDocument:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 , [UIScreen mainScreen].bounds.origin.y + 135, kLoginBG.size.width , kLoginBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 - 15, [UIScreen mainScreen].bounds.origin.y + 135, kLoginBG.size.width + 30, kLoginBG.size.height) Tag:22557];
    
    [self addUpdateButtonOnView:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 , [UIScreen mainScreen].bounds.origin.y + 185, kLoginBG.size.width , kLoginBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 - 15, [UIScreen mainScreen].bounds.origin.y + 185, kLoginBG.size.width + 30, kLoginBG.size.height) Title:@"Add Vehicle Document" Tag:65549 BGImage:kLoginBG];
    
}
-(void)addLabelOnView:(CGRect)frame title:(NSString *)title Tag:(int)tag {
    
    UILabel *label = [[UILabel alloc]init];
    [label setFrame:frame];
    [label setTag:tag];
    [label setText:title];
    [label setTextColor:[UIColor blackColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:(IS_IPHONE_5)? [UIFont boldSystemFontOfSize:13.0]:[UIFont boldSystemFontOfSize:15.0]];
    [self.view addSubview:label];
}

-(void)addUpdateButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addDocumentButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(void)addLabelForDocument:(CGRect)frame Tag:(int)tag {
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.tag = tag;
    [label.layer setBorderWidth:1.0];
    [label setTextColor:[UIColor blackColor]];
    label.backgroundColor = [UIColor whiteColor];
    [label setFont:[UIFont systemFontOfSize:14.0]];
    [label.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:label];
}

-(IBAction)addDocumentButtonAction:(id)sender {
    
   // NSURL *furl = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"temp.pdf"]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(
                                                         NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory
                          stringByAppendingPathComponent:@"ENERGYSTAR.pdf"];
    NSURL *fileUrl = [NSURL fileURLWithPath:filePath];

    self.intractController = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
    
    [self.intractController setDelegate:(id)self];
    [self.intractController presentPreviewAnimated:YES];
    
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}



@end
