//
//  VehicleViewController.h
//  VolShare
//
//  Created by Vivek Kumar on 4/3/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "VehicleTableCell.h"
#import "VSVehicleInfoViewController.h"


@interface VehicleViewController : UIViewController <UITableViewDataSource, UITableViewDelegate >

@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSString *userId;
@end
