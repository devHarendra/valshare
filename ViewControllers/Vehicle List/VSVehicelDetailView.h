//
//  VSVehicelDetailView.h
//  ValShare
//
//  Created by Vivek Kumar on 5/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@interface VSVehicelDetailView : UIViewController
@property (strong , nonatomic) VSVehicleListModal *vehicleModal;
@property (strong , nonatomic) UIDocumentInteractionController *intractController;
@end
