//
//  VSVehicleViewController.h
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "NIDropDown.h"
#import "VSVehicleListModal.h"
#import "VehicleViewController.h"

@protocol VSVehicleInfoViewControllerDelegate

-(void)addVehicleOnTableAfterAdd;

@end

@interface VSVehicleInfoViewController : UIViewController <NIDropDownDelegate> {
    
    NIDropDown *dropDown;
}

@property (nonatomic, strong) id <VSVehicleInfoViewControllerDelegate> infoViewControllerDelegate;
@property (nonatomic, strong) NSString *VCTitle;
@property (nonatomic, strong) NSString *loginStatus;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *VehicleInfoStatus;

@property (nonatomic, strong) NSMutableArray *vehicleTypeArray;
@property (nonatomic, retain) VSVehicleListModal *vehicleModal;

@end
