//
//  VSResetPasswordView.m
//  ValShare
//
//  Created by Vivek Kumar on 4/9/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSResetPasswordView.h"

@interface VSResetPasswordView ()

@end

@implementation VSResetPasswordView

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationItem setTitle:@"Reset Password"];
    [self addComponentOnView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    NSArray *textArray = @[@"New Password",@"Conform New Password",@"Old Password"];
    
    for (int i = 0; i  < 3 ; i++) {
        
        [self addTextFieldOnView:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.origin.x +35.0f, [UIScreen mainScreen].bounds.origin.y + 65.0f + (i * 43), [UIScreen mainScreen].bounds.size.width/2+93, 30):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x +35.0f, [UIScreen mainScreen].bounds.origin.y + 65.0f + (i * 43), [UIScreen mainScreen].bounds.size.width/2+93, 30):CGRectMake([UIScreen mainScreen].bounds.origin.x +45.0f, [UIScreen mainScreen].bounds.origin.y + 65.0f + (i * 43), [UIScreen mainScreen].bounds.size.width/2+93, 30) Title:textArray[i] Tag:1000+i];
    }
    
    [self addSubmitButtonOnView:(IS_IPHONE6PLUS) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2-15, [UIScreen mainScreen].bounds.origin.y+193, kLoginBG.size.width+13, kLoginBG.size.height):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2+20, [UIScreen mainScreen].bounds.origin.y+193, kLoginBG.size.width-35, kLoginBG.size.height):(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2+20, [UIScreen mainScreen].bounds.origin.y+193, kLoginBG.size.width-35, kLoginBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2+5 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y+193, kLoginBG.size.width-8, kLoginBG.size.height)  Title:@"Submit" Tag:8899 BGImage:kLoginBG];
    
}

-(void)addTextFieldOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setFrame:frame];
    [textField setTag:tag];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:title];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:textField];
}

-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(submitButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

//***** Use of UiTextField delegate *****

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}



-(IBAction)submitButtonAction:(UIButton *)sender {
    
    UITextField *textfield = (UITextField *)[self.view viewWithTag:1000];
    UITextField *textfield1 = (UITextField *)[self.view viewWithTag:1001];
    UITextField *textfield2 = (UITextField *)[self.view viewWithTag:1002];
    
    if (textfield.text.length == 0 || textfield1.text.length == 0 || textfield2.text.length == 0) {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"Please Fill All Detail" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    }else {}
    
}

@end
