//
//  VSPurchesHistoryView.m
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSPurchesHistoryView.h"

@interface VSPurchesHistoryView ()

@end

@implementation VSPurchesHistoryView
@synthesize dateArray = _dateArray;

-(NSMutableArray *)dateArray {
    
    if (!_dateArray) {
        _dateArray = [[NSMutableArray alloc] initWithObjects:@"1",@"2", nil];
    }
    return _dateArray;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationItem setTitle:@"Purches History Detail"];
    
    [self adddComponentOnView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adddComponentOnView {
    
    
    
    [self addTableOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 5.0f, [UIScreen mainScreen].bounds.origin.y+10, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-150) Tag:2450];
    
     [self addSubmitButtonOnView:(IS_IPHONE6PLUS) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2-18, [UIScreen mainScreen].bounds.origin.y+[UIScreen mainScreen].bounds.size.height-120, kLoginBG.size.width+20, kLoginBG.size.height):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2+5,[UIScreen mainScreen].bounds.origin.y +[UIScreen mainScreen].bounds.size.height-120, kLoginBG.size.width-23, kLoginBG.size.height):(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2+5 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y +[UIScreen mainScreen].bounds.size.height-120, kLoginBG.size.width-5, kLoginBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2+5 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y +[UIScreen mainScreen].bounds.size.height-120, kLoginBG.size.width-5, kLoginBG.size.height)  Title:@"Save" Tag:8899 BGImage:kLoginBG];
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    UITableView *cardTable = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
    [cardTable setBackgroundColor:[UIColor clearColor]];
    [cardTable setTag:tag];
    [cardTable setDelegate:(id)self];
    [cardTable setDataSource:(id)self];
    [cardTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [self.view addSubview:cardTable];
}

-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(saveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 180;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dateArray count];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.dateArray removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    MyRouteTableCell *cell = (MyRouteTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[MyRouteTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    [cell.myId setText:@"Package Name"];
    [cell.myId setTextColor:[UIColor whiteColor]];
  
    
    [cell.source setText:@"Card Number"];
    [cell.source setTextColor:[UIColor whiteColor]];
    
    [cell.destination setText:@"Crated Time"];
    [cell.destination setTextColor:[UIColor whiteColor]];
    
    [cell.date setText:@"Effective From"];
    [cell.date setTextColor:[UIColor whiteColor]];
    
    [cell.effectiveTo setText:@"Effective To"];
    [cell.purchesStatus setText:@"Purches Status"];
    [cell.packagePrice setText:@"Package Price"];
    [cell.loyalityPoint setText:@"Loyality Point"];
    
    [cell.myFinalId setText:@": Monthly"];
    [cell.myFinalId setTextColor:[UIColor whiteColor]];

    [cell.finalSource setText:@": 425555445556568"];
    [cell.finalSource setTextColor:[UIColor whiteColor]];

    [cell.finalDestination setText:@": Mar 15,2015 4:05:23 PM"];
    [cell.finalDestination setTextColor:[UIColor whiteColor]];

    [cell.finalDate setText:@": Mar 15,2015 4:05:22 PM"];
    [cell.finalDate setTextColor:[UIColor whiteColor]];

    [cell.finalEffectiveTo setText:@": 2015 4:05:22:0"];
    [cell.finalPurchesStatus setText:@": Succes"];
    [cell.finalPackagePrice setText:@": 100.0"];
    [cell.finalLoyalityPoint setText:@": 0"];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}



-(IBAction)saveButtonAction:(UIButton *)sender {
}

@end
