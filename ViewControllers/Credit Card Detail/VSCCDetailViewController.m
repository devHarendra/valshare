//
//  VSCCDetailViewController.m
//  ValShare
//
//  Created by Vivek Kumar on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCCDetailViewController.h"

@interface VSCCDetailViewController ()

@end

@implementation VSCCDetailViewController
@synthesize dateArray = _dateArray;

-(NSMutableArray *)dateArray {
    
    if (!_dateArray) {
        _dateArray = [[NSMutableArray alloc] init];
    }
    return _dateArray;
}

-(void)getCardDetailFromServer:(NSString *)userID {
    
    [[VSParserClass sharedParser] VSGetCardDetail:userID WithCompletionBlock:^(BOOL result ,id resultObject, NSError *error){
        
        NSLog(@"resultObjectresultObjectresultObject:-%@",resultObject);
        if (result) {
            NSMutableDictionary *lDictData = (NSMutableDictionary *)resultObject;
            
            if ([[lDictData valueForKey:@"status"] isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
                [SVProgressHUD dismiss];
                
            }else if ([[lDictData valueForKey:@"status"] isEqualToString:@"Exception"]) {
                
                [[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",[resultObject valueForKey:@"message"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [SVProgressHUD dismiss];
            } else {
                
                NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[lDictData valueForKey:@"Data"]];
                
                if ([dictdata isKindOfClass:[NSDictionary class]]) {
                    
                    NSArray *arr = [dictdata objectForKey:@"creditDebitCardModelList"];
                    for (int i =0; i<arr.count; i++) {
                        NSLog(@"debug 4 ,%lu",(unsigned long)[arr count]);

                        NSDictionary *newDictionary = [arr objectAtIndex:i];
                        VSCCDetailModelClass *modelClass = [[VSCCDetailModelClass alloc]init];
                        
                        [modelClass setAmountToBePaid:[newDictionary valueForKey:@"amountToBePaid"]];
                        [modelClass setCardId:[newDictionary valueForKey:@"cardId"]];
                        [modelClass setCardNumber:[newDictionary valueForKey:@"cardNumber"]];
                        [modelClass setCardType:[newDictionary valueForKey:@"cardType"]];
                        [modelClass setCardTypeId:[newDictionary valueForKey:@"cardTypeId"]];
                        [modelClass setCreditDebitCardInfoStatusId:[newDictionary valueForKey:@"creditDebitCardInfoStatusId"]];
                        [modelClass setExpiryDate:[newDictionary valueForKey:@"expiryDate"]];
                        [modelClass setFirstName:[newDictionary valueForKey:@"firstName"]];
                        [modelClass setLastModifiedTime:[newDictionary valueForKey:@"lastModifiedTime"]];
                        [modelClass setLastName:[newDictionary valueForKey:@"lastName"]];
                        [modelClass setLoyaltyPrice:[newDictionary valueForKey:@"loyaltyPrice"]];
                        [modelClass setPackagePrice:[newDictionary valueForKey:@"packagePrice"]];
                        [modelClass setPaymentPackageId:[newDictionary valueForKey:@"paymentPackageId"]];
                        [modelClass setStatusTypeId:[newDictionary valueForKey:@"statusTypeId"]];
                        [modelClass setTotalLoyalityPoint:[newDictionary valueForKey:@"totalLoyalityPoint"]];
                        [modelClass setUserId:[newDictionary valueForKey:@"userId"]];
                        [modelClass setCreditDebitCardInfoStatus:[newDictionary valueForKey:@"creditDebitCardInfoStatus"]];
                        
                        [modelClass setStreetAddress:[newDictionary objectForKey:@"streetAddress"]];
                        [modelClass setCity:[newDictionary objectForKey:@"city"]];
                        [modelClass setCountry:[newDictionary objectForKey:@"country"]];
                        [modelClass setZipCode:[newDictionary objectForKey:@"zipCode"]];
                        [modelClass setIsDefault:[newDictionary objectForKey:@"isDefault"]];
                        [self.dateArray addObject:modelClass];
                        
                    }
                    
                }else if ([dictdata isKindOfClass:[NSArray class]]) {
                    
                    NSLog(@"NSArrayNSArrayNSArray%@",dictdata);
                }else {
                    
                    NSLog(@"else%@",dictdata);
                }
                [self addTableOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kRegisterButtonBG.size.height + 85)) Tag:2450];
            }

            [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];
        }
    }];
}

- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)deleteCardDetailFromServer:(NSString *)cradID {
    
    [[VSParserClass sharedParser] VSDeleteCCDetail:cradID WithCompletionBlock:^(BOOL result ,id resultObject, NSError *error) {
        
        NSLog(@"VSDeleteCCDetailresultObjectresultObject:-%@",resultObject);
        
        if (result) {
            
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                [[[UIAlertView alloc]initWithTitle:nil message:@"Your card detail is delete successfully " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
            }else {
                
                [[[UIAlertView alloc]initWithTitle:nil message:@"Your card detail is not delete successfully " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];
            
            NSLog(@"VSDeleteCCDetailresultObjectresultObject:-%@",resultObject);
            
        }
        
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:@"Credit Card Detail"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    [self addComponentOnView];
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        
        NSLog(@"SVProgressHUDMaskTypeGradient :- %@", self.userID);
        
        [self getCardDetailFromServer:self.userID];
        
    }else {
        
        [[[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    NSArray *textArray = @[@"Add Cradit/Dabit Card",@"Make Payment"];
    
    for (int i = 0; i < 2; i++) {
        
        [self addButtonOnView:(IS_IPHONE_5 || IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 10 + ((kRegisterButtonBG.size.height/2 + 130) * i), [UIScreen mainScreen].bounds.size.height - (kRegisterButtonBG.size.height + 76), kRegisterButtonBG.size.width/2 , kRegisterButtonBG.size.height) : CGRectMake([UIScreen mainScreen].bounds.origin.x + 15 + ((kRegisterButtonBG.size.height/2 + 155) * i), [UIScreen mainScreen].bounds.size.height - (kRegisterButtonBG.size.height + 76), kRegisterButtonBG.size.width/2 + 25, kRegisterButtonBG.size.height) title:textArray[i] Tag:55645 + i];
    }
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    UITableView *cardTable = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
    [cardTable setTag:tag];
    [cardTable setDelegate:(id)self];
    [cardTable setDataSource:(id)self];
    [cardTable setBackgroundColor:[UIColor clearColor]];
    [cardTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [cardTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [self.view addSubview:cardTable];
}

-(void)addButtonOnView:(CGRect)frame title:(NSString *)text Tag:(int)tag {
    
    UIButton *saveButton = [[UIButton alloc]init];
    [saveButton setTag:tag];
    [saveButton setFrame:frame];
    [saveButton setTitle:text forState:UIControlStateNormal];
    [saveButton.titleLabel setFont:(IS_IPHONE_5 || IS_IPHONE_4S) ? [UIFont boldSystemFontOfSize:12] : [UIFont boldSystemFontOfSize:14]];
    [saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [saveButton setBackgroundImage:kRegisterButtonBG forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(addCardAndPaymentAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveButton];
}

-(IBAction)addCardAndPaymentAction:(UIButton *)sender {
    
    if (sender.tag == 55645) {
        
        VSCardDetailViewController *cardDetail = [[VSCardDetailViewController alloc]init];
        [cardDetail setVCTitle:@"add"];
        [cardDetail setUserID:self.userID];
        [self.navigationController pushViewController:cardDetail animated:YES];
        
    } else {
        
        VSPackageSelectionView *packageSelection = [[VSPackageSelectionView alloc]init];
        [packageSelection setUserID:(self.userID) ? self.userID : [VSSingletonClass sharedMySingleton].loginUserId];
        [self.navigationController pushViewController:packageSelection animated:YES];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return ([self.dateArray count] != 0) ? [self.dateArray count] : 0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    
    VSCCDetailModelClass *modelClass = [self.dateArray objectAtIndex:indexPath.row];
    
    if ([modelClass.creditDebitCardInfoStatus isEqualToString:@"Deleted"]) {
        return NO;
    } else {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VSCCDetailModelClass *modelClass = [self.dateArray objectAtIndex:indexPath.row];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if ([[Reachability reachabilityForInternetConnection]isReachable]) {
            
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self deleteCardDetailFromServer:modelClass.cardId];
            NSLog(@"debug 1 ,%lu",(unsigned long)[self.dateArray count]);
            [self.dateArray removeObjectAtIndex:indexPath.row];
            
        }else {
            
           [[[UIAlertView alloc]initWithTitle:nil  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        [tableView reloadData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    MyRouteTableCell *cell = (MyRouteTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[MyRouteTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setBackgroundColor:[UIColor clearColor]];
        UIView *lineView = [[UIView alloc]init];
        [lineView setFrame:CGRectMake(0.0, 120.0, [UIScreen mainScreen].bounds.size.width, 0.5)];
        [lineView setBackgroundColor:[UIColor lightGrayColor]];
        [cell.contentView addSubview:lineView];
        [lineView setHidden:([self.dateArray count] != 0) ? NO : YES];
    }
    
    if ([self.dateArray count] != 0) {
        
        [cell.myId setHidden:NO];
        [cell.source setHidden:NO];
        [cell.destination setHidden:NO];
        [cell.date setHidden:NO];
        [cell.myFinalId setHidden:NO];
        [cell.finalSource setHidden:NO];
        [cell.finalDestination setHidden:NO];
        [cell.finalDate setHidden:NO];
        [cell.line setHidden:YES];
       
        NSLog(@"debug 2 ,%lu",(unsigned long)[self.dateArray count]);

        VSCCDetailModelClass *modelClass = [self.dateArray objectAtIndex:indexPath.row];
        
        [cell.myId setText:@"Id"];
        [cell.source setText:@"Card Type"];
        [cell.destination setText:@"Card No"];
        [cell.date setText:@"Expiry Date"];
        [cell.status setText:@"Card Status"];
        
        [cell.myFinalId setText:[NSString stringWithFormat:@": %@",modelClass.cardId]];
        [cell.finalSource setText:[NSString stringWithFormat:@": %@",modelClass.cardType]];
        [cell.finalDestination setText:[NSString stringWithFormat:@": %@",modelClass.cardNumber]];
        [cell.finalDate setText:[NSString stringWithFormat:@": %@",modelClass.expiryDate]];
        [cell.finalStatus setText:[NSString stringWithFormat:@": %@",modelClass.creditDebitCardInfoStatus]];
        
    }else {
        
        [cell.myId setHidden:YES];
        [cell.source setHidden:YES];
        [cell.destination setHidden:YES];
        [cell.date setHidden:YES];
        [cell.myFinalId setHidden:YES];
        [cell.finalDestination setHidden:YES];
        [cell.finalDate setHidden:YES];
        [cell.line setHidden:YES];
        [cell.finalSource setText:@"No Data Found"];
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"debug 3 ,%lu",(unsigned long)[self.dateArray count]);

    VSCCDetailModelClass *modelClass = [self.dateArray objectAtIndex:indexPath.row];
    [self navigateToDetailscreen:modelClass];
}

-(void)navigateToDetailscreen:(VSCCDetailModelClass *)modelClass {
    
    VSUpdateCardDetails *updateCardVC = [[VSUpdateCardDetails alloc] init];
    [updateCardVC setUpdateCardModal:modelClass];
    [updateCardVC setUserID:self.userID];
    [self.navigationController pushViewController:updateCardVC animated:YES];
}


@end
