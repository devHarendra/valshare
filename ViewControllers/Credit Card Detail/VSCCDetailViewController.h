//
//  VSCCDetailViewController.h
//  ValShare
//
//  Created by Vivek Kumar on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@interface VSCCDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) NSMutableArray *dateArray;

@property (nonatomic,strong) NSString *userID;

@end
