//
//  VSCardDetailViewController.m
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCardDetailViewController.h"

@interface VSCardDetailViewController (){
    
    int  currentbuttonTag;
    Boolean radioSelected;
    NSUInteger PressCount;
}

@property (nonatomic, strong) LTHMonthYearPickerView  *monthYearPicker;
@end

@implementation VSCardDetailViewController
@synthesize monthsArray = _monthsArray;
@synthesize yearArray = _yearArray;
@synthesize VCTitle;
@synthesize ccModelClass;
@synthesize datePicker;
@synthesize monthYearPicker;
@synthesize firstLogin;
@synthesize userID;


-(NSMutableArray *)monthsArray {
    
    if(!_monthsArray) {
        _monthsArray = [[NSMutableArray alloc] init];
    }
    return _monthsArray;
}

-(NSMutableArray *)yearArray {
    if(!_yearArray) {
        _yearArray = [[NSMutableArray alloc] init];
    }
    return _yearArray;
}

-(void)checkName:(NSString *)name TextField:(UITextField *)textField {
    
    [[VSParserClass sharedParser] checkFirstOrLastName:name WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSString *string = [[NSString alloc] initWithData:resultObject encoding:NSUTF8StringEncoding];
        
        if ([string isEqualToString:@"true"]) {
            if (textField.tag == 2225) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Your first name is not valid" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [textField setText:@""];
            } else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Your last name is not valid" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [textField setText:@""];
            }
        }
    }];
}

-(void)addCardService:(NSString *)userId ExpiryDate:(NSString *)expiryDate FirstName:(NSString *)firstName LastName:(NSString *)lastName CardNumber:(NSString *)cardNumber cardTypeId:(NSString *)cardTypeId CardType:(NSString *)cardType {
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
    
    [dictData setValue:firstName forKey:@"firstName"];
    [dictData setValue:lastName forKey:@"lastName"];
    [dictData setValue:cardNumber forKey:@"cardNumber"];
    [dictData setValue:expiryDate forKey:@"expiryDate"];
    [dictData setValue:cardType forKey:@"cardType"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[userId intValue]] forKey:@"userId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[cardTypeId intValue]] forKey:@"cardTypeId"];
    
    [[VSParserClass sharedParser] valShareCardDetail:dictData WithCompletionBlock:^(BOOL result ,id resultObject, NSError *error) {
        
        if (result) {
            
            NSMutableDictionary *loginDict = (NSMutableDictionary *)resultObject;
            
            if ([[loginDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                
                [self navigateToPackageSelectionVC:self.userID];

                //[[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", @"Your card add successfully."] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [loginDict valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];
            NSLog(@"valShareCardDetail:-%@",resultObject);
        }
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setHidden:NO];
    if(![firstLogin isEqualToString:@"varification"]) {
        self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    }
    [self.navigationItem setTitle:@"Card Detail"];
    [self addComponentOnView];
    NSLog(@"Card DetailCard DetailCard Detail :- %@", self.userID);

    UIView *view = (UIView *)[self.view viewWithTag:9999];
    [view setHidden:YES];
    PressCount = 0;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeKeyBoard:)];
    [tap setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tap];
}

-(void)removeKeyBoard:(UITapGestureRecognizer *)tap {
    
    UITextField *cvvField = (UITextField *)[self.view viewWithTag:8000];
    UITextField *fNameField = (UITextField *)[self.view viewWithTag:2225];
    UITextField *lNameField = (UITextField *)[self.view viewWithTag:2226];
    UITextField *cardNumberField = (UITextField *)[self.view viewWithTag:2222];
    
    [cvvField resignFirstResponder];
    [lNameField resignFirstResponder];
    [fNameField resignFirstResponder];
    [cardNumberField resignFirstResponder];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    
    NSArray *text = @[@"First Name",@"Last Name"];
    NSArray *image = @[kVisaCardBG,kMasterCardBG];
    NSArray *buttonText = @[@"Submit",@"Skip>>"];
    
    for (int i = 0; i < 2; i++) {
        
        [self addImageOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 62 + (i * (kVisaCardBG.size.width * 4.3)), [UIScreen mainScreen].bounds.origin.y + 30, kVisaCardBG.size.width, kVisaCardBG.size.height) BGImage:image[i] Tag:2000+i];
    }
    
    for (int i = 0; i < 2; i++) {
        
        [self addRadioButtonOnview:CGRectMake([UIScreen mainScreen].bounds.origin.x + 42 + (i *(kVisaCardBG.size.width * 4)), [UIScreen mainScreen].bounds.origin.y + 30, kVisaCardBG.size.width, kVisaCardBG.size.height) Tag:1500 + i Selected:(i == 0) ? YES : NO];
    }
    
    for (int i =0; i <= 1; i++) {
        
        [self addTextFieldOnFrame:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 30 + (i * 135), [UIScreen mainScreen].bounds.origin.y + 75 , 125, 35) : (IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 30 + (i * 135), [UIScreen mainScreen].bounds.origin.y + 75 , 125, 35) : CGRectMake([UIScreen mainScreen].bounds.origin.x + 45 +(i * 150), [UIScreen mainScreen].bounds.origin.y + 75 ,140, 35) Title:text[i] Tag:2225 + i];
    }
    
    [self addTexFieldForCardNo:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 30, [UIScreen mainScreen].bounds.origin.y + 120 , [UIScreen mainScreen].bounds.size.width - 60, 35):(IS_IPHONE_4S) ?CGRectMake([UIScreen mainScreen].bounds.origin.x + 30, [UIScreen mainScreen].bounds.origin.y + 120 , [UIScreen mainScreen].bounds.size.width/2 + 100, 35): CGRectMake([UIScreen mainScreen].bounds.origin.x + 45, [UIScreen mainScreen].bounds.origin.y + 120 , [UIScreen mainScreen].bounds.size.width/2 + 100, 35) Title:@"Card Number" Tag:2222];
    
    [self addButtonOnView:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 45  , [UIScreen mainScreen].bounds.origin.y + 165, [UIScreen mainScreen].bounds.size.width - 85, 35):CGRectMake([UIScreen mainScreen].bounds.origin.x + 30  , [UIScreen mainScreen].bounds.origin.y + 165, [UIScreen mainScreen].bounds.size.width - 60, 35) Title:@"Expiry" Tag:5000];
    
    //[self addCvvTextFieldOnView:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 185  , [UIScreen mainScreen].bounds.origin.y + 165, 110, 35):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 183 , [UIScreen mainScreen].bounds.origin.y + 165, 110, 35):CGRectMake([UIScreen mainScreen].bounds.origin.x + 210  , [UIScreen mainScreen].bounds.origin.y + 165, 125, 35)];
    
    
    //[self addCheckBoxButtonOnView:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 11), [UIScreen mainScreen].bounds.origin.y + 210, kCheckBlackBG1.size.width, kCheckBlackBG1.size.height) : (IS_IPHONE6PLUS) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 +20), [UIScreen mainScreen].bounds.origin.y + 215, kCheckBlackBG1.size.width, kCheckBlackBG1.size.height):(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 10), [UIScreen mainScreen].bounds.origin.y + 210, kCheckBlackBG1.size.width, kCheckBlackBG1.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kEmail_BG.size.width/2 - 5), [UIScreen mainScreen].bounds.origin.y + 210, kCheckBlackBG1.size.width, kCheckBlackBG1.size.height) BGImage:kCheckBlackBG1 BGSelected:kCheckedBlackBG1 Tag:45654 ];
    
    //[self addLabelForCheckboxOnView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 35 + kCheckBlackBG1.size.width, [UIScreen mainScreen].bounds.origin.y + 210, [UIScreen mainScreen].bounds.size.width/3 + 20, 25) : CGRectMake([UIScreen mainScreen].bounds.origin.x + 55 + kCheckBlackBG1.size.width, [UIScreen mainScreen].bounds.origin.y + 210, [UIScreen mainScreen].bounds.size.width/3 + 20, 25) Title:@"Mark As Defult" Tag:4425 Font:[UIFont systemFontOfSize:14.0]];
    
    if ([self.VCTitle isEqualToString:@"add"]) {
        
       [self addSubmitButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kLoginBG.size.width/2 ) , [UIScreen mainScreen].bounds.origin.y + 230, kLoginBG.size.width, kLoginBG.size.height)  Title:@"Submit" Tag:8899 BGImage:kLoginBG];
    }else {
       
        for ( int i = 0; i < 2; i ++) {
            
            [self addSubmitButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - (kLoginBG.size.width/2 - 15) + (i * kLoginBG.size.width/2 ), [UIScreen mainScreen].bounds.origin.y + 230, kLoginBG.size.width/2 - 35, kLoginBG.size.height)  Title:buttonText[i] Tag:8899+i BGImage:kLoginBG];
        }
    }
  

    [self addView:(IS_IPHONE_4S) ? CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 - 35,  [UIScreen mainScreen].bounds.size.width, 44) : CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 ,  [UIScreen mainScreen].bounds.size.width, 44)];
}


-(void)addImageOnView:(CGRect)frame BGImage:(UIImage *)bgimage Tag:(int)tag {
    
    UIImageView *imageView = [[UIImageView alloc]init];
    [imageView setFrame:frame];
    [imageView setTag:tag];
    [imageView setImage:bgimage];
    [imageView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:imageView];
}

-(void)addRadioButtonOnview:(CGRect)frame Tag:(int)tag Selected:(Boolean)selected {
    
    QRadioButton *rUserName = [[QRadioButton alloc] initWithDelegate:self groupId:@"groupId1"];
    [rUserName setFrame:frame];
    [rUserName setTag:tag];
    [rUserName setTitle:@"" forState:UIControlStateNormal];
    [rUserName setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rUserName.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [self.view addSubview:rUserName];
    (tag == 1500) ? [rUserName setChecked:YES] : @"";
}


- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId {
    

    radioSelected = (radio.tag == 1500) ? YES : NO;
}

- (void)addTextFieldOnFrame:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setFrame:frame];
    [textField setTag:tag];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:title];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setKeyboardType:UIKeyboardTypeDefault];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField.layer setBorderWidth:0.5];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:textField];
}


- (void)addTexFieldForCardNo:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setFrame:frame];
    [textField setTag:tag];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:title];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField.layer setBorderWidth:0.5];
    [textField setKeyboardType:UIKeyboardTypeNumberPad];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:textField];
}


-(void)addButtonOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UIButton *btnSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnSelect.tag = tag;
    btnSelect.frame = frame;
    [btnSelect.layer setBorderWidth:0.5];
    [btnSelect setBackgroundColor:[UIColor whiteColor]];
    [btnSelect setTitle:title forState:UIControlStateNormal];
    btnSelect.contentEdgeInsets = UIEdgeInsetsMake(1, 5, 0, 0);
    [btnSelect.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [btnSelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnSelect addTarget:self action:@selector(selectExpiryClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSelect];
}


-(void)addCheckBoxButtonOnView:(CGRect)frame BGImage:(UIImage *)bgImage BGSelected:(UIImage *)s_bgImage Tag:(int)tag {
    
    VSButton *button = [VSButton buttonWithType:UIButtonTypeCustom];
    [button setTag:tag];
    [button setFrame:frame];
    [button setBackgroundColor:[UIColor clearColor]];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button setBackgroundImage:s_bgImage forState:UIControlStateSelected];
    [button addTarget:self action:@selector(selectCheckBoxButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(void)addLabelForCheckboxOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag Font:(UIFont *)font {
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:frame];
    titleLabel.text = title;
    titleLabel.tag = tag;
    titleLabel.font = font;
    [titleLabel setTextColor:[UIColor blackColor]];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleLabel];
}

-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(submitButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}


-(void)addCvvTextFieldOnView:(CGRect)frame {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setFrame:frame];
    [textField setTag:8000];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:@"CVV"];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField.layer setBorderWidth:0.5];
    [textField setKeyboardType:UIKeyboardTypeNumberPad];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:textField];
}


-(void)addView:(CGRect)frame {
    
    UIView *myview = [[UIView alloc]init];
    [myview setFrame:frame];
    [myview setBackgroundColor:[UIColor colorWithRed:183.0f/255.0f green:40.0f/255.0f blue:35.0f/255.0f alpha:1.0f]];
    [myview setTag:9999];
    [self.view addSubview:myview];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 44)];
    titleLabel.text = @"Select month and year";
    titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.backgroundColor = [UIColor clearColor];
    [myview addSubview:titleLabel];
    
    UIButton *doneButton = [[UIButton alloc]init];
    [doneButton setFrame:(IS_IPHONE_5) ? CGRectMake(240, 0.0, 60, 44) : (IS_IPHONE_4S) ? CGRectMake(240, 0.0, 60, 44) : CGRectMake(280, 0.0, 60, 44)];
    [doneButton setBackgroundColor:[UIColor clearColor]];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
    [doneButton addTarget:self action:@selector(cancelButtonActionOnExpiry:) forControlEvents:UIControlEventTouchUpInside];
    [myview addSubview:doneButton];
}

//************ Use textField Delegate *************

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField.tag == 2225) {
        [self checkName:textField.text TextField:textField];
    } else if (textField.tag == 2226) {
        [self checkName:textField.text TextField:textField];
    } else {
        
    }
}

//************ Drop down button Clicked *************

-(IBAction)selectExpiryClicked:(UIButton *)sender {
    
    UITextField *cardNumberField = (UITextField *)[self.view viewWithTag:2222];
    UITextField *cvvField = (UITextField *)[self.view viewWithTag:8000];
    
    [cardNumberField resignFirstResponder];
    [cvvField resignFirstResponder];
    
    UIView *view = (UIView *)[self.view viewWithTag:9999];
    [view setHidden:NO];
    if (PressCount > 0) {
        [self.monthYearPicker removeFromSuperview];
    }
    self.monthYearPicker = [[LTHMonthYearPickerView alloc] initWithDate: [NSDate date]
                                                            shortMonths: NO
                                                         numberedMonths: NO
                                                             andToolbar: YES];
    [self.monthYearPicker setDelegate:(id)self];
    [self.monthYearPicker setTag:6666];
    [self.monthYearPicker setBackgroundColor:[UIColor whiteColor]];
    PressCount++;
    [self.view addSubview:self.monthYearPicker];
}

- (void)pickerDidPressDoneWithMonth:(NSString *)month andYear:(NSString *)year {
    
    UIButton *dateButton = (UIButton *)[self.view viewWithTag:5000];
    [dateButton setTitle:[NSString stringWithFormat: @"%@/%@", month, year] forState:UIControlStateNormal];
}

- (void)pickerDidPressCancelWithInitialValues:(NSDictionary *)initialValues {
    
}
- (void)pickerDidSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"row: %li in component: %li", (long)row, (long)component);
}

- (void)pickerDidSelectMonth:(NSString *)month {
    NSLog(@"month: %@ ", month);
}

- (void)pickerDidSelectYear:(NSString *)year {
    NSLog(@"year: %@ ", year);
}

- (void)pickerDidSelectMonth:(NSString *)month andYear:(NSString *)year {
    UIButton *dateButton = (UIButton *)[self.view viewWithTag:5000];
    [dateButton setTitle:[NSString stringWithFormat: @"%@ / %@", month, year] forState:UIControlStateNormal];
}

-(IBAction)doneButtonClickedOnExpiry:(UIButton *)sender {
    
    UIView *view = (UIView *)[self.view viewWithTag:9999];
    LTHMonthYearPickerView *pickerView= (LTHMonthYearPickerView *)[self.view viewWithTag:6666];
    [pickerView removeFromSuperview];
    [view setHidden:YES];
}

-(IBAction)cancelButtonActionOnExpiry:(id)sender {
    
    UIView *view = (UIView *)[self.view viewWithTag:9999];
    LTHMonthYearPickerView *pickerView= (LTHMonthYearPickerView *)[self.view viewWithTag:6666];
    [pickerView removeFromSuperview];
    [view setHidden:YES];
    
}

-(IBAction)selectCheckBoxButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}

-(IBAction)submitButtonAction:(UIButton *)sender {
    
    UITextField *firstName = (UITextField *)[self.view viewWithTag:2225];
    UITextField *lastName = (UITextField *)[self.view viewWithTag:2226];
    UITextField *cardNumber = (UITextField *)[self.view viewWithTag:2222];
    //UITextField *cvvNumber = (UITextField *)[self.view viewWithTag:8000];
    
    UIButton *expiryDate = (UIButton *)[self.view viewWithTag:5000];
    NSString *expireyString = [NSString stringWithFormat:@"%@",[expiryDate.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
    if (sender.tag == 8899) {
        
        if ([firstName.text length] == 0) {
            
            [[[UIAlertView alloc]initWithTitle:nil message:@"Please enter first name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            
        } else if (lastName.text.length == 0) {
            
            [[[UIAlertView alloc]initWithTitle:nil message:@"Please enter last name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            
        }else if (cardNumber.text.length == 0) {
            
            [[[UIAlertView alloc]initWithTitle:nil message:@"Please enter card number." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }else if ([expireyString isEqualToString:@"Expiry"]) {
            
            [[[UIAlertView alloc]initWithTitle:nil message:@"Please select card expiry day" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }else {
            
            if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                
                [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
                
                [self addCardService:self.userID ExpiryDate:[expireyString stringByReplacingOccurrencesOfString:@" " withString:@""] FirstName:firstName.text LastName:lastName.text CardNumber:cardNumber.text cardTypeId:[NSString stringWithFormat:@"%d",radioSelected] CardType:(radioSelected) ? @"Visa" : @"Master Card"];
            }else {
                
                [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
        }
    }else {
        
        if ([self.firstLogin isEqualToString:@"varification"]) {
            
            VSMapController *mapView = [[VSMapController alloc] init];
            [mapView setUserID:self.userID];
            [self.navigationController pushViewController:mapView animated:YES];
        } else {
            
            [self navigateToPackageSelectionVC:self.userID];
        }
    }
}

-(void)navigateToPackageSelectionVC:(NSString *)userID1 {
    
    VSPackageSelectionView *selectionView = [[VSPackageSelectionView alloc]init];
    [selectionView setLoginStatus:@"varification"];
    [selectionView setUserID:self.userID];
    [selectionView setModelClass:self.ccModelClass];
    [self.navigationController pushViewController:selectionView animated:YES];

}

@end
