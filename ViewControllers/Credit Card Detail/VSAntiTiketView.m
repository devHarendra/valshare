//
//  VSAntiTiketView.m
//  ValShare
//
//  Created by Vivek Kumar on 4/21/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSAntiTiketView.h"

@interface VSAntiTiketView () {
    
    int ammount;
}

@end

@implementation VSAntiTiketView
@synthesize packageID;
@synthesize dataArray = _dataArray;
@synthesize scrollView = _scrollView;

-(UIScrollView *)scrollView {
    
    if (!_scrollView) {
        
        _scrollView = [[UIScrollView alloc] initWithFrame:(IS_IPHONE_4S) ? CGRectMake(0.0, [UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 80):CGRectMake(0.0, [UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 10)];
        [_scrollView setTag:78235];
        [_scrollView setScrollEnabled:YES];
        [_scrollView setUserInteractionEnabled:YES];
        [_scrollView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_scrollView];
    }
    return _scrollView;
}

-(NSMutableArray *)dataArray {
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

-(void)getDefaultCardDetailFromServer:(NSString *)userid PackageId:(NSString *)packageid {
    
    [[VSParserClass sharedParser] getDefaultCardId:userid PackageID:packageid WithCompletionBlock:^(BOOL result ,id resultObject, NSError *error){
        
        NSLog(@"resultObjectresultObjectresultObject:-%@",resultObject);
        if (result) {
            
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                
                NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                
                if ([dictdata isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"NSDictionaryNSDictionary:-%@",dictdata);
                    
                    VSCCDetailModelClass *modelClass = [[VSCCDetailModelClass alloc]init];
                    [modelClass setAmountToBePaid:[dictdata valueForKey:@"amountToBePaid"]];
                    [modelClass setCardId:[dictdata valueForKey:@"cardId"]];
                    [modelClass setCardNumber:[dictdata valueForKey:@"cardNumber"]];
                    [modelClass setCardType:[dictdata valueForKey:@"cardType"]];
                    [modelClass setCardTypeId:[dictdata valueForKey:@"cardTypeId"]];
                    [modelClass setCreditDebitCardInfoStatusId:[dictdata valueForKey:@"creditDebitCardInfoStatusId"]];
                    [modelClass setExpiryDate:[dictdata valueForKey:@"expiryDate"]];
                    [modelClass setFirstName:[dictdata valueForKey:@"firstName"]];
                    [modelClass setLastModifiedTime:[dictdata valueForKey:@"lastModifiedTime"]];
                    [modelClass setLastName:[dictdata valueForKey:@"lastName"]];
                    [modelClass setLoyaltyPrice:[dictdata valueForKey:@"loyaltyPrice"]];
                    [modelClass setPackagePrice:[dictdata valueForKey:@"packagePrice"]];
                    [modelClass setPaymentPackageId:[dictdata valueForKey:@"paymentPackageId"]];
                    [modelClass setStatusTypeId:[dictdata valueForKey:@"statusTypeId"]];
                    [modelClass setTotalLoyalityPoint:[dictdata valueForKey:@"totalLoyalityPoint"]];
                    [modelClass setUserId:[dictdata valueForKey:@"userId"]];
                    [modelClass setStreetAddress:[dictdata valueForKey:@"streetAddress"]];
                    [modelClass setZipCode:[dictdata valueForKey:@"zipCode"]];
                    [modelClass setCity:[dictdata valueForKey:@"city"]];
                    [modelClass setCountry:[dictdata valueForKey:@"country"]];
                    
                    [self.dataArray addObject:modelClass];
                    
                }else {
                    
                    NSLog(@"elseelse:-%@",dictdata);
                }
            }else {
                
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show ];
            }
            [SVProgressHUD dismiss];
        }else {
            
            [SVProgressHUD dismiss];
        }
        VSCCDetailModelClass *modelClass = [self.dataArray objectAtIndex:0];
        NSArray *billingAddress = @[[NSString stringWithFormat:@"%@",modelClass.streetAddress],[NSString stringWithFormat:@"%@",modelClass.city], [NSString stringWithFormat:@"%@",modelClass.country],[NSString stringWithFormat:@"%@",modelClass.zipCode]];
        [self billingAddressTextField:billingAddress];
        [self addComponentOnView];
        [self addViewForLoylityPoint];
    }];
    
}
-(void)sendPaymentDetailOnServer:(NSString *)userId ExpiryDate:(NSString *)expirydate CardTypeId:(NSString *)cardtypeid CardType:(NSString *)cardtype FirstName:(NSString *)firstname LastName:(NSString *)lastname CardID:(NSString *)cardid PaymentPackageId:(NSString *)paymetpackageid PackagePrice:(NSString *)packageprice TotalLoyalityPoint:(NSString *)totalloyalitypoint LoyalityPrice:(NSString *)loyalityprice AmmountToBePaid:(NSString *)ammounttobepaid StreetAddress:(NSString *)streetaddress City:(NSString *)city Country:(NSString *)country ZipCode:(NSString *)zipcode CardNumber:(NSString *)cardnumber{
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
    
    [dictData setValue:[NSString stringWithFormat:@"%d",[userId intValue]] forKey:@"userId"];
    [dictData setValue:expirydate forKey:@"expiryDate"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[cardtypeid intValue]] forKey:@"cardTypeId"];
    [dictData setValue:cardtype forKey:@"cardType"];
    [dictData setValue:firstname forKey:@"firstName"];
    [dictData setValue:lastname forKey:@"lastName"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[cardid intValue]] forKey:@"cardId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[paymetpackageid intValue]] forKey:@"paymentPackageId"];
    [dictData setValue:[NSString stringWithFormat:@"%f",[packageprice floatValue]] forKey:@"packagePrice"];
    [dictData setValue:[NSString stringWithFormat:@"%f",[loyalityprice floatValue]] forKey:@"loyaltyPrice"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[totalloyalitypoint intValue]] forKey:@"totalLoyalityPoint"];
    [dictData setValue:[NSString stringWithFormat:@"%f",[ammounttobepaid floatValue]] forKey:@"amountToBePaid"];
    [dictData setValue:streetaddress forKey:@"streetAddress"];
    [dictData setValue:country forKey:@"country"];
    [dictData setValue:city forKey:@"city"];
    [dictData setValue:zipcode forKey:@"zipCode"];
    [dictData setValue:cardnumber forKey:@"cardNumber"];
    
    [[VSParserClass sharedParser] VSPaymentProcess:dictData WithCompletionBlock:^(BOOL result ,id resultObject, NSError *error) {
        
        NSLog(@"resultObjectresultObjectresultObject:-%@",resultObject);
        if (result) {
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                
                for (UIViewController *viewcontroller in [self.navigationController viewControllers]) {
                   if([viewcontroller isKindOfClass:[VSMapController class]])
                   {
                       [self.navigationController popToViewController:viewcontroller animated:YES];
                       break;

                   }
                }
                [[[UIAlertView alloc]initWithTitle:nil message:@"Your payment has been done successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }else {
                
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"]delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            }
        [SVProgressHUD dismiss];
        }else {
            [SVProgressHUD dismiss];
        }
    }];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    [self.navigationItem setTitle:@"Anti Ticket"];
    int value = 120 - 150;
    NSLog(@"value:-%d",value);
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        
        [self getDefaultCardDetailFromServer:[VSSingletonClass sharedMySingleton].loginUserId PackageId:self.packageID];
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addComponentOnView {
    
    VSCCDetailModelClass *modelClass = [self.dataArray objectAtIndex:0];
    
    NSArray *labelArray = @[@"Card Details",@"Billing Address"];
    NSArray *textArray = @[modelClass.cardType,modelClass.cardNumber,modelClass.expiryDate,modelClass.firstName,modelClass.lastName,[NSString stringWithFormat:@"%@",modelClass.packagePrice]];
    
    NSArray *labelText = @[@"Card Type",@"Card Number",@"Expiry Date",@"First Name",@"Last Name"];
    
    NSArray *buttonText = @[@"Process Payment",@"Skip>>"];
    
    for (int i = 0; i < 5; i++) {
        
        if (IS_IPHONE_4S) {
            
            [self addTextFieldOnView1: CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 + 100, [UIScreen mainScreen].bounds.origin.y + 45 + (i * 45), kLoginBG.size.width - 100, 35)  Title:textArray[i] Tag:5000 + i];
        }else {
            
            [self addTextFieldOnView1: (IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 + 100, [UIScreen mainScreen].bounds.origin.y + 45 + (i * 45), kLoginBG.size.width - 100, 35):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 + 75, [UIScreen mainScreen].bounds.origin.y + 45 + (i * 45), kLoginBG.size.width - 60, 35)  Title:textArray[i] Tag:5000 + i];
        }
        
    }
    
    for (int i = 0; i < 5; i++) {
        
        [self addlabelOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 45 + (i * 45), [UIScreen mainScreen].bounds.size.width/3+20, 35) Title:labelText[i]];
    }
    
    for ( int i = 0; i < 2; i ++) {
        
        if (IS_IPHONE_4S) {
            
            [self addSubmitButtonOnView: CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 + 140 + (i * kLoginBG.size.width/2 - 110), [UIScreen mainScreen].bounds.origin.y + 685, kLoginBG.size.width/2-25, kLoginBG.size.height) Title:buttonText[i] Tag:8899+i BGImage:kLoginBG];
            
            [self addLabelOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 5 , [UIScreen mainScreen].bounds.origin.y + 10 + (([UIScreen mainScreen].bounds.size.width/2 + 145) * i), [UIScreen mainScreen].bounds.size.width - 20, 20) Text:labelArray[i]];
            
        }else {
            
            [self addSubmitButtonOnView: (IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 + 138 + (i * kLoginBG.size.width/2 - 110), [UIScreen mainScreen].bounds.origin.y + 685, kLoginBG.size.width/2-25, kLoginBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 + 120 + (i * kLoginBG.size.width/2 - 70), [UIScreen mainScreen].bounds.origin.y + 685, kLoginBG.size.width/2 - 25, kLoginBG.size.height)   Title:buttonText[i] Tag:8899+i BGImage:kLoginBG];

            [self addLabelOnView:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 5 , [UIScreen mainScreen].bounds.origin.y + 10 + (([UIScreen mainScreen].bounds.size.width/2 + 115) * i), [UIScreen mainScreen].bounds.size.width - 20, 20):CGRectMake([UIScreen mainScreen].bounds.origin.x + 5 , [UIScreen mainScreen].bounds.origin.y + 10 + (([UIScreen mainScreen].bounds.size.width/2 + 85) * i), [UIScreen mainScreen].bounds.size.width - 20, 20) Text:labelArray[i]];
        }
    }

    [self.scrollView setContentSize:CGSizeMake(0.0, [UIScreen mainScreen].bounds.size.height + 450)];
}


-(void)billingAddressTextField:(NSArray *)data {
    
    NSArray *bPHArray = @[@"Street Address",@"City",@"Country",@"Zip Code"];
    
    for (int j = 0; j< 4; j++) {
        
        [self addTextFieldOnView:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 + 75, [UIScreen mainScreen].bounds.origin.y + 320 + (40 * j), kLoginBG.size.width - 60, 35):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 + 100, [UIScreen mainScreen].bounds.origin.y + 325 + (40 * j), kLoginBG.size.width - 100, 35) Tag:59456 + j Text:data[j] PlaceHolder:bPHArray[j]];
        
        [self addlabeForBillingAddresslOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 320 + (j * 40), [UIScreen mainScreen].bounds.size.width/3+20, 35) Title:bPHArray[j]];
    }
}

-(void)addViewForLoylityPoint {
    
    VSCCDetailModelClass *modelClass = [self.dataArray objectAtIndex:0];
    ammount = [modelClass.packagePrice intValue];

    NSArray *textArray = @[[NSString stringWithFormat:@"Ammount($)  :%@",modelClass.packagePrice],[NSString stringWithFormat:@"Total Loyality Point  :%@",modelClass.totalLoyalityPoint],[NSString stringWithFormat:@"1 Loyality Price($)  :%@",modelClass.loyaltyPrice],[NSString stringWithFormat:@"Ammount To Paid($)  :%@",modelClass.packagePrice]];
    
    for (int i = 0; i < 4; i ++) {
        
        [self addLabelForLoyalityPoint:CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 485 + (30 * i), kLoginBG.size.width - 30, 30) Title:textArray[i] Tag:8000 + i];
    }
    
    [self addTextFieldForLoyalityPoint:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 615, kLoginBG.size.width, 35) Title:@"Enter Loylity Point" Tag:60300];
}


-(void)addLabelForLoyalityPoint:(CGRect)frame Title:(NSString *)text Tag:(int)tag {
    
    UILabel *label = [[UILabel alloc]init];
    [label setFrame:frame];
    [label setText:text];
    [label setTag:tag];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont systemFontOfSize:14.0]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [self.scrollView addSubview:label];
    
}
-(void)addTextFieldForLoyalityPoint:(CGRect)frame Title:(NSString *)text Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setFrame:frame];
    [textField setTag:tag];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:text];
    [textField.layer setBorderWidth:1.0];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setKeyboardType:UIKeyboardTypeDefault];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.scrollView addSubview:textField];
}
-(void)addTextFieldOnView:(CGRect)frame Tag:(int)tag Text:(NSString *)title PlaceHolder:(NSString *)pHolder {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setFrame:frame];
    [textField setTag:tag];
    [textField setText:title];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:pHolder];
    [textField.layer setBorderWidth:1.0];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setKeyboardType:UIKeyboardTypeDefault];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.scrollView addSubview:textField];
}

-(void)addTextFieldOnView1:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setFrame:frame];
    [textField setTag:tag];
    [textField setText:title];
    [textField setDelegate:(id)self];
    [textField.layer setBorderWidth:1.0];
    [textField setUserInteractionEnabled:NO];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setFont:[UIFont systemFontOfSize:15.0f]];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.scrollView addSubview:textField];
}

-(void)addlabelOnView:(CGRect)frame Title:(NSString *)title {
    
    UILabel *label = [[UILabel alloc]init];
    [label setFrame:frame];
    [label setText:title];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont systemFontOfSize:14.0]];
    [label setBackgroundColor:[UIColor clearColor]];
    [self.scrollView addSubview:label];
}

-(void)addlabeForBillingAddresslOnView:(CGRect)frame Title:(NSString *)title {
    
    UILabel *label = [[UILabel alloc]init];
    [label setFrame:frame];
    [label setText:title];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont systemFontOfSize:14.0]];
    [label setBackgroundColor:[UIColor clearColor]];
    [self.scrollView addSubview:label];
}

-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(paymentButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:button];
}
-(void)addLabelOnView:(CGRect)frame Text:(NSString *)text {
    
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:frame];
    [label setText:text];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [self.scrollView addSubview:label];
}

//***************Use of textfield deligat ***************************//


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
   
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
  
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    ammount = 0;
    VSCCDetailModelClass *modelClass = [self.dataArray objectAtIndex:0];
    
    UILabel *label = (UILabel *)[self.view viewWithTag:8003];

    if (textField.tag == 60300) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

         ammount  = [modelClass.packagePrice intValue] - ([newString intValue] * [modelClass.loyaltyPrice intValue]);
        
        if (ammount < 1) {
            
            [[[UIAlertView alloc]initWithTitle:nil message:@"Invalid Ammount" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            [label setText:[NSString stringWithFormat:@"Ammount To Paid($)  :%@",@"0"]];

        }else {
            
            NSLog(@"ammountammountammount:-%d",ammount);
            [label setText:[NSString stringWithFormat:@"Ammount To Paid($)  :%d",ammount]];
        }
        
    }
    return YES;
}

//*********************Implement uialertView delegate*******************//

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  
    VSCCDetailModelClass *modelClass = [self.dataArray objectAtIndex:0];

    UITextField * addressField = (UITextField *)[self.view viewWithTag:59456];
    UITextField * city = (UITextField *)[self.view viewWithTag:59457];
    UITextField * country = (UITextField *)[self.view viewWithTag:59458];
    UITextField * zipCode = (UITextField *)[self.view viewWithTag:59459];
    
    if (buttonIndex == 1) {
        NSLog(@"no");
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            
             [self sendPaymentDetailOnServer:[VSSingletonClass sharedMySingleton].loginUserId ExpiryDate:modelClass.expiryDate CardTypeId:modelClass.cardTypeId CardType:modelClass.cardType FirstName:modelClass.firstName LastName:modelClass.lastName CardID:modelClass.cardId PaymentPackageId:modelClass.paymentPackageId PackagePrice:modelClass.packagePrice TotalLoyalityPoint:modelClass.totalLoyalityPoint LoyalityPrice:modelClass.loyaltyPrice AmmountToBePaid:[NSString stringWithFormat:@"%d",ammount] StreetAddress:addressField.text City:city.text Country:country.text ZipCode:zipCode.text CardNumber:modelClass.cardNumber];
            
        }else {
            
            [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        

    }else {
        
    }
    
}


-(IBAction)paymentButtonAction:(UIButton *)sender {
    
    UITextField * addressField = (UITextField *)[self.view viewWithTag:59456];
    UITextField * city = (UITextField *)[self.view viewWithTag:59457];
    UITextField * country = (UITextField *)[self.view viewWithTag:59458];
    UITextField * zipCode = (UITextField *)[self.view viewWithTag:59459];
    
    if (sender.tag == 8899) {
        if (addressField.text.length == 0) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Please fill your street address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }else if (city.text.length == 0) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Please fill your city" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }else if (country.text.length == 0) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Please fill your country" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }else if (zipCode.text.length == 0) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Please fill your zipCode" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }else if (ammount < 1) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Your ammount is not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"You want to pay" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [alertView show];
        }
        
    }else {
        for (UIViewController *viewcontroller in [self.navigationController viewControllers]) {
            if([viewcontroller isKindOfClass:[VSMapController class]])
            {
                [self.navigationController popToViewController:viewcontroller animated:YES];
                break;
                
            }
        }    }
    
}
@end
