//
//  VSPackageSelectionView.m
//  ValShare
//
//  Created by Vivek Kumar on 4/20/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSPackageSelectionView.h"

@interface VSPackageSelectionView (){
    NSUInteger *pressCount;
    UIView *viewPicker;
    VSGetPackByMonth_YearModel *model;
}

@end

@implementation VSPackageSelectionView
@synthesize yearDataArray = _yearDataArray;
@synthesize monthDataArray = _monthDataArray;
@synthesize packageDictionaryArray = _packageDictionaryArray;
@synthesize modelClass;
@synthesize segmentID;
@synthesize pickerView;
@synthesize userID;
@synthesize packageDictionaryKey;
@synthesize packageDictionaryString;


-(NSMutableArray *)yearDataArray {
    
    if (!_yearDataArray) {
        _yearDataArray = [[NSMutableArray alloc]init];
    }
    return _yearDataArray;
}

-(NSMutableArray *)monthDataArray {
    
    if (!_monthDataArray) {
        _monthDataArray = [[NSMutableArray alloc]init];
    }
    return _monthDataArray;
}

-(NSMutableArray *)packageDictionaryArray {
    
    if (!_packageDictionaryArray) {
        _packageDictionaryArray = [[NSMutableArray alloc]init];
    }
    return _packageDictionaryArray;
}

-(void)getPackgeDataByMonth:(NSString *)userId ByMonth:(NSString *)bymonth {
    
    [[VSParserClass sharedParser] VSGetPackByMonth:userId ByMonth:bymonth WithCompletionBlock:^(BOOL result , id resultObject ,NSError *error) {
        
        NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
        if (result) {
            
            if ([dictdata isKindOfClass:[NSDictionary class]]) {
                
                NSLog(@"dictdatadictdatadictdataNSDictionary:-%@",dictdata);
                
                model = [[VSGetPackByMonth_YearModel alloc]init];
                [model setEffectiveDays:[dictdata valueForKey:@"effectiveDays"]];
                [model setIsActive:[dictdata valueForKey:@"isActive"]];
                [model setPackageId:[dictdata valueForKey:@"packageId"]];
                [model setPackageName:[dictdata valueForKey:@"packageName"]];
                [model setPackagePrice:[dictdata valueForKey:@"packagePrice"]];
                [model setPackageStatusId:[dictdata valueForKey:@"packageStatusId"]];
                [model setPackageDictionary:([[dictdata allKeys] containsObject:@"packageDictionary"]) ? [dictdata valueForKey:@"packageDictionary"] : @""];
                [model setUserId:[dictdata valueForKey:@"userId"]];
                [self.monthDataArray addObject:model];
                
            }
            else if ([dictdata isKindOfClass:[NSArray class]]) {
                NSLog(@"dictdatadictdatadictdataNSArray:-%@",dictdata);
            }else {
                NSLog(@"dictdatadictdatadictdataelse:-%@",dictdata);
            }
            [SVProgressHUD dismiss];
            
        }else {
            [SVProgressHUD dismiss];
            
        }
        model = [self.monthDataArray objectAtIndex:0];
        NSMutableDictionary *lPackageDictionary = model.packageDictionary;
        
        [lPackageDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            VSGetPackByMonth_YearModel *lPackageModel = [[VSGetPackByMonth_YearModel alloc] init];
            [lPackageModel setPackageDictionaryKey:key];
            [lPackageModel setPackageDictionaryObject:obj];
            [self.packageDictionaryArray addObject:lPackageModel];
        }];
        
        [self addTableOnView:(IS_IPHONE6PLUS) ? CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y + 70, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-250):CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y + 50, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-250) Tag:2450];
        
        NSArray *buttonText = @[@"Pay Now",@"Skip>>"];
        
        for ( int i = 0; i < 2; i ++) {
            
            [self addSubmitButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2 + (i * kLoginBG.size.width/2 + 5 ),[UIScreen mainScreen].bounds.origin.y + 50 + [UIScreen mainScreen].bounds.size.height-200, kLoginBG.size.width/2-10, kLoginBG.size.height)  Title:buttonText[i] Tag:8899+i BGImage:kLoginBG];
        }
        
        [self addView:(IS_IPHONE_4S) ? CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 - 35,  [UIScreen mainScreen].bounds.size.width, 44) : (IS_IPHONE_5) ? CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 + 20 ,  [UIScreen mainScreen].bounds.size.width, 44):CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 + 75 ,  [UIScreen mainScreen].bounds.size.width, 44)];
        [self addComponentOnView];

        [viewPicker setHidden:YES];
        
    }];
}

-(void)getPackDataByYear:(NSString *)userId ByYearly:(NSString *)byyear {
    
    [self.yearDataArray removeAllObjects];
    
    [[VSParserClass sharedParser] VSGetPackByYearly:userId ByYearly:byyear WithCompletionBlock:^(BOOL result , id resultObject ,NSError *error) {

        NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
        
        if (result) {
            if ([dictdata isKindOfClass:[NSDictionary class]]) {
                
                NSLog(@"dictdatadictdatadictdataNSDictionary:-%@",dictdata);
                
                VSGetPackByMonth_YearModel *model2 = [[VSGetPackByMonth_YearModel alloc]init];
                [model2 setEffectiveDays:[dictdata valueForKey:@"effectiveDays"]];
                [model2 setIsActive:[dictdata valueForKey:@"isActive"]];
                [model2 setPackageId:[dictdata valueForKey:@"packageId"]];
                [model2 setPackageName:[dictdata valueForKey:@"packageName"]];
                [model2 setPackagePrice:[dictdata valueForKey:@"packagePrice"]];
                [model2 setPackageStatusId:[dictdata valueForKey:@"packageStatusId"]];
                [model setUserId:[dictdata valueForKey:@"userId"]];
                [self.yearDataArray addObject:model2];
                
            }else if ([dictdata isKindOfClass:[NSArray class]]) {
                NSLog(@"dictdatadictdatadictdataNSArray:-%@",dictdata);
            }else {
                NSLog(@"dictdatadictdatadictdataelse:-%@",dictdata);
            }
            [SVProgressHUD dismiss];
            
        }else {
            [SVProgressHUD dismiss];
            
        }
        UITableView *tableView = (UITableView *)[self.view viewWithTag:2450];
        [tableView reloadData];
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.title = @"Package Selection";
    [self.navigationItem setHidesBackButton:YES];
    
    if(![self.loginStatus isEqualToString:@"varification"]) {
        self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    }
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        
        [self getPackgeDataByMonth:self.userID ByMonth:@"0"];
        
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addComponentOnView {
    
    if (IS_IPHONE_4S ) {
        
        [self addsegmentedControlOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 5, [UIScreen mainScreen].bounds.size.width - 30, 35)];
    }else {
        [self addsegmentedControlOnView:(IS_IPHONE_5) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 5, [UIScreen mainScreen].bounds.size.width - 30, 35):CGRectMake([UIScreen mainScreen].bounds.origin.x + 15, [UIScreen mainScreen].bounds.origin.y + 5, [UIScreen mainScreen].bounds.size.width - 30, 35)];
    }
    
}

#pragma mark -UISegmentedControl

-(void)addsegmentedControlOnView:(CGRect)frame {
    
    VSGetPackByMonth_YearModel *lPackageModel = [self.monthDataArray objectAtIndex:0];
    
    UIButton *button = [[UIButton alloc]init];
    [button setFrame:frame];
    [button setTag:2999];
    //[button.layer setBorderWidth:0.5];
    [button setBackgroundColor:[UIColor lightGrayColor]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:[model.packageDictionary valueForKey:[NSString stringWithFormat:@"%@",lPackageModel.packageId]] forState:UIControlStateNormal];

    [self.view addSubview:button];
    
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    UITableView *cardTable = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
    [cardTable setTag:tag];
    [cardTable setDelegate:(id)self];
    [cardTable setDataSource:(id)self];
    [cardTable setBackgroundColor:[UIColor clearColor]];
    [cardTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [cardTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:cardTable];
}

-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(payButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(void)addView:(CGRect)frame {
    
    viewPicker = [[UIView alloc]init];
    [viewPicker setFrame:frame];
    [viewPicker setBackgroundColor:[UIColor colorWithRed:183.0f/255.0f green:40.0f/255.0f blue:35.0f/255.0f alpha:1.0f]];
    [viewPicker setTag:9999];
    [self.view addSubview:viewPicker];
    
    UIButton *donebutton = [[UIButton alloc]init];
    [donebutton setFrame:CGRectMake(viewPicker.frame.origin.x, 0.0, 80, 44)];
    [donebutton setTitle:@"Done" forState:UIControlStateNormal];
    [donebutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [donebutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [donebutton addTarget:self action:@selector(doneButtonClickedForPackageSelection:) forControlEvents:UIControlEventTouchUpInside];
    [viewPicker addSubview:donebutton];
    
    UIButton *cancelbutton = [[UIButton alloc]init];
    [cancelbutton setFrame:CGRectMake(viewPicker.frame.size.width - 80, 0.0, 80, 44)];
    [cancelbutton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelbutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [cancelbutton addTarget:self action:@selector(cancelButonForPackageSelection:) forControlEvents:UIControlEventTouchUpInside];
    [viewPicker addSubview:cancelbutton];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

#pragma mark -TableView Delegate and DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return (!self.packageDictionaryKey.length == 0) ? self.yearDataArray.count : self.monthDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    MyRouteTableCell *cell = (MyRouteTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[MyRouteTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setBackgroundColor:[UIColor clearColor]];
        
        UIView *lineView = [[UIView alloc]init];
        [lineView setFrame:CGRectMake(0.0, 60.0, [UIScreen mainScreen].bounds.size.width, 0.5)];
        [lineView setBackgroundColor:[UIColor lightGrayColor]];
        [cell.contentView addSubview:lineView];
    }
    
    
    VSGetPackByMonth_YearModel *model111 = (!self.packageDictionaryKey.length == 0) ?[self.yearDataArray objectAtIndex:indexPath.row]:[self.monthDataArray objectAtIndex:indexPath.row];
    [cell.line setHidden:YES];
    [cell.myId setText:@"Feature List"];
    [cell.source setText:@"Ammount($)"];
    
    [cell.myFinalId setText:[NSString stringWithFormat:@": %@",model111.packageName ]];
    [cell.finalSource setText:[NSString stringWithFormat:@": %@",model111.packagePrice]];
    
    return cell;
}

/*
 *******************************************************************
 *  UIPickerView delegate and data source methods..
 *******************************************************************
 */

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return self.packageDictionaryArray.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    VSGetPackByMonth_YearModel *lPackgeModel;
    lPackgeModel = [self.packageDictionaryArray objectAtIndex:row];
    return lPackgeModel.packageDictionaryObject;
}

-(IBAction)payButtonAction:(UIButton *)sender {
    
    if (sender.tag == 8899) {
        
        VSGetPackByMonth_YearModel *model11 = [self.monthDataArray objectAtIndex:0];
      
            VSAntiTiketView *antiTicket = [[VSAntiTiketView alloc]init];
            [antiTicket setPackageID:(!self.packageDictionaryKey.length == 0) ? self.packageDictionaryKey:model11.packageId];
            [self.navigationController pushViewController:antiTicket animated:YES];
     
    }else {
        NSLog(@"self.userIDself.userID:-%@",self.userID);
        VSMapController *mapView = [[VSMapController alloc]init];
        [mapView setUserID:self.userID];
        [self.navigationController pushViewController:mapView animated:YES];
    }
}

-(IBAction)buttonClick:(UIButton *)sender {
    
    [viewPicker setHidden:NO];
    if (pressCount > 0) {
        [self.pickerView removeFromSuperview];
    }
    
    self.pickerView = [[UIPickerView alloc]init];
    [self.pickerView setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 220 , [UIScreen mainScreen].bounds.size.width, 220)];
    [self.pickerView setBackgroundColor:[UIColor lightGrayColor]];
    [self.pickerView setUserInteractionEnabled:YES];
    [self.pickerView setDataSource:(id)self];
    [self.pickerView setDelegate:(id)self];
    [self.view addSubview:self.pickerView];
    pressCount++;
}
-(IBAction)doneButtonClickedForPackageSelection:(id)sender {
    NSInteger row;
    UIButton *packageTypeButton = (UIButton *)[self.view viewWithTag:2999];
    VSGetPackByMonth_YearModel *lModel;
    
    row = [self.pickerView selectedRowInComponent:0];
    lModel = [self.packageDictionaryArray objectAtIndex:row];
    [packageTypeButton setTitle:lModel.packageDictionaryObject forState:UIControlStateNormal];
    self.packageDictionaryKey = [NSString stringWithFormat:@"%@", lModel.packageDictionaryKey];
    [self getDataByPackageId:self.packageDictionaryKey];
    
    [viewPicker setHidden:YES];
    [self.pickerView setHidden:YES];
}

-(IBAction)cancelButonForPackageSelection:(id)sender {
    
    [viewPicker setHidden:YES];
    [self.pickerView setHidden:YES];
}

-(void)getDataByPackageId:(NSString*)byPackageId {
    
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getPackDataByYear:self.userID ByYearly:byPackageId];
    }else {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertview show];
    }
}

@end
