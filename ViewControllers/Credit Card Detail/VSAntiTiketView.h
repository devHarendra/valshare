//
//  VSAntiTiketView.h
//  ValShare
//
//  Created by Vivek Kumar on 4/21/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@interface VSAntiTiketView : UIViewController

@property (strong , nonatomic) NSString *packageID;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UIScrollView *scrollView;



@end
