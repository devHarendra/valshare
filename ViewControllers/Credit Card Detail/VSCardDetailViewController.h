//
//  VSCardDetailViewController.h
//  ValShare
//
//  Created by Vivek Kumar on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"
#import "VSCCDetailModelClass.h"
#import "LTHMonthYearPickerView.h"

@interface VSCardDetailViewController : UIViewController {
    
}

@property (nonatomic, strong) NSMutableArray *monthsArray;
@property (nonatomic, strong) NSMutableArray *yearArray;
@property (nonatomic, assign) NSString *VCTitle;
@property (nonatomic, assign) NSString *firstLogin;
@property (nonatomic, assign) NSString *userID;

@property (nonatomic, strong) VSCCDetailModelClass *ccModelClass;
@property (nonatomic, strong) UIDatePicker *datePicker;



@end
