//
//  VSPackageSelectionView.h
//  ValShare
//
//  Created by Vivek Kumar on 4/20/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@interface VSPackageSelectionView : UIViewController <UITableViewDelegate ,UITableViewDataSource,UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic,strong)  NSMutableArray       *monthDataArray;
@property (nonatomic,strong)  NSMutableArray       *yearDataArray;
@property (nonatomic,strong)  VSCCDetailModelClass *modelClass;
@property (nonatomic,strong)  NSMutableArray       *packageDictionaryArray;
@property (nonatomic,strong)  NSString             *segmentID;
@property (nonatomic, strong) UIPickerView        *pickerView;
@property (nonatomic,strong)  NSString             *packageDictionaryKey;
@property (nonatomic,strong)  NSString             *packageDictionaryString;

@property (nonatomic,strong)  NSString             *userID;
@property (nonatomic,strong)  NSString             *loginStatus;

@end
