//
//  VSUpdateCardDetails.h
//  ValShare
//
//  Created by Sharda Prasad on 4/27/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@interface VSUpdateCardDetails : UIViewController

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong)  VSCCDetailModelClass *updateCardModal;
@property (nonatomic, strong)  NSString *userID;
@property (nonatomic, strong)  NSMutableArray *getCardArray;

@end
