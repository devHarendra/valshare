//
//  VSUpdateCardDetails.m
//  ValShare
//
//  Created by Sharda Prasad on 4/27/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSUpdateCardDetails.h"

@interface VSUpdateCardDetails () <LTHMonthYearPickerViewDelegate> {
   int pressCount;
    UIView *myview;
    NSString *newCardNumber;
    NSString *selectedCard;
    SSCheckBoxView *checkBox;

}

@property (nonatomic, strong) LTHMonthYearPickerView  *monthYearPicker;

@end

@implementation VSUpdateCardDetails
@synthesize updateCardModal;
@synthesize scrollView = _scrollView;
@synthesize monthYearPicker;
@synthesize userID;
@synthesize getCardArray = _getCardArray;

-(NSMutableArray *)getCardArray {
    
    if (!_getCardArray) {
        _getCardArray = [[NSMutableArray alloc] init];
    }
    return _getCardArray;
}

-(UIScrollView *)scrollView {
    
    if (!_scrollView) {
        
        _scrollView = [[UIScrollView alloc] initWithFrame:(IS_IPHONE_4S) ? CGRectMake(0.0, [UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 80):CGRectMake(0.0, [UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 10)];
        [_scrollView setTag:78235];
        [_scrollView setScrollEnabled:YES];
        [_scrollView setUserInteractionEnabled:YES];
        [_scrollView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_scrollView];
    }
    return _scrollView;
}


-(void)checkName:(NSString *)name TextField:(UITextField *)textField {
    
    [[VSParserClass sharedParser] checkFirstOrLastName:name WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSString *string = [[NSString alloc] initWithData:resultObject encoding:NSUTF8StringEncoding];
        
        if ([string isEqualToString:@"true"]) {
            if (textField.tag == 2225) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Your first name is not valid" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [textField setText:@""];
            } else {
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"Your last name is not valid" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [textField setText:@""];
            }
        }
    }];
}

-(void)getCardDetatilFromServer:(NSString *)cardID {
    
    [[VSParserClass sharedParser] VSGetCardDetail:cardID WithCompletionBlock:^(BOOL result ,id resultObject, NSError *error){
        
        NSLog(@"resultObjectresultObjectresultObject:-%@",resultObject);
        if (result) {
            
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
                [SVProgressHUD dismiss];
                
            }else{
                
                NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                if ([dictdata isKindOfClass:[NSDictionary class]]) {
                    
                    
                    NSArray *arr = [dictdata objectForKey:@"creditDebitCardModelList"];
                    if (arr.count>0) {
                        
                    NSDictionary *newDictionary = [arr firstObject];
                    
                    NSLog(@"NSDictionaryNSDictionaryNSDictionary%@",newDictionary);
                    VSCCDetailModelClass *modelClass = [[VSCCDetailModelClass alloc]init];
                    
                    [modelClass setAmountToBePaid:[newDictionary valueForKey:@"amountToBePaid"]];
                    [modelClass setCardId:[newDictionary valueForKey:@"cardId"]];
                    [modelClass setCardNumber:[newDictionary valueForKey:@"cardNumber"]];
                    [modelClass setCardType:[newDictionary valueForKey:@"cardType"]];
                    [modelClass setCardTypeId:[newDictionary valueForKey:@"cardTypeId"]];
                    [modelClass setCreditDebitCardInfoStatusId:[newDictionary valueForKey:@"creditDebitCardInfoStatusId"]];
                    [modelClass setExpiryDate:[newDictionary valueForKey:@"expiryDate"]];
                    [modelClass setFirstName:[newDictionary valueForKey:@"firstName"]];
                    [modelClass setLastModifiedTime:[newDictionary valueForKey:@"lastModifiedTime"]];
                    [modelClass setLastName:[newDictionary valueForKey:@"lastName"]];
                    [modelClass setLoyaltyPrice:[newDictionary valueForKey:@"loyaltyPrice"]];
                    [modelClass setPackagePrice:[newDictionary valueForKey:@"packagePrice"]];
                    [modelClass setPaymentPackageId:[newDictionary valueForKey:@"paymentPackageId"]];
                    [modelClass setStatusTypeId:[newDictionary valueForKey:@"statusTypeId"]];
                    [modelClass setTotalLoyalityPoint:[newDictionary valueForKey:@"totalLoyalityPoint"]];
                    [modelClass setUserId:[newDictionary valueForKey:@"userId"]];
                    [modelClass setIsDefault:[newDictionary valueForKey:@"isDefault"]];
                    
                    [modelClass setStreetAddress:([[newDictionary allKeys] containsObject:@"streetAddress"]) ?[newDictionary valueForKey:@"streetAddress"] : @""];
                    [modelClass setCity:([[newDictionary allKeys] containsObject:@"city"]) ? [dictdata valueForKey:@"city"] : @""];
                    [modelClass setCountry:([[newDictionary allKeys] containsObject:@"country"]) ? [newDictionary valueForKey:@"country"] : @""];
                    [modelClass setZipCode:([[newDictionary allKeys] containsObject:@"zipCode"]) ? [newDictionary valueForKey:@"zipCode"] : @""];
                    NSLog(@"---- [dictdata valueForKey: %@", modelClass.streetAddress);

                    [self.getCardArray addObject:modelClass];
                    [self billingAddressTextField:[NSArray arrayWithObjects:self.updateCardModal.streetAddress,self.updateCardModal.city,self.updateCardModal.country,self.updateCardModal.zipCode, nil]];
                }else if ([dictdata isKindOfClass:[NSArray class]]) {
                    NSLog(@"NSArrayNSArrayNSArray%@",dictdata);
                }else {
                    NSLog(@"else%@",dictdata);
                }
                }
                 //pervious code
                
               /* VSCCDetailModelClass *modelClass123 = [self.getCardArray objectAtIndex:0];
                NSArray *billingAddress = @[[NSString stringWithFormat:@"%@",modelClass123.streetAddress],[NSString stringWithFormat:@"%@",modelClass123.city], [NSString stringWithFormat:@"%@",modelClass123.country],[NSString stringWithFormat:@"%@",modelClass123.zipCode]];
                [self billingAddressTextField:billingAddress];

                */
                
                
                //new Code Create by dheeraj
                //for tempary use
                
                
                [SVProgressHUD dismiss];
            }
            
        }else {
            [SVProgressHUD dismiss];
        }
    }];
}

-(void)updateCardDetailOnServer:(NSString *)firstname LastName:(NSString *)lastname CardNumber:(NSString *)cardnubmer City:(NSString *)city ExpiryDate:(NSString *)expirydate UserID:(NSString *)userid CardID:(NSString *)cardid Country:(NSString *)country CardTypeID:(NSString *)cardtypeid IsDefault:(NSString *)isDefault oldCardNumber:(NSString *)oldCardNumber StreetAddress:(NSString *)streetAddress zipCode:(NSString *)zipCode creditDebitCardInfoStatusId:(NSString *)creditDebitCardInfoStatusId isActive:(NSString *)isActive {
    
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
    [dictData setValue:cardnubmer forKey:@"cardNumber"];
    [dictData setValue:city forKey:@"city"];
    [dictData setValue:country forKey:@"country"];
    [dictData setValue:expirydate forKey:@"expiryDate"];
    [dictData setValue:firstname forKey:@"firstName"];
    [dictData setValue:lastname forKey:@"lastName"];
    [dictData setValue:oldCardNumber forKey:@"oldCardNumber"];
    [dictData setValue:streetAddress forKey:@"streetAddress"];
    [dictData setValue:zipCode forKey:@"zipCode"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[creditDebitCardInfoStatusId intValue]] forKey:@"creditDebitCardInfoStatusId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[cardtypeid intValue]] forKey:@"cardTypeId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[cardid intValue]] forKey:@"cardId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[userid intValue]] forKey:@"userId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[isDefault intValue]] forKey:@"isDefault"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[isActive intValue]] forKey:@"isActive"];
    
    [[VSParserClass sharedParser] updateCardDetails:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSLog(@"updateCardDetailsupdateCardDetailsupdateCardDetails :- %@", resultObject);
        if (result) {
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                [VSEnumClass showAlertWithTitle:nil msg:@"Card update successfully."];
            } else {
                [VSEnumClass showAlertWithTitle:nil msg:[NSString stringWithFormat:@"%@", [resultObject valueForKey:@"message"]]];
            }
        } else {
            
        }
    }];
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [self getCardDetatilFromServer:updateCardModal.cardId];
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Update Card Detail";
    
    
    pressCount = 0;
    [self.navigationItem setHidesBackButton:YES];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    
    [self addCoponentOnView];
    
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector

- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addCoponentOnView {
    
    NSArray *labelArray = @[@"Card Details",@"Billing Address"];
    NSArray *placeHolderArray = @[@"First Name",@"Last Name",@"Card Number"];
    NSArray *cardDetailsA = @[updateCardModal.firstName,updateCardModal.lastName,updateCardModal.cardNumber];
    
   // NSArray *billingAddress = @[updateCardModal.firstName,updateCardModal.lastName,updateCardModal.cardNumber];
    // Need :-- Street Address, City, Country, zip Key from server side.
    
    NSArray *image = @[kVisaCardBG,kMasterCardBG];

    for (int i = 0; i<2; i++) {
        
        [self addLabelOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 10, [UIScreen mainScreen].bounds.origin.y + 65 + (([UIScreen mainScreen].bounds.size.width/2 + 55) * i), [UIScreen mainScreen].bounds.size.width - 20, 20) Text:labelArray[i]];
        
        [self addImageOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 62 + (i * (kVisaCardBG.size.width * 4.3)), [UIScreen mainScreen].bounds.origin.y + 30, kVisaCardBG.size.width, kVisaCardBG.size.height) BGImage:image[i] Tag:2000+i];
        [self addRadioButtonOnview:CGRectMake([UIScreen mainScreen].bounds.origin.x + 42 + (i *(kVisaCardBG.size.width * 4)), [UIScreen mainScreen].bounds.origin.y + 30, kVisaCardBG.size.width, kVisaCardBG.size.height) Tag:15090 + i];
    }

    for (int j = 0; j< 3; j++) {
        
        [self addTextFieldOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.origin.y + 90 + (40 * j), [UIScreen mainScreen].bounds.size.width - 40, 35) Tag:555456 + j Text:cardDetailsA[j] PlaceHolder:placeHolderArray[j]];
    }
    
    [self addExpiryButtonOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.origin.y + 210, [UIScreen mainScreen].bounds.size.width - 40, 35) Title:@"Expiry" Tag:43536];
    
    [self addCheckMark:CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.origin.y + 250, [UIScreen mainScreen].bounds.size.width - 40, 35)];

    if ([updateCardModal.creditDebitCardInfoStatus isEqualToString:@"Active"] && [updateCardModal.isDefault boolValue]) {
        [checkBox setHidden:YES];
        
    }
    if ([updateCardModal.creditDebitCardInfoStatus isEqualToString:@"Deleted"]) {
        
        [checkBox setHidden:NO];
        [checkBox setText:@"Mark As Active"];
        
    }
    if ([updateCardModal.creditDebitCardInfoStatus isEqualToString:@"Active"] && ![updateCardModal.isDefault boolValue]) {
        [checkBox setHidden:NO];
        [checkBox setText:@"Mark As Default"];
    }
    
    [self addUpdateButtonOnView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.size.height , [UIScreen mainScreen].bounds.size.width - 40, 35):(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.size.height - 150, [UIScreen mainScreen].bounds.size.width - 40, 35):CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.size.height - 50, [UIScreen mainScreen].bounds.size.width - 40, 35) Title:@"Update" Tag:787676];
    
    [self addView:(IS_IPHONE_4S) ? CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 - 35 ,  [UIScreen mainScreen].bounds.size.width, 44) :(IS_IPHONE6) ?  CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 + 55 ,  [UIScreen mainScreen].bounds.size.width, 44):CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 ,  [UIScreen mainScreen].bounds.size.width, 44)];

    [self.scrollView setContentSize:CGSizeMake(0.0, [UIScreen mainScreen].bounds.size.height + 190)];
}

-(void)billingAddressTextField:(NSArray *)data {
    
    if (data.count<=0) {
        [VSEnumClass showAlertWithTitle:@"Alert" msg:@"Network issue Try Again"];
    }
    else
    {
    
    NSArray *bPHArray = @[@"Street Address",@"City",@"Country",@"Zip Code"];
    for (int j = 0; j< 4; j++) {
        
        [self addTextFieldOnView:(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.origin.y + 335 + (40 * j), [UIScreen mainScreen].bounds.size.width - 40, 35):CGRectMake([UIScreen mainScreen].bounds.origin.x + 20, [UIScreen mainScreen].bounds.origin.y + 315 + (40 * j), [UIScreen mainScreen].bounds.size.width - 40, 35) Tag:59456 + j Text:data[j] PlaceHolder:bPHArray[j]];
    }
}
}

-(void)addRadioButtonOnview:(CGRect)frame Tag:(int)tag {
    
    QRadioButton *rUserName = [[QRadioButton alloc] initWithDelegate:self groupId:@"cardID"];
    [rUserName setFrame:frame];
    [rUserName setTag:tag];
    [rUserName setTitle:@"" forState:UIControlStateNormal];
    [rUserName setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rUserName.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [self.scrollView addSubview:rUserName];
    [rUserName setChecked:(tag == 15090) ? YES : NO];
}

- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId {
    
    selectedCard = [NSString stringWithFormat:@"%d", radio.checked];
}

-(void)addCheckMark:(CGRect)frame {
    
    checkBox = [[SSCheckBoxView alloc] initWithFrame:frame style:kSSCheckBoxViewStyleMono checked:NO];
    [checkBox setTag:4343];
    [checkBox setStateChangedBlock:^(SSCheckBoxView *check) {
        
    }];
    
    [self.scrollView addSubview:checkBox];
}

-(void)addImageOnView:(CGRect)frame BGImage:(UIImage *)bgimage Tag:(int)tag {
    
    UIImageView *imageView = [[UIImageView alloc]init];
    [imageView setFrame:frame];
    [imageView setTag:tag];
    [imageView setImage:bgimage];
    [imageView setBackgroundColor:[UIColor clearColor]];
    [self.scrollView addSubview:imageView];
}

-(void)addLabelOnView:(CGRect)frame Text:(NSString *)text {
    
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:frame];
    [label setText:text];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont systemFontOfSize:15.0f]];
    [self.scrollView addSubview:label];
}

-(void)addTextFieldOnView:(CGRect)frame Tag:(int)tag Text:(NSString *)title PlaceHolder:(NSString *)pHolder {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setFrame:frame];
    [textField setTag:tag];
    [textField setText:title];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:pHolder];
    [textField.layer setBorderWidth:1.0];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setKeyboardType:UIKeyboardTypeDefault];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    [textField.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    [self.scrollView addSubview:textField];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
//    switch (textField.tag) {
//        case 59456:
//            [self animateTextFieldAndView:textField up:YES Distance:30];
//            break;
//        case 59457:
//            [self animateTextFieldAndView:textField up:YES Distance:50];
//            break;
//        case 59458:
//            [self animateTextFieldAndView:textField up:YES Distance:60];
//            break;
//        case 59459:
//            [self animateTextFieldAndView:textField up:YES Distance:70];
//            break;
//
//        default:
//            break;
//    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    //[self animateTextFieldAndView:textField up:NO Distance:0];
    if (textField.tag == 555456) {
        
        [self checkName:textField.text TextField:textField];

    }else if (textField.tag == 555457) {
        
        [self checkName:textField.text TextField:textField];

    }
}

- (void)animateTextFieldAndView: (UITextField*) textField up:(BOOL) up Distance:(int)distance {
    
    const int movementDistance = distance;
    const float movementDuration = 0.3f;
    
    int movement = (up ? - movementDistance : movementDistance);
    
    [UIView beginAnimations:@"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration:movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
   
    return YES;
}

-(void)addExpiryButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.layer setBorderWidth:1.0f];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(expiryButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:button];
}

-(void)addUpdateButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [button addTarget:self action:@selector(updateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundColor:[UIColor colorWithRed:183.0f/255.0f green:40.0f/255.0f blue:35.0f/255.0f alpha:1.0f]];
    [self.scrollView addSubview:button];
}


-(void)addView:(CGRect)frame {
    
    myview = [[UIView alloc]init];
    [myview setFrame:frame];
    [myview setBackgroundColor:[UIColor colorWithRed:183.0f/255.0f green:40.0f/255.0f blue:35.0f/255.0f alpha:1.0f]];
    [myview setTag:9999];
    [myview setHidden:YES];
    [self.view addSubview:myview];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 44)];
    titleLabel.text = @"Select month and year";
    titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.backgroundColor = [UIColor clearColor];
    [myview addSubview:titleLabel];
    
    UIButton *doneButton = [[UIButton alloc]init];
    [doneButton setFrame:(IS_IPHONE_5) ? CGRectMake(240, 0.0, 60, 44) : (IS_IPHONE_4S) ? CGRectMake(240, 0.0, 60, 44) : CGRectMake(280, 0.0, 60, 44)];
    [doneButton setBackgroundColor:[UIColor clearColor]];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
    [doneButton addTarget:self action:@selector(doneButtonClickedOnExpiry:) forControlEvents:UIControlEventTouchUpInside];
    [myview addSubview:doneButton];
}

-(IBAction)expiryButtonAction:(id)sender {
    
    NSLog(@"expiryButtonActionexpiryButtonAction");
    if (pressCount > 0) {
        [self.monthYearPicker removeFromSuperview];
    }
    [myview setHidden:NO];
    self.monthYearPicker = [[LTHMonthYearPickerView alloc] initWithDate: [NSDate date]
                                                            shortMonths: NO
                                                         numberedMonths: NO
                                                             andToolbar: YES];
    [self.monthYearPicker setDelegate:(id)self];
    [self.monthYearPicker setTag:6666];
    [self.monthYearPicker setBackgroundColor:[UIColor whiteColor]];
    pressCount++;
    [self.view addSubview:self.monthYearPicker];
}

-(IBAction)doneButtonClickedOnExpiry:(UIButton *)sender {
    
    [self.monthYearPicker removeFromSuperview];
    [myview setHidden:YES];
}

- (void)pickerDidPressDoneWithMonth:(NSString *)month andYear:(NSString *)year {
    
    UIButton *dateButton = (UIButton *)[self.view viewWithTag:43536];
    [dateButton setTitle:[NSString stringWithFormat: @"%@/%@", month,year] forState:UIControlStateNormal];
}

- (void)pickerDidSelectMonth:(NSString *)month andYear:(NSString *)year {
    
    UIButton *dateButton = (UIButton *)[self.view viewWithTag:43536];
    [dateButton setTitle:[NSString stringWithFormat: @"%@ / %@", month, year] forState:UIControlStateNormal];
}
-(IBAction)updateButtonAction:(VSButton *)sender {
    
    
    UITextField *firstName = (UITextField *)[self.view viewWithTag:555456];
    UITextField *lastName = (UITextField *)[self.view viewWithTag:555457];
    UITextField *cardNumber = (UITextField *)[self.view viewWithTag:555458];
    
    UITextField *streetAddress = (UITextField *)[self.view viewWithTag:59456];
    UITextField *city = (UITextField *)[self.view viewWithTag:59457];
    UITextField *country = (UITextField *)[self.view viewWithTag:59458];
    UITextField *zip = (UITextField *)[self.view viewWithTag:59459];
    
    if ([firstName.text length] == 0) {
        [VSEnumClass showAlertWithTitle:nil msg:@"Please enter first name"];
    } else if ([lastName.text length] == 0) {
        [VSEnumClass showAlertWithTitle:nil msg:@"Please enter last name"];
    }else if ([cardNumber.text length] == 0) {
        [VSEnumClass showAlertWithTitle:nil msg:@"Please enter card number"];
    }else if ([streetAddress.text length] == 0) {
        [VSEnumClass showAlertWithTitle:nil msg:@"Please enter street Address"];
    }else if ([city.text length] == 0) {
        [VSEnumClass showAlertWithTitle:nil msg:@"Please enter city name"];
    }else if ([country.text length] == 0) {
        [VSEnumClass showAlertWithTitle:nil msg:@"Please enter country name"];
    }else if ([zip.text length] == 0) {
        [VSEnumClass showAlertWithTitle:nil msg:@"Please enter zip code"];
    } else {

        VSCCDetailModelClass *modelClass123 = [self.getCardArray objectAtIndex:0];

        [self updateCardDetailOnServer:firstName.text LastName:lastName.text CardNumber:cardNumber.text City:city.text ExpiryDate:modelClass123.expiryDate UserID:self.userID CardID:updateCardModal.cardId Country:country.text CardTypeID:selectedCard IsDefault:modelClass123.isDefault oldCardNumber:cardNumber.text StreetAddress:streetAddress.text zipCode:zip.text creditDebitCardInfoStatusId:modelClass123.creditDebitCardInfoStatusId isActive:@"1"];
    }
}

@end
