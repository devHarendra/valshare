//
//  VSUpdateProfileViewController.h
//  ValShare
//
//  Created by Vivek Kumar on 4/6/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"


@interface VSUpdateProfileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate> {
    
}

@property (nonatomic, retain) NSString *dobString;
@property (nonatomic, retain) NSString *genderString;
@property (nonatomic, retain) NSString *languageString;
@property (nonatomic, retain) NSString *countryString;
@property (nonatomic, retain) NSString *cityString;
@property (nonatomic, assign) NSString *userID;

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIPickerView *genderPicker;
@property (nonatomic, strong) UIPickerView *languagePicker;
@property (nonatomic, strong) UIPickerView *countryPicker;
@property (nonatomic, strong) UIPickerView *cityPicker;

@property (nonatomic, strong) NSMutableArray *dayArray;
@property (nonatomic, strong) NSMutableArray *monthsArray;
@property (nonatomic, strong) NSMutableArray *yearArray;
@property (nonatomic, strong) NSMutableArray *genderArray;
@property (nonatomic, strong) NSMutableArray *languageArray;
@property (nonatomic, strong) NSMutableArray *countryArray;
@property (nonatomic, strong) NSMutableArray *cityArray;
@property (nonatomic, strong) NSMutableArray *userDetailArray;

@property (nonatomic, strong) NSMutableArray *labelArray;
@property (nonatomic, strong) NSMutableArray *labelArray1;
@property (nonatomic, strong) NSMutableArray *labelArray2;


@end
