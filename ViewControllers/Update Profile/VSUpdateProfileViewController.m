//
//  VSUpdateProfileViewController.m
//  ValShare
//
//  Created by Vivek Kumar on 4/6/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSUpdateProfileViewController.h"
#import "VSHeader.h"

@interface VSUpdateProfileViewController (){
    
    int pickerRemove;
    UIView *viewPicker;
    UITableView *routeTable;
    NSInteger  currentbuttonTag;
    VSUserDetailModel *userDetailModel;
    NSString *updatedEmail;
    int doneClick;
}

@end

@implementation VSUpdateProfileViewController
@synthesize dayArray = _dayArray;
@synthesize monthsArray = _monthsArray;
@synthesize yearArray = _yearArray;
@synthesize genderArray = _genderArray;
@synthesize languageArray = _languageArray;
@synthesize countryArray = _countryArray;
@synthesize cityArray = _cityArray;
@synthesize userDetailArray = _userDetailArray;
@synthesize datePicker;
@synthesize genderPicker;
@synthesize languagePicker;
@synthesize countryPicker;
@synthesize cityPicker;
@synthesize labelArray = _labelArray;
@synthesize labelArray1 = _labelArray1;
@synthesize labelArray2 = _labelArray2;

@synthesize dobString;
@synthesize genderString;
@synthesize languageString;
@synthesize countryString;
@synthesize cityString;
@synthesize userID;

-(NSMutableArray *)labelArray {
    
    if(!_labelArray) {
        _labelArray = [[NSMutableArray alloc] initWithObjects:@"First Name",@"Last Name",@"Date Of Barth",@"Gender",@"Language", nil];
    }
    return _labelArray;
}

-(NSMutableArray *)labelArray1 {
    
    if(!_labelArray1) {
        _labelArray1 = [[NSMutableArray alloc] initWithObjects:@"Country",@"City",@"Address one",@"Address two",@"Zip Code",@"Contact Number", nil];
    }
    return _labelArray1;
}

-(NSMutableArray *)labelArray2 {
    
    if(!_labelArray2) {
        _labelArray2 = [[NSMutableArray alloc] initWithObjects:@"Next Payment date",@"User Name",@"Account Type",@"Email", nil];
    }
    return _labelArray2;
}


-(NSMutableArray *)dayArray {
    
    if(!_dayArray) {
        _dayArray = [[NSMutableArray alloc] init];
    }
    return _dayArray;
}


-(NSMutableArray *)monthsArray {
    
    if(!_monthsArray) {
        _monthsArray = [[NSMutableArray alloc] init];
    }
    return _monthsArray;
}

-(NSMutableArray *)yearArray {
    
    if(!_yearArray) {
        _yearArray = [[NSMutableArray alloc] init];
    }
    return _yearArray;
}
-(NSMutableArray *)genderArray {
    
    if(!_genderArray) {
        _genderArray = [[NSMutableArray alloc] init];
    }
    return _genderArray;
}


-(NSMutableArray *)languageArray {
    
    if(!_languageArray) {
        _languageArray = [[NSMutableArray alloc] init];
    }
    return _languageArray;
}

-(NSMutableArray *)countryArray {
    
    if(!_countryArray) {
        _countryArray = [[NSMutableArray alloc] init];
    }
    return _countryArray;
}

-(NSMutableArray *)cityArray {
    
    if(!_cityArray) {
        _cityArray = [[NSMutableArray alloc] init];
    }
    return _cityArray;
}

-(NSMutableArray *)userDetailArray {
    
    if(!_userDetailArray) {
        _userDetailArray = [[NSMutableArray alloc] init];
    }
    return _userDetailArray;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

-(void)getUserDetailFromServer:(NSString *)userId {
    
    [[VSParserClass sharedParser]valShareGetUserDetail:userId WithCompletionBlock:^(BOOL result, id resultObject,NSError *error) {
        NSLog(@"resultObjectresultObject:-%@",resultObject);
        if (result) {
            
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Failure"]) {
                [SVProgressHUD dismiss];

              [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }else if ([[resultObject valueForKey:@"status"]isEqualToString:@"Exception"]) {
                    [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                [SVProgressHUD dismiss];
                return ;
  
            }
            else {
            if ([resultObject isKindOfClass:[NSDictionary class]]) {
                
                NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                
                NSLog(@"dictdatadictdatadictdatadictdatadictdatadictdatadictdata :- %@", dictdata);
                
                userDetailModel = [[VSUserDetailModel alloc]init];
                
                [userDetailModel setDob:([[dictdata allKeys] containsObject:@"DOB"]) ? [dictdata valueForKey:@"DOB"] : @""];
                [userDetailModel setAccountType:([[dictdata allKeys] containsObject:@"accountType"]) ? [dictdata valueForKey:@"accountType"] : @""];
                [userDetailModel setAccountTypeId:([[dictdata allKeys] containsObject:@"accountTypeId"]) ?[dictdata valueForKey:@"accountTypeId"] : @""];
                [userDetailModel setCityId:([[dictdata allKeys] containsObject:@"cityId"]) ? [dictdata valueForKey:@"cityId"] : @""];
                [userDetailModel setContactNumber:([[dictdata allKeys] containsObject:@"contactNumber"]) ?[dictdata valueForKey:@"contactNumber"] : @""];
                [userDetailModel setCountryId:([[dictdata allKeys] containsObject:@"countryId"]) ? [dictdata valueForKey:@"countryId"] : @""];
                [userDetailModel setEmail:([[dictdata allKeys] containsObject:@"email"]) ? [dictdata valueForKey:@"email"] : @""];
                [userDetailModel setFirstName:([[dictdata allKeys] containsObject:@"firstName"]) ? [dictdata valueForKey:@"firstName"] : @""];
                [userDetailModel setGender:([[dictdata allKeys] containsObject:@"gender"]) ? [dictdata valueForKey:@"gender"] : @""];
                [userDetailModel setGenderTypeId:([[dictdata allKeys] containsObject:@"genderTypeId"]) ? [dictdata valueForKey:@"genderTypeId"] : @""];
                [userDetailModel setLanguage:([[dictdata allKeys] containsObject:@"language"]) ? [dictdata valueForKey:@"language"] : @""];
                [userDetailModel setLanguageId:([[dictdata allKeys] containsObject:@"languageId"]) ? [dictdata valueForKey:@"languageId"] : @""];
                [userDetailModel setLastModifiedTime:([[dictdata allKeys] containsObject:@"lastModifiedTime"]) ? [dictdata valueForKey:@"lastModifiedTime"] : @""];
                [userDetailModel setLastName:([[dictdata allKeys] containsObject:@"lastName"]) ? [dictdata valueForKey:@"lastName"] : @""];
                [userDetailModel setLocationId:([[dictdata allKeys] containsObject:@"locationId"]) ? [dictdata valueForKey:@"locationId"] : @""];
                [userDetailModel setUserId:([[dictdata allKeys] containsObject:@"userId"]) ? [dictdata valueForKey:@"userId"] : @""];
                [userDetailModel setUserName:([[dictdata allKeys] containsObject:@"userName"]) ? [dictdata valueForKey:@"userName"] : @""];
                [userDetailModel setUserNameRequired:([[dictdata allKeys] containsObject:@"userNameRequired"]) ? [dictdata valueForKey:@"userNameRequired"] : @""];
                [userDetailModel setUserRoleId:([[dictdata allKeys] containsObject:@"userRoleId"]) ? [dictdata valueForKey:@"userRoleId"] : @""];
                [userDetailModel setUserStatusId:([[dictdata allKeys] containsObject:@"userRoleId"]) ? [dictdata valueForKey:@"userStatusId"] : @""];
                [userDetailModel setAddressOne:([[dictdata allKeys] containsObject:@"addressOne"]) ? [dictdata valueForKey:@"addressOne"] : @""];
                [userDetailModel setAddressTwo:([[dictdata allKeys] containsObject:@"addressTwo"]) ? [dictdata valueForKey:@"addressTwo"] : @""];
                [userDetailModel setZipCode:([[dictdata allKeys] containsObject:@"zipCode"]) ? [dictdata valueForKey:@"zipCode"] : @""];
                [userDetailModel setNextPaymentDate:([[dictdata allKeys] containsObject:@"nextPaymentDate"]) ? [dictdata valueForKey:@"nextPaymentDate"] : @""];
                [userDetailModel setCityDictionary:([[dictdata allKeys] containsObject:@"cityDictionary"]) ? [dictdata valueForKey:@"cityDictionary"] : @""];
                [userDetailModel setCountryDictionary:([[dictdata allKeys] containsObject:@"countryDictionary"]) ? [dictdata valueForKey:@"countryDictionary"] : @""];
                [userDetailModel setGenderDictionary:([[dictdata allKeys] containsObject:@"genderDictionary"]) ? [dictdata valueForKey:@"genderDictionary"] : @""];
                [userDetailModel setLanguageDictionary:([[dictdata allKeys] containsObject:@"genderDictionary"]) ? [dictdata valueForKey:@"languageDictionary"] : @""];
                [userDetailModel setUserModelList:([[dictdata allKeys] containsObject:@"userModelList"]) ? [dictdata valueForKey:@"userModelList"] : @""];
                [self.userDetailArray addObject:userDetailModel];
                
                
            }else if ([resultObject isKindOfClass:[NSArray class]]) {
                NSLog(@"NSArray:-%@",resultObject);
            }
                [SVProgressHUD dismiss];

            }
        }
       else {
           
            [SVProgressHUD dismiss];
             [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
        
        userDetailModel = [self.userDetailArray objectAtIndex:0];
        NSMutableDictionary *lCityDictionary = userDetailModel.cityDictionary;
        NSMutableDictionary *lCountryDictionary = userDetailModel.countryDictionary;
        NSMutableDictionary *lgenderDictionary = userDetailModel.genderDictionary;
        NSMutableDictionary *lLanguageDictionary = userDetailModel.languageDictionary;
        
        [lCityDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            VSUserDetailModel *lCityModel = [[VSUserDetailModel alloc] init];
            [lCityModel setCityKey:key];
            [lCityModel setCityObject:obj];
            [self.cityArray addObject:lCityModel];
        }];
        
        [lCountryDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            VSUserDetailModel *lCountryModel = [[VSUserDetailModel alloc] init];
            [lCountryModel setCountryKey:key];
            [lCountryModel setCountryObject:obj];
            [self.countryArray addObject:lCountryModel];
        }];
        
        [lgenderDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            VSUserDetailModel *lgenderModel = [[VSUserDetailModel alloc] init];
            [lgenderModel setGenderKey:key];
            [lgenderModel setGenderObject:obj];
            [self.genderArray addObject:lgenderModel];
        }];
        
        [lLanguageDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            VSUserDetailModel *llanguageModel = [[VSUserDetailModel alloc] init];
            [llanguageModel setLanguageKey:key];
            [llanguageModel setLanguageObject:obj];
            [self.languageArray addObject:llanguageModel];
        }];
        
        NSLog(@"userDetailModel.firstName :- %@ -- %@", userDetailModel.firstName, userDetailModel.lastName);
        [self addTableOnView:CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y + 10.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 130) Tag:2450];
        
        [self addComponentOnView];
        [viewPicker setHidden:YES];
    }];
}


-(void)postDataOnServer:(NSString *)firstName LastName:(NSString *)lastname DOB:(NSString *)dob AddressOne:(NSString *)addressone AddressTwo:(NSString *)addresstwo ContactNumber:(NSString *)contactnumber UserID:(NSString *)userid AccountTypeID:(NSString *)accounttype UserName:(NSString *)username GenderTypeID:(NSString *)gendertypeid Email:(NSString *)email Password:(NSString *)password CityID:(NSString *)cityid CountryID:(NSString *)countryid ZipCode:(NSString *)zipcode Language:(NSString *)languege LocationId:(NSString *)locationId PreviousEmail:(NSString *)previousEmail {
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc]init];
    
    [dictData setValue:firstName forKey:@"firstName"];
    [dictData setValue:lastname forKey:@"lastName"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[locationId intValue]] forKey:@"locationId"];
    [dictData setValue:[NSString stringWithFormat:@"%lld",[dob longLongValue]]  forKey:@"DOB"];
    [dictData setValue:addressone forKey:@"addressOne"];
    [dictData setValue:addresstwo forKey:@"addressTwo"];
    [dictData setValue:contactnumber forKey:@"contactNumber"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[userid intValue]]  forKey:@"userId"];
    [dictData setValue:[NSString stringWithFormat:@"%@",accounttype] forKey:@"accountType"];
    [dictData setValue:username forKey:@"userName"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[gendertypeid intValue]] forKey:@"genderTypeId"];
    [dictData setValue:email forKey:@"email"];
    [dictData setValue:previousEmail forKey:@"previousEmail"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[cityid intValue]] forKey:@"cityId"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[countryid intValue]] forKey:@"countryId"];
    [dictData setValue:zipcode forKey:@"zipCode"];
    [dictData setValue:[NSString stringWithFormat:@"%d", [languege intValue]] forKey:@"languageId"];
    
    [[VSParserClass sharedParser] valShareUpdateProfile:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error){
        NSLog(@"resultObjectresultObject:%@",resultObject);
        if (result) {
            
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                
                [VSEnumClass showAlertWithTitle:nil msg:[NSString stringWithFormat:@"%@", @"User profile updated successfully."]];
            }else {
                
                [VSEnumClass showAlertWithTitle:nil msg:[NSString stringWithFormat:@"%@", [resultObject valueForKey:@"message"]]];
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            [VSEnumClass showAlertWithTitle:nil msg:[NSString stringWithFormat:@"%@", @"Please contact to Valshare."]];
        }
    }];
}

-(void)dealloc {
    
    self.dobString  = nil;
    self.cityString = nil;
    self.genderString = nil;
    self.countryString = nil;
    self.languageString = nil;
    
    self.datePicker = nil;
    self.cityPicker = nil;
    self.genderPicker = nil;
    self.countryPicker = nil;
    self.languagePicker = nil;
    
    self.dayArray  = nil;
    self.cityArray  = nil;
    self.yearArray  = nil;
    self.genderArray  = nil;
    self.monthsArray  = nil;
    self.countryArray  = nil;
    self.languageArray  = nil;
    self.userDetailArray  = nil;
    
    self.labelArray  = nil;
    self.labelArray1  = nil;
    self.labelArray2  = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
    self.title = [NSString stringWithFormat:@"%@", @"Update Profile"];
    pickerRemove = 0;
    doneClick = 0;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    NSLog(@"self.userIDself.userIDself.userID:-%@",self.userID);
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        [self getUserDetailFromServer:self.userID];
    }else {
        
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertview show];
    }
}


#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector

- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    [self addSubmitButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 10 + [UIScreen mainScreen].bounds.size.height-120, kLoginBG.size.width, kLoginBG.size.height)  Title:@"Save" Tag:8899 BGImage:kLoginBG];
    
    [self addView:(IS_IPHONE6) ? CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 + 75,  [UIScreen mainScreen].bounds.size.width, 44) : (IS_IPHONE_5) ? CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 + 25,  [UIScreen mainScreen].bounds.size.width, 44) : CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 - 5,  [UIScreen mainScreen].bounds.size.width, 44)];
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    routeTable = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
    [routeTable setBackgroundColor:[UIColor clearColor]];
    [routeTable setTag:tag];
    [routeTable setDelegate:(id)self];
    [routeTable setDataSource:(id)self];
    [routeTable setTag:5000];
    [routeTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, routeTable.frame.size.height/2, 0.0f)];
    [routeTable setSeparatorColor:[UIColor colorWithRed:190.0f/255.0f green:52.0f/255.0f blue:41.0f/255.0f alpha:1.0f]];
    [routeTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:routeTable];
}

-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button setBackgroundColor:[UIColor grayColor]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [button addTarget:self action:@selector(updateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(void)addView:(CGRect)frame {
    
    viewPicker = [[UIView alloc]init];
    [viewPicker setFrame:frame];
    [viewPicker setBackgroundColor:[UIColor colorWithRed:183.0f/255.0f green:40.0f/255.0f blue:35.0f/255.0f alpha:1.0f]];
    [viewPicker setTag:9999];
    [self.view addSubview:viewPicker];
    
    UIButton *donebutton = [[UIButton alloc]init];
    [donebutton setFrame:CGRectMake(viewPicker.frame.origin.x, 0.0, 80, 44)];
    [donebutton setTitle:@"Done" forState:UIControlStateNormal];
    [donebutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [donebutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [donebutton addTarget:self action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [viewPicker addSubview:donebutton];
    
    UIButton *cancelbutton = [[UIButton alloc]init];
    [cancelbutton setFrame:CGRectMake(viewPicker.frame.size.width - 80, 0.0, 80, 44)];
    [cancelbutton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelbutton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [cancelbutton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [viewPicker addSubview:cancelbutton];
}


/*
 *************************************************************************
 *  UITableView delegate and data source methods..
 *************************************************************************
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSArray *textArray = @[@"General Information",@"Contact Information",@"Account Information"];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 7.0, tableView.frame.size.width, 30)];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *label = [[UILabel alloc]init];
    [label setFrame:CGRectMake(10.0f, 5.0f, 150.0f, 20)];
    for (int i = 0; i <= section; i++) { [label setText:textArray[i]]; }
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [label setBackgroundColor:[UIColor whiteColor]];
    [view addSubview:label];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    if (section == 0) { return [self.labelArray count];}
    else if (section == 1) { return [self.labelArray1 count];}
    else {return [self.labelArray2 count];}
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    VSUpdateProfileCell *cell = (VSUpdateProfileCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[VSUpdateProfileCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textFieldText.delegate = (id)self;
    }
    
    userDetailModel = [self.userDetailArray objectAtIndex:0];
    switch (indexPath.section) {
            
        case 0:
            
            if (indexPath.row == 2) {
                
                [cell.dateButton setHidden:NO];
                [cell.yearButton setHidden:YES];
                [cell.monthButton setHidden:YES];
                [cell.textFieldText setHidden:YES];
                
                NSTimeInterval timeInSecond = [userDetailModel.dob doubleValue]/1000;
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInSecond];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MM/dd/yyyy"];
                NSString *formattedDate=[formatter stringFromDate:date];
                
                NSLog(@"Formatted Date :---- %@",formattedDate);
                
                NSString *lDOBString = [NSString stringWithFormat:@"%@", (formattedDate) ? formattedDate : @"DOB"];
                [cell.dateButton setTitle:lDOBString forState:UIControlStateNormal];
                //[cell.dateButton setFrame:(IS_IPHONE_5) ? CGRectMake(130.0f, 10.0f, 180.0f, 30):(IS_IPHONE_4S) ? CGRectMake(130.0f, 10.0f, 180.0f, 30):CGRectMake(180.0f, 10.0f, 180.0f, 30)];
                
                [cell.dateButton addTarget:self action:@selector(getDateClicked:) forControlEvents:UIControlEventTouchUpInside];
                [cell.dateButton setTag:4506];
                
            }else if (indexPath.row == 3) {
                
                [cell.textFieldText setHidden:YES];
                [cell.dateButton setHidden:NO];
                [cell.monthButton setHidden:YES];
                [cell.yearButton setHidden:YES];
                //[cell.dateButton setFrame:(IS_IPHONE_5) ? CGRectMake(130.0f, 10.0f, 180.0f, 30):(IS_IPHONE_4S) ? CGRectMake(130.0f, 10.0f, 180.0f, 30):CGRectMake(180.0f, 10.0f, 180.0f, 30)];
               

                NSString *lGenderString = [NSString stringWithFormat:@"%@", (userDetailModel.gender) ? userDetailModel.gender : @""];
                
                [cell.dateButton setTitle:lGenderString forState:UIControlStateNormal];
                [cell.dateButton setTag:4509];
                [cell.dateButton addTarget:self action:@selector(getMaleClicked:) forControlEvents:UIControlEventTouchUpInside];
                
            }else if (indexPath.row == 4) {
                
                [cell.textFieldText setHidden:YES];
                [cell.dateButton setHidden:NO];
                [cell.monthButton setHidden:YES];
                [cell.yearButton setHidden:YES];
                
                //[cell.dateButton setFrame:(IS_IPHONE_5) ? CGRectMake(130.0f, 10.0f, 180.0f, 30):(IS_IPHONE_4S) ? CGRectMake(130.0f, 10.0f, 180.0f, 30):CGRectMake(180.0f, 10.0f, 180.0f, 30)];
                NSString *lLanguageString = [NSString stringWithFormat:@"%@", (userDetailModel.language) ? userDetailModel.language : @""];
                [cell.dateButton setTitle:lLanguageString forState:UIControlStateNormal];
                [cell.dateButton setTag:4510];

                [cell.dateButton addTarget:self action:@selector(getLanguageClicked:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                
                [cell.yearButton setHidden:YES];
                [cell.dateButton setHidden:YES];
                [cell.monthButton setHidden:YES];
                [cell.textFieldText setHidden:NO];
                [cell.textFieldText setPlaceholder:[self.labelArray objectAtIndex:indexPath.row]];
                [cell.textFieldText setTag:443343 + (indexPath.row + indexPath.section)];
                [cell.textFieldText setDelegate:(id)self];
                [cell.textFieldText setKeyboardType:UIKeyboardTypeDefault];
                
                if (cell.textFieldText.tag == 443343) {
                    [cell.textFieldText setUserInteractionEnabled:true];
                }
                else if  (cell.textFieldText.tag == 443344) {
                    [cell.textFieldText setUserInteractionEnabled:true];
                }
                
                NSString *firstAndLastName = [NSString stringWithFormat:@"%@", (indexPath.row == 0) ? userDetailModel.firstName : userDetailModel.lastName];
                [cell.textFieldText setText:firstAndLastName];
            }
            break;
            
        case 1:
            
            //[cell.labelName setText:[self.labelArray1 objectAtIndex:indexPath.row]];
            
            if (indexPath.row == 0) {
               
                [cell.textFieldText setHidden:YES];
                [cell.dateButton setHidden:NO];
                [cell.monthButton setHidden:YES];
                [cell.yearButton setHidden:YES];
                
                NSString *countryTitle = [userDetailModel.countryDictionary objectForKey:[NSString stringWithFormat:@"%@",userDetailModel.countryId]];
                self.countryString = userDetailModel.countryId;
                 [cell.dateButton setTitle:countryTitle forState:UIControlStateNormal];
                [cell.dateButton addTarget:self action:@selector(getCountryClicked:) forControlEvents:UIControlEventTouchUpInside];
                [cell.dateButton setTag:4511];
                
            }else if (indexPath.row == 1) {
                
                [cell.textFieldText setHidden:YES];
                [cell.dateButton setHidden:NO];
                [cell.monthButton setHidden:YES];
                [cell.yearButton setHidden:YES];
                
                NSString *cityTitle = [userDetailModel.cityDictionary objectForKey:[NSString stringWithFormat:@"%@",userDetailModel.cityId]];
                self.cityString = userDetailModel.cityId;
                [cell.dateButton setTitle:cityTitle forState:UIControlStateNormal];
                [cell.dateButton addTarget:self action:@selector(getCityClicked:) forControlEvents:UIControlEventTouchUpInside];
                [cell.dateButton setTag:4512];
            }else {
                
                [cell.textFieldText setHidden:NO];
                [cell.dateButton setHidden:YES];
                [cell.monthButton setHidden:YES];
                [cell.yearButton setHidden:YES];
                [cell.textFieldText setTag:4643343 + (indexPath.row + indexPath.section)];
                [cell.textFieldText setPlaceholder:[self.labelArray1 objectAtIndex:indexPath.row]];
                [cell.textFieldText setKeyboardType:(indexPath.row == 4 || indexPath.row == 5) ? UIKeyboardTypeNumberPad : UIKeyboardTypeDefault];
                [cell.textFieldText setText:[NSString stringWithFormat:@"%@", (indexPath.row == 2) ? userDetailModel.addressOne : (indexPath.row == 3) ? userDetailModel.addressTwo : (indexPath.row == 4) ? userDetailModel.zipCode : userDetailModel.contactNumber]];
                
                
                
                UIToolbar *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
                numberToolbar.barStyle = UIBarStyleDefault;
                numberToolbar.barTintColor = [UIColor colorWithRed:224.0f/255.0f green:245.0f/255.0f blue:252.0f/255.0f alpha:1.0f];
                
                numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextBtnNumberPad)],[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneBtnNumberPad)],nil];
                
                [numberToolbar sizeToFit];
                cell.textFieldText.inputAccessoryView = numberToolbar;
            }
            
            break;
        case 2:
            
            [cell.dateButton setHidden:YES];
            [cell.yearButton setHidden:YES];
            [cell.monthButton setHidden:YES];
            [cell.textFieldText setHidden:NO];
            [cell.textFieldText setKeyboardType:UIKeyboardTypeDefault];
            [cell.textFieldText setPlaceholder:[self.labelArray2 objectAtIndex:indexPath.row]];
            [cell.textFieldText setTag:40433403 + (indexPath.row + indexPath.section)];
            
            if (cell.textFieldText.tag == 40433408) {
                [cell.textFieldText setUserInteractionEnabled:false];
            }
            else if (cell.textFieldText.tag == 40433406) {
                [cell.textFieldText setUserInteractionEnabled:false];
            }
            else if (cell.textFieldText.tag == 40433407) {
                [cell.textFieldText setUserInteractionEnabled:false];
            }
            
            [cell.textFieldText setText:[NSString stringWithFormat:@"%@", (indexPath.row == 0) ? userDetailModel.nextPaymentDate : (indexPath.row == 1) ? userDetailModel.userName : (indexPath.row == 2) ? userDetailModel.accountType : userDetailModel.email]];
            
        break;
        default:
            
            break;
    }
    return cell;
}

- (void)keyboardWillShow:(NSNotification *)sender {
    
    CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    CGFloat height = UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation]) ? kbSize.height : kbSize.width;
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = [routeTable contentInset];
        edgeInsets.bottom = height;
        [routeTable setContentInset:edgeInsets];
        edgeInsets = [routeTable scrollIndicatorInsets];
        edgeInsets.bottom = height;
        [routeTable setScrollIndicatorInsets:edgeInsets];
    }];
}

- (void)keyboardWillHide:(NSNotification *)sender {
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = [routeTable contentInset];
        edgeInsets.bottom = 0;
        [routeTable setContentInset:edgeInsets];
        edgeInsets = [routeTable scrollIndicatorInsets];
        edgeInsets.bottom = 0;
        [routeTable setScrollIndicatorInsets:edgeInsets];
    }];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString * original_string = [textField.text stringByReplacingCharactersInRange:range withString:string];
    userDetailModel = [self.userDetailArray objectAtIndex:0];
    
    switch (textField.tag) {
            
        case 443343:
            [userDetailModel setFirstName:original_string];
            NSLog(@"shouldChangeCharactersInRange443343 :- %@ ", original_string);
            break;
        case 443344:
            
            NSLog(@"443344 :- %@ ", original_string);
            [userDetailModel setLastName:original_string];
            break;
        case 443345:
            
            NSLog(@"443345 :- %@ ", original_string);
            [userDetailModel setNextPaymentDate:original_string];
            break;
        case 4643346:
            
            NSLog(@"443346 :- %@ ", original_string);
            [userDetailModel setAddressOne:original_string];
            break;
        case 4643347:
            
            NSLog(@"443347 :- %@ ", original_string);
            [userDetailModel setAddressTwo:original_string];
            break;
        case 4643348:
            
            NSLog(@"443348 :- %@ ", original_string);
            [userDetailModel setZipCode:original_string];
            break;
        case 4643349:
            
            NSLog(@"443349 :- %@ ", original_string);
            [userDetailModel setContactNumber:original_string];
        case 40433405:
            
            NSLog(@"4433405 :- %@ ", original_string);
            [userDetailModel setNextPaymentDate:original_string];
        case 40433406:
            
            NSLog(@"4433406 :- %@ ", original_string);
            [userDetailModel setUserName:original_string];
            break;
        case 40433407:
            
            NSLog(@"4433407 :- %@ ", original_string);
            [userDetailModel setAccountType:original_string];
            break;
        case 40433408:
            
            NSLog(@"4433408 :- %@ ", original_string);
            //[userDetailModel setEmail:original_string];
            updatedEmail = [NSString stringWithFormat:@"%@", original_string];
            break;
        default:
            
            NSLog(@"default :- %@", original_string);
            break;
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
   
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)isContactNumberValid:(NSString *)mNumber {
    
    if ( [mNumber length] < 10)
        return NO;
    NSRange rang;
    rang = [mNumber rangeOfCharacterFromSet:[NSCharacterSet alphanumericCharacterSet]];
    if ( !rang.length )
        return NO;  // no number;
    return YES;
}

-(BOOL)isZipNumberValid:(NSString *)mNumber {
    
   
    
    if ([mNumber length] < 6)
        return NO;
    NSRange rang;
    rang = [mNumber rangeOfCharacterFromSet:[NSCharacterSet alphanumericCharacterSet]];
    if ( !rang.length )
        return NO;  // no number;
    return YES;
}

-(IBAction)getDateClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    currentbuttonTag = sender.tag;
    [viewPicker setHidden:NO];
    if (pickerRemove > 0) {
        [self.datePicker removeFromSuperview];
    }
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 216.0, [UIScreen mainScreen].bounds.size.width, 216.0)];
    [self.datePicker setBackgroundColor:[UIColor grayColor]];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    [self.datePicker setSelected:YES];
    self.datePicker.date = [NSDate date];
    [self.view addSubview:self.datePicker];
    pickerRemove++;
}

-(IBAction)getMaleClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    currentbuttonTag = sender.tag;
    [viewPicker setHidden:NO];
    if (pickerRemove > 0) {
        [self.genderPicker removeFromSuperview];
    }
    self.genderPicker = [[UIPickerView alloc]init];
    [self.genderPicker setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 216.0, [UIScreen mainScreen].bounds.size.width, 216.0)];
    [self.genderPicker setBackgroundColor:[UIColor grayColor]];
    [self.genderPicker setUserInteractionEnabled:YES];
    [self.genderPicker setDataSource:(id)self];
    [self.genderPicker setDelegate:(id)self];
    [self.view addSubview:self.genderPicker];
    pickerRemove++;
}


-(IBAction)getLanguageClicked:(UIButton *)sender {
    
    currentbuttonTag = sender.tag;
    [viewPicker setHidden:NO];
    if (pickerRemove > 0) {
        [self.languagePicker removeFromSuperview];
    }
    self.languagePicker = [[UIPickerView alloc]init];
    [self.languagePicker setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 216.0, [UIScreen mainScreen].bounds.size.width, 216.0)];
    [self.languagePicker setBackgroundColor:[UIColor grayColor]];
    [self.languagePicker setUserInteractionEnabled:YES];
    [self.languagePicker setDataSource:(id)self];
    [self.languagePicker setDelegate:(id)self];
    [self.view addSubview:self.languagePicker];
    pickerRemove++;
}

-(IBAction)getCountryClicked:(UIButton *)sender {
    
    currentbuttonTag = sender.tag;
    [viewPicker setHidden:NO];
    if (pickerRemove > 0) {
        [self.countryPicker removeFromSuperview];
    }
    self.countryPicker = [[UIPickerView alloc]init];
    [self.countryPicker setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 216.0, [UIScreen mainScreen].bounds.size.width, 216.0)];
    [self.countryPicker setBackgroundColor:[UIColor grayColor]];
    [self.countryPicker setUserInteractionEnabled:YES];
    [self.countryPicker setDataSource:(id)self];
    [self.countryPicker setDelegate:(id)self];
    [self.view addSubview:self.countryPicker];
    pickerRemove++;
}


-(IBAction)getCityClicked:(UIButton *)sender {
    
    currentbuttonTag = sender.tag;
    [viewPicker setHidden:NO];
    if (pickerRemove > 0) {
        [self.cityPicker removeFromSuperview];
    }
    self.cityPicker = [[UIPickerView alloc]init];
    [self.cityPicker setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 216.0, [UIScreen mainScreen].bounds.size.width, 216.0)];
    [self.cityPicker setBackgroundColor:[UIColor grayColor]];
    [self.cityPicker setUserInteractionEnabled:YES];
    [self.cityPicker setDataSource:(id)self];
    [self.cityPicker setDelegate:(id)self];
    [self.view addSubview:self.cityPicker];
    pickerRemove ++;
}


/*
 *******************************************************************
 *  UIPickerView delegate and data source methods..
 *******************************************************************
 */

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (currentbuttonTag == 4510) {return self.languageArray.count;}
    else if (currentbuttonTag == 4511) {return self.countryArray.count;}
    else if (currentbuttonTag == 4512) {return self.cityArray.count;}
    else {return self.genderArray.count;}
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    VSUserDetailModel *lCityModel;
    if (currentbuttonTag == 4510) {
        
        lCityModel = [self.languageArray objectAtIndex:row];
        return lCityModel.languageObject;
    }else if (currentbuttonTag == 4511) {
        
        lCityModel = [self.countryArray objectAtIndex:row];
        return lCityModel.countryObject;
    }else if (currentbuttonTag == 4512) {
        
        lCityModel = [self.cityArray objectAtIndex:row];
        return lCityModel.cityObject;
    }else {
        
        lCityModel = [self.genderArray objectAtIndex:row];
        return lCityModel.genderObject;
    }
}

-(IBAction)doneButtonClicked:(UIButton *)sender {
    
    UIButton *dateButton = (UIButton *)[self.view viewWithTag:4506];
    UIButton *genderButton = (UIButton *)[self.view viewWithTag:4509];
    UIButton *languageButton = (UIButton *)[self.view viewWithTag:4510];
    UIButton *countryButton = (UIButton *)[self.view viewWithTag:4511];
    UIButton *cityButton = (UIButton *)[self.view viewWithTag:4512];
    NSInteger row;
    userDetailModel = [self.userDetailArray objectAtIndex:0];
    
    VSUserDetailModel *lModel;
    if (currentbuttonTag == 4509) {
        
        [self.datePicker setHidden:YES];
        row = [self.genderPicker selectedRowInComponent:0];
        lModel = [self.genderArray objectAtIndex:row];
        self.genderString = [NSString stringWithFormat:@"%@", (lModel.genderKey) ? lModel.genderKey : userDetailModel.genderTypeId];
        [genderButton setTitle:lModel.genderObject forState:UIControlStateNormal];
        [userDetailModel setGender:self.genderString];
        
    }else if (currentbuttonTag == 4506) {
        
        [self.genderPicker setHidden:YES];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM/dd/yyyy"];
        [df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        self.dobString = [NSString stringWithFormat:@"%@", [df stringFromDate:self.datePicker.date]];
        
        [dateButton setTitle:self.dobString forState:UIControlStateNormal];
        
        NSTimeInterval seconds = [self.datePicker.date timeIntervalSince1970];
        double milliseconds = seconds*1000;
        
        [userDetailModel setDob:[NSString stringWithFormat:@"%f",milliseconds]];
    } else if (currentbuttonTag == 4510) {
        
        [self.datePicker setHidden:YES];
        row = [self.languagePicker selectedRowInComponent:0];
        lModel = [self.languageArray objectAtIndex:row];
        self.languageString = [NSString stringWithFormat:@"%@", (lModel.languageKey) ? lModel.languageKey : userDetailModel.genderTypeId];
        [languageButton setTitle:lModel.languageObject forState:UIControlStateNormal];
        [userDetailModel setLanguage:self.languageString];
    }else if (currentbuttonTag == 4511) {
        
        [self.datePicker setHidden:YES];
        row = [self.countryPicker selectedRowInComponent:0];
        lModel = [self.countryArray objectAtIndex:row];
        self.countryString = [NSString stringWithFormat:@"%@", (lModel.countryKey) ? lModel.countryKey : userDetailModel.countryId];
        [countryButton setTitle:lModel.countryObject forState:UIControlStateNormal];
        [userDetailModel setCountry:self.countryString];
        [self updateCityArrayFromCountryCode:lModel.countryKey];
        
    }else if (currentbuttonTag == 4512) {
        
        [self.datePicker setHidden:YES];
        row = [self.cityPicker selectedRowInComponent:0];
        lModel = [self.cityArray objectAtIndex:row];
        self.cityString = [NSString stringWithFormat:@"%@", (lModel.cityKey) ? lModel.cityKey : userDetailModel.cityId];
        [cityButton setTitle:lModel.cityObject forState:UIControlStateNormal];
        [userDetailModel setCity:self.cityString];
    }
    
    [viewPicker setHidden:YES];
    [self.datePicker setHidden:YES];
    [self.genderPicker setHidden:YES];
    [self.languagePicker setHidden:YES];
    [self.countryPicker setHidden:YES];
    [self.cityPicker setHidden:YES];
}

-(IBAction)cancel:(id)sender {
    
    [self.cityPicker setHidden:YES];
    [self.datePicker setHidden:YES];
    [self.genderPicker setHidden:YES];
    [self.countryPicker setHidden:YES];
    [self.languagePicker setHidden:YES];
    [viewPicker setHidden:YES];
}
-(void)doneBtnNumberPad {
    [self.view endEditing:YES];
}
-(void)nextBtnNumberPad {

    UITextField *textField = (UITextField *)[self.view viewWithTag:4643349];
    //UITextField *textField1 = (UITextField *)[self.view viewWithTag:40433405];
   
    [textField becomeFirstResponder];
  
}
-(IBAction)updateButtonAction:(UIButton *)sender {
    
    UIButton *genderButton = (UIButton *)[self.view viewWithTag:4509];

    userDetailModel = [self.userDetailArray objectAtIndex:0];
    NSLog(@"------user----- : %@ --- %@", userDetailModel.zipCode, userDetailModel.contactNumber);
  
    if (self.genderString.length == 0 && [genderButton.titleLabel.text isEqualToString:@"Female"]) {
        self.genderString = @"2";
    }else if (self.genderString.length == 0 && [genderButton.titleLabel.text isEqualToString:@"Male"]) {
         self.genderString = @"1";
    }else if (self.genderString.length == 0 && [genderButton.titleLabel.text isEqualToString:@"Trans Gender"]) {
        self.genderString = @"3";
    }

    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        
        if (![self isZipNumberValid:userDetailModel.zipCode]) {
            
            NSLog(@"------userDetailModel.zipCode----- : %@", userDetailModel.zipCode);
            [VSEnumClass showAlertWithTitle:nil msg:@"Please enter 6 digits zip code."];
        }else if (![self isContactNumberValid:userDetailModel.contactNumber]) {
            
            NSLog(@"------userDetailModel.contactNumber----- : %@", userDetailModel.contactNumber);
            
            [VSEnumClass showAlertWithTitle:nil msg:@"Please enter 10 digits mobile number."];
        }else {
            
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            
            [self postDataOnServer:userDetailModel.firstName LastName:userDetailModel.lastName DOB:userDetailModel.dob AddressOne:userDetailModel.addressOne AddressTwo:userDetailModel.addressTwo ContactNumber:userDetailModel.contactNumber UserID:[VSSingletonClass sharedMySingleton].loginUserId AccountTypeID:userDetailModel.accountType UserName:userDetailModel.userName GenderTypeID:self.genderString Email:(updatedEmail) ? updatedEmail : userDetailModel.email Password:@"" CityID:self.cityString CountryID:self.countryString ZipCode:userDetailModel.zipCode  Language:userDetailModel.languageId LocationId:userDetailModel.locationId PreviousEmail:userDetailModel.email];
        }
    }else {
        
        [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

-(BOOL)validateEmail:(NSString*)email {
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if (![emailTest evaluateWithObject:email]) {
        return NO;
    }
    return YES;
}
-(void)updateCityArrayFromCountryCode:(NSString *)countryCode{
    
     [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
    [[VSParserClass sharedParser] valShareGetCityList:countryCode WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSLog(@"resultObjectresultObject:-%@",resultObject);
        if(result){
            
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Failure"]) {
                [SVProgressHUD dismiss];
                
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }else if ([[resultObject valueForKey:@"status"]isEqualToString:@"Exception"]) {
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                [SVProgressHUD dismiss];
                
            }
            else {
                if ([resultObject isKindOfClass:[NSDictionary class]]) {
                    
                    NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                    
                     NSDictionary *cityDictionary = [dictdata objectForKey:@"cityDictionary"];
                    [self.cityArray removeAllObjects];
                    [cityDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                        
                        
                        VSUserDetailModel *lCityModel = [[VSUserDetailModel alloc] init];
                        [lCityModel setCityKey:key];
                        [lCityModel setCityObject:obj];
                        [self.cityArray addObject:lCityModel];
                    }];
                    NSLog(@"CityList:->%@",self.cityArray);
                    [self.cityPicker reloadAllComponents];
                    [SVProgressHUD dismiss];
                    UIButton *cityButton = (UIButton *)[self.view viewWithTag:4512];
                   VSUserDetailModel *lModel = [self.cityArray objectAtIndex:0];
                    self.cityString = [NSString stringWithFormat:@"%@", (lModel.cityKey) ? lModel.cityKey : userDetailModel.cityId];
                    [cityButton setTitle:lModel.cityObject forState:UIControlStateNormal];
                    [userDetailModel setCity:self.cityString];
                    
                }
            }
        }else{
            [SVProgressHUD dismiss];
        }
    }];

    
    
    
}
@end
