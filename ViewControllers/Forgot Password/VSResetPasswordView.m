//
//  VSResetPasswordView.m
//  ValShare
//
//  Created by Vivek Kumar on 4/9/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSResetPasswordView.h"

@interface VSResetPasswordView ()

@end

@implementation VSResetPasswordView

-(void)postResetPassword:(NSString *)newPassword UserID:(NSString *)userid CurrentPassword:(NSString *)currentpassword {
    
    NSDictionary *dictData = [[NSMutableDictionary alloc] init];

    [dictData setValue:newPassword forKey:@"newPassword"];
    [dictData setValue:[NSString stringWithFormat:@"%d",[userid intValue]] forKey:@"userId"];
    [dictData setValue:currentpassword forKey:@"currentPassword"];
    
    [[VSParserClass sharedParser] valShareResetPassword:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSLog(@"valS1111hareResetPasswordvalShareResetPassword :- %@", resultObject);
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Ok"]) {
                
                   [[[UIAlertView alloc] initWithTitle:nil message:@"Your password is change successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }else{
                
                NSMutableDictionary *resetDict = (NSMutableDictionary *)resultObject;
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [resetDict valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            [SVProgressHUD dismiss];
        }else {
            
            [SVProgressHUD dismiss];
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationItem setTitle:@"Reset Password"];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    
    [self addComponentOnView];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addComponentOnView {
    
    NSArray *textArray = @[@"Current Password",@"New Password",@"Conform New Password"];
    for (int i = 0; i  < 3 ; i++) {
        
        [self addTextFieldOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kPasswordBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 25.0f + (i * (kPasswordBG.size.height + 10)), kPasswordBG.size.width, kEmail_BG.size.height) Title:textArray[i] Tag:1100 + i];
    }
    
    [self addSubmitButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 193, kLoginBG.size.width, kEmail_BG.size.height)  Title:@"Submit" Tag:8899 BGImage:kLoginBG];
}


-(void)addTextFieldOnView:(CGRect)frame Title:(NSString *)title Tag:(int)tag {
    
    UITextField *textField = [[UITextField alloc]init];
    [textField setTag:tag];
    [textField setFrame:frame];
    [textField setDelegate:(id)self];
    [textField setPlaceholder:title];
    [textField setSecureTextEntry:YES];
    [textField.layer setBorderWidth:0.5];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setFont:[UIFont italicSystemFontOfSize:15.0f]];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    [self.view addSubview:textField];
}


-(void)addSubmitButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(submitButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

//***** Use of UiTextField delegate *****

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}


-(IBAction)submitButtonAction:(UIButton *)sender {
    
    UITextField *currentPasswrd = (UITextField *)[self.view viewWithTag:1100];
    UITextField *newPassword = (UITextField *)[self.view viewWithTag:1101];
    UITextField *conformNewPassWord = (UITextField *)[self.view viewWithTag:1102];
    
    
    if (newPassword.text.length == 0) {

        [[[UIAlertView alloc]initWithTitle:nil message:@"Please enter new password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }
    else if (newPassword.text.length <8 || newPassword.text.length >16) {
        [VSEnumClass showAlertWithTitle:@"password should be 8-12 characters long"];
    }
    else if (![conformNewPassWord.text isEqualToString:newPassword.text]) {
        
        [[[UIAlertView alloc] initWithTitle:nil message:@"Please conform new password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    } else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self postResetPassword:newPassword.text UserID:[VSSingletonClass sharedMySingleton].loginUserId CurrentPassword:currentPasswrd.text];
        }else {
            
            [[[UIAlertView alloc] initWithTitle:nil message:@"Please check internet connection.q" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }
}

@end
