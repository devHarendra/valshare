//
//  VSForgotView.m
//  ValShare
//
//  Created by Sharda Prasad on 3/30/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSForgotView.h"

@implementation VSForgotView


-(void)forgotPasswordService:(NSString *)email {
    
    [[VSParserClass sharedParser] forgotPassword:email WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSLog(@"forgotPassword: :- %@", resultObject);
        if (result) {
            NSMutableDictionary *forgotDict = (NSMutableDictionary *)resultObject;
            if ([[forgotDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                [self  navigationToVarificationScreen:[forgotDict valueForKey:@"userId"]];
            } else {
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [forgotDict valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
            
            [SVProgressHUD dismiss];
        }

        else {
            [SVProgressHUD dismiss];

            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}


-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = [NSString stringWithFormat:@"%@", @"Forgot password"];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationItem.leftBarButtonItem = [self setLeftBarButton];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self addComponentOnView];
}

#pragma mark - Set Navigation Bar Left Button

- (UIBarButtonItem *)setLeftBarButton
{
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, kBackButtonBG.size.width, kBackButtonBG.size.height);
    [btnSettings setImage:kBackButtonBG forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
    
    return barBtnItem;
}

#pragma mark - Navigation Bar Left Button Selector
- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addComponentOnView {
    
    NSString *placeHolder = [NSString stringWithFormat:@"%@", @"Please enter email id."];

    [self addTextFieldOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kEmail_BG.size.width/2, [UIScreen mainScreen].bounds.origin.y + (kEmail_BG.size.height + 10), kEmail_BG.size.width, kEmail_BG.size.height) Title:placeHolder Tag:45340 BGImage:nil SecureText:YES KeyBoardType:UIKeyboardTypeEmailAddress];
    
    [self addButtonOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kRegisterButtonBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 85, kRegisterButtonBG.size.width, kRegisterButtonBG.size.height) BGImage:kRegisterButtonBG Tag:88781 Title:@"Submit"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTextFieldOnView:(CGRect)frame Title:(NSString *)placeHolder Tag:(int)tag BGImage:(UIImage *)bgImage SecureText:(Boolean)secureText KeyBoardType:(UIKeyboardType)kType{
    
    VSTextField *textField = [[VSTextField alloc] init];
    [textField setTag:tag];
    [textField setFrame:frame];
    [textField setDelegate:(id)self];
    [textField setSecureTextEntry:NO];
    [textField setBackground:bgImage];
    [textField setKeyboardType:kType];
    [textField setPlaceholder:placeHolder];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentCenter];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setFont:[UIFont italicSystemFontOfSize:14.0f]];
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 5, 5)];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    [textField setRightView:rightView];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 5, 5)];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    [textField setLeftView:leftView];
    
    [self.view addSubview:textField];
    
}

-(void)addButtonOnView:(CGRect)frame BGImage:(UIImage *)bgImage Tag:(int)tag Title:(NSString *)title {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button setEnabled:YES];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont italicSystemFontOfSize:15.0f]];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(forgotButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

//**************Use of textFieldDeligate ***************//

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [textField becomeFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}

-(IBAction)forgotButtonAction:(VSButton *)button {
    
    VSTextField *textField = (VSTextField *)[self.view viewWithTag:45340];
    
    if (![self validateEmail11:textField.text]) {
        
        [[[UIAlertView alloc] initWithTitle:nil message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    } else {
        
        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
            [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
            [self forgotPasswordService:textField.text];

        }else {
            
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertview show];
        }
    }
}

-(void)navigationToVarificationScreen:(NSString *)userID
{
    VSUserVarification *userVarification = [[VSUserVarification alloc] init];
    [userVarification setUserID:userID];
    [userVarification setLoginStatus:@"varification"];
    [self.navigationController pushViewController:userVarification animated:YES];
}


-(BOOL)validateEmail11:(NSString*)email {
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if (![emailTest evaluateWithObject:email]) {
        return NO;
    }
    return YES;
}
@end
