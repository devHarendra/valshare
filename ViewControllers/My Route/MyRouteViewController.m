//
//  MyRouteViewController.m
//  VolShare
//
//  Created by Vivek Kumar on 4/3/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import "MyRouteViewController.h"
#import "VSHeader.h"

@interface MyRouteViewController ()

@end
@implementation MyRouteViewController
@synthesize dateArray = _dateArray;

-(NSMutableArray *)dateArray {
    
    if (!_dateArray) {
        _dateArray = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5", nil];
    }
    return _dateArray;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationItem setTitle:@"My Route"];
    self.navigationItem.rightBarButtonItem = nil;
    
    [self addTableOnView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-70):CGRectMake([UIScreen mainScreen].bounds.origin.x, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height) Tag:2450];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIBarButtonItem *)setLeftBarButton {
    
    UIButton * btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSettings.frame = CGRectMake(0, 0, [UIImage imageNamed:@"navbar_btn_back@2x.png"].size.width+10, [UIImage imageNamed:@"navbar_btn_back@2x.png"].size.height+10);
    [btnSettings setImage:[UIImage imageNamed:@"navbar_btn_back@2x.png"] forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(btnLeftBarClicked) forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
}

- (void)btnLeftBarClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addTableOnView:(CGRect)frame Tag:(int)tag {
    
    UITableView *routeTable = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
    [routeTable setBackgroundColor:[UIColor clearColor]];
    [routeTable setTag:tag];
    [routeTable setDelegate:(id)self];
    [routeTable setDataSource:(id)self];
    [routeTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [self.view addSubview:routeTable];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dateArray count];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.dateArray removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    MyRouteTableCell *cell = (MyRouteTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[MyRouteTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [cell.effectiveTo setHidden:YES];
    [cell.purchesStatus setHidden:YES];
    [cell.packagePrice setHidden:YES];
    [cell.loyalityPoint setHidden:YES];
    [cell.finalEffectiveTo setHidden:YES];
    [cell.finalPackagePrice setHidden:YES];
    [cell.finalPurchesStatus setHidden:YES];
    [cell.finalLoyalityPoint setHidden:YES];
    
    [cell.myId setText:@"id"];
    [cell.source setText:@"Source"];
    [cell.destination setText:@"Destination"];
    [cell.date setText:@"Date"];
    
    [cell.myFinalId setText:@":Route0"];
    [cell.finalSource setText:@":Nashik,Delhi,India"];
    [cell.finalDestination setText:@":Pune,Delhi,India"];
    [cell.finalDate setText:@":02/04/2014 14:33:25"];
    
    return cell;
}
@end
