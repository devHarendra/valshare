//
//  ViewController.h
//  ValShare
//
//  Created by Sharda Prasad on 3/30/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSHeader.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <Accounts/Accounts.h>

@interface ViewController : UIViewController <QRadioButtonDelegate>


@end

