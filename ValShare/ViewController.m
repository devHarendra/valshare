//
//  ViewController.m
//  ValShare
//
//  Created by Sharda Prasad on 3/30/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "ViewController.h"
#import "VSSignUpView.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>

static NSString * const kClientId = @"483034031129-0evmgu8aj3td2nopeo521v1eefae951m.apps.googleusercontent.com";




@interface ViewController ()<GPPSignInDelegate> {
    
    Boolean userNameOrEmailLogin;
}

@end

GPPSignIn *signIn;


@implementation ViewController

-(void)signInWithUserName:(NSString *)userName Password:(NSString *)password GenderTypeId:(NSString *)genderTypeId DOB:(NSString *)DOB AccountTypeId:(NSString *)accountTypeId userId:(NSString *)userId roleTypeId:(NSString *)roleTypeId {
    
    NSDictionary *dictData = [[NSMutableDictionary alloc] init];
    [dictData setValue:userName forKey:@"userName"];
    [dictData setValue:password forKey:@"password"];
    //[dictData setValue:@"0" forKey:@"DOB"];
    //[dictData setValue:@"0" forKey:@"userId"];
    //[dictData setValue:@"0" forKey:@"accountTypeId"];
    //[dictData setValue:@"0" forKey:@"roleTypeId"];
    
    [[VSParserClass sharedParser] valShareUserNameLogin:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            NSLog(@"user Name :- %@", resultObject);
            
            NSMutableDictionary *loginDict = (NSMutableDictionary *)resultObject;
            if ([[loginDict valueForKey:@"status"] isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [loginDict valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else {
                
                if ([[loginDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                    
                    if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Verification Pending"]) {
                        [self navigateToVarificationView:[loginDict valueForKey:@"userId"]];
                        
                    } else if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Add Vehicle"]) {
                        [self navigateToAddVehicleView:[loginDict valueForKey:@"userId"]];
                    }  else if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Add CC Detail"]) {
                        [self navigateToMapView:[loginDict valueForKey:@"userId"]];
                    }else {
                        [self navigateToMapView:[loginDict valueForKey:@"userId"]];
                    }
                }
            }
            [SVProgressHUD dismiss];
        } else {
            
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            [SVProgressHUD dismiss];
        }
    }];
}

-(void)signInWithEmail:(NSString *)email Password:(NSString *)password GenderTypeId:(NSString *)genderTypeId DOB:(NSString *)DOB AccountTypeId:(NSString *)accountTypeId userId:(NSString *)userId roleTypeId:(NSString *)roleTypeId {
    
    NSDictionary *dictData = [[NSMutableDictionary alloc] init];
    [dictData setValue:email forKey:@"email"];
    [dictData setValue:password forKey:@"password"];
    //[dictData setValue:@"0" forKey:@"DOB"];
    //[dictData setValue:@"0" forKey:@"genderTypeId"];
    //[dictData setValue:@"0" forKey:@"userId"];
    //[dictData setValue:accountTypeId forKey:@"accountTypeId"];
    //[dictData setValue:@"0" forKey:@"roleTypeId"];
    
    [[VSParserClass sharedParser] valShareUserEmailLogin:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSLog(@"valShareUserEmailLogin:-%@",resultObject );
        
        if (result) {
            NSMutableDictionary *loginDict = (NSMutableDictionary *)resultObject;
            
            if ([[loginDict valueForKey:@"status"] isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [loginDict valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else {
                
                if ([[loginDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                    
                    if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Verification Pending"]) {
                        [self navigateToVarificationView:[loginDict valueForKey:@"userId"]];
                        
                    } else if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Add Vehicle"]) {
                        [self navigateToAddVehicleView:[loginDict valueForKey:@"userId"]];
                    } else if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Add CC Detail"]) {
                        [self navigateToMapView:[loginDict valueForKey:@"userId"]];
                    }else {
                        [self navigateToMapView:[loginDict valueForKey:@"userId"]];
                    }
                }
            }
            [SVProgressHUD dismiss];
            
        } else {
            
            [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            [SVProgressHUD dismiss];
        }
    }];
}






-(void)loginWithFaceBookServise:(id)userDicFB {
    
    NSString*email= [userDicFB objectForKey:@"email"];
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
    [dictData setObject:email forKey:@"email"];
    [[VSParserClass sharedParser] valShareFaceBookLogin:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        id string = [[NSString alloc] initWithData:resultObject encoding:NSUTF8StringEncoding];
        NSMutableDictionary *loginDict = [[VSConvertToJSON alloc] convertToJSONObject:string];
        if (result) {
            
            if ([[loginDict valueForKey:@"status"] isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [resultObject valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else if ([[loginDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                    [SVProgressHUD dismiss];
                    if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Verification Pending"]) {
                        [self navigateToAddVehicleView:[loginDict valueForKey:@"userId"]];
                    } else if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Add Vehicle"]) {
                        [self navigateToAddVehicleView:[loginDict valueForKey:@"userId"]];
                    } else if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Add CC Detail"]) {
                        [self navigateToMapView:[loginDict valueForKey:@"userId"]];
                    }else {
                        [self navigateToMapView:[loginDict valueForKey:@"userId"]];
                    }
            }
            
            else if ([[loginDict valueForKey:@"status"] isEqualToString:@"New user"])
            {
                VSSignUpView *signUpVC = [[VSSignUpView alloc] init];
                signUpVC.fbDic = userDicFB;
                [self.navigationController pushViewController:signUpVC animated:YES];

                
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"New registration" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [SVProgressHUD dismiss];

            }
            
        }else {
            
            [[[UIAlertView alloc] initWithTitle:nil message:@"Server error please contact to admin." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            [SVProgressHUD dismiss];
        }
    }];
}

-(void)loginWithGmailServise:(NSString *)gmail {
    
//    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
//    [dictData setObject:gmail forKey:@"email"];
//    [[VSParserClass sharedParser] valShareGmailLogin:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
    
        
    [[VSParserClass sharedParser] loginUsingGmail:gmail Token:nil WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {

        NSLog(@"gmILloginResponse :- %@", resultObject);

        
//        id string = [[NSString alloc] initWithData:resultObject encoding:NSUTF8StringEncoding];
//        NSMutableDictionary *loginDict = [[VSConvertToJSON alloc] convertToJSONObject:string];
        if (result) {
            
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [resultObject valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                [SVProgressHUD dismiss];
                if ([[resultObject valueForKey:@"landingPage"] isEqualToString:@"Verification Pending"]) {
                    [self navigateToAddVehicleView:[resultObject valueForKey:@"userId"]];
                } else if ([[resultObject valueForKey:@"landingPage"] isEqualToString:@"Add Vehicle"]) {
                    [self navigateToAddVehicleView:[resultObject valueForKey:@"userId"]];
                } else if ([[resultObject valueForKey:@"landingPage"] isEqualToString:@"Add CC Detail"]) {
                    [self navigateToMapView:[resultObject valueForKey:@"userId"]];
                }else {
                    [self navigateToMapView:[resultObject valueForKey:@"userId"]];
                }
            }
        }else {
            
            [[[UIAlertView alloc] initWithTitle:nil message:@"Server error please contact to admin." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            [SVProgressHUD dismiss];
        }
    }];
}


-(void)loginWithTwitterServise:(NSString *)twitterEmail {
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
    [dictData setObject:twitterEmail forKey:@"email"];
    [[VSParserClass sharedParser] valShareFaceBookLogin:dictData WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        if (result) {
            
            if ([[resultObject valueForKey:@"status"] isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@", [resultObject valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            } else {
                
                id string = [[NSString alloc] initWithData:resultObject encoding:NSUTF8StringEncoding];
                NSMutableDictionary *loginDict = [[VSConvertToJSON alloc] convertToJSONObject:string];
                
                if ([[loginDict valueForKey:@"status"] isEqualToString:@"Ok"]) {
                    
                    if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Verification Pending"]) {
                        [self navigateToAddVehicleView:[loginDict valueForKey:@"userId"]];
                    } else if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Add Vehicle"]) {
                        [self navigateToAddVehicleView:[loginDict valueForKey:@"userId"]];
                    } else if ([[loginDict valueForKey:@"landingPage"] isEqualToString:@"Add CC Detail"]) {
                        [self navigateToMapView:[loginDict valueForKey:@"userId"]];
                    }else {
                        [self navigateToMapView:[loginDict valueForKey:@"userId"]];
                    }
                }
            }
        }else {
            
            [[[UIAlertView alloc] initWithTitle:nil message:@"Server error please contact to admin." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    //if ([[VSSingletonClass sharedMySingleton].loginUserId intValue] > 0) {
    //    [self navigateToMapView:[VSSingletonClass sharedMySingleton].loginUserId];
    //}
    
//    VSCustomAddTicketView *customAddTiket = [[VSCustomAddTicketView alloc]init];
//    [self presentPopupViewController:customAddTiket animationType:MJPopupViewAnimationSlideLeftLeft];
}

-(void)navigateToMapView:(NSString *)userId {
    
    [[VSSingletonClass sharedMySingleton] setLoginUserId:userId];
    
    VSMapController *mapViewVC = [[VSMapController alloc] init];
    [mapViewVC setUserID:userId];
    [self.navigationController pushViewController:mapViewVC animated:YES];
}

-(void)navigateToVarificationView:(NSString *)userID {
    
    VSUserVarification *viewController = [[VSUserVarification alloc] init];
    [viewController setUserID:userID];
    [viewController setLoginStatus:@"varification"];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)navigateToAddVehicleView:(NSString *)userID {
    
    VSVehicleInfoViewController *addVehicleVC = [[VSVehicleInfoViewController alloc] init];
    [addVehicleVC setVCTitle:@"add"];
    [addVehicleVC setUserID:userID];
    [addVehicleVC setLoginStatus:@"varification"];
    [self.navigationController pushViewController:addVehicleVC animated:YES];
}

-(void)navigateToAddCCDetailsView:(NSString *)userID {
    
    VSCardDetailViewController *addCardVC = [[VSCardDetailViewController alloc] init];
    [addCardVC setFirstLogin:@"varification"];
    [self.navigationController pushViewController:addCardVC animated:YES];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
        
    [self.navigationController setNavigationBarHidden:YES];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self addComponentOnView];
    
    UITapGestureRecognizer *ontap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Dismisskeys)];
    ontap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:ontap];
}

-(void) Dismisskeys
{
    [self.view endEditing:YES];
}

-(void)addComponentOnView {
    
    
    NSArray *bgImage = @[kTwitterBG,kFacebookBG,kGoogleBG];
    NSArray *textFieldBG = @[kUserNameBG,kPasswordBG];
    
    NSArray *titleText = @[@"Login with Twitter",@"Login with Facebook",@"Login with Google+"];
    NSArray *placeHolderText = @[@"Email",@"Password"];
    NSArray *forgotText = @[@"Forgot Password?",@"New here Sign up"];
    
    [self addImageViewOnView:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kValLogoBG.size.width/2, [UIScreen mainScreen].bounds.origin.y + 40, kValLogoBG.size.width, kValLogoBG.size.height) Tag:3234 Image:kValLogoBG];
    //[self addImageViewOnView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kOrBG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 - 20, kOrBG.size.width, kOrBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kOrBG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 - 50, kOrBG.size.width, kOrBG.size.height) Tag:3234 Image:kOrBG];
    
    [self addRadioButtonForEmail:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 110, [UIScreen mainScreen].bounds.size.height/2 - 110, kRadioButtonBG.size.width + 100, kRadioButtonBG.size.height) : (IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 110, [UIScreen mainScreen].bounds.origin.y + 90 + (kValLogoBG.size.height + 15), kRadioButtonBG.size.width + 100, kRadioButtonBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 110, [UIScreen mainScreen].bounds.origin.y + 40 + (kValLogoBG.size.height + 15), kRadioButtonBG.size.width + 100, kRadioButtonBG.size.height) Tag:1001];
    
    [self addRadioButtonForUserName:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 + 30, [UIScreen mainScreen].bounds.size.height/2 - 110, kRadioButtonBG.size.width + 100, kRadioButtonBG.size.height):(IS_IPHONE6) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 + 30, [UIScreen mainScreen].bounds.origin.y + 90 + (kValLogoBG.size.height + 15), kRadioButtonBG.size.width + 100, kRadioButtonBG.size.height):CGRectMake([UIScreen mainScreen].bounds.size.width/2 + 30, [UIScreen mainScreen].bounds.origin.y + 40 + (kValLogoBG.size.height + 15), kRadioButtonBG.size.width + 100, kRadioButtonBG.size.height) Tag:1002];
    
    for (int i = 0; i<= 1; i++) {
        
        [self addTextFieldOnView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kFacebookBG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 - 85 + ((kFacebookBG.size.height + 10) * i), kFacebookBG.size.width, kFacebookBG.size.height - 5):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kFacebookBG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 - 130 + ((kFacebookBG.size.height + 10) * i), kFacebookBG.size.width, kFacebookBG.size.height)  Title:placeHolderText[i] Tag:45343 + i BGImage:textFieldBG[i] SecureText:(i == 0) ? NO : YES];
    }
    
    [self addButtonOnView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 + 15, kLoginBG.size.width, kLoginBG.size.height) : CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kLoginBG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 - 25, kLoginBG.size.width, kLoginBG.size.height) Title:@"Login" Tag:96756 BGImage:kLoginBG];
    
    for (int j = 0; j<= 2; j++) {
        
        [self addButtonOnView:(IS_IPHONE_4S) ? CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kFacebookBG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 + 75+ ((kFacebookBG.size.height + 10) * j), kFacebookBG.size.width, kFacebookBG.size.height - 5) : CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kFacebookBG.size.width/2, [UIScreen mainScreen].bounds.size.height/2 + 40 + ((kFacebookBG.size.height + 10) * j), kFacebookBG.size.width, kFacebookBG.size.height) Title:titleText[j] Tag:67567 + j BGImage:bgImage[j]];
    }
    
    for (int l = 0; l< 2; l++) {
        
        [self addSignUpAndForgotPassword:(IS_IPHONE_4S) ?CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kFacebookBG.size.width/2 + (195 * l), [UIScreen mainScreen].bounds.size.height/2 + 180, 120, 20):CGRectMake([UIScreen mainScreen].bounds.size.width/2 - kFacebookBG.size.width/2 + (195 * l), [UIScreen mainScreen].bounds.size.height/2 + 200, 120, 20) Title:forgotText[l] Tag:12032 + l];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addImageViewOnView:(CGRect)frame Tag:(int)tag Image:(UIImage *)image {
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setTag:tag];
    [imageView setImage:image];
    [imageView setFrame:frame];
    [self.view addSubview:imageView];
}

-(void)addRadioButtonForEmail:(CGRect)frame Tag:(int)tag {
    
    QRadioButton *rEmail = [[QRadioButton alloc] initWithDelegate:self groupId:@"groupId2"];
    rEmail.frame = frame;
    rEmail.tag = tag;
    [rEmail setTitle:@"Email" forState:UIControlStateNormal];
    [rEmail.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [rEmail setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:rEmail];
    [rEmail setChecked:YES];
}

-(void)addRadioButtonForUserName:(CGRect)frame Tag:(int)tag {
    
    QRadioButton *rUserName = [[QRadioButton alloc] initWithDelegate:self groupId:@"groupId2"];
    [rUserName setFrame:frame];
    [rUserName setTag:tag];
    [rUserName setTitle:@"Username" forState:UIControlStateNormal];
    [rUserName setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rUserName.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [self.view addSubview:rUserName];
}


-(void)addButtonOnView:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag BGImage:(UIImage *)bgImage{
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(loginButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}


-(void)addTextFieldOnView:(CGRect)frame Title:(NSString *)placeHolder Tag:(int)tag BGImage:(UIImage *)bgImage SecureText:(Boolean)secureText {
    
    VSTextField *textField = [[VSTextField alloc] init];
    [textField setTag:tag];
    [textField setFrame:frame];
    [textField setDelegate:(id)self];
    [textField setSecureTextEntry:NO];
    [textField setBackground:bgImage];
    [textField setPlaceholder:placeHolder];
    [textField setSecureTextEntry:secureText];
    [textField setTextColor:[UIColor blackColor]];
    [textField setTextAlignment:NSTextAlignmentLeft];
    [textField setKeyboardType:UIKeyboardTypeDefault];
    [textField setFont:[UIFont systemFontOfSize:15.0f]];
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 47, 47)];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    [textField setLeftView:leftView];
    [self.view addSubview:textField];
}


-(void)addSignUpAndForgotPassword:(CGRect)frame Title:(NSString *)titleText Tag:(int)tag {
    
    VSButton *button = [[VSButton alloc] init];
    [button setTag:tag];
    [button setFrame:frame];
    [button setTitle:titleText forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0f]];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    button.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [button addTarget:self action:@selector(signUpAndForgotPasswordAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

#pragma mark - Helpers

- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId {
    
    userNameOrEmailLogin = ([radio.titleLabel.text isEqualToString:@"Email"]) ? YES : NO;
    VSTextField *email = (VSTextField *)[self.view viewWithTag:45343];
    if ([radio.titleLabel.text isEqualToString:@"Email"]) {
        [email setPlaceholder:@"Email"];
    } else {
        [email setPlaceholder:@"User Name"];
    }
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    
//    if(range.length + range.location > textField.text.length) { return NO; }
//    
//    NSUInteger newLength = [textField.text length] + [string length] - range.length;
//    
//    if (textField.tag == 45343) {
//        
//        if ([textField.text length] == 1) {
//        }
//        return newLength <= 30;
//    } else {
//        return newLength <= 20;
//    }
//    return NO;
//}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
//    [self animateTextFieldAndView:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
//    [self animateTextFieldAndView:textField up:NO];
}

- (void)animateTextFieldAndView: (UITextField*) textField up:(BOOL) up {
    
    const int movementDistance = 80;
    const float movementDuration = 0.3f;
    
    int movement = (up ? - movementDistance : movementDistance);
    
    [UIView beginAnimations:@"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration:movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    //[self animateTextFieldAndView:textField up:NO];
    
    return YES;
}

-(void)addAnimationOnTextField:(float)cSet {
    
    [UIView animateWithDuration:.5
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self.view setFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, cSet)];
                     }
                     completion:nil];
}

-(IBAction)loginButtonAction:(VSButton *)sender {
    
    if (sender.tag == 67567) {
         [self loginWithTwitter];
    } else if (sender.tag == 67568) {
        [self loginWithFaceBook];
    }else if (sender.tag == 67569) {
        [self loginWithGoogle];
    } else {

        VSTextField *email = (VSTextField *)[self.view viewWithTag:45343];
        VSTextField *password = (VSTextField *)[self.view viewWithTag:45344];
//        email.text = @"harvanshkumar801269@gmail.com";
//        email.text = @"harendra.sharma@search-value.com";
//        password.text = @"123456789";

        if ([password.text length] == 0){
            
            [[[UIAlertView alloc] initWithTitle:nil message:@"Please enter username/email or password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }else {

            switch (userNameOrEmailLogin) {
                    
                case 0:
                    
                    if ([email.text length] == 0) {
                        
                        [[[UIAlertView alloc] initWithTitle:nil message:@"Please enter valid username/email." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
                    } else {
                        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
                        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                            
                            [self signInWithUserName:email.text Password:password.text GenderTypeId:@"0" DOB:@"0" AccountTypeId:@"0" userId:@"0" roleTypeId:@"0"];
                        }else {
                            
                            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            
                            [alertview show];
                        }
                    }
                    break;
                case 1:
                    if (![self validateEmail:email.text]) {
                        
                        [[[UIAlertView alloc] initWithTitle:nil message:@"Please enter valid email." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                    } else {
                        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
                        if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                            [self signInWithEmail:email.text Password:password.text GenderTypeId:@"0" DOB:@"0" AccountTypeId:@"0" userId:@"0" roleTypeId:@"0"];
                        }else {
                            
                            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alertview show];
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

-(void)loginWithTwitter {
    
// #warning message Tempary used.
//    return;
//    [[Twitter sharedInstance] startWithConsumerKey:@"VJASIkR7qDKvdZwiK7wcNO9hy" consumerSecret:@"WNZTwZlkZ3W0XojGslRuUb5E1X6CZtKhOv3jvBTYKKzYgOHd9x"];
//    [Fabric with:@[[Twitter sharedInstance]]];
//    
//    [[Twitter sharedInstance] logInWithCompletion:^
//     (TWTRSession *session, NSError *error) {
//         if (session) {
//             NSLog(@"signed in as %@", [session userName]);
//         } else {
//             NSLog(@"error: %@", [error localizedDescription]);
//         }
//     }];
    
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        
        if (session) {
            
            NSLog(@"session And UserName :- %@ -- %@", session.userID, session.userName);
            
            TWTRShareEmailViewController* shareEmailViewController = [[TWTRShareEmailViewController alloc] initWithCompletion:^(NSString* email, NSError* error) {
                if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                    
                    NSLog(@"twitter email %@",email);
                    [self usersShow:session.userID];
//                    [self loginWithTwitterServise:(email) ? email : session.userName];
                }else {
                    [[[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                }
            }];
            
            [self presentViewController:shareEmailViewController
                               animated:YES
                             completion:nil];
        } else {
            
        }
    }];
}

-(void)usersShow:(NSString *)userID
{
    NSString *statusesShowEndpoint = @"https://api.twitter.com/1.1/users/show.json";
    NSDictionary *params = @{@"user_id": userID};
    
    NSError *clientError;
    NSURLRequest *request = [[[Twitter sharedInstance] APIClient]
                             URLRequestWithMethod:@"GET"
                             URL:statusesShowEndpoint
                             parameters:params
                             error:&clientError];
    
    if (request) {
        [[[Twitter sharedInstance] APIClient]
         sendTwitterRequest:request
         completion:^(NSURLResponse *response,
                      NSData *data,
                      NSError *connectionError) {
             if (data) {
                 // handle the response data e.g.
                 NSError *jsonError;
                 NSDictionary *json = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:0
                                       error:&jsonError];
                 NSLog(@"%@ twittertwittertwittertwitter",[json description]);
             }
             else {
                 NSLog(@"Error code: %ld | Error description: %@", (long)[connectionError code], [connectionError localizedDescription]);
             }
         }];
    }
    else {
        NSLog(@"Error: %@", clientError);
    }
}


-(void)loginWithFaceBook {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    //if (appDelegate.session.state != FBSessionStateCreated) {
    appDelegate.session = [[FBSession alloc] init];
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"email",@"user_about_me",@"offline_access",@"user_birthday",@"user_photos", @"user_location",@"read_friendlists",nil];
    //[facebook authorize:permissions];
    
    id temp = [appDelegate.session initWithPermissions:permissions];
    if(temp) {
        
    }
    //}
    
    [FBSession setActiveSession:appDelegate.session];
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        //[self pushToVitalsHomeViewController];
        
        FBSession.activeSession = session;
        [FBSession setActiveSession:session];
        [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id<FBGraphUser> user, NSError *error) {
            
            NSLog(@"useruser :- %@", user);
            
            if (!error) {
                
                if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                    [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
                    [self loginWithFaceBookServise:user];
                    
                }else {
                    
                    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertview show];
                }
            }
        }];
    }];
}
-(void)loginWithGoogle
{
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    signIn.clientID = kClientId;
    signIn.scopes = @[kGTLAuthScopePlusLogin];           // "profile" scope
    signIn.delegate = self;
    [signIn authenticate];
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    
    NSLog(@"Received error %@ and auth object %@", error, auth);
    if (error) {
        // Do some error handling here.
    } else {
        
        GTLServicePlus* plusService = [[GTLServicePlus alloc] init] ;
        plusService.retryEnabled = YES;
        [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
        
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error)
         {
             if (error){
                 
                 GTMLoggerError(@"Error: %@", error);
             }
             else{
                 
                 //encrypt the id for security purpose
                 
                 // NSString *userID = [person.JSON valueForKey:@"id"];
                 
                 //  NSLog(@"%@",signIn.authentication.userEmail);
                 
                 if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                     [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
                     [self loginWithGmailServise:signIn.authentication.userEmail];
                     
                 }else {
                     
                     UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alertview show];
                 }
                 
             }
         }];
    }
}




-(BOOL)validateEmail:(NSString*)email {
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if (![emailTest evaluateWithObject:email]) {
        return NO;
    }
    return YES;
}

-(IBAction)signUpAndForgotPasswordAction:(VSButton *)sender {
    
    if (sender.tag == 12032) {
        
        VSForgotView *forgotVC = [[VSForgotView alloc] init];
        [self.navigationController pushViewController:forgotVC animated:YES];
    } else {
        
        VSSignUpView *signUpVC = [[VSSignUpView alloc] init];
        [self.navigationController pushViewController:signUpVC animated:YES];
    }
}

@end
