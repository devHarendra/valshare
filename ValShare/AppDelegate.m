//
//  AppDelegate.m
//  ValShare
//
//  Created by Sharda Prasad on 3/30/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import <GooglePlus/GooglePlus.h>
#import <Parse/Parse.h>


#define FACEBOOK_SCHEME @"fb346917958825309"


@interface AppDelegate ()

@property (strong, nonatomic) ViewController *viewController;

@end

@implementation AppDelegate
@synthesize viewController = _viewController;
@synthesize session = _session;
@synthesize delegate = _delegate;

/*
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    
    
    
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication fallbackHandler:^(FBAppCall *call) {
        // Facebook SDK * App Linking *
        // For simplicity, this sample will ignore the link if the session is already
        // open but a more advanced app could support features like user switching.
        if (call.accessTokenData) {
            if ([FBSession activeSession].isOpen) {
                [FBSession setActiveSession:self.session];
                FBSession.activeSession = self.session;
                
                NSLog(@"INFO: Ignoring app link because current session is open.");
            }
            else {
                [self handleAppLink:call.accessTokenData];
            }
        }
    }];
    return YES;
}
*/

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    
    if ([[url scheme] isEqualToString:FACEBOOK_SCHEME]){
        return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication fallbackHandler:^(FBAppCall *call) {
            // Facebook SDK * App Linking *
            // For simplicity, this sample will ignore the link if the session is already
            // open but a more advanced app could support features like user switching.
            if (call.accessTokenData) {
                if ([FBSession activeSession].isOpen) {
                    [FBSession setActiveSession:self.session];
                    FBSession.activeSession = self.session;
                    
                    NSLog(@"INFO: Ignoring app link because current session is open.");
                }
                else {
                    [self handleAppLink:call.accessTokenData];
                }
            }
        }];
    }else{
        
        return [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    
    return YES;
}




// Helper method to wrap logic for handling app links.
- (void)handleAppLink:(FBAccessTokenData *)appLinkToken {
    // Initialize a new blank session instance...
    FBSession *appLinkSession = [[FBSession alloc] initWithAppID:nil
                                                     permissions:nil
                                                 defaultAudience:FBSessionDefaultAudienceNone
                                                 urlSchemeSuffix:nil
                                              tokenCacheStrategy:[FBSessionTokenCachingStrategy nullCacheInstance] ];
    [FBSession setActiveSession:appLinkSession];
    // ... and open it from the App Link's Token.
    
    [appLinkSession openFromAccessTokenData:appLinkToken
                          completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                              // Forward any errors to the FBLoginView delegate.
                              if (error) {
                                  //TODO: Show error here
                              }
                          }];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    

    
    
    [Parse setApplicationId:@"zfD7OC5cvjowE98lfyRAbHGepIr8GnIPvMZcAWQc" clientKey:@"KkKHAZY5Ttrb9OSVEWmAatyz44z9IUGYaLcDkee3"];
    
    if( [application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        /*
         NSString *const NotificationCategoryIdent= @"ACTIONABLE";
         NSString * const NotificationActionOneIdent = @"ACTION_ONE";
         NSString * const NotificationActionTwoIdent = @"ACTION_TWO";
         
         UIMutableUserNotificationAction *action1;
         action1=[[UIMutableUserNotificationAction alloc]init];
         
         [action1 setActivationMode:UIUserNotificationActivationModeBackground];
         [action1 setTitle:@"Action 1"];
         [action1 setIdentifier:NotificationActionOneIdent];
         [action1 setDestructive:NO];
         [action1 setAuthenticationRequired:NO];
         [action1 release];
         
         
         UIMutableUserNotificationAction *action2;
         action2 = [[UIMutableUserNotificationAction alloc] init];
         [action2 setActivationMode:UIUserNotificationActivationModeBackground];
         [action2 setTitle:@"Action 2"];
         [action2 setIdentifier:NotificationActionTwoIdent];
         [action2 setDestructive:NO];
         [action2 setAuthenticationRequired:NO];
         [action2 release];
         
         
         UIMutableUserNotificationCategory *actionCategory;
         actionCategory=[[UIMutableUserNotificationCategory alloc]init];
         [actionCategory setIdentifier:NotificationCategoryIdent];
         [actionCategory setActions:@[action1,action2] forContext:UIUserNotificationActionContextDefault];
         [actionCategory release];
         
         NSSet *categories=[NSSet setWithObject:actionCategory];
         */
        
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        
        [application registerUserNotificationSettings:settings];
        
        [application registerForRemoteNotifications];
    }
    
    
    else
    {
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    

    
    
    [GMSServices provideAPIKey:@"AIzaSyAwtlpU0EOfd8CRhSy0vSs4Ez88aIp4g7g"];
    [NSThread sleepForTimeInterval:2.0];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.viewController = [[ViewController alloc] init];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    [navigation setNavigationBarHidden:NO];
    [navigation.navigationBar setBarTintColor:[UIColor colorWithRed:190.0f/255.0f green:52.0f/255.0f blue:41.0f/255.0f alpha:1.0f]];
    [navigation.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:15]}];
    
    navigation.navigationBar.translucent = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    [UIApplication sharedApplication].idleTimerDisabled = YES; // disable screen lock while app is running.
    
    self.window.rootViewController = navigation;
    [self.window makeKeyAndVisible];
  
    [[Twitter sharedInstance] startWithConsumerKey:@"j1zNnAisEKk7BatuU7DvJdC3I" consumerSecret:@"gJMzg4JW8brJokXVjNH1sHWMJLNaYX1kfFN3V259hxQ9nqUZ8l"];
    [Fabric with:@[[Twitter sharedInstance]]];
    
    [Fabric with:@[TwitterKit]];

    
    return YES;
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation setChannels: @[ @"Test" ]];
    
    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        NSLog(@"objectId : %@",currentInstallation.objectId);
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:currentInstallation.objectId forKey:@"objectID"];
        
        //  [[NSUserDefaults standardUserDefaults]setValue:currentInstallation.objectId forKey:@"object_id"];
        
        
    }];
    
 }


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"methed called--didReceiveRemoteNotification");
    
    if (userInfo !=nil && [userInfo objectForKey:@"aps"] !=[NSNull null]  )
    {
    if([[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] isEqualToString:@"Report Notification"])
    {
        NSLog(@"Notification recieved by running app");
        
        if (_delegate && [_delegate respondsToSelector:@selector(AddMarkerRuntimeWithTitle:withDescription:infoModel:)])
        {
            
            NSArray * message = [[userInfo objectForKey:@"message" ] componentsSeparatedByString:@","];
            NSArray * ReportID = [[message objectAtIndex:0] componentsSeparatedByString:@":"];
            NSArray * ReportType = [[message objectAtIndex:1] componentsSeparatedByString:@":"];
            NSArray * Latitude = [[message objectAtIndex:2] componentsSeparatedByString:@":"];
            NSArray * Longitude = [[message objectAtIndex:3] componentsSeparatedByString:@":"];
            NSArray * Price = [[message objectAtIndex:4] componentsSeparatedByString:@":"];
            NSArray * ImpactZone = [[message objectAtIndex:5] componentsSeparatedByString:@":"];
            NSArray * Description = [[message objectAtIndex:6] componentsSeparatedByString:@":"];
            NSArray * reportLocation = [[message objectAtIndex:7] componentsSeparatedByString:@":"];
          
            
            
            NSMutableDictionary *dictData = [[NSMutableDictionary alloc]init];
            
            [dictData setValue:[self str:[Description objectAtIndex:1]] forKey:@"description"];
            [dictData setValue:[self str:[Price objectAtIndex:1]] forKey:@"price"];
            [dictData setValue:[self str:[Latitude objectAtIndex:1]] forKey:@"latitude"];
            [dictData setValue:[self str:[Longitude objectAtIndex:1]] forKey:@"longitude"];
            [dictData setValue:[self str:[ReportID objectAtIndex:1]] forKey:@"reportStatusId"];
            [dictData setValue:[self str:[ReportType objectAtIndex:1]]  forKey:@"reportTypeId"];
            [dictData setValue:[self str:[ImpactZone objectAtIndex:1]] forKey:@"radiusofImpact"];
//            [dictData setValue:[NSString stringWithFormat:@"%d",[thumbsdown intValue]] forKey:@"thumbsDown"];
//            [dictData setValue:[NSString stringWithFormat:@"%d",[thumbsup intValue]] forKey:@"thumbsUp"];
//            [dictData setValue:[NSString stringWithFormat:@"%d",[hostId intValue]] forKey:@"hostId"];
//            [dictData setValue:[NSString stringWithFormat:@"%d",[userid intValue]] forKey:@"userId"];
//            [dictData setValue:[NSString stringWithFormat:@"%d",[ishost intValue]] forKey:@"isHost"];
//            [dictData setValue:[NSString stringWithFormat:@"%d",[addedby intValue]] forKey:@"addedBy"];
//            [dictData setValue:reportvideomodel forKey:@"reportVideoModelList"];

            
        [_delegate AddMarkerRuntimeWithTitle:@"Parking" withDescription:[dictData objectForKey:@"description"] infoModel:dictData];
            
            
            NSLog(@"respondsToSelectorrespondsToSelectorrespondsToSelectorrespondsToSe");
            message = nil;
            ReportID = nil;
            ReportType = nil;
            Latitude = nil;
            Longitude = nil;
            Price = nil;
            ImpactZone = nil;
            Description = nil;
            reportLocation = nil;
      }

    }
 }
}

-(NSString*)str :(NSString*)Replace
{
  return [Replace stringByReplacingOccurrencesOfString:@" " withString:@""];
}


- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    
    NSLog(@"%@",str);
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppEvents activateApp];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSession setActiveSession:self.session];
    [FBAppCall handleDidBecomeActiveWithSession:self.session];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
