//
//  AppDelegate.h
//  ValShare
//
//  Created by Sharda Prasad on 3/30/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import <GoogleMaps/GoogleMaps.h>

@protocol RealTimeNotiDelegate <NSObject>

-(void)AddMarkerRuntimeWithTitle:(NSString *)title withDescription:(NSString *)des infoModel:(NSMutableDictionary*)infoModel;
@end


@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic,weak) id<RealTimeNotiDelegate>delegate;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) FBSession *session;


@end

