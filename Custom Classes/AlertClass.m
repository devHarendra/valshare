//
//  AlertClass.m
//  ValShare
//
//  Created by iMac Apple on 30/05/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "AlertClass.h"

@implementation AlertClass


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [_v.layer setCornerRadius:5.0f];
    
    // border
    [_v.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_v.layer setBorderWidth:1.5f];
    
    // drop shadow
    [_v.layer setShadowColor:[UIColor blackColor].CGColor];
    [_v.layer setShadowOpacity:0.8];
    [_v.layer setShadowRadius:4.0];
    [_v.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
    [self setBackgroundColor:[UIColor clearColor]];
    [self setFrame:rect];
}

-(IBAction)RemoveIt:(id)sender
{
    [self removeFromSuperview];
}


@end
