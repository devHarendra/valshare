//
//  AlertClass.h
//  ValShare
//
//  Created by iMac Apple on 30/05/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface AlertClass : UIView
@property (weak, nonatomic) IBOutlet UILabel *Alertlbl;
@property (weak, nonatomic) IBOutlet UILabel *placeLbl;
@property (weak, nonatomic) IBOutlet UILabel *radiusLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *OkBtn;
@property(weak, nonatomic)  IBOutlet UIView *v;

@end
