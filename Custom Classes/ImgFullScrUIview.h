//
//  ImgFullScrUIview.h
//  ValShare
//
//  Created by Sagar Singh on 25/06/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImgFullScrUIview : UIView<UIGestureRecognizerDelegate>

@property(nonatomic,weak) UIImage *imgForZoom;
@end
