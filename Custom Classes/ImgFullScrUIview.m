//
//  ImgFullScrUIview.m
//  ValShare
//
//  Created by Sagar Singh on 25/06/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "ImgFullScrUIview.h"

@implementation ImgFullScrUIview

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [self setFrame:rect];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]init];
    [tapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:tapGestureRecognizer];
  
    UITapGestureRecognizer *tapimg = [[UITapGestureRecognizer alloc]init];
    [tapimg setDelegate:self];
    UIImageView *imgV = (UIImageView*)[self viewWithTag:100];
    imgV.image = _imgForZoom;
    [imgV addGestureRecognizer:tapimg];

}





- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if (touch.view == self)
    {
        NSLog(@"ImgFullScrUIviewImgFullScrUIview");
        [self removeFromSuperview];
    }
       return YES;
}


@end
