//
//  WeatherNewUI.h
//  ValShare
//
//  Created by iMac Apple on 09/06/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"


@interface WeatherNewUI : UIView <UITableViewDataSource, UITableViewDelegate,UIGestureRecognizerDelegate>
{

}

@property (nonatomic, strong) NSMutableArray *weatherArray;

@end
