//
//  DMConvertToJSON.m
//  DreamMeaning
//
//  Created by Anis Ansari on 12/31/14.
//  Copyright (c) 2014 Intigate Technologies. All rights reserved.
//

#import "VSConvertToJSON.h"

@implementation VSConvertToJSON

-(id)convertToJSONObject:(NSDictionary *)jsonString {
    
    NSData *data = [(NSString *)jsonString dataUsingEncoding:NSUTF8StringEncoding];
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
}

@end
