//
//  DMConvertToJSON.h
//  DreamMeaning
//
//  Created by Anis Ansari on 12/31/14.
//  Copyright (c) 2014 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSConvertToJSON : NSObject

-(id)convertToJSONObject:(NSDictionary *)jsonString;

@end
