//
//  WeatherNewUI.m
//  ValShare
//
//  Created by iMac Apple on 09/06/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "WeatherNewUI.h"

@implementation WeatherNewUI
@synthesize weatherArray = _weatherArray;


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [self setFrame:rect];
    [self setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
    
    [self.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.layer setShadowOpacity:0.8];
    [self.layer setShadowRadius:4.0];
    [self.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
    
    
    UIView *childView = (UIView*)[self viewWithTag:10];
    childView.layer.borderWidth = 5.0f;
    [childView.layer setBorderColor:[UIColor whiteColor].CGColor];
    childView.center = self.center;
    [childView.layer setShadowColor:[UIColor blackColor].CGColor];
    [childView.layer setShadowOpacity:0.8];
    [childView.layer setShadowRadius:4.0];
    [childView.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
    [childView setHidden:YES];
    
    NSLog(@"userIDuserIDuserID :- %@", [VSSingletonClass sharedMySingleton].loginUserId);
    if ([[Reachability reachabilityForInternetConnection] isReachable]) {
        
        [SVProgressHUD showWithStatus:@"please wait..." maskType:SVProgressHUDMaskTypeGradient];
        
        [self getWeatherInformMation:[VSSingletonClass sharedMySingleton].loginUserId];
        
    }else {
        
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Attentation!!"  message:@"Please check your internet connection or try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertview show];
    }

}

-(NSMutableArray *)weatherArray {
    if (!_weatherArray) {
        _weatherArray = [[NSMutableArray alloc] init];
    }
    return _weatherArray;
}


-(void)getWeatherInformMation:(NSString *)userID
{
    
    [[VSParserClass sharedParser] weatherInformation:userID WithCompletionBlock:^(BOOL result, id resultObject, NSError *error) {
        
        NSLog(@"resultObjectresultObjectresultObjectresultObject:%@",resultObject);
        
        if (result) {
            
            if ([[resultObject valueForKey:@"status"]isEqualToString:@"Failure"]) {
                
                [[[UIAlertView alloc]initWithTitle:nil message:[resultObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            }
            
            else {
                
                NSMutableDictionary *dictdata = [[VSConvertToJSON alloc] convertToJSONObject:[resultObject valueForKey:@"Data"]];
                NSLog(@"dictdatadictdata:%@",dictdata);
                
                if ([dictdata isKindOfClass:[NSDictionary class]]){
                    
                    if ([[resultObject valueForKey:@"status"] isEqualToString:@"Ok"]) {
                        
                        NSMutableArray *weatherArry = (NSMutableArray *)[dictdata valueForKey:@"weatherModelList"];
                        [weatherArry enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                            NSMutableDictionary *dataDict = (NSMutableDictionary *)[weatherArry objectAtIndex:idx];
                            
                        
                            VSWeatherModal *weatherModel = [[VSWeatherModal alloc]init];
                            [weatherModel setAverageTemprature:[dataDict valueForKey:@"averageTemprature"]];
                            [weatherModel setCity:[dataDict valueForKey:@"city"]];
                            [weatherModel setCountry:[dataDict valueForKey:@"country"]];
                            [weatherModel setDateTime:[dataDict valueForKey:@"dateTime"]];
                            [weatherModel setDescription:[dataDict valueForKey:@"description"]];
                            [weatherModel setHumadity:[dataDict valueForKey:@"humadity"]];
                            [weatherModel setMaximumTemprature:[dataDict valueForKey:@"maximumTemprature"]];
                            [weatherModel setMinimumTemprature:[dataDict valueForKey:@"minimumTemprature"]];
                            [weatherModel setPressure:[dataDict valueForKey:@"pressure"]];
                            [weatherModel setShortDescription:[dataDict valueForKey:@"shortDescription"]];
                            [weatherModel setWindSpeed:[dataDict valueForKey:@"windSpeed"]];
                            [self.weatherArray addObject:weatherModel];

                           
                        }];
                      
                          if (self.weatherArray.count>0)  [self SetupUIinfo];
                           }
                    else
                    {
                        
                    }
                }
            }
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            
        }
    }];
}

-(void)SetupUIinfo
{
    VSWeatherModal *weatherModel = [[VSWeatherModal alloc]init];
    weatherModel = [self.weatherArray objectAtIndex:0];
    
    
    UIView *childView = (UIView*)[self viewWithTag:10];
    UILabel * lbl;
    lbl = (UILabel*)[childView viewWithTag:11];
    [lbl setText:[NSString stringWithFormat:@"%@",[self changeformate_string12hr:weatherModel.dateTime isDate:YES]]];

    
    lbl = (UILabel*)[childView viewWithTag:12];
    [lbl setText:[NSString stringWithFormat:@"%@",weatherModel.humadity]];

    lbl = (UILabel*)[childView viewWithTag:13];
    [lbl setText:[NSString stringWithFormat:@"%@",[self KelvinToCelcious:weatherModel.averageTemprature]]];

    
    lbl = (UILabel*)[childView viewWithTag:14];
    [lbl setText:[NSString stringWithFormat:@"%@",weatherModel.description]];

    
    UITableView *tbl = (UITableView*)[childView viewWithTag:200];
    [tbl setDelegate:self];
    [tbl setDataSource:self];
    [tbl reloadData];
    
    [[self viewWithTag:2107199090] setHidden:false];// cancel button
    [childView setHidden:false];

}

-(IBAction)RemoveSuperView:(id)sender
{
    [self removeFromSuperview];
}



#pragma Tableview data source and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%lu count - ",(unsigned long)self.weatherArray.count);
    return [self.weatherArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
        if (cell == nil)
        {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 8, 94, 20)];
        [lbl setBackgroundColor:[UIColor clearColor]];
        [lbl setTextColor:[UIColor colorWithRed:(117/255.0) green:(18/255.0) blue:(47/255.0) alpha:1.0]];
        [lbl setFont:[UIFont systemFontOfSize:14]];
        [lbl setTag:140];
        [cell addSubview:lbl];
        
        UILabel *lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(135, 8, 94, 20)];
        [lbl2 setBackgroundColor:[UIColor clearColor]];
        [lbl2 setTextColor:[UIColor colorWithRed:(117/255.0) green:(18/255.0) blue:(47/255.0) alpha:1.0]];
        [lbl2 setFont:[UIFont systemFontOfSize:14]];
        [lbl2 setTag:150];
        [cell addSubview:lbl2];
    }
    
    VSWeatherModal *weatherModel = [[VSWeatherModal alloc]init];
    weatherModel = [self.weatherArray objectAtIndex:indexPath.row];
    
    UILabel *lbl = (UILabel*)[cell viewWithTag:140];
    [lbl setText:[NSString stringWithFormat:@"%@",[self changeformate_string12hr:weatherModel.dateTime isDate:NO]]];
    
    UILabel *lbl2 = (UILabel*)[cell viewWithTag:150];
    [lbl2 setText:[NSString stringWithFormat:@"%@",[self KelvinToCelcious:weatherModel.averageTemprature]]];
    
    return cell;
}


-(NSString *)changeformate_string12hr:(NSString *)date isDate:(BOOL)isReturnDate
{
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* wakeTime = [df dateFromString:date];
 
    if (isReturnDate) [df setDateFormat:@"yyyy-MM-dd"]; // returen date
    else [df setDateFormat:@"HH:mm aa"]; // return time
    
    return [df stringFromDate:wakeTime];
}

-(NSString*)KelvinToCelcious :(NSString*)kValue
{
    float kalvin = [kValue floatValue];
    float celcius = kalvin-273.15;
    return [NSString stringWithFormat:@"%0.2f",celcius];
  }

@end
