//
//  VSSearchDropDown.h
//  ValShare
//
//  Created by Sharda Prasad on 4/3/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSHeader.h"

@class VSSearchDropDown;

@protocol VSSearchDropDownDelegate

- (void)searchMenuPopover:(VSSearchDropDown *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex;

@end

@interface VSSearchDropDown : UIView <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,assign) id<VSSearchDropDownDelegate> searchDropDownDelegate;

- (id)initWithFrame:(CGRect)frame menuItems:(NSArray *)menuItems;
- (void)showInView:(UIView *)view;
- (void)dismissMenuPopover;
- (void)layoutUIForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

@end