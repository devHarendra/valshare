//
//  VSEnumClass.m
//  ValShare
//
//  Created by Sharda Prasad on 3/25/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSEnumClass.h"

@implementation VSEnumClass

+ (UIFont *)font:(VSFontType)fontType size:(NSUInteger)fontsize {
    
    switch (fontType) {
            
        case VSHelveticaNeue_Bold:
            return [UIFont fontWithName:kHeveticaNeue_Bold size:fontsize];
            break;
        case VSHelveticaNeue_light:
            return [UIFont fontWithName:kHeveticaNeue_Light size:fontsize];
            break;
        case VSHelveticaNeue_Medium:
            return [UIFont fontWithName:kHeveticaNeue_Medium size:fontsize];
            break;
        case VSHelveticaNeue_UltraLight:
            return [UIFont fontWithName:kHeveticaNeue_ULtraLight size:fontsize];
            break;
            
        default:
            return [UIFont fontWithName:kHelvetica_45 size:fontsize];
            break;
    }
}


+ (NSString *)getBaseURL {
    
    return [NSString stringWithFormat:@"%@", kBaseURL];
}

+ (NSString *)servicesName:(VSServiceType)serviceType {
    
    switch (serviceType) {
            
        case VSLoginService:
            
            return [NSString stringWithFormat:@"%@",@"userservice/signInWithEmailId"];
            break;
        case VSSignInWithUserName:
            
            return [NSString stringWithFormat:@"%@",@"userservice/signInWithUserName"];
            break;
        case VSSignInWithFacebook:
            
            return [NSString stringWithFormat:@"%@",@"userservice/signInWithFacebook"];
            break;
            
        case VSSignInWithGmail:
            
            return [NSString stringWithFormat:@"%@",@"userservice/signInWithGmail"];
            break;

            
            
        case VSSignInWithTwitter:
            
            return [NSString stringWithFormat:@"%@",@"userservice/signInWithTwitter"];
            break;
        case VSSignUpService:
            
            return [NSString stringWithFormat:@"%@",@"userservice/registerStepI"];
            break;
        case VSCheckEmail:
            
            return [NSString stringWithFormat:@"%@",@"userservice/isEmailUnique"];
            break;
        case VSCheckUserName:
            
            return [NSString stringWithFormat:@"%@",@"userservice/isUserNameUnique"];
            break;
        case VSVarifyToken:
            
            return [NSString stringWithFormat:@"%@",@"userservice/verifyToken"];
            break;
        case VSResendToken:
            
            return [NSString stringWithFormat:@"%@",@"userservice/resendVerificationCode"];
            break;
        case VSForgotPassword:
            return [NSString stringWithFormat:@"%@",@"userservice/forgetpassword"];
            break;
        case VSWeatherInformation:
            return [NSString stringWithFormat:@"%@",@"userservice/weatherInformation"];
            break;
        case VSThumbsDown:
            return [NSString stringWithFormat:@"%@",@"userservice/thumbsDown"];
            break;
        case VSThumbsUp:
            return [NSString stringWithFormat:@"%@",@"userservice/thumbsUp"];
            break;
        case VSReportVote1:
            return [NSString stringWithFormat:@"%@",@"userservice/reportVote"];
            break;
        case VSMapComment:
            return [NSString stringWithFormat:@"%@",@"resourceservice/add/ats_system,reportcommentmanager"];
            break;
            //routeservice/getroutes
        case VSGetComment:
            return [NSString stringWithFormat:@"%@",@"userservice/getReportComments"];
            break;
        case VSGetRoute:
            return [NSString stringWithFormat:@"%@",@"routeservice/getroutes"];
            break;
        case VSSaveRouteService:
            return [NSString stringWithFormat:@"%@",@"routeservice/saveroute"];
            break;
        case VSShareDataWithGmail:
            return [NSString stringWithFormat:@"%@",@"userservice/shareReport"];
            break;
        case VSResetPassword:
            return [NSString stringWithFormat:@"%@",@"userservice/resetpassword"];
            break;
            
        case VSFeedback:
            return [NSString stringWithFormat:@"%@",@"userservice/feedback"];
            break;
        case VSCardDEtail:
            
            return [NSString stringWithFormat:@"%@",@"userservice/paymentprocess"];
            break;
        case VSUpdateProfile:
            
            return [NSString stringWithFormat:@"%@",@"userservice/updateUserProfile"];
            break;
        case VSGetProfile:
            
            return [NSString stringWithFormat:@"%@",@"userservice/getUserProfile"];
            break;
        case VSGetRouteList:
            
            return [NSString stringWithFormat:@"%@",@"routeservice/getRouteList"];
            break;
        case VSGetVehicleList:
            
            return [NSString stringWithFormat:@"%@",@"resourceservice/getList/ats_system,vehiclemanager"];
            break;
        case VSDeleteVehicle:
            
            return [NSString stringWithFormat:@"%@",@"resourceservice/delete/ats_system,vehiclemanager"];
            break;
        case VSAddVehicle:
            
            return [NSString stringWithFormat:@"%@",@"resourceservice/add/ats_system,vehiclemanager"];
            break;
        case VSUpdateVehicle:
            
            return [NSString stringWithFormat:@"%@",@"resourceservice/update/ats_system,vehiclemanager"];
            break;
            
        case VSGetReportDisplaySetting:
            
            return [NSString stringWithFormat:@"%@", @"userservice/getreportdisplaysetting"];
            break;
        case VSSaveReportDisplaySetting:
            
            return [NSString stringWithFormat:@"%@", @"userservice/addreportdisplaysetting"];
            break;
            
        case VSPurchesHistoryList:
            
            return [NSString stringWithFormat:@"%@",@"userservice/getpurchasehistorylist"];
            break;
            
        case VSPurchesHistory:
            
            return [NSString stringWithFormat:@"%@",@"userservice/getpurchasehistory"];
            break;
            
        case VSDeletePurchesHistory:
            
            return [NSString stringWithFormat:@"%@",@"userservice/deletepurchasehistory"];
            break;
            
        case VSGetCardDetail:
            
            return [NSString stringWithFormat:@"%@",@"resourceservice/getList/ats_system,cardmanager"];
            break;
            
        case VSGetReportsView:
            
            return [NSString stringWithFormat:@"%@",@"resourceservice/getList/ats_system,reportmanager"];
            
        case VSDeleteCCDetail:
            
            return [NSString stringWithFormat:@"%@",@"resourceservice/delete/ats_system,cardmanager"];
            
        case VSReportDetailSave:
            
            return [NSString stringWithFormat:@"%@",@"resourceservice/update/ats_system,reportmanager"];
            
        case VSDeletRoute:
            
            return [NSString stringWithFormat:@"%@",@"routeservice/delete"];
            break;
            
        case VSGetselectedRouteDetail:
            
            return [NSString stringWithFormat:@"%@",@"routeservice/get"];
            break;
            
        case VSGetTicketInitiat:
            
            return [NSString stringWithFormat:@"%@",@"userservice/getReport"];
            break;
            
        case VSGetPackByMonth:
            
            return [NSString stringWithFormat:@"%@",@"userservice/getpackage"];
            break;
            
        case VSGetPackByYearly:
            
            return [NSString stringWithFormat:@"%@",@"userservice/getpackage"];
            break;
            
        case VSGetRegistrationType:
            return [NSString stringWithFormat:@"%@",@"userservice/register"];
            break;
            
        case VSDeleteMyReports:
            return [NSString stringWithFormat:@"%@",@"resourceservice/delete/ats_system,reportmanager"];
            break;
            
        case VSAddCard:
            return [NSString stringWithFormat:@"%@",@"resourceservice/add/ats_system,cardmanager"];
            break;
            
        case VSCheckCardName:
            return [NSString stringWithFormat:@"%@",@"userservice/isNameValid"];
            break;
            
        case VSCheckCommercialUser:
            return [NSString stringWithFormat:@"%@",@"companyservice/isCompanyIdValid"];
            break;

        case VSGetDefaultCardDetail:
            return [NSString stringWithFormat:@"%@",@"userservice/getdefaultcarddetails"];
            break;
            
        case VSUpdateCard:
            return [NSString stringWithFormat:@"%@",@"resourceservice/update/ats_system,cardmanager"];
            break;
            
        case VSPaymentProcess:
            return [NSString stringWithFormat:@"%@",@"userservice/paymentprocess"];
            break;
            
        case VSAddTcket:
            return [NSString stringWithFormat:@"%@",@"resourceservice/add/ats_system,reportmanager"];
            break;
            
        case VSAddTcketList:
            return [NSString stringWithFormat:@"%@",@"adminservice/getList/reporttypemanager"];
            break;
            
        case VSUpadetReport:
            
            return [NSString stringWithFormat:@"%@",@"resourceservice/update/ats_system,reportmanager"];
            break;
        case VSCityList:
            
            return [NSString stringWithFormat:@"%@",@"userservice/getcitylist"];
            break;
            
        default:
            break;
    }
    return nil;
}
//

#pragma mark -
#pragma mark UIAlertView showing Methods

+ (void)showAlertWithTitle:(NSString *)title msg:(NSString *)msg buttonTitle:(NSString *)btnTitle {
    
    [[[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:btnTitle otherButtonTitles:nil] show];
}

+ (void) showAlertWithTitle:(NSString *)title {
    
    [VSEnumClass showAlertWithTitle:title msg:nil buttonTitle:NSLocalizedString(@"Ok",@"Ok")];
}

+ (void) showAlertWithTitle:(NSString *)title msg:(NSString *)msg {
    
    [VSEnumClass showAlertWithTitle:title msg:msg buttonTitle:NSLocalizedString(@"Ok",@"Ok")];
}

@end
