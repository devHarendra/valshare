//
//  VSSingletonClass.m
//  ValShare
//
//  Created by Sharda Prasad on 4/23/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSSingletonClass.h"

@implementation VSSingletonClass
@synthesize loginUserId;

static VSSingletonClass * _sharedMySingleton = nil;

+(VSSingletonClass *)sharedMySingleton {
    
    @synchronized([VSSingletonClass class])
    {
        if (!_sharedMySingleton) {
            _sharedMySingleton = [[self alloc] init];
        }
        return _sharedMySingleton;
    }
    return nil;
}

@end
