//
//  VSEnumClass.h
//  ValShare
//
//  Created by Sharda Prasad on 3/25/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "VSHeader.h"

// Define the font
#define kHeveticaNeue_Bold                  @"HelveticaNeue-Bold"
#define kHeveticaNeue_Italic                @"HelveticaNeue-Italic"
#define kHeveticaNeue_Medium                @"HelveticaNeue-Medium"
#define kHeveticaNeue_ULtraLight            @"HelveticaNeue-UltraLight"
#define kHeveticaNeue_Light                 @"HelveticaNeue-Light"
#define kHelvetica_45                       @"Helvetica 45 Light"
#define kelvetica_Neue_LT_Std_35_Thin       @"Helvetica Neue LT Std 35 Thin"
#define kCandara_regular                    @"candara regular"
#define kHelvetica_Neue_LT_Std_67_Medium    @"Helvetica Neue LT Std 67 Medium"
#define kHelvetica_CE_Narrow                @"Helvetica CE Narrow"
#define kHelvetica_Neue_LT_Std_bold_condsed @"HelveticaLTStd-BoldCond"
#define kAparajita_Regular                  @"aparajita regular"
#define kHelvetica_neue_LT_std_47_LC        @"helvetica neue LT std 47 light condensed"
#define kHelvetica_condensed_light_regular  @"helvetica condensed light regular"
#define kCalibri_Regular                    @"calibri-regular"
#define kCalibri                            @"calibri"


#define KColor [UIColor colorWithRed:32.0f/255.0f green:114.0f/255.0f blue:144.0f/255.0f alpha:1.0f]
#define TABLE_TEXT_COLOR        [UIColor colorWithRed:39.0/255.0 green:106.0/255.0 blue:133.0/255.0 alpha:1.0]

#define RgbHex2UIColor(r, g, b)                 [UIColor colorWithRed:((r) / 256.0) green:((g) / 256.0) blue:((b) / 256.0) alpha:1.0]
#define RgbHex2UIColorWithAlpha(r, g, b, a)     [UIColor colorWithRed:((r) / 256.0) green:((g) / 256.0) blue:((b) / 256.0) alpha:(a)]
#define CArrayLength(arr)                       (sizeof(arr) / sizeof(*(arr)))
#define GetStringFromCArraySafely(arr, idx)     (((idx) >= 0) && (((idx) < CArrayLength(arr))) ? (arr)[idx] : @"")
#define GetNumberFromCArraySafely(arr, idx)     (((idx) >= 0) && (((idx) < CArrayLength(arr))) ? (arr)[idx] : 0)
#define NSNumWithInt(i)                         ([NSNumber numberWithInt:(i)])
#define NSNumWithFloat(f)                       ([NSNumber numberWithFloat:(f)])
#define NSNumWithBool(b)                        ([NSNumber numberWithBool:(b)])
#define IntFromNSNum(n)                         ([(n) intValue])
#define FloatFromNSNum(n)                       ([(n) floatValue])
#define BoolFromNSNum(n)                        ([(n) boolValue])
#define ToString(o)                             [NSString stringWithFormat:@"%@", (o)]


#define ContinueIf(expr)            if ((expr))  { continue;     }
#define BreakIf(expr)               if ((expr))  { break;        }
#define ReturnIf(expr)              if ((expr))  { return;       }
#define ReturnValIf(expr, val)      if ((expr))  { return (val); }
#define ContinueIfNot(expr)         if (!(expr)) { continue;     }
#define BreakIfNot(expr)            if (!(expr)) { break;        }
#define ReturnIfNot(expr)           if (!(expr)) { return;       }
#define ReturnValIfNot(expr, val)   if (!(expr)) { return (val); }

#define kBaseURL @"http://web-service-dev.elasticbeanstalk.com/rest/"
//#define kBaseURL @"http://207.46.129.45:8080/anti-ticket-web-service/rest/"

/*
 * enum for font types
 */

typedef enum FontType {
    
    VSHelveticaNeue_Bold        = 0,
    VSHelveticaNeue_light       = 1,
    VSHelveticaNeue_Medium      = 2,
    VSHelveticaNeue_UltraLight  = 3,
} VSFontType;

/*
 * enum for all services
 */
typedef enum ServiceType {
    
    VSLoginService          = 0,
    VSSignInWithUserName    = 1,
    VSSignUpService         = 2,
    VSSignInWithFacebook    = 3,
    VSSignInWithTwitter     = 4,
    VSCheckUserName         = 5,
    VSCheckEmail            = 6,
    VSVarifyToken           = 7,
    VSResendToken           = 8,
    VSForgotPassword        = 9,
    VSWeatherInformation    = 10,
    VSThumbsUp              = 11,
    VSThumbsDown            = 12,
    VSReportVote1           = 13,
    VSMapComment            = 14,
    VSGetComment            = 15,
    VSGetRoute              = 16,
    VSSaveRouteService      = 17,
    VSShareDataWithGmail    = 18,
    VSResetPassword         = 19,
    VSFeedback              = 20,
    VSCardDEtail            = 21,
    VSUpdateProfile         = 22,
    VSGetProfile            = 23,
    VSGetRouteList          = 24,
    VSGetVehicleList        = 25,
    VSDeleteVehicle         = 26,
    VSAddVehicle            = 27,
    VSUpdateVehicle         = 28,
    VSGetReportDisplaySetting  = 29,
    VSSaveReportDisplaySetting  = 30,
    VSPurchesHistoryList    = 31,
    VSPurchesHistory        = 32,
    VSDeletePurchesHistory  = 33,
    VSGetCardDetail         = 34,
    VSGetReportsView        = 35,
    VSDeleteCCDetail        = 36,
    VSReportDetailSave      = 37,
    VSDeletRoute            = 38,
    VSGetPackByMonth        = 39,
    VSGetPackByYearly       = 40,
    VSGetRegistrationType   = 41,
    VSDeleteMyReports       = 42,
    VSAddCard               = 43,
    VSCheckCardName         = 44,
    VSCheckCommercialUser   = 45,
    VSGetDefaultCardDetail  = 46,
    VSUpdateCard            = 47,
    VSPaymentProcess        = 48,
    VSAddTcket              = 49,
    VSAddTcketList          = 50,
    VSUpadetReport          = 51,
    VSCityList              = 52,
    VSSignInWithGmail       = 53,
    VSGetselectedRouteDetail   = 54,
    VSGetTicketInitiat      = 55,


    
} VSServiceType;

@interface VSEnumClass : NSObject

+ (NSString *)getBaseURL;
+ (NSString *)servicesName:(VSServiceType)serviceType;
+ (UIFont *)font:(VSFontType)fontType size:(NSUInteger)fontsize;

+ (void) showAlertWithTitle:(NSString *)title;
+ (void) showAlertWithTitle:(NSString *)title
                        msg:(NSString *)msg;
+ (void) showAlertWithTitle:(NSString *)title
                        msg:(NSString *)msg
                buttonTitle:(NSString *)btnTitle;

@end