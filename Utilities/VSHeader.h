//
//  VSHeader.h
//  ValShare
//
//  Created by Sharda Prasad on 3/30/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifndef ValShare_VSHeader_h
#define ValShare_VSHeader_h

//! Block for completion of multiple tasks
typedef void (^CompletionBlock) (BOOL result, id resultObject, NSError *error);

//! Block for Core Data Operations
typedef void (^DataBlock) (BOOL success, NSError *error);

/*
 * * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * *
 * Launch screen programmatically in info.plist :----- http://stackoverflow.com/questions/25926661/how-do-i-create-launch-images-for-iphone-6-6-plus-landscape-only-apps
 ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * * ** * *
 */

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4S (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_IPHONE6      ((int)(MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)) == 667)
#define IS_IPHONE6PLUS  ((int)(MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)) == 736)

/*
 * Parser key object
 */

#define kHost           @"Host"
#define kTextText       @"text/plain"


#define kSOAPAction     @"SOAPAction"
#define kContent_Type   @"Content-Type"
#define kContent_Length @"Content-Length"

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]


#define kFacebookBG                 [UIImage imageNamed:@"facebook"]
#define kGoogleBG                   [UIImage imageNamed:@"loginWithGoogle"]
#define kTwitterBG                  [UIImage imageNamed:@"twitter"]
#define kLoginBG                    [UIImage imageNamed:@"login_Button"]
#define kCancelBG                   [UIImage imageNamed:@"cancel"]
#define kUserNameBG                 [UIImage imageNamed:@"user_login"]
#define kPasswordBG                 [UIImage imageNamed:@"user_password"]
#define kValLogoBG                  [UIImage imageNamed:@"ValShare_Logo"]
#define kOrBG                       [UIImage imageNamed:@"or"]
#define kVisaCardBG                 [UIImage imageNamed:@"visa"]
#define kMasterCardBG               [UIImage imageNamed:@"masterCard"]
#define kAddVehicleBG               [UIImage imageNamed:@"add_vehicle"]

#define kBackButtonBG               [UIImage imageNamed:@"backButton"]
#define kLikeHighLightBG            [UIImage imageNamed:@"un_like"]
#define kLikeBG                     [UIImage imageNamed:@"like_image"]

#define kUnLikeBG                   [UIImage imageNamed:@"deslike_BG"]
#define kUnLikeHighLightBG          [UIImage imageNamed:@"deslike_highlight"]

#define kCommentBG                  [UIImage imageNamed:@"comment_Button"]
#define kGmailShareBG               [UIImage imageNamed:@"gmailShare"]
#define kRadioButtonBG              [UIImage imageNamed:@"RadioButton"]
#define kRadioButtonSelectedBG      [UIImage imageNamed:@"RadioButtonSelected"]
#define kCheckboxBG                 [UIImage imageNamed:@"cb_mono_off_white"]
#define kCheckedBG                  [UIImage imageNamed:@"cb_mono_on_white"]
#define kRegisterButtonBG           [UIImage imageNamed:@"register-button_BG"]
#define kPassword_BG                [UIImage imageNamed:@"password_BG"]
#define kEmail_BG                   [UIImage imageNamed:@"email_BG"]
#define kMobileNo_BG                [UIImage imageNamed:@"mobileNo_BG"]
#define kUserName_BG                [UIImage imageNamed:@"userName_BG"]
#define kRadioButtonBG              [UIImage imageNamed:@"RadioButton"]
#define kCalendarBG                 [UIImage imageNamed:@"calendar"]
#define kUpThumpBg                  [UIImage imageNamed:@"upThump"]
#define kDownThumpBG                [UIImage imageNamed:@"downThump"]
#define kUnLikeThumpBG              [UIImage imageNamed:@"unLike"]

#define kCheckBlackBG1               [UIImage imageNamed:@"checkbox"]
#define kCheckedBlackBG1             [UIImage imageNamed:@"checked"]

#define kCarBG                      [UIImage imageNamed:@"newcar"]
#define kSchoolBg                   [UIImage imageNamed:@"first"]
#define kUmbrellaBG                 [UIImage imageNamed:@"second"]
#define kboatBG                     [UIImage imageNamed:@"third"]
#define kgitarBG                     [UIImage imageNamed:@"gitar"]

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Twitter/Twitter.h>
#import "VSViewReportNew.h"

#import "VSButton.h"
#import "VSTextField.h"

#import "QRadioButton.h"

#import "SVProgressHUD.h"

#import "VSParserClass.h"

#import "VSMapController.h"

#import "VSConvertToJSON.h"

#import "VSDirectionModal.h"

#import "LatAndLongModelClass.h"

#import "VSDropDownView.h"

#import "NIDropDown.h"

#import "TWSReleaseNotesView.h"

#import "VSSearchView.h"

#import "VSSignUpView.h"

#import "VSSaveRouteViewController.h"

#import "VSPinPupUpCell.h"

#import "VehicleViewController.h"

#import "MyRouteViewController.h"

#import "VSSaveRouteCell.h"

#import "VSUpdateProfileViewController.h"

#import "VSUpdateProfileCell.h"

#import "VSForgotView.h"

#import "VSUserVarification.h"

#import "MyRouteTableCell.h"

#import "VSWeatherModal.h"

#import "VSCCDetailViewController.h"

#import "VSFeedBackViewController.h"

#import "VSCardDetailViewController.h"

#import "VSReportDisplayView.h"

#import "VSPurchesHistoryView.h"

#import "VSCommentPopUpView.h"

#import "VSCommentModal.h"

#import "VSResetPasswordView.h"

#import "VSUserDetailModel.h"

#import "MyAnnonationClass.h"

#import "VSVehicleListModal.h"

#import "SSCheckBoxView.h"

#import "VSPurchesHistoryListModel.h"

#import "VSPurchesDetailView.h"

#import "VSHistoryModelClass.h"

#import "VSCCDetailModelClass.h"

#import "VSReportModelClass.h"

#import "VSReportDetailController.h"

#import "VSPackageSelectionView.h"

#import "VSGetPackByMonth&YearModel.h"

#import "VSAntiTiketView.h"

#import "LTHMonthYearPickerView.h"

#import "Reachability.h"

#import "VSSingletonClass.h"

#import "VSUpdateCardDetails.h"

#import "VSCustomAddTicketView.h"

#import "VSGetAddTicketListModel.h"

#import "VSCustomAddReportView.h"

#import "VSVehicelDetailView.h"
#import "NotiSettingsVC.h"

#endif
