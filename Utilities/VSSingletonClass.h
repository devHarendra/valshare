//
//  VSSingletonClass.h
//  ValShare
//
//  Created by Sharda Prasad on 4/23/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSSingletonClass : NSObject

+(VSSingletonClass *)sharedMySingleton;

@property (nonatomic, assign) id customDelegate;
@property (nonatomic, strong) NSString *loginUserId;

@end
