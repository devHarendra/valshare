//
//  LatAndLongModelClass.h
//  VolShare
//
//  Created by Vivek Kumar on 3/25/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LatAndLongModelClass : NSObject

@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;

@property float mylatitude;
@property float mylongitude;


@end
