//
//  VSCommanReportModal.m
//  ValShare
//
//  Created by Sharda Prasad on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCommanReportModal.h"

@implementation VSCommanReportModal

@synthesize distanceUnitKey;
@synthesize distanceUnitValue;
@synthesize reportTypeKey;
@synthesize reportTypeValue;
@synthesize timeUnitKey;
@synthesize timeUnitValue;

@end
