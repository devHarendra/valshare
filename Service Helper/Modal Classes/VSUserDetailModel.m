//
//  UserDetailModel.m
//  ValShare
//
//  Created by Vivek Kumar on 4/14/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSUserDetailModel.h"

@implementation VSUserDetailModel
@synthesize  dob;
@synthesize accountType;
@synthesize accountTypeId;
@synthesize cityId;
@synthesize contactNumber;
@synthesize countryId;
@synthesize email;
@synthesize gender;
@synthesize genderTypeId;
@synthesize language;
@synthesize languageId;
@synthesize lastModifiedTime;
@synthesize locationId;
@synthesize userId;
@synthesize userName;
@synthesize userNameRequired;
@synthesize userRoleId;
@synthesize userStatusId;
@synthesize firstName;
@synthesize lastName;
@synthesize addressOne;
@synthesize addressTwo;
@synthesize zipCode;
@synthesize nextPaymentDate;
@synthesize country;
@synthesize city;

@synthesize cityDictionary;
@synthesize countryDictionary;
@synthesize genderDictionary;
@synthesize languageDictionary;
@synthesize userModelList;

@synthesize genderKey;
@synthesize genderObject;
@synthesize cityKey;
@synthesize cityObject;
@synthesize countryKey;
@synthesize countryObject;
@synthesize languageKey;
@synthesize languageObject;

@end
