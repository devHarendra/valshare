//
//  VSReportModelClass.h
//  ValShare
//
//  Created by Vivek Kumar on 4/20/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSReportModelClass : NSObject

@property (strong ,nonatomic) NSString *addedBy;
@property (strong ,nonatomic) NSString *createdTime;
@property (strong ,nonatomic) NSString *myDescription;
@property (strong ,nonatomic) NSString *hostId;
@property (strong ,nonatomic) NSString *isHost;
@property (strong ,nonatomic) NSString *lastModifiedTime;
@property (strong ,nonatomic) NSString *latitude;
@property (strong ,nonatomic) NSString *location;
@property (strong ,nonatomic) NSString *longitude;
@property (strong ,nonatomic) NSString *price;
@property (strong ,nonatomic) NSString *radiusofImpact;
@property (strong ,nonatomic) NSString *reportId;
@property (strong ,nonatomic) NSString *reportStatus;
@property (strong ,nonatomic) NSString *reportStatusId;
@property (strong ,nonatomic) NSString *reportType;
@property (strong ,nonatomic) NSString *reportTypeId;
@property (strong ,nonatomic) NSString *reporterEmail;
@property (strong ,nonatomic) NSString *statusType;
@property (strong ,nonatomic) NSString *thumbsDown;
@property (strong ,nonatomic) NSString *thumbsUp;
@property (strong ,nonatomic) NSString *userId;

@end
