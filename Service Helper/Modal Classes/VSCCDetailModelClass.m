//
//  VSCCDetailModelClass.m
//  ValShare
//
//  Created by Vivek Kumar on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCCDetailModelClass.h"

@implementation VSCCDetailModelClass

@synthesize  amountToBePaid;
@synthesize cardId;
@synthesize cardNumber;
@synthesize cardType;
@synthesize cardTypeId;
@synthesize creditDebitCardInfoStatusId;
@synthesize creditDebitCardInfoStatus;
@synthesize expiryDate;
@synthesize firstName;
@synthesize lastModifiedTime;
@synthesize lastName;
@synthesize loyaltyPrice;
@synthesize packagePrice;
@synthesize paymentPackageId;
@synthesize statusTypeId;
@synthesize totalLoyalityPoint;
@synthesize userId;

@synthesize streetAddress;
@synthesize city;
@synthesize country;
@synthesize zipCode;
@synthesize isDefault;

@end
