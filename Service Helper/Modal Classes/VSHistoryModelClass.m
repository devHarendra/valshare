//
//  VSHistoryModelClass.m
//  ValShare
//
//  Created by Vivek Kumar on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSHistoryModelClass.h"

@implementation VSHistoryModelClass

@synthesize  cardNumber;
@synthesize cashPrice;
@synthesize city;
@synthesize country;
@synthesize createdTime;
@synthesize effectiveFrom;
@synthesize effectiveTo;
@synthesize lastModifiedTime;
@synthesize loyaltyPoint;
@synthesize loyaltyPointEquivalent;
@synthesize packageName;
@synthesize packagePrice;
@synthesize paymentPackageId;
@synthesize puchaseHistoryId;
@synthesize purcahseHistoryStatus;
@synthesize purcahseHistoryStatusId;
@synthesize streetAddress;
@synthesize transactionId;
@synthesize userId;
@synthesize zipCode;

@end
