//
//  VSVehicleListModal.m
//  ValShare
//
//  Created by Sharda Prasad on 4/17/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSVehicleListModal.h"

@implementation VSVehicleListModal

@synthesize isDefault;
@synthesize lastModifiedTime;
@synthesize registrationNumber;
@synthesize userId;
@synthesize userVehicleInfoId;
@synthesize userVehicleInfoStatus;
@synthesize userVehicleInfoStatusId;
@synthesize vehicleDocumentModelList;
@synthesize vehicleModelList;
@synthesize vehicleType;
@synthesize vehicleTypeId;
@synthesize vehicleTypeName;

@end
