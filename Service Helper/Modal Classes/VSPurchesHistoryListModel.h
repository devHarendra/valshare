//
//  VSPurchesHistoryList.h
//  ValShare
//
//  Created by Vivek Kumar on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSPurchesHistoryListModel : NSObject

@property (strong ,nonatomic) NSString *cashPrice ;
@property (strong ,nonatomic) NSString *loyaltyPoint;
@property (strong ,nonatomic) NSString *loyaltyPointEquivalent;
@property (strong ,nonatomic) NSString *nextPaymentDate;
@property (strong ,nonatomic) NSString *packagePrice;
@property (strong ,nonatomic) NSString *paymentPackageId;
@property (strong ,nonatomic) NSString *puchaseHistoryId;
@property (strong ,nonatomic) NSString *purcahseHistoryStatusId;
@property (strong ,nonatomic) NSString *purchaseHistoryModelList;

@property (strong ,nonatomic) NSString  *cardNumber;
@property (strong ,nonatomic) NSString  *createdTime;
@property (strong ,nonatomic) NSString  *email;

@property (strong ,nonatomic) NSString  *packageName;

@property (strong ,nonatomic) NSString  *purcahseHistoryStatus;
@property (strong ,nonatomic) NSString  *transactionId;
@property (strong ,nonatomic) NSString  *userId;


@end
