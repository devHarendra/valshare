//
//  VSReportDisplaySetting.m
//  ValShare
//
//  Created by Sharda Prasad on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSReportDisplaySetting.h"

@implementation VSReportDisplaySetting

@synthesize distanceUnitDictionary;
@synthesize reportTypeIdList;
@synthesize timeUnitDictionary;
@synthesize reportTypeDictionary;
@synthesize distanceUnitId;
@synthesize reportDisplaySettingId;
@synthesize reportRange;
@synthesize reportTime;
@synthesize reportTypeId;
@synthesize timeUnitId;
@synthesize userId;

@end
