//
//  VSReportVideo_ImageList.m
//  ValShare
//
//  Created by Vivek Kumar on 5/6/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSReportVideo_ImageList.h"

@implementation VSReportVideo_ImageList

@synthesize imageName;
@synthesize videoName;
@synthesize documentType;

-(id)initWithCode:(NSString *)image_name
       Video_Name:(NSString *)video_name
         Document:(NSString *)documen_type {
    
    self = [super init];
    if (self) {
        self.imageName = image_name;
        self.videoName = video_name;
        self.documentType = documen_type;
    }
    return self;
}

- (NSMutableDictionary *)toNSDictionary
{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:self.imageName forKey:@"imageName"];
    [dictionary setValue:self.videoName forKey:@"videoName"];
    [dictionary setValue:self.documentType forKey:@"documentType"];
    return dictionary;
}

@end
