//
//  SearchLocationModel.h
//  VolShare
//
//  Created by Vivek Kumar on 4/2/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchLocationModel : NSObject
@property (nonatomic, strong) NSString *long_name;
@property (nonatomic, strong) NSString *location_name;

@end
