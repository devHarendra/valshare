//
//  VSWeatherModal.h
//  ValShare
//
//  Created by Sharda Prasad on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSWeatherModal : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *userId;

@property (nonatomic, strong) NSString *averageTemprature;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *dateTime;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *humadity;
@property (nonatomic, strong) NSString *maximumTemprature;
@property (nonatomic, strong) NSString *minimumTemprature;
@property (nonatomic, strong) NSString *pressure;
@property (nonatomic, strong) NSString *shortDescription;
@property (nonatomic, strong) NSString *weatherModelList ;
@property (nonatomic, strong) NSString *windSpeed;

@end
