//
//  VSReportVote.m
//  ValShare
//
//  Created by Sharda Prasad on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSReportVote.h"

@implementation VSReportVote

@synthesize status;
@synthesize userId;
@synthesize reportId;
@synthesize reportVote;
@synthesize thumbsUp;
@synthesize thumbsDown;

@end
