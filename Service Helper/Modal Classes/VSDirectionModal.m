//
//  VSDirectionModal.m
//  ValShare
//
//  Created by Sharda Prasad on 3/31/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSDirectionModal.h"

@implementation VSDirectionModal

@synthesize  destination;
@synthesize  drivingMode;
@synthesize  lastModifiedTime;
@synthesize  routeEncodedPath;
@synthesize  routeId;
@synthesize  routeIndexId;
@synthesize  source;
@synthesize  sourceuserId;

@synthesize  localRouteDestination;
@synthesize  localRouteDistance;
@synthesize  localRouteDrivingMode;
@synthesize  localRouteDuration;
@synthesize  localRouteRouteEncodedPath;
@synthesize  localRouteRouteId;
@synthesize  localRouteRouteIndexId;
@synthesize  localRouteSource;
@synthesize  localRouteSummary;
@synthesize  localRouteUserId;
@synthesize  reportModalAddedByName;

@synthesize  reportModalAddedBy;
@synthesize  reportModalDescription;
@synthesize  reportModalHostId;
@synthesize  reportModalIsHost;
@synthesize  reportModalLastModifiedTime;
@synthesize  reportModalLatitude;
@synthesize  reportModalLocation;
@synthesize  reportModalLongitude;
@synthesize  reportModalPrice;
@synthesize  reportModalRadiusofImpact;
@synthesize  reportModalReportId;
@synthesize  reportModalReportStatus;
@synthesize  reportModalReportStatusId;
@synthesize  reportModalReportType;
@synthesize  reportModalReportTypeId;
@synthesize  reportModalStatusType;
@synthesize  reportModalThumbsDown;
@synthesize  reportModalThumbsUp;
@synthesize  reportModalUserId;
@synthesize  reportVideoModelList;
@synthesize ReportModelDistance;


@end
