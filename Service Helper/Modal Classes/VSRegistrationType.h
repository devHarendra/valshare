//
//  VSRegistrationType.h
//  ValShare
//
//  Created by Sharda Prasad on 4/22/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSRegistrationType : NSObject

@property (strong , nonatomic) NSString *genderTypeKey;
@property (strong , nonatomic) NSString *genderTypeObject;
@property (strong , nonatomic) NSString *accountTypeKey;
@property (strong , nonatomic) NSString *accountTypeObject;

@end
