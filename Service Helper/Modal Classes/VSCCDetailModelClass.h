//
//  VSCCDetailModelClass.h
//  ValShare
//
//  Created by Vivek Kumar on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSCCDetailModelClass : NSObject

@property (strong,nonatomic) NSString *amountToBePaid;
@property (strong,nonatomic) NSString *cardId;
@property (strong,nonatomic) NSString *cardNumber;
@property (strong,nonatomic) NSString *cardType;
@property (strong,nonatomic) NSString *cardTypeId;
@property (strong,nonatomic) NSString *creditDebitCardInfoStatusId;
@property (strong,nonatomic) NSString *creditDebitCardInfoStatus;

@property (strong,nonatomic) NSString *expiryDate;
@property (strong,nonatomic) NSString *firstName;
@property (strong,nonatomic) NSString *lastModifiedTime;
@property (strong,nonatomic) NSString *lastName;
@property (strong,nonatomic) NSString *loyaltyPrice;
@property (strong,nonatomic) NSString *packagePrice;
@property (strong,nonatomic) NSString *paymentPackageId;
@property (strong,nonatomic) NSString *statusTypeId;
@property (strong,nonatomic) NSString *totalLoyalityPoint;
@property (strong,nonatomic) NSString *userId;

@property (strong,nonatomic) NSString *isDefault;
@property (strong,nonatomic) NSString *streetAddress;
@property (strong,nonatomic) NSString *city;
@property (strong,nonatomic) NSString *country;
@property (strong,nonatomic) NSString *zipCode;

@end
