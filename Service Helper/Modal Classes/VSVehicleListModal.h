//
//  VSVehicleListModal.h
//  ValShare
//
//  Created by Sharda Prasad on 4/17/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSVehicleListModal : NSObject

@property (nonatomic, strong) NSString *isDefault;
@property (nonatomic, strong) NSString *lastModifiedTime;
@property (nonatomic, strong) NSString *registrationNumber;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userVehicleInfoId;
@property (nonatomic, strong) NSString *userVehicleInfoStatus;
@property (nonatomic, strong) NSString *userVehicleInfoStatusId;
@property (nonatomic, strong) NSString *vehicleDocumentModelList;
@property (nonatomic, strong) NSString *vehicleModelList;
@property (nonatomic, strong) NSString *vehicleType;
@property (nonatomic, strong) NSString *vehicleTypeId;
@property (nonatomic, strong) NSString *vehicleTypeName;

@end
