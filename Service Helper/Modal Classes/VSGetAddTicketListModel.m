//
//  VSGetAddTicketListModel.m
//  ValShare
//
//  Created by Vivek Kumar on 5/5/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSGetAddTicketListModel.h"

@implementation VSGetAddTicketListModel

@synthesize isActive;
@synthesize lastModifiedTime;
@synthesize report;
@synthesize reportStatusType;
@synthesize reportStatusTypeId;
@synthesize reportTypeId;

@end
