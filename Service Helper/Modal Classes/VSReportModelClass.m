//
//  VSReportModelClass.m
//  ValShare
//
//  Created by Vivek Kumar on 4/20/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSReportModelClass.h"

@implementation VSReportModelClass

@synthesize addedBy;
@synthesize createdTime;
@synthesize myDescription;
@synthesize hostId;
@synthesize isHost;
@synthesize lastModifiedTime;
@synthesize latitude;
@synthesize location;
@synthesize longitude;
@synthesize price;
@synthesize radiusofImpact;
@synthesize reportId;
@synthesize reportStatus;
@synthesize reportStatusId;
@synthesize reportType;
@synthesize reportTypeId;
@synthesize reporterEmail;
@synthesize statusType;
@synthesize thumbsDown;
@synthesize thumbsUp;
@synthesize userId;

@end
