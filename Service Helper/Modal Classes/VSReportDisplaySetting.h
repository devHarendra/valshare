//
//  VSReportDisplaySetting.h
//  ValShare
//
//  Created by Sharda Prasad on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSReportDisplaySetting : NSObject

@property (nonatomic, strong) id        distanceUnitDictionary;
@property (nonatomic, strong) id        reportTypeIdList;
@property (nonatomic, strong) id        timeUnitDictionary;
@property (nonatomic, strong) id        reportTypeDictionary;

@property (nonatomic, strong) NSString *distanceUnitId;
@property (nonatomic, strong) NSString *reportDisplaySettingId;
@property (nonatomic, strong) NSString *reportRange;
@property (nonatomic, strong) NSString *reportTime;
@property (nonatomic, strong) NSString *reportTypeId;
@property (nonatomic, strong) NSString *timeUnitId;
@property (nonatomic, strong) NSString *userId;

@end
