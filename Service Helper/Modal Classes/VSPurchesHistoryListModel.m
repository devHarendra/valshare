//
//  VSPurchesHistoryList.m
//  ValShare
//
//  Created by Vivek Kumar on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSPurchesHistoryListModel.h"

@implementation VSPurchesHistoryListModel

@synthesize cashPrice;
@synthesize loyaltyPoint;
@synthesize loyaltyPointEquivalent;
@synthesize nextPaymentDate;
@synthesize packagePrice;
@synthesize paymentPackageId;
@synthesize puchaseHistoryId;
@synthesize purcahseHistoryStatusId;
@synthesize purchaseHistoryModelList;

@synthesize cardNumber;
@synthesize createdTime;
@synthesize email;
@synthesize packageName;
@synthesize purcahseHistoryStatus;
@synthesize transactionId;
@synthesize userId;



@end
