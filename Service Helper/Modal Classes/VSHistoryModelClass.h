//
//  VSHistoryModelClass.h
//  ValShare
//
//  Created by Vivek Kumar on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSHistoryModelClass : NSObject

@property (strong , nonatomic) NSString *cardNumber;
@property (strong , nonatomic) NSString *cashPrice;
@property (strong , nonatomic) NSString *city;
@property (strong , nonatomic) NSString *country;
@property (strong , nonatomic) NSString *createdTime;
@property (strong , nonatomic) NSString *effectiveFrom;
@property (strong , nonatomic) NSString *effectiveTo;
@property (strong , nonatomic) NSString *lastModifiedTime;
@property (strong , nonatomic) NSString *loyaltyPoint;
@property (strong , nonatomic) NSString *loyaltyPointEquivalent;
@property (strong , nonatomic) NSString *packageName;
@property (strong , nonatomic) NSString *packagePrice;
@property (strong , nonatomic) NSString *paymentPackageId;
@property (strong , nonatomic) NSString *puchaseHistoryId;
@property (strong , nonatomic) NSString *purcahseHistoryStatus;
@property (strong , nonatomic) NSString *purcahseHistoryStatusId;
@property (strong , nonatomic) NSString *streetAddress;
@property (strong , nonatomic) NSString *transactionId;
@property (strong , nonatomic) NSString *userId;
@property (strong , nonatomic) NSString *zipCode;

@end
