//
//  VSGetPackByMonth&YearModel.m
//  ValShare
//
//  Created by Vivek Kumar on 4/21/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSGetPackByMonth&YearModel.h"

@implementation VSGetPackByMonth_YearModel

@synthesize  effectiveDays;
@synthesize isActive;
@synthesize packageId;
@synthesize packageName;
@synthesize packagePrice;
@synthesize packageStatusId;
@synthesize userId;
@synthesize monthly;
@synthesize yearly;
@synthesize daily;
@synthesize hourly;
@synthesize packageDictionary;
@synthesize packageDictionaryKey;
@synthesize packageDictionaryObject;

@end
