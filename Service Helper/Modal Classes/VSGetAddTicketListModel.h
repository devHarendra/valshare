//
//  VSGetAddTicketListModel.h
//  ValShare
//
//  Created by Vivek Kumar on 5/5/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSGetAddTicketListModel : NSObject

@property (strong ,nonatomic) NSString *isActive;
@property (strong ,nonatomic) NSString *lastModifiedTime;
@property (strong ,nonatomic) NSString *report;
@property (strong ,nonatomic) NSString *reportStatusType;
@property (strong ,nonatomic) NSString *reportStatusTypeId;
@property (strong ,nonatomic) NSString *reportTypeId;

@end
