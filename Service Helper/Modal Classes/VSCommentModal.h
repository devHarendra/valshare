//
//  VSCommentModal.h
//  ValShare
//
//  Created by Sharda Prasad on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSCommentModal : NSObject

@property (nonatomic, strong) NSString *comment;
@property (nonatomic, strong) NSString *lastModifiedTime;
@property (nonatomic, strong) NSString *reportCommentId;
@property (nonatomic, strong) NSString *reportCommentStatusType;
@property (nonatomic, strong) NSString *reportId;
@property (nonatomic, strong) NSString *reportType;
@property (nonatomic, strong) NSString *thumbsDown;
@property (nonatomic, strong) NSString *thumbsUp;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *yourChoice;
@end
