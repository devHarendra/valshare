//
//  MyAnnonationClass.m
//  VolShare
//
//  Created by Vivek Kumar on 4/13/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import "MyAnnonationClass.h"

@implementation MyAnnonationClass

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;
@synthesize color;
@synthesize image;
@synthesize anotationType;

@end
