//
//  VSReportVote.h
//  ValShare
//
//  Created by Sharda Prasad on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSReportVote : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *reportId;
@property (nonatomic, strong) NSString *reportVote;
@property (nonatomic, strong) NSString *thumbsUp;
@property (nonatomic, strong) NSString *thumbsDown;
@end
