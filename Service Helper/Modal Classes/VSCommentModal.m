//
//  VSCommentModal.m
//  ValShare
//
//  Created by Sharda Prasad on 4/8/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSCommentModal.h"

@implementation VSCommentModal

@synthesize comment;
@synthesize lastModifiedTime;
@synthesize reportCommentId;
@synthesize reportCommentStatusType;
@synthesize reportId;
@synthesize reportType;
@synthesize thumbsDown;
@synthesize thumbsUp;
@synthesize userId;
@synthesize userName;
@synthesize yourChoice;

@end
