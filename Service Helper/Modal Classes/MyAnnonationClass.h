//
//  MyAnnonationClass.h
//  VolShare
//
//  Created by Vivek Kumar on 4/13/15.
//  Copyright (c) 2015 Vivek Kumar. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "ZSPinAnnotation.h"

@interface MyAnnonationClass : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy)     NSString            *title;
@property (nonatomic, copy)     NSString            *subtitle;
@property (nonatomic, strong)   UIColor             *color;
@property (nonatomic, strong)   UIImage             *image;
@property (nonatomic, copy)     NSString            *anotationType;



@end
