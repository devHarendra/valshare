//
//  VSDirectionModal.h
//  ValShare
//
//  Created by Sharda Prasad on 3/31/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSDirectionModal : NSObject

@property (nonatomic, strong) NSString *destination;
@property (nonatomic, strong) NSString *drivingMode;
@property (nonatomic, strong) NSString *lastModifiedTime;
@property (nonatomic, strong) NSString *routeEncodedPath;
@property (nonatomic, strong) NSString *routeId;
@property (nonatomic, strong) NSString *routeIndexId;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) NSString *sourceuserId;

@property (nonatomic, strong) NSString *localRouteDestination;
@property (nonatomic, strong) NSString *localRouteDistance;
@property (nonatomic, strong) NSString *localRouteDrivingMode;
@property (nonatomic, strong) NSString *localRouteDuration;
@property (nonatomic, strong) NSString *localRouteRouteEncodedPath;
@property (nonatomic, strong) NSString *localRouteRouteId;
@property (nonatomic, strong) NSString *localRouteRouteIndexId;
@property (nonatomic, strong) NSString *localRouteSource;
@property (nonatomic, strong) NSString *localRouteSummary;
@property (nonatomic, strong) NSString *localRouteUserId;

@property (nonatomic, strong) NSString *reportModalAddedBy;
@property (nonatomic, strong) NSString *reportModalAddedByName;
@property (nonatomic, strong) NSString *reportModalDescription;
@property (nonatomic, strong) NSString *reportModalHostId;
@property (nonatomic, strong) NSString *reportModalIsHost;
@property (nonatomic, strong) NSString *reportModalLastModifiedTime;
@property (nonatomic, strong) NSString *reportModalLatitude;
@property (nonatomic, strong) NSString *reportModalLocation;
@property (nonatomic, strong) NSString *reportModalLongitude;
@property (nonatomic, strong) NSString *reportModalPrice;
@property (nonatomic, strong) NSString *reportModalRadiusofImpact;
@property (nonatomic, strong) NSString *reportModalReportId;

@property (nonatomic, strong) NSString *reportModalReportStatus;
@property (nonatomic, strong) NSString *reportModalReportStatusId;
@property (nonatomic, strong) NSString *reportModalReportType;
@property (nonatomic, strong) NSString *reportModalReportTypeId;
@property (nonatomic, strong) NSString *reportModalStatusType;
@property (nonatomic, strong) NSString *reportModalThumbsDown;
@property (nonatomic, strong) NSString *reportModalThumbsUp;
@property (nonatomic, strong) NSMutableArray *reportVideoModelList;
@property (nonatomic, assign) float ReportModelDistance;

@property (nonatomic, strong) NSString *reportModalUserId;



@end
