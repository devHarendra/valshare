//
//  VSReportVideo_ImageList.h
//  ValShare
//
//  Created by Vivek Kumar on 5/6/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSReportVideo_ImageList : NSObject

@property (strong ,nonatomic) NSString *imageName;
@property (strong ,nonatomic) NSString *videoName;
@property (strong ,nonatomic) NSString *documentType;

-(id)initWithCode:(NSString *)image_name
             Video_Name:(NSString *)video_name
       Document:(NSString *)documen_type;

- (NSMutableDictionary *)toNSDictionary;

@end
