//
//  VSRegistrationType.m
//  ValShare
//
//  Created by Sharda Prasad on 4/22/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSRegistrationType.h"

@implementation VSRegistrationType

@synthesize genderTypeKey;
@synthesize accountTypeKey;
@synthesize genderTypeObject;
@synthesize accountTypeObject;

@end
