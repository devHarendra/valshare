//
//  VSCommanReportModal.h
//  ValShare
//
//  Created by Sharda Prasad on 4/18/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSCommanReportModal : NSObject

@property (nonatomic, strong) NSString *distanceUnitKey;
@property (nonatomic, strong) NSString *distanceUnitValue;

@property (nonatomic, strong) NSString *reportTypeKey;
@property (nonatomic, strong) NSString *reportTypeValue;

@property (nonatomic, strong) NSString *timeUnitKey;
@property (nonatomic, strong) NSString *timeUnitValue;
@end
