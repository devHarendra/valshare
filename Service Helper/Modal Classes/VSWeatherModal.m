//
//  VSWeatherModal.m
//  ValShare
//
//  Created by Sharda Prasad on 4/7/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSWeatherModal.h"

@implementation VSWeatherModal

@synthesize status;
@synthesize userId;
@synthesize averageTemprature;
@synthesize city;
@synthesize country;
@synthesize dateTime;
@synthesize description;
@synthesize humadity;
@synthesize maximumTemprature;
@synthesize minimumTemprature;
@synthesize pressure;
@synthesize shortDescription;
@synthesize weatherModelList ;
@synthesize windSpeed;

@end
