//
//  UserDetailModel.h
//  ValShare
//
//  Created by Vivek Kumar on 4/14/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSUserDetailModel : NSObject

@property (nonatomic, strong) NSString *dob;
@property (nonatomic, strong) NSString *accountType;
@property (nonatomic, strong) NSString *accountTypeId;
@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) NSString *contactNumber;
@property (nonatomic, strong) NSString *countryId;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *genderTypeId;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSString *languageId;
@property (nonatomic, strong) NSString *lastModifiedTime;
@property (nonatomic, strong) NSString *locationId;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userNameRequired;
@property (nonatomic, strong) NSString *userRoleId;
@property (nonatomic, strong) NSString *userStatusId;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *addressOne;
@property (nonatomic, strong) NSString *addressTwo;
@property (nonatomic, strong) NSString *zipCode;
@property (nonatomic, strong) NSString *nextPaymentDate;

@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;

@property (nonatomic, strong) id cityDictionary;
@property (nonatomic, strong) id countryDictionary;
@property (nonatomic, strong) id genderDictionary;
@property (nonatomic, strong) id languageDictionary;
@property (nonatomic, strong) id userModelList;


@property (nonatomic, strong) NSString *genderKey;
@property (nonatomic, strong) NSString *genderObject;
@property (nonatomic, strong) NSString *cityKey;
@property (nonatomic, strong) NSString *cityObject;
@property (nonatomic, strong) NSString *countryKey;
@property (nonatomic, strong) NSString *countryObject;
@property (nonatomic, strong) NSString *languageKey;
@property (nonatomic, strong) NSString *languageObject;


@end
