//
//  VSGetPackByMonth&YearModel.h
//  ValShare
//
//  Created by Vivek Kumar on 4/21/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSGetPackByMonth_YearModel : NSObject

@property (strong , nonatomic) NSString *effectiveDays;
@property (strong , nonatomic) NSString *isActive;
@property (strong , nonatomic) NSString *packageId;
@property (strong , nonatomic) NSString *packageName;
@property (strong , nonatomic) NSString *packagePrice;
@property (strong , nonatomic) NSString *packageStatusId;
@property (strong , nonatomic) NSString *userId;
@property (strong , nonatomic) NSString *monthly;
@property (strong , nonatomic) NSString *yearly;
@property (strong , nonatomic) NSString *daily;
@property (strong , nonatomic) NSString *hourly;

@property (nonatomic, strong) id packageDictionary;
@property (nonatomic, strong) NSString *packageDictionaryKey;
@property (nonatomic, strong) NSString *packageDictionaryObject;

@end
