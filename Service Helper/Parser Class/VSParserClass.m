//
//  VSParserClass.m
//  ValShare
//
//  Created by Sharda Prasad on 3/25/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSParserClass.h"

@interface VSParserClass () {
    
    NSURLConnection * connection;
    NSMutableData * webData;
}

@property (nonatomic, copy, readwrite) CompletionBlock completionBlock;
@end

@implementation VSParserClass
@synthesize completionBlock;

static VSParserClass * sharedParser;


#pragma mark - Initialization Method

+ (void)initialize {
    
    static BOOL initialized = NO;
    if(!initialized) {
        
        initialized = YES;
        sharedParser = [[VSParserClass alloc] init];
    }
}

#pragma mark - Singleton Object

+ (VSParserClass *)sharedParser {
    
    return (sharedParser);
}

/**
 *  UserLogin Method
 *
 *  @param Email            Email
 *  @param Name             Name
 *  @param Name             Mobile
 *  @param Name             DeviceId
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareUserEmailLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSLoginService]];
    
    NSLog(@"valShareUserLogin :- %@ --- %@", serviceURL, dictData);
    
    [self getRequestForPOST:serviceURL Data:dictData WithCompletionBlock:block];
}


/**
 *  UserLogin with UserName Method
 *
 *  @param UserName         UserName
 *  @param Name             DeviceId
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareUserNameLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSSignInWithUserName]];
    [self getRequestForPOST:serviceURL Data:dictData WithCompletionBlock:block];
}

/**
 *  FaceBook Post Method
 *
 *  @param Email            Email
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareFaceBookLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSSignInWithFacebook],[dictData valueForKey:@"email"]];
    [self getRequestForGET:serviceURL Data:nil WithCompletionBlock:block];
}

/**
 *  Twitter Method
 *
 *  @param Email            Email
 *  @param block Block Returning Success OR Failure
 */

/**
 *  Gmail Post Method
 *
 *  @param Email            Email
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareGmailLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSSignInWithGmail],[dictData valueForKey:@"email"]];
    [self getRequestForGET:serviceURL Data:nil WithCompletionBlock:block];
}



-(void)valShareTwitterLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSSignInWithTwitter]];
    [self getRequestForPOST:serviceURL Data:dictData WithCompletionBlock:block];

}

/**
 *  check Varify TokenGet Method
 *
 *  @param userId      userId
 *  @param block Block Returning Success OR Failure
 */

-(void)checkVerifyToken:(NSString *)userID Token:(NSString *)token WithCompletionBlock:(CompletionBlock)block {
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
    [dictData setObject:token forKey:@"token"];
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSVarifyToken],userID];
    [self getRequestForVarificatioToken:serviceURL Data:token WithCompletionBlock:block];
}
-(void)loginUsingGmail:(NSString *)userID Token:(NSString *)token WithCompletionBlock:(CompletionBlock)block {
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSSignInWithGmail]];
    [self getRequestForVarificatioToken:serviceURL Data:userID WithCompletionBlock:block];

}



/**
 *  Resend Token Method
 *
 *  @param userId      userId
 *  @param block Block Returning Success OR Failure
 */

-(void)resendToken:(NSString *)userID WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSResendToken],userID];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}


/**
 *  Forgot password Method
 *
 *  @param email      email
 *  @param block Block Returning Success OR Failure
 */

-(void)forgotPassword:(NSString *)email WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@"@",getPassword",[VSEnumClass servicesName:VSForgotPassword],email];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

/**
 *  weatherInformation Method
 *
 *  @param UserId      UserId
 *  @param block Block Returning Success OR Failure
 */

-(void)weatherInformation:(NSString *)userId WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSWeatherInformation],userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

/**
 *  UserSignUp Post Method
 *
 *  @param Email            Email
 *  @param UserName         UserName
 *  @param DOB              DOB
 *  @param Password         Password
 *  @parm  UserId           UserId
 *  @param RoleTypeId       RoleTypeId
 *  @param Status           Status
 *  @param Language         Language
 *  @param ContactNumber    ContactNumber
 *  @parm  Location         Location
 *  @param AccountType      accountType
 *  @param LastName         LastName
 *  @param FirstName        DOB
 *  @param NextPaymentDate  NextPaymentDate
 *  @parm  Gender           UserId
 *  @param GenderTypeId     GenderTypeId
 *  @param AccountTypeId    AccountTypeId
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareUserSignUp:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSSignUpService]];
    
    [self getRequestForPOST:serviceURL Data:dictData WithCompletionBlock:block];
}

/**
 *  routeservice Get Method
 *
 *  @param routeID      RouteID
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareRouteservice:(NSDictionary *)routeDict WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSGetRoute]];
    [self getRequestForPOST:serviceURL Data:routeDict WithCompletionBlock:block];
}

/**
 *  saveroute Get Method
 *
 *  @param routeID      RouteID
 *  @param block Block Returning Success OR Failure
 */
-(void)saveRouteService:(NSDictionary *)routeDict WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSSaveRouteService]];
    [self getRequestForPOST:serviceURL Data:routeDict WithCompletionBlock:block];
}
/**
 *  Check Email Method
 *
 *  @param Email      Email
 *  @param block Block Returning Success OR Failure
 */
-(void)checkEmail:(NSString *)email WithCompletionBlock:(CompletionBlock)block {

    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSCheckEmail],email];
    [self getRequestForGET:serviceURL Data:email WithCompletionBlock:block];
}


/*
 *  Check First name and password in add card.
 *  @param First Name Or Last Name
 *  @param block Block Returning Success OR Failure
 */
-(void)checkFirstOrLastName:(NSString *)name WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSCheckCardName],name];
    [self getRequestForGET:serviceURL Data:name WithCompletionBlock:block];
}

/**
 *  check UserName Get Method
 *
 *  @param userName      userName
 *  @param block Block Returning Success OR Failure
 */

-(void)checkUserName:(NSString *)userName WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSCheckUserName],userName];
    [self getRequestForGET:serviceURL Data:userName WithCompletionBlock:block];
}

/*
 *  Check Commercial User.
 *  @param First Name Or Last Name
 *  @param block Block Returning Success OR Failure
 */
-(void)checkCommercialUser:(NSString *)companyID WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSCheckCommercialUser],companyID];
    [self getRequestForGET:serviceURL Data:companyID WithCompletionBlock:block];
}

/**
 *  thumbsUp Method
 *
 *  @param UserId      UserId
 *  @param ReportId    ReportId
 *  @param block Block Returning Success OR Failure
 */

-(void)thumbsUpService:(NSString *)reportId UserID:(NSString *)userId WithCompletionBlock:(CompletionBlock)block {
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@,%@",[VSEnumClass servicesName:VSThumbsUp],reportId,userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

/**
 *  thumbsDown Method
 *
 *  @param UserId      UserId
 *  @param ReportId    ReportId
 *  @param block Block Returning Success OR Failure
 */

-(void)thumbsDownService:(NSString *)reportId UserID:(NSString *)userId WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@,%@",[VSEnumClass servicesName:VSThumbsDown],reportId,userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

/**
 *  reportcommentmanager Method
 *
 *  @param ats_system      ats_system
 *  @param reportcommentmanager    reportcommentmanager
 *  @param block Block Returning Success OR Failure
 */

-(void)reportCommentManager:(NSMutableDictionary *)comment WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSMapComment]];
    [self getRequestForPOST:serviceURL Data:comment WithCompletionBlock:block];
}

/**
 *  reportVote Method
 *
 *  @param UserId      UserId
 *  @param markerid    markerid = ticketid
 *  @param block Block Returning Success OR Failure
 */

-(void)reportVoteService:(NSString *)markerid UserID:(NSString *)userId WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@,%@",[VSEnumClass servicesName:VSReportVote1],markerid,userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}


/**
 *  Get Comment Method
 *
 *  @param User ID      User ID
 *  @param report ID    report ID
 *  @param block Block Returning Success OR Failure
 */

-(void)getComment:(NSString *)reportId UserID:(NSString *)userId WithCompletionBlock:(CompletionBlock)block {
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@,%@",[VSEnumClass servicesName:VSGetComment],reportId,userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}


/**
 *  Get Comment Method
 *
 *  @param User ID      User ID
 *  @param report ID    Report ID
 *  @param Email        Email
 *  @param block Block Returning Success OR Failure
 */

-(void)shareReportInGmail:(NSString *)reportId UserID:(NSString *)userId Email:(NSString *)email WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@,%@,%@",[VSEnumClass servicesName:VSShareDataWithGmail],reportId,userId,email];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)valShareResetPassword:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSResetPassword]];
    
    [self getRequestForPOST:serviceURL Data:dictData WithCompletionBlock:block];
}

-(void)valShareFeedback:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSFeedback]];
    
    NSLog(@"valShareFeedback :- %@ --- %@", serviceURL, dictData);
    
    [self getRequestForPOST:serviceURL Data:dictData WithCompletionBlock:block];
}

-(void)valShareCardDetail:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSAddCard]];
    [self getRequestForPOST:serviceURL Data:dictData WithCompletionBlock:block];
}


-(void)valShareUpdateProfile:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSUpdateProfile]];
    [self getRequestForPOST:serviceURL Data:dictData WithCompletionBlock:block];
}

-(void)valShareGetUserDetail:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSGetProfile],userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)getPurchesHistoryList:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSPurchesHistoryList],userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)getPurchesHistory:(NSString *)purchesHistoryId  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSPurchesHistory],purchesHistoryId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
    
}

-(void)deletePurchesHistory:(NSString *)deletePurchesHistoryId  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSDeletePurchesHistory],deletePurchesHistoryId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)VSGetCardDetail:(NSString *)cardid WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@,%@",[VSEnumClass servicesName:VSGetCardDetail],cardid];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)VSGetReportDetail:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@,%@",[VSEnumClass servicesName:VSGetReportsView],userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)VSDeleteCCDetail:(NSString *)cardid WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@,%@",[VSEnumClass servicesName:VSDeleteCCDetail],cardid];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)VSSaveReportDetail:(NSMutableDictionary *)saveReport  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSReportDetailSave]];
    [self getRequestForPOST:serviceURL Data:saveReport WithCompletionBlock:block];
}
-(void)VSDeletRoute:(NSString *)routeId  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSDeletRoute],routeId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)VSGetPackByMonth:(NSString *)userId ByMonth:(NSString *)bymonth WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@,%@",[VSEnumClass servicesName:VSGetPackByMonth],userId,bymonth];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)VSGetPackByYearly:(NSString *)userId ByYearly:(NSString *)byyear WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@,%@",[VSEnumClass servicesName:VSGetPackByYearly],userId,byyear];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)getDefaultCardId:(NSString *)userId PackageID:(NSString *)packageId WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@,%@",[VSEnumClass servicesName:VSGetDefaultCardDetail],userId,packageId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

-(void)VSPaymentProcess:(NSDictionary *)paymentData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSPaymentProcess]];
    [self getRequestForPOST:serviceURL Data:paymentData WithCompletionBlock:block];
}

-(void)VSAddTicket:(NSMutableDictionary *)saveAddTicketDetail  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSAddTcket]];
    [self getRequestForPOST:serviceURL Data:saveAddTicketDetail WithCompletionBlock:block];
}

-(void)VSGetAddTicketList:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@,%@",[VSEnumClass servicesName:VSAddTcketList],userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}
-(void)VSUpadetReport:(NSMutableDictionary *)updateReport  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSUpadetReport]];
    [self getRequestForPOST:serviceURL Data:updateReport WithCompletionBlock:block];
}

-(void)valShareGetCityList:(NSString *)countryCode  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSCityList],countryCode];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}
/**
 *  Get MyRoutes
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */
-(void)getMyRoutesList:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSGetRouteList],userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}


/**
 *  Get selected route details for plptting on map
 *  @param Routeid
 *  @param block Block Returning Success OR Failure
 */
-(void)getSelectedRouteDetails:(NSString *)Routeid  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSGetselectedRouteDetail],Routeid];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}






/**
 *  Get Vehicle List
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */
-(void)getUserVehicleList:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@,%@",[VSEnumClass servicesName:VSGetVehicleList],userId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}


/**
 *  Add Vehicle
 *  @param registrationNumber
 *  @param vehicleTypeName
 *  @param userId
 *  @param vehicleType
 *  @param isDefault = true or false
 *  @param VehicleTypeId = 1 or 2, 3,
 *  @param block Block Returning Success OR Failure
 */
-(void)getAddVehicle:(NSMutableDictionary *)addData  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSAddVehicle]];
    [self getRequestForPOST:serviceURL Data:addData WithCompletionBlock:block];
}

/**
 *  Get Delete Vehicle
 *  @param VehicleID
 *  @param block Block Returning Success OR Failure
 */
-(void)getDeleteVehicleList:(NSString *)vehicleId  WithCompletionBlock:(CompletionBlock)block {
    NSString *serviceURL = [NSString stringWithFormat:@"%@,%@",[VSEnumClass servicesName:VSDeleteVehicle],vehicleId];
    
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

/*
 *  Get delete/ats_system,reportmanager,2
 *  @param reportId
 *  @param block Block Returning Success OR Failure
 */
-(void)getMyReportDelete:(NSString *)reportId WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@,%@",[VSEnumClass servicesName:VSDeleteMyReports],reportId];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}
/**
 *  Get Update Vehicle
 *  @param registrationNumber
 *  @param vehicleTypeName
 *  @param userId
 *  @param vehicleType
 *  @param isDefault = true or false
 *  @param VehicleTypeId = 1 or 2, 3,
 *  @param block Block Returning Success OR Failure
 */
-(void)getUpdateVehicleList:(NSMutableDictionary *)updateData  WithCompletionBlock:(CompletionBlock)block {
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSUpdateVehicle]];
    [self getRequestForPOST:serviceURL Data:updateData WithCompletionBlock:block];
}

/**
 *  getreportdisplaysetting Methods
 *  @param userID
 *  @param block Block Returning Success OR Failure
 */
-(void)getReportDisplaySetting:(NSString *)userID  WithCompletionBlock:(CompletionBlock)block {
 
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSGetReportDisplaySetting],userID];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

/**
 *  register
 *  @param block Block Returning Success OR Failure
 */

-(void)getRegistrationTypeWithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSGetRegistrationType]];
    [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
}

/**
 *  add Report Display Setting Methods
 *  @param userId               Int
 *  @param distanceUnitId       Int
 *  @param timeUnitId           Int
 *  @param reportTime           Double
 *  @param reportRange          Float
 *  @param reportTypeId         Int
 *  @param reportTypeIdString   String
 *  @param block Block Returning Success OR Failure
 */
-(void)saveReportDisplaySetting:(NSMutableDictionary *)saveReport  WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@/%@",[VSEnumClass servicesName:VSSaveReportDisplaySetting],[saveReport valueForKey:@"userId"]];
    [self getRequestForPOST:serviceURL Data:saveReport WithCompletionBlock:block];
}

/*
 *  Update card details.
 *  @param First Name Or Last Name
 *  @param block Block Returning Success OR Failure
 */
-(void)updateCardDetails:(NSDictionary *)updateData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSUpdateCard]];
    [self getRequestForPOST:serviceURL Data:updateData WithCompletionBlock:block];
}

-(void)GetTicketReport:(NSString *)TktId WithCompletionBlock:(CompletionBlock)block {
            NSString *serviceURL = [NSString stringWithFormat:@"%@%@",@"resourceservice/get/ats_system,reportmanager,",TktId];
        [self valShareGetJSONResponse:serviceURL WithCompletionBlock:block];
    }


-(void)valShareGetticketsAround:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",[VSEnumClass servicesName:VSGetTicketInitiat]];
    [self getRequestForPOST:serviceURL Data:dictData WithCompletionBlock:block];
}



/*
 *************************************************************************************************
 *AFHTTPRequestOperation JSON Request for POST.
 *************************************************************************************************
 */


-(void)getRequestForVarificatioToken:(NSString *)serviceURL Data:(NSString *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    self.completionBlock = block;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[VSEnumClass getBaseURL],serviceURL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSLog(@"requestrequest ------:- %@ -- %@", url, dictData);
    
    [request setHTTPMethod:@"POST"];
    [request setValue:kTextText forHTTPHeaderField:kContent_Type];
    [request setTimeoutInterval:400];
    
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictData options:0 error:&error];
//    NSString *JSONString;
//    if (!jsonData) {
//        NSLog(@"JSON error: %@", error);
//    } else {
//        JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
//        NSLog(@"------JSON OUTPUT: %@",JSONString);
//    }
    
    NSData *postData123 = [dictData dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    //NSData *postdata = [NSJSONSerialization dataWithJSONObject:dictData options:0 error:&error];
    [request setHTTPBody:postData123];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        block (YES, responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"--------Error text: %@", error);
        dispatch_async(dispatch_get_main_queue(), ^{
            block (NO, nil, error);
        });
    }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}

-(void)getRequestForPOST:(NSString *)serviceURL Data:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block {
    
    NSError *error;
    self.completionBlock = block;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[VSEnumClass getBaseURL],serviceURL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSLog(@"requestrequest ------:- %@ --", url);
    
    [request setHTTPMethod:@"POST"];
    [request setValue:kTextText forHTTPHeaderField:kContent_Type];
    [request setTimeoutInterval:400];

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictData options:0 error:&error];
    NSString *JSONString;
    if (!jsonData)
    {
        NSLog(@"JSON error: %@", error);
    } else
    {
        JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        NSLog(@"------JSON OUTPUT: %@",JSONString);
    }
    
    NSData *postData123 = [JSONString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];

    //NSData *postdata = [NSJSONSerialization dataWithJSONObject:dictData options:0 error:&error];
    [request setHTTPBody:postData123];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        block (YES, responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"--------Error text: %@", error);
        dispatch_async(dispatch_get_main_queue(), ^{
            block (NO, nil, error);
        });
    }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}


/**
 *  Search suggestation data Get Method
 *
 *  @param SearchText      SearchText
 *  @param block Block Returning Success OR Failure
 */
-(void)volShareGetLocationBySearch:(NSString *)searchText WithCompletionBlock:(CompletionBlock)block {
    
    self.completionBlock = block;
    
    //NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=true&radius=5",searchText];
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&radius=500&sensor=true&key=AIzaSyDf96c8DL6XMrPP-Akzo4JxlECytSUrJcg",searchText];

    NSString *newString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:newString]];
    
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:400];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        block (YES, responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"--------Error text: %@", error);
        block (NO, nil, error);
    }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}

/*
 *******************************************************************************************************
 *AFHTTPRequestOperation JSON Request For GET.
 *******************************************************************************************************
 */

/**
 *  Search suggestation data Get Method
 *
 *  @param SearchText      SearchText
 *  @param block Block Returning Success OR Failure
 */
-(void)valShareGetJSONResponse:(NSString *)serviceURL WithCompletionBlock:(CompletionBlock)block {
    
    self.completionBlock = block;
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[VSEnumClass getBaseURL],serviceURL];
    
    NSLog(@"serviceURLserviceURLserviceURLserviceURLserviceURL :-- %@", urlString);

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:400];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {

        block (YES, responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"--------Error text: %@", error);
        block (NO, nil, error);
    }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}

-(void)getRequestForGET:(NSString *)serviceURL Data:(NSString *)stringData WithCompletionBlock:(CompletionBlock)block {
    
    self.completionBlock = block;
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[VSEnumClass getBaseURL],serviceURL];
    
    NSLog(@"getRequestForGETurlStringurlString :- %@", urlString);
    
    NSString *newString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:newString]];
   
    [request setHTTPMethod:@"GET"];
    [request setValue:kTextText forHTTPHeaderField:kContent_Type];
    [request setTimeoutInterval:400];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"valShareGetJSONResponse -- urlStringurlString :- %@", string);

        block (YES, responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"----Get----Error text: %@", error);
        block (NO, nil, error);
    }];
    [[NSOperationQueue mainQueue] addOperation:operation];
}

@end