//
//  VSParserClass.h
//  ValShare
//
//  Created by Sharda Prasad on 3/25/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "VSHeader.h"
#import "XMLReader.h"
#import "VSEnumClass.h"
#import "AFNetworking.h"
#import <Foundation/Foundation.h>

@interface VSParserClass : NSObject


/**
 *  Singleton Object
 *
 *  @return VSParserClass Singleton Class
 */
+(VSParserClass *)sharedParser;


/**
 *  UserLogin Post Method
 *
 *  @param Email            Email
 *  @param Password         Password
 *  @param GenderTypeId     GenderTypeId
 *  @param DOB              DOB
 *  @param AccountTypeId    AccountTypeId
 *  @param UserId           UserId
 *  @param RoleTypeId       RoleTypeId
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareUserEmailLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

/**
 *  FaceBook Post Method
 *
 *  @param UserName    UserName
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareUserNameLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

/**
 *  UserName Method
 *
 *  @param email    email
 *  @param block Block Returning Success OR Failure
 */
-(void)valShareFaceBookLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

/**
 *  Twitter Method
 *
 *  @param Email            Email
 *  @param block Block Returning Success OR Failure
 */

/**
 *  Gmail Method
 *
 *  @param email    email
 *  @param block Block Returning Success OR Failure
 */
-(void)valShareGmailLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;


-(void)valShareTwitterLogin:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

/**
 *  UserSignUp Post Method
 *
 *  @param Email            Email
 *  @param UserName         UserName
 *  @param DOB              DOB
 *  @param Password         Password
 *  @parm  UserId           UserId
 *  @param RoleTypeId       RoleTypeId
 *  @param Status           Status
 *  @param Language         Language
 *  @param ContactNumber    ContactNumber
 *  @parm  Location         Location
 *  @param AccountType      accountType
 *  @param LastName         LastName
 *  @param FirstName        DOB
 *  @param NextPaymentDate  NextPaymentDate
 *  @parm  Gender           UserId
 *  @param GenderTypeId     GenderTypeId
 *  @param AccountTypeId    AccountTypeId
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareUserSignUp:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

/**
 *  routeservice Get Method
 *
 *  @param routeID      RouteID
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareRouteservice:(NSDictionary *)routeDict WithCompletionBlock:(CompletionBlock)block;

/**
 *  saveroute Get Method
 *
 *  @param routeID      RouteID
 *  @param block Block Returning Success OR Failure
 */
-(void)saveRouteService:(NSDictionary *)routeDict WithCompletionBlock:(CompletionBlock)block;

/**
 *  Search suggestation data Get Method
 *
 *  @param SearchText      SearchText
 *  @param block Block Returning Success OR Failure
 */
-(void)volShareGetLocationBySearch:(NSString *)searchText WithCompletionBlock:(CompletionBlock)block;

/**
 *  Check Email Method
 *
 *  @param Email      Email
 *  @param block Block Returning Success OR Failure
 */
-(void)checkEmail:(NSString *)email WithCompletionBlock:(CompletionBlock)block;

/**
 *  check UserName Get Method
 *
 *  @param userName      userName
 *  @param block Block Returning Success OR Failure
 */
-(void)checkUserName:(NSString *)userName WithCompletionBlock:(CompletionBlock)block;

/**
 *  check Varify TokenGet Method
 *
 *  @param userId      userId
 *  @param block Block Returning Success OR Failure
 */
-(void)checkVerifyToken:(NSString *)userID Token:(NSString *)token WithCompletionBlock:(CompletionBlock)block;

/**
 *  Resend Token Method
 *
 *  @param userId      userId
 *  @param block Block Returning Success OR Failure
 */
-(void)resendToken:(NSString *)userID WithCompletionBlock:(CompletionBlock)block;


-(void)loginUsingGmail:(NSString *)userID Token:(NSString *)token WithCompletionBlock:(CompletionBlock)block;

/**
 *  Forgot password Method
 *
 *  @param email      email
 *  @param block Block Returning Success OR Failure
 */
-(void)forgotPassword:(NSString *)email WithCompletionBlock:(CompletionBlock)block;

/**
 *  weatherInformation Method
 *
 *  @param UserId      UserId
 *  @param block Block Returning Success OR Failure
 */
-(void)weatherInformation:(NSString *)userId WithCompletionBlock:(CompletionBlock)block;

/**
 *  thumbsUp Method
 *
 *  @param UserId      UserId
 *  @param ReportId    ReportId
 *  @param block Block Returning Success OR Failure
 */
-(void)thumbsUpService:(NSString *)reportId UserID:(NSString *)userId WithCompletionBlock:(CompletionBlock)block;

/**
 *  thumbsDown Method
 *
 *  @param UserId      UserId
 *  @param ReportId    ReportId
 *  @param block Block Returning Success OR Failure
 */
-(void)thumbsDownService:(NSString *)reportId UserID:(NSString *)userId WithCompletionBlock:(CompletionBlock)block;

/**
 *  reportVote Method
 *
 *  @param UserId      UserId
 *  @param markerid    markerid
 *  @param block Block Returning Success OR Failure
 */
-(void)reportVoteService:(NSString *)markerid UserID:(NSString *)userId WithCompletionBlock:(CompletionBlock)block;

/**
 *  reportcommentmanager Method
 *
 *  @param ats_system      ats_system
 *  @param reportcommentmanager    reportcommentmanager
 *  @param block Block Returning Success OR Failure
 */
-(void)reportCommentManager:(NSMutableDictionary *)comment WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get Comment Method
 *
 *  @param User ID      User ID
 *  @param report ID    report ID
 *  @param block Block Returning Success OR Failure
 */
-(void)getComment:(NSString *)reportId UserID:(NSString *)userId WithCompletionBlock:(CompletionBlock)block;

/**
 *  shareReport Method
 *
 *  @param User ID      User ID
 *  @param report ID    Report ID
 *  @param Email        Email
 *  @param block Block Returning Success OR Failure
 */

-(void)shareReportInGmail:(NSString *)reportId UserID:(NSString *)userId Email:(NSString *)email WithCompletionBlock:(CompletionBlock)block;

-(void)valShareResetPassword:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get Feedback
 *
 *  @param message
 *  @param feedbackTypeId
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareFeedback:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get CardDetail
 *
 *  @param first name
 *  @param last name
 *  @param card number
 *  @param expiry date
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareCardDetail:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get UpdateProfile
 *
 *  @param first name
 *  @param last name
 *  @param dob
 *  @param AddressOne
 *  @param Addresstwo
 *  @param gender
 *  @param contact number
 *  @param country
 *  @param city
 *  @param language
 *  @param block Block Returning Success OR Failure
 */

-(void)valShareUpdateProfile:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get GetUserProfile
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */
-(void)valShareGetUserDetail:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get MyRoutes
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */
-(void)getMyRoutesList:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get Vehicle List
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */
-(void)getUserVehicleList:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Add Vehicle
 *  @param registrationNumber
 *  @param vehicleTypeName
 *  @param userId
 *  @param vehicleType
 *  @param isDefault = true or false
 *  @param VehicleTypeId = 1 or 2, 3,
 *  @param block Block Returning Success OR Failure
 */
-(void)getAddVehicle:(NSMutableDictionary *)addData  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get Delete Vehicle
 *  @param VehicleID
 *  @param block Block Returning Success OR Failure
 */
-(void)getDeleteVehicleList:(NSString *)vehicleId  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get Update Vehicle
 *  @param registrationNumber
 *  @param vehicleTypeName
 *  @param userId
 *  @param vehicleType
 *  @param isDefault = true or false
 *  @param VehicleTypeId = 1 or 2, 3,
 *  @param block Block Returning Success OR Failure
 */
-(void)getUpdateVehicleList:(NSMutableDictionary *)updateData  WithCompletionBlock:(CompletionBlock)block;

/**
 *  getreportdisplaysetting Methods
 *  @param userID
 *  @param block Block Returning Success OR Failure
 */
-(void)getReportDisplaySetting:(NSString *)userID  WithCompletionBlock:(CompletionBlock)block;

/**
 *  add Report Display Setting Methods
 *  @param userId               Int
 *  @param distanceUnitId       Int
 *  @param timeUnitId           Int
 *  @param reportTime           Double
 *  @param reportRange          Float
 *  @param reportTypeId         Int
 *  @param reportTypeIdString   String
 *  @param block Block Returning Success OR Failure
 */
-(void)saveReportDisplaySetting:(NSMutableDictionary *)saveReport  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get PurchesHistoryList
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */
-(void)getPurchesHistoryList:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get PurchesHistoryList
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */
-(void)getPurchesHistory:(NSString *)purchesHistoryId  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get deletePurchesHistoryId
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */

-(void)deletePurchesHistory:(NSString *)deletePurchesHistoryId  WithCompletionBlock:(CompletionBlock)block;
/**
 *  Get deletePurchesHistoryId
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */
-(void)VSGetCardDetail:(NSString *)cardid WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get VSGetReportDetail
 *  @param userId
 *  @param block Block Returning Success OR Failure
 */

-(void)VSGetReportDetail:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get VSDeleteCCDetail
 *  @param cardid
 *  @param block Block Returning Success OR Failure
 */

-(void)VSDeleteCCDetail:(NSString *)cardid WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get VSSaveReportDetail
 *  @param NSMutableDictionary
 *  @param block Block Returning Success OR Failure
 */
-(void)VSSaveReportDetail:(NSMutableDictionary *)saveReport  WithCompletionBlock:(CompletionBlock)block;
/**
 *  Get VSDeletRoute
 *  @param routeId
 *  @param block Block Returning Success OR Failure
 */
-(void)VSDeletRoute:(NSString *)routeId  WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get VSGetPackByMonth
 *  @param userId
 *  @param bymonth
 *  @param block Block Returning Success OR Failure
 */

-(void)VSGetPackByMonth:(NSString *)userId ByMonth:(NSString *)bymonth WithCompletionBlock:(CompletionBlock)block;

/**
 *  Get VSGetPackByYearly
 *  @param userId
 *  @param byYear
 *  @param block Block Returning Success OR Failure
 */

-(void)VSGetPackByYearly:(NSString *)userId ByYearly:(NSString *)byyear WithCompletionBlock:(CompletionBlock)block;

/**
 *  register
 *  @param block Block Returning Success OR Failure
 */

-(void)getRegistrationTypeWithCompletionBlock:(CompletionBlock)block;

/*
 *  Get delete/ats_system,reportmanager,2
 *  @param reportId
 *  @param block Block Returning Success OR Failure
 */
-(void)getMyReportDelete:(NSString *)reportId WithCompletionBlock:(CompletionBlock)block;

/*
 *  Check First name and password in add card.
 *  @param First Name Or Last Name
 *  @param block Block Returning Success OR Failure
 */
-(void)checkFirstOrLastName:(NSString *)name WithCompletionBlock:(CompletionBlock)block;

/*
 *  Check Commercial User.
 *  @param First Name Or Last Name
 *  @param block Block Returning Success OR Failure
 */
-(void)checkCommercialUser:(NSString *)companyID WithCompletionBlock:(CompletionBlock)block;

/*
 *  getDefaultCardId
 *  @param userId Or packageId
 *  @param block Block Returning Success OR Failure
 */
-(void)getDefaultCardId:(NSString *)userId PackageID:(NSString *)packageId WithCompletionBlock:(CompletionBlock)block;
/*
 *  Update card details.
 *  @param First Name Or Last Name
 *  @param block Block Returning Success OR Failure
 */
-(void)updateCardDetails:(NSDictionary *)updateData WithCompletionBlock:(CompletionBlock)block;
/**
 *  Payment
 *  @param block Block Returning Success OR Failure
 */
-(void)VSPaymentProcess:(NSDictionary *)paymentData WithCompletionBlock:(CompletionBlock)block;
/**
 *  VSAddTicket
 *  @param block Block Returning Success OR Failure
 */

-(void)VSAddTicket:(NSMutableDictionary *)saveAddTicketDetail  WithCompletionBlock:(CompletionBlock)block;
/**
 *  VSGetAddTicketList
 *  @param block Block Returning Success OR Failure
*/
-(void)VSGetAddTicketList:(NSString *)userId  WithCompletionBlock:(CompletionBlock)block;


/**
 *  Get selected route details for plptting on map
 *  @param Routeid
 *  @param block Block Returning Success OR Failure
 */
-(void)getSelectedRouteDetails:(NSString *)Routeid  WithCompletionBlock:(CompletionBlock)block;

/**
 *  getcitylist/countryId/id
 *  @param block Block Returning Success OR Failure
 */
-(void)valShareGetCityList:(NSString *)countryCode  WithCompletionBlock:(CompletionBlock)block;

-(void)VSUpadetReport:(NSMutableDictionary *)updateReport  WithCompletionBlock:(CompletionBlock)block;

-(void)GetTicketReport:(NSString *)TktId WithCompletionBlock:(CompletionBlock)block;
-(void)valShareGetticketsAround:(NSDictionary *)dictData WithCompletionBlock:(CompletionBlock)block;

@end
