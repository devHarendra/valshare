//
//  LocationManager.m
//  ValShare
//
//  Created by iMac Apple on 02/06/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager
@synthesize routeListArray = _routeListArray;
@synthesize userReportListArray = _userReportListArray;
@synthesize modelArray = _modelArray;
@synthesize UserReportArrForNotify = _UserReportArrForNotify;



+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        LocationManager *instance = sharedInstance;
        
        instance.locationManager = [CLLocationManager new];
        instance.locationManager.delegate = instance;
        instance.locationManager.desiredAccuracy = kCLLocationAccuracyBest; // you can use kCLLocationAccuracyHundredMeters to get better battery life
        instance.locationManager.pausesLocationUpdatesAutomatically = NO;
        [instance.locationManager requestWhenInUseAuthorization];
        [instance.locationManager setDistanceFilter:kCLDistanceFilterNone];
        instance.locationManager.activityType = CLActivityTypeOtherNavigation;

      });
    
    return sharedInstance;
}

-(void)setDelegate:(id<LocationDelegate>)delegate{
    
    _delegate = delegate;
}

-(NSMutableArray *)routeListArray {
    
    if (!_routeListArray) {
        _routeListArray = [[NSMutableArray alloc] init];
    }
    return _routeListArray;
}

-(NSMutableArray *)userReportListArray
{
    if (!_userReportListArray)
    {
        _userReportListArray = [[NSMutableArray alloc] init];
    }
    return _userReportListArray;
}

-(NSMutableArray *)modelArray {
    
    if (!_modelArray) {
        _modelArray = [[NSMutableArray alloc] init];
    }
    return _modelArray;
}

-(NSMutableArray *)UserReportArrForNotify
{
    if (!_UserReportArrForNotify)
    {
        _UserReportArrForNotify = [[NSMutableArray alloc] init];
    }
    return _UserReportArrForNotify;
}

- (void)startUpdatingLocation
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusDenied)
    {
        NSLog(@"Location services are disabled in settings.");
    }
    else
    {
        // for iOS 8
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            [self.locationManager requestAlwaysAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
//    [self.locationManager stopUpdatingLocation];

    CLLocation *mostRecentLocation = locations.lastObject;
    NSLog(@"Current location: %@ %@", @(mostRecentLocation.coordinate.latitude), @(mostRecentLocation.coordinate.longitude));
    
        NSLog(@"Sending current location to web service.");
    
    
    if (_delegate && [_delegate respondsToSelector:@selector(DidUpdateLocations:)])
    {

        [_delegate DidUpdateLocations:locations];
    
    }

}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (_delegate && [_delegate respondsToSelector:@selector(DidUpdateToLocation:fromLocation:)]) {
        [_delegate DidUpdateToLocation:newLocation fromLocation:oldLocation];
    }
    
}


@end
