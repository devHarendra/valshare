//
//  LocationManager.h
//  ValShare
//
//  Created by iMac Apple on 02/06/15.
//  Copyright (c) 2015 Intigate Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationDelegate <NSObject>

-(void)DidUpdateToLocation :(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;
-(void)DidUpdateLocations:(NSArray *)locations;

@end



@interface LocationManager : NSObject <CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSDate *lastTimestamp;
@property (nonatomic, strong)  NSMutableArray *routeListArray;
@property (nonatomic, strong)  NSMutableArray *userReportListArray;
@property (nonatomic, strong)  NSMutableArray *modelArray;
@property (nonatomic, strong)  NSMutableArray *UserReportArrForNotify;
@property (nonatomic, weak) id<LocationDelegate>delegate;







+ (instancetype)sharedInstance;
- (void)startUpdatingLocation;
-(NSMutableArray *)routeListArray;
-(NSMutableArray *)UserReportArrForNotify;


@end
